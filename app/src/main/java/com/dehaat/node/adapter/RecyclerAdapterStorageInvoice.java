package com.dehaat.node.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.dehaat.node.R;
import com.dehaat.node.fragments.EntityCreationFragment;
import com.dehaat.node.rest.response.EntityDataJson;
import com.dehaat.node.rest.response.JsonDataStore;
import com.dehaat.node.utilities.AppUtils;
import com.dehaat.node.utilities.UrlConstant;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

public class RecyclerAdapterStorageInvoice extends RecyclerView.Adapter<RecyclerAdapterStorageInvoice.EntityView> {
    private Context context;
    private ArrayList<EntityDataJson> getViewFields;
    private String keyName;

    public RecyclerAdapterStorageInvoice(Context context,
                                         ArrayList<EntityDataJson> getViewFields, String keyName) {
        this.context = context;
        this.getViewFields = getViewFields;
        this.keyName = keyName;
    }

    @Override
    public RecyclerAdapterStorageInvoice.EntityView onCreateViewHolder(ViewGroup parent,
                                                                       int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_picking_link, parent, false);
        return new RecyclerAdapterStorageInvoice.EntityView(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterStorageInvoice.EntityView holder,
                                 final int position) {
        holder.setIsRecyclable(false);
        if (getViewFields == null)
            return;

        Gson gson = new Gson();
        ArrayList<JsonDataStore> arrayList = gson.fromJson(
                getViewFields.get(holder.getAdapterPosition()).getJson_payload(),
                new TypeToken<ArrayList<JsonDataStore>>() {
                }.getType());
        String data = "";
        if (arrayList != null) {
            for (int i = 0; i < arrayList.size(); i++)
                if (arrayList.get(i).getKey().equals(UrlConstant.NUMBER))
                    data = data + arrayList.get(i).getValue() + "   ";
        }

        if (AppUtils.isNullCase(data.trim())) {
            holder.poName.setText("N/A");
        } else {
            holder.poName.setText(data);
        }

        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                keyName = UrlConstant.STORAGE_INVOICE;
                EntityCreationFragment fragment = EntityCreationFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("KEY_NAME", keyName);
                bundle.putLong("ROW_ID", getViewFields.get(holder.getAdapterPosition()).getRow_id());
                bundle.putBoolean("NOTES", false);
                fragment.setArguments(bundle);
                AppUtils.changeFragment((FragmentActivity) context, fragment);

            }
        });
    }

    @Override
    public int getItemCount() {
        return getViewFields.size();
    }

    public class EntityView extends RecyclerView.ViewHolder {
        private TextView poName;
        private LinearLayout back;

        public EntityView(View itemView) {
            super(itemView);
            back = itemView.findViewById(R.id.back);
            poName = itemView.findViewById(R.id.poName);
        }
    }
}
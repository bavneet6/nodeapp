package com.dehaat.node.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dehaat.node.R;
import com.dehaat.node.database.DatabaseHandler;
import com.dehaat.node.fragments.EntityCreationFragment;
import com.dehaat.node.rest.ApiCallBack;
import com.dehaat.node.rest.AppRestClient;
import com.dehaat.node.rest.body.NewRecord;
import com.dehaat.node.rest.response.GetEntityList;
import com.dehaat.node.rest.response.GetIdName;
import com.dehaat.node.rest.response.GetIdNames;
import com.dehaat.node.rest.response.GetViewFields;
import com.dehaat.node.rest.response.LotNumber;
import com.dehaat.node.rest.response.MandiCreate;
import com.dehaat.node.rest.response.ResponseCreateEntity;
import com.dehaat.node.utilities.AppPreference;
import com.dehaat.node.utilities.AppUtils;
import com.dehaat.node.utilities.BOMCallBack;
import com.dehaat.node.utilities.PostJsonValues;
import com.dehaat.node.utilities.RequiredField;
import com.dehaat.node.utilities.SelectionId;
import com.dehaat.node.utilities.ShowMultipleLines;
import com.dehaat.node.utilities.UrlConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.ConnectException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dehaat.node.utilities.AppUtils.getReadEditState;
import static com.dehaat.node.utilities.AppUtils.getTimeStamp;

public class RecyclerAdapterFields extends RecyclerView.Adapter<RecyclerAdapterFields.EntityView>
        implements SelectionId {
    private Context context;
    private ArrayList<GetViewFields> getViewFields;
    private PostJsonValues postJsonValues;
    private RecyclerAdapterSelectionValues recyclerAdapterSelectionValues;
    private Calendar myCalendar = Calendar.getInstance();
    private HashMap<String, String> postDataMap;
    private HashMap<String, String> validationMap;
    private HashSet<String> requiredMap = new HashSet<>();
    private int textPos, uomPos = 0, qtyPos;
    private long nestedSpinner, commodPos;
    private TextView qtyTextView, ifscTextView, accountTextView, pointOfContactTextView;
    private DatabaseHandler databaseHandler;
    private String valueStored, entity_status, uomKey, keyName, qtyValue, rateValue, ifsc_code = "",
            account_no = "", point_of_contact = "";
    private Dialog dialog;
    private EditText amountTextView;
    private RequiredField requiredField;
    private BOMCallBack bomCallBack;
    private ShowMultipleLines showMultipleLines;
    private ArrayList<String> stateList = new ArrayList<>();
    private ArrayList<String> districtList = new ArrayList<>();
    private ArrayList<String> blockList = new ArrayList<>();

    private ArrayList<GetIdNames.GetData> stateNames = new ArrayList<>();
    private ArrayList<GetIdNames.GetData> districtNames = new ArrayList<>();
    private ArrayList<GetIdNames.GetData> blockNames = new ArrayList<>();
    private int stateId, districtId, blockId;
    private Object selectedPayload;

    public RecyclerAdapterFields(Context context, String keyName,
                                 ArrayList<GetViewFields> getViewFields,
                                 HashMap<String, String> postDataMap,
                                 HashMap<String, String> validationMap,
                                 String entity_status, PostJsonValues postJsonValues,
                                 RequiredField requiredField, BOMCallBack bomCallBack,
                                 ShowMultipleLines showMultipleLines) {
        this.context = context;
        this.keyName = keyName;
        this.getViewFields = getViewFields;
        this.postDataMap = postDataMap;
        this.validationMap = validationMap;
        this.entity_status = entity_status;
        this.postJsonValues = postJsonValues;
        this.requiredField = requiredField;
        this.bomCallBack = bomCallBack;
        this.showMultipleLines = showMultipleLines;
    }

    @Override
    public EntityView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_sale_fields, parent, false);
        context = parent.getContext();
        return new EntityView(itemView);
    }

    @Override
    public void onBindViewHolder(final EntityView holder, final int position) {
        holder.setIsRecyclable(false);
        final int fieldPosition = holder.getAdapterPosition();

        //returns the value if the data is already prent in the db
        valueStored = checkKey(getViewFields.get(fieldPosition).getKey());

        if (!AppUtils.isNullCase(getViewFields.get(fieldPosition).getDisplay_Name()))
            holder.textName.setText(getViewFields.get(fieldPosition).getDisplay_Name());

        if ((getViewFields.get(fieldPosition).getAttributes() != null)
                && (getViewFields.get(fieldPosition).getAttributes().getInvisible() != null))
            if (getReadEditState(getViewFields.get(fieldPosition).getAttributes().getInvisible(),
                    postDataMap, entity_status)) {
                holder.mainBack.setVisibility(View.GONE);
            }
        // checking the type for each component
        String compKey = getViewFields.get(fieldPosition).getKey();
        String compType = getViewFields.get(fieldPosition).getType();
        GetViewFields.Attributes attributes = getViewFields.get(fieldPosition).getAttributes();
        if (compType.equals(UrlConstant.MANY2ONE)) {
            holder.selectionBack.setVisibility(View.VISIBLE);
            displayValidations(compKey, holder.errorImg);
            if (attributes != null && attributes.getReadonly() != null)
                if (getReadEditState(attributes.getReadonly(), postDataMap, entity_status)) {
                    holder.selection.setClickable(false);
                    holder.selection.setEnabled(false);
                    holder.selection.setTextColor(context.getResources().getColor(R.color.lightgrey));
                }

            storeRequiredFields(fieldPosition, holder.required);

            if (!AppUtils.isNullCase(valueStored)) {
                String name = getNameFromId(fieldPosition, Integer.parseInt(valueStored),
                        compType, 0);
                holder.selection.setText(name);
                if (compKey.equals(UrlConstant.PRODUCT_ID)) {
                    nestedSpinner = Long.parseLong(valueStored);
                }
            }
        } else if (compType.equals(UrlConstant.SELECTION)) {
            holder.stringSelectionBack.setVisibility(View.VISIBLE);
            displayValidations(compKey, holder.errorImg);
            if (attributes != null && attributes.getReadonly() != null)
                if (getReadEditState(attributes.getReadonly(), postDataMap, entity_status)) {
                    holder.string_selection.setClickable(false);
                    holder.string_selection.setEnabled(false);
                    holder.string_selection.setTextColor(context.getResources().getColor(R.color.lightgrey));
                }

            storeRequiredFields(fieldPosition, holder.required);

            if (!AppUtils.isNullCase(valueStored)) {
                int pos = getPosFromMap(fieldPosition, valueStored, compType);
                holder.string_selection.setText(
                        getViewFields.get(fieldPosition).getSelection_values().get(
                                pos).getDisplay_name());
            }

        } else if (compType.equals(UrlConstant.UNIT_OF_MEAS)) {
            holder.quantityUOMBack.setVisibility(View.VISIBLE);
            uomPos = fieldPosition;
            qtyTextView = holder.qty_uom_unit;
            uomKey = getViewFields.get(fieldPosition).getKey();
            String data = printUOM(commodPos);
            if (!AppUtils.isNullCase(data))
                qtyTextView.setText(data);

            if (attributes != null && attributes.getReadonly() != null)
                if (getReadEditState(attributes.getReadonly(), postDataMap, entity_status)) {
                    holder.qty_uom_unit.setFocusable(false);
                    holder.qty_uom_unit.setEnabled(false);
                    holder.qty_uom_unit.setTextColor(context.getResources().getColor(R.color.lightgrey));
                    AppPreference.getInstance().setREADNOTES(true);
                }
        } else if (compType.equals(UrlConstant.DATETIME) || compType.equals(UrlConstant.DATE)) {
            holder.dateTimeBack.setVisibility(View.VISIBLE);
            String date = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());

            if (entity_status.equals(UrlConstant.NEW))
                holder.dateTime.setText(date);

            if (!AppUtils.isNullCase(valueStored))
                holder.dateTime.setText(valueStored);
            if (attributes != null && attributes.getReadonly() != null)
                if (getReadEditState(attributes.getReadonly(), postDataMap, entity_status)) {
                    holder.dateTime.setClickable(false);
                    holder.dateTime.setEnabled(false);
                    holder.dateTime.setTextColor(context.getResources().getColor(R.color.lightgrey));
                }
            storeRequiredFields(fieldPosition, holder.required);
            storeInMap(compKey, holder.dateTime.getText().toString());
        } else if (compType.equals(UrlConstant.FLOAT)) {
            if (compKey.equals(UrlConstant.AMOUNT_TOTAL)) {
                amountTextView = holder.floatTxt;
            }
            if (compKey.equals(UrlConstant.PRICE_UNIT)) {
                rateValue = postDataMap.get(UrlConstant.PRICE_UNIT);
            }
            displayData(holder.floatBack, valueStored, holder.floatTxt);
            displayValidations(compKey, holder.errorImg);
            if (attributes != null && attributes.getReadonly() != null)
                if (getReadEditState(attributes.getReadonly(), postDataMap, entity_status)) {
                    holder.floatTxt.setFocusable(false);
                    holder.floatTxt.setEnabled(false);
                    holder.floatTxt.setTextColor(context.getResources().getColor(R.color.lightgrey));
                }

            storeRequiredFields(fieldPosition, holder.required);
        } else if (getViewFields.get(fieldPosition).getType().equals(UrlConstant.QUANTITY)) {
            if (getViewFields.get(fieldPosition).getKey().equals("quantity")) {
                qtyValue = postDataMap.get("quantity");
            }
            holder.quantityBack.setVisibility(View.VISIBLE);
            qtyPos = fieldPosition;

            if (attributes != null && attributes.getRequired() != null)
                if (getReadEditState(attributes.getRequired(), postDataMap, entity_status))
                    holder.required.setVisibility(View.VISIBLE);

            if (getViewFields.get(fieldPosition).getSub_fields() == null)
                return;

            ArrayList<GetViewFields> list = getViewFields.get(fieldPosition).getSub_fields();

            for (int i = 0; i < list.size(); i++) {
                //check for at what fieldPosition is textview and edittext
                if (list.get(i).getType().equals(UrlConstant.UNIT_OF_MEAS)) {
                    uomPos = i;
                    uomKey = getViewFields.get(fieldPosition).getSub_fields().get(uomPos).getKey();
                    qtyTextView = holder.qty_unit;
                    String data = printUOM(commodPos);
                    if (!AppUtils.isNullCase(data))
                        holder.qty_unit.setText(data);
                } else if (list.get(i).getType().equals(UrlConstant.FLOAT)) {
                    displayValidations(getViewFields.get(fieldPosition).getSub_fields().get(i).getKey(),
                            holder.errorImg);
                    textPos = i;
                    if (list.get(i).getAttributes() != null) {
                        if (list.get(i).getAttributes().getReadonly() != null)
                            if (getReadEditState(list.get(i).getAttributes().getReadonly(),
                                    postDataMap, entity_status)) {
                                holder.quantity.setFocusable(false);
                                holder.quantity.setEnabled(false);
                                holder.quantity.setTextColor(context.getResources().
                                        getColor(R.color.lightgrey));
                                holder.qty_unit.setEnabled(false);
                                holder.qty_unit.setTextColor(context.getResources().
                                        getColor(R.color.lightgrey));
                            }

                        if (list.get(i).getAttributes().getRequired() != null)
                            if (getReadEditState(list.get(i).getAttributes().getRequired(),
                                    postDataMap, entity_status)) {
                                holder.required.setVisibility(View.VISIBLE);
                                requiredMap.add(getViewFields.get(fieldPosition).getKey());
                            }
                    }
                }
            }

            String textData, selData;
            textData = checkKey(list.get(textPos).getKey());
            selData = checkKey(list.get(uomPos).getKey());

            if (!AppUtils.isNullCase(textData)) {
                holder.quantity.setText(textData);
            }
            if (!AppUtils.isNullCase(selData)) {
                ArrayList<GetViewFields.MappingValues> arrayList = getViewFields.get(fieldPosition).
                        getSub_fields().get(uomPos).getMapping_values();
                for (int i = 0; i < arrayList.size(); i++) {
                    if (selData.equals(arrayList.get(i).getUom_id().get(0)))
                        holder.qty_unit.setText(arrayList.get(i).getUom_id().get(1));
                }

            }
        } else if (compType.equals(UrlConstant.STRING)) {
            if (compKey.equals(UrlConstant.PARTNER_ID)) {
                if (keyName.equals(UrlConstant.STORAGE_ORDER)
                        || keyName.equals(UrlConstant.COMMODITY_LOAN)
                        || keyName.equals(UrlConstant.STORAGE_DISPATCH)
                        || keyName.equals(UrlConstant.STORAGE_RECEIPT)
                        || keyName.equals(UrlConstant.COMMODITY_LOAN_INVOICE)
                        || keyName.equals(UrlConstant.STORAGE_INVOICE)) {
                    valueStored = checkKey(UrlConstant.PARTNER_NAME);
                }
            }

            displayData(holder.stringBack, valueStored, holder.stringTxt);
            displayValidations(compKey, holder.errorImg);
            if (attributes != null && attributes.getReadonly() != null)
                if (getReadEditState(attributes.getReadonly(), postDataMap, entity_status)) {
                    holder.stringTxt.setFocusable(false);
                    holder.stringTxt.setEnabled(false);
                    holder.stringTxt.setTextColor(context.getResources().getColor(R.color.lightgrey));
                }
            storeRequiredFields(fieldPosition, holder.required);
            if (keyName.equals(UrlConstant.PURCHASE_ORDER)) {
                if (compKey.equals("ifsc_code")) {
                    ifscTextView = holder.stringTxt;
                    if (!AppUtils.isNullCase(ifsc_code))
                        holder.stringTxt.setText(ifsc_code);
                } else if (compKey.equals("vendor_account_no")) {
                    accountTextView = holder.stringTxt;
                    if (!AppUtils.isNullCase(account_no))
                        holder.stringTxt.setText(account_no);
                } else if (compKey.equals(UrlConstant.POINT_OF_CONTACT)) {
                    pointOfContactTextView = holder.stringTxt;
                    if (!AppUtils.isNullCase(point_of_contact))
                        holder.stringTxt.setText(point_of_contact);
                }
            } else if (keyName.equals(UrlConstant.STORAGE_ORDER)) {
                if (compKey.equals("partner_id")) {
                    holder.stringTxt.setFocusable(false);
                    holder.stringTxt.setClickable(true);
                    if (selectedPayload != null) {
                        holder.stringTxt.setText(
                                ((GetEntityList.CreateBody) selectedPayload).getField(
                                        UrlConstant.NAME));
                    }
                    holder.stringTxt.setOnClickListener(
                            getPartnerClickListener(
                                    this, holder.stringTxt, fieldPosition));
                }
            }
        } else if (compType.equals(UrlConstant.INTEGER)) {
            if (compKey.equals(UrlConstant.PARTNER_ID)) {
                if (keyName.equals(UrlConstant.STORAGE_ORDER)
                        || keyName.equals(UrlConstant.COMMODITY_LOAN)
                        || keyName.equals(UrlConstant.STORAGE_RECEIPT)
                        || keyName.equals(UrlConstant.STORAGE_DISPATCH)
                        || keyName.equals(UrlConstant.COMMODITY_LOAN_INVOICE)
                        || keyName.equals(UrlConstant.STORAGE_INVOICE)) {
                    valueStored = checkKey(UrlConstant.PARTNER_NAME);
                }
            }

            displayData(holder.intBack, valueStored, holder.intTxt);
            displayValidations(compKey, holder.errorImg);

            if (attributes != null && attributes.getReadonly() != null) {
                if (getReadEditState(attributes.getReadonly(), postDataMap, entity_status)) {
                    holder.intTxt.setFocusable(false);
                    holder.intTxt.setEnabled(false);
                    holder.intTxt.setTextColor(context.getResources().getColor(R.color.lightgrey));
                }
            }
            storeRequiredFields(fieldPosition, holder.required);

            if (keyName.equals(UrlConstant.STORAGE_ORDER)) {
                if (compKey.equals(UrlConstant.PARTNER_ID)) {
                    holder.intTxt.setFocusable(false);
                    holder.intTxt.setClickable(true);
                    if (selectedPayload != null) {
                        holder.intTxt.setText(
                                ((GetEntityList.CreateBody) selectedPayload).getField(
                                        UrlConstant.NAME));
                    }
                    holder.intTxt.setOnClickListener(
                            getPartnerClickListener(
                                    this, holder.intTxt, fieldPosition));
                }
            }
        } else if (getViewFields.get(fieldPosition).getType().equals(UrlConstant.NOTE)) {
            displayData(holder.noteBack, valueStored, holder.noteString);
            displayValidations(getViewFields.get(fieldPosition).getKey(), holder.errorImg);
            if (getViewFields.get(fieldPosition).getAttributes() != null)
                if (getViewFields.get(fieldPosition).getAttributes().getReadonly() != null)
                    if (getReadEditState(getViewFields.get(fieldPosition).getAttributes().getReadonly(),
                            postDataMap, entity_status)) {
                        holder.noteString.setFocusable(false);
                        holder.noteString.setEnabled(false);
                        holder.noteString.setTextColor(context.getResources().getColor(R.color.lightgrey));
                        AppPreference.getInstance().setREADNOTES(true);
                    }
            storeRequiredFields(fieldPosition, holder.required);

        } else if (getViewFields.get(fieldPosition).getType().equals(UrlConstant.MRP_LINES)) {
            holder.mainBack.setVisibility(View.GONE);
        } else if (getViewFields.get(fieldPosition).getType().equals(UrlConstant.ONE2MANY)) {
            holder.mainBack.setVisibility(View.GONE);
            if (getViewFields.get(fieldPosition).getAttributes() != null)
                if (getViewFields.get(fieldPosition).getAttributes().getReadonly() != null)
                    if (getReadEditState(getViewFields.get(fieldPosition).getAttributes().getReadonly(),
                            postDataMap, entity_status)) {
                        showMultipleLines.show(true, false);
                    } else
                        showMultipleLines.show(true, true);

        } else if (getViewFields.get(fieldPosition).getType().equals(UrlConstant.LOT_NUMBER)) {
            displayValidations(getViewFields.get(fieldPosition).getKey(), holder.errorImg);
            holder.lotBack.setVisibility(View.VISIBLE);
            if (!AppUtils.isNullCase(valueStored))
                holder.lotText.setText(valueStored);
            if (getViewFields.get(fieldPosition).getAttributes() != null)
                if (getViewFields.get(fieldPosition).getAttributes().getReadonly() != null)
                    if (getReadEditState(getViewFields.get(fieldPosition).getAttributes().getReadonly(),
                            postDataMap, entity_status)) {
                        holder.lotText.setClickable(false);
                        holder.lotText.setEnabled(false);
                        holder.lotText.setTextColor(context.getResources().getColor(R.color.lightgrey));
                    }
            storeRequiredFields(fieldPosition, holder.required);

        }

        holder.dateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onText(fieldPosition, holder.dateTime);
            }
        });

        holder.lotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setCancelable(true);
                dialog.setTitle("Select Lot Number");
                dialog.setContentView(R.layout.dialog_lot_numbers);
                final SearchView lotSearch = dialog.findViewById(R.id.lotSearch);
                TextView createTemp = dialog.findViewById(R.id.createTemp);
                if (keyName.equals(UrlConstant.SALES_DISPATCH)) {
                    createTemp.setVisibility(View.GONE);
                } else
                    createTemp.setVisibility(View.VISIBLE);
                ListView lotList = dialog.findViewById(R.id.lotList);
                databaseHandler = new DatabaseHandler(context);
                lotSearch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        lotSearch.setIconified(false);
                    }
                });
                ArrayList<LotNumber> lotNumbersList = databaseHandler.getLotNumbersData(nestedSpinner);
                List<String> perNameList = new ArrayList<>();
                for (int i = 0; i < lotNumbersList.size(); i++) {
                    if (!AppUtils.isNullCase(lotNumbersList.get(i).getPerm_lot_number()))
                        perNameList.add(lotNumbersList.get(i).getPerm_lot_number());
                    else
                        perNameList.add(lotNumbersList.get(i).getTemp_lot_number());
                }
                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                        android.R.layout.simple_list_item_1, perNameList);
                lotList.setAdapter(adapter);
                lotList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                        String value = adapter.getItem(position);

                        holder.lotText.setText(value);
                        dialog.dismiss();

                        if (!holder.lotText.getText().toString().equals(
                                postDataMap.get(getViewFields.get(fieldPosition).getKey())))
                            AppPreference.getInstance().setDATA_CHANGE_FLAG(true);

                        storeInMap(getViewFields.get(fieldPosition).getKey(), "" +
                                holder.lotText.getText().toString());


                    }
                });
                createTemp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        String tempLotNo = "T_" + getTimeStamp() + "_" +
                                AppPreference.getInstance().getUSER_ID();
                        holder.lotText.setText(tempLotNo);
                        storeInMap(getViewFields.get(fieldPosition).getKey(), tempLotNo);
                    }
                });
                lotSearch.setQueryHint("Search..");
                lotSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                    @Override
                    public boolean onQueryTextSubmit(String txt) {
                        // TODO Auto-generated method stub
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String txt) {
                        // TODO Auto-generated method stub

                        adapter.getFilter().filter(txt);
                        return false;
                    }
                });

                dialog.show();
            }
        });

        holder.quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (getViewFields.get(fieldPosition).getKey().equals("quantity")) {
                    qtyValue = holder.quantity.getText().toString();
                    setTotalValue();
                }
                if (!holder.quantity.getText().toString().equals(
                        postDataMap.get(getViewFields.get(fieldPosition).getSub_fields().
                                get(textPos).getKey())))
                    AppPreference.getInstance().setDATA_CHANGE_FLAG(true);

                storeInMap(getViewFields.get(fieldPosition).getSub_fields().get(textPos).getKey(),
                        "" + holder.quantity.getText().toString());

            }
        });
        holder.floatTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (getViewFields.get(fieldPosition).getKey().equals(UrlConstant.PRICE_UNIT)) {
                    rateValue = holder.floatTxt.getText().toString();
                    setTotalValue();
                }
                if (!holder.floatTxt.getText().toString().equals(
                        postDataMap.get(getViewFields.get(fieldPosition).getKey())))
                    AppPreference.getInstance().setDATA_CHANGE_FLAG(true);

                storeInMap(getViewFields.get(fieldPosition).getKey(), holder.floatTxt.getText().toString());
            }
        });
        holder.stringTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!holder.stringTxt.getText().toString().equals(postDataMap.get(
                        getViewFields.get(fieldPosition).getKey())))
                    AppPreference.getInstance().setDATA_CHANGE_FLAG(true);

                storeInMap(getViewFields.get(fieldPosition).getKey(), holder.stringTxt.getText().toString());
            }
        });
        holder.noteString.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!holder.noteString.getText().toString().equals(postDataMap.get(
                        getViewFields.get(fieldPosition).getKey())))
                    AppPreference.getInstance().setDATA_CHANGE_FLAG(true);

                storeInMap(getViewFields.get(fieldPosition).getKey(), holder.noteString.getText().toString());
            }
        });

        holder.selection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(context);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.dialog_show_selection_values);
                Button create = dialog.findViewById(R.id.create);
                TextView selectLine = dialog.findViewById(R.id.selectLine);
                final SearchView search = dialog.findViewById(R.id.search);
                search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        search.setIconified(false);
                    }
                });
                if (getViewFields.get(fieldPosition).getKey().equals(UrlConstant.PARTNER_ID)) {
                    selectLine.setVisibility(View.VISIBLE);
                } else
                    selectLine.setVisibility(View.GONE);
                RecyclerView partList = dialog.findViewById(R.id.partList);
                databaseHandler = new DatabaseHandler(context);
                if (getViewFields.get(fieldPosition).getAttributes() != null)
                    if (getReadEditState("" + getViewFields.get(fieldPosition).getAttributes().isCreate(), postDataMap, entity_status))
                        create.setVisibility(View.VISIBLE);
                    else
                        create.setVisibility(View.GONE);

                ArrayList<LotNumber> lotNumbersList = databaseHandler.getLotNumbersData(nestedSpinner);
                List<String> perNameList = new ArrayList<>();
                for (int i = 0; i < lotNumbersList.size(); i++) {
                    if (!AppUtils.isNullCase(lotNumbersList.get(i).getPerm_lot_number()))
                        perNameList.add(lotNumbersList.get(i).getPerm_lot_number());
                    else
                        perNameList.add(lotNumbersList.get(i).getTemp_lot_number());
                }
                displaySelectValues(fieldPosition, partList, holder.selection,
                        getViewFields.get(fieldPosition).getType());
                create.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Dialog innerDialog = new Dialog(context);
                        Button create;
                        final EditText mobile, name;
                        innerDialog.setCanceledOnTouchOutside(true);
                        innerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        if (keyName.equals(UrlConstant.MANDI_PRICE)) {
                            innerDialog.setContentView(R.layout.dialog_mandi_create);
                            showInnerDialog(innerDialog);
                            mandiCreateHandler(innerDialog, fieldPosition);
                            return;
                        } else {
                            innerDialog.setContentView(R.layout.dialog_add_record);
                            showInnerDialog(innerDialog);
                        }

                        create = innerDialog.findViewById(R.id.create);
                        mobile = innerDialog.findViewById(R.id.userMob);
                        name = innerDialog.findViewById(R.id.userName);
                        name.setText(holder.selection.getText().toString());

                        create.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (TextUtils.isEmpty(name.getText().toString())) {
                                    name.setError("Enter name");
                                    name.requestFocus();
                                } else if (mobile.getText().toString().length() != 10) {
                                    mobile.setError("Enter a valid mobile number");
                                    mobile.requestFocus();
                                } else {
                                    String tag = null;
                                    if ((keyName.equals(UrlConstant.SALES_ORDER)) ||
                                            (keyName.equals(UrlConstant.SALES_DISPATCH)) ||
                                            (keyName.equals(UrlConstant.SALES_PAYMENT)))
                                        tag = "Customer";
                                    else if ((keyName.equals(UrlConstant.EXPENSE_BILL)))
                                        tag = "Service Provider";
                                    else
                                        tag = "Output Vendor";

                                    AppUtils.showProgressDialog(context);
                                    NewRecord.Data data = new NewRecord.Data();
                                    data.setMobile(mobile.getText().toString());
                                    data.setName(name.getText().toString());
                                    data.setTag(tag);
                                    NewRecord newRecord = new NewRecord(data);
                                    AppRestClient client = AppRestClient.getInstance();
                                    Call<ResponseCreateEntity> call = client.postNewRecord(newRecord);
                                    call.enqueue(new Callback<ResponseCreateEntity>() {
                                        @Override
                                        public void onResponse(Call<ResponseCreateEntity> call,
                                                               Response<ResponseCreateEntity> response) {
                                            AppUtils.hideProgressDialog();
                                            if (response.code() == 200) {
                                                getSchemaData((Activity) context,
                                                        UrlConstant.key_mapping.get(keyName).get("key"));
                                                holder.selection.setText(name.getText().toString());
                                                storeInMap(getViewFields.get(fieldPosition).getKey(),
                                                        "" + response.body().getMetadata().getEntity_id());
                                                innerDialog.dismiss();
                                                dialog.dismiss();
                                            } else if (response.code() == 500) {
                                                JSONObject jObjError = null;
                                                try {
                                                    jObjError = new JSONObject(response.errorBody().string());
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                                try {
                                                    if (jObjError.get("error_type").equals(UrlConstant.UNHANDLED_EXCEPTION)) {
                                                        showAlertDialog("Technical Problem");
                                                    } else
                                                        showAlertDialog(String.valueOf(jObjError.get("error_message")));
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<ResponseCreateEntity> call, Throwable t) {
                                            AppUtils.hideProgressDialog();
                                            if (t instanceof ConnectException) {
                                                if (!AppUtils.haveNetworkConnection(context))
                                                    AppUtils.showToast(UrlConstant.NO_INTERNET);
                                                else
                                                    AppUtils.showToast(UrlConstant.SERVER_NOT_RES);

                                            } else {
                                                AppUtils.showToast(UrlConstant.TECH_PROB);
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                });

                search.setQueryHint("Search..");
                search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                    @Override
                    public boolean onQueryTextSubmit(String txt) {
                        // TODO Auto-generated method stub
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String txt) {
                        if (getViewFields.get(fieldPosition).getSelection_values().size() != 0) {
                            recyclerAdapterSelectionValues.getFilter().filter(txt);
                        }
                        return false;
                    }
                });
                dialog.getWindow().
                        setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.getWindow().
                        setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().
                        setGravity(Gravity.CENTER_VERTICAL);
                dialog.show();
            }
        });

        holder.string_selection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(context);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.dialog_show_selection_values);
                Button create = dialog.findViewById(R.id.create);
                TextView selectLine = dialog.findViewById(R.id.selectLine);
                final SearchView search = dialog.findViewById(R.id.search);
                search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        search.setIconified(false);
                    }
                });

                selectLine.setVisibility(View.GONE);
                RecyclerView partList = dialog.findViewById(R.id.partList);
                databaseHandler = new DatabaseHandler(context);
                create.setVisibility(View.GONE);

                displaySelectValues(fieldPosition, partList, holder.string_selection,
                        getViewFields.get(fieldPosition).getType());

                search.setQueryHint("Search..");
                search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String txt) {
                        // TODO Auto-generated method stub
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String txt) {
                        if (getViewFields.get(fieldPosition).getSelection_values().size() != 0) {
                            recyclerAdapterSelectionValues.getFilter().filter(txt);
                        }
                        return false;
                    }
                });
                dialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.getWindow().setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                dialog.show();
            }
        });
    }

    private View.OnClickListener getPartnerClickListener(SelectionId selectionReceiver,
                                                         EditText receivingInput,
                                                         int fieldPrintIndex) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = new Dialog(context);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.dialog_partner_search);

                EditText searchInput = dialog.findViewById(R.id.searchText);
                Button createNew = dialog.findViewById(R.id.createNew);
                Button searchButton = dialog.findViewById(R.id.searchPartner);
                RecyclerView contactList = dialog.findViewById(R.id.contactList);
                if (selectedPayload != null) {
                    dialog.findViewById(R.id.searchCTA).setVisibility(View.GONE);
                    contactList.setLayoutManager(new LinearLayoutManager(context));
                    ArrayList<GetEntityList.CreateBody> contactListBody = new ArrayList<>();
                    contactListBody.add((GetEntityList.CreateBody) selectedPayload);

                    searchInput.setText(
                            ((GetEntityList.CreateBody) selectedPayload)
                                    .getField(UrlConstant.PRIMARY_NUMBER));
                    contactList.setAdapter(
                            new RecyclerAdapterContactList(
                                    context, contactListBody,
                                    selectionReceiver, receivingInput, fieldPrintIndex));
                }

                searchButton.setOnClickListener(
                        getSearchContactListener(
                                dialog, selectionReceiver, receivingInput, fieldPrintIndex));
                createNew.setOnClickListener(
                        getCreateNewContactListener(receivingInput, fieldPrintIndex));

                dialog.getWindow().setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                dialog.show();
                searchInput.requestFocus();

            }
        };
    }

    private View.OnClickListener getSearchContactListener(Dialog dialog,
                                                          SelectionId selectionReceiver,
                                                          EditText revceivingInput,
                                                          int fieldPrintIndex) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText searchInput = dialog.findViewById(R.id.searchText);
                RecyclerView contactList = dialog.findViewById(R.id.contactList);
                if (AppUtils.isNullCase(searchInput.getText().toString())) {
                    AppUtils.showToast("Search param can't be empty.");
                    return;
                }

                dialog.findViewById(R.id.responseMsg).setVisibility(View.GONE);
                dialog.findViewById(R.id.createNew).setVisibility(View.GONE);
                dialog.findViewById(R.id.searchCTA).setVisibility(View.GONE);

                AppUtils.showProgressDialog(context);
                String mobileNumber = searchInput.getText().toString();

                AppRestClient client = AppRestClient.getInstance();
                Call<GetEntityList> call = client.getContacts(
                        null, 0, 10, "mobile", mobileNumber);
                call.enqueue(new Callback<GetEntityList>() {
                    @Override
                    public void onResponse(Call<GetEntityList> call,
                                           Response<GetEntityList> response) {
                        AppUtils.hideProgressDialog();
                        if (response.body() == null || response.body().getCreateBody().size() == 0) {
                            contactList.setAdapter(
                                    new RecyclerAdapterContactList(
                                            context, new ArrayList<>(),
                                            selectionReceiver, revceivingInput, fieldPrintIndex));
                            proceedToNewCreation();
                        } else {
                            contactList.setLayoutManager(new LinearLayoutManager(context));
                            contactList.setAdapter(
                                    new RecyclerAdapterContactList(
                                            context, response.body().getCreateBody(),
                                            selectionReceiver, revceivingInput, fieldPrintIndex));
                        }
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        AppUtils.hideProgressDialog();
                        proceedToNewCreation();
                    }

                    private void proceedToNewCreation() {
                        TextView responseMsg = dialog.findViewById(R.id.responseMsg);
                        Button createNew = dialog.findViewById(R.id.createNew);
                        responseMsg.setVisibility(View.VISIBLE);
                        createNew.setVisibility(View.VISIBLE);
                    }
                });
            }
        };
    }

    private View.OnClickListener getCreateNewContactListener(EditText receivingInput,
                                                             int fieldPrintIndex) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText searchInput = dialog.findViewById(R.id.searchText);
                String primaryContact = searchInput.getText().toString();
                if (primaryContact.length() < 10) {
                    AppUtils.showToast("Mobile number can't be less than 10 digits");
                    return;
                } else if (primaryContact.substring(0, 1).equals("0")) {
                    AppUtils.showToast("Mobile number can't begin with zero.");
                    return;
                }

                dialog.dismiss();
                AppPreference.getInstance().setDATA_CHANGE_FLAG(true);

                EntityCreationFragment fragment = EntityCreationFragment.newInstance();
                EntityCreationFragment storageOrderFragment = (EntityCreationFragment) bomCallBack;
                fragment.setReceivingFragment(storageOrderFragment);
                Bundle bundle = new Bundle();
                bundle.putString("KEY_NAME", UrlConstant.PARTNER_CONTACT);
                bundle.putString(UrlConstant.PRIMARY_NUMBER, primaryContact);
                bundle.putLong("ROW_ID", 0L);
                bundle.putBoolean("NOTES", false);
                fragment.setArguments(bundle);
                AppUtils.changeNoStackFragment((FragmentActivity) context, fragment);
            }
        };
    }

    private void showInnerDialog(Dialog innerDialog) {
        innerDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        innerDialog.getWindow().setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        innerDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
        innerDialog.show();
    }

    private void mandiCreateHandler(final Dialog innerDialog, final int fieldPosition) {
        final EditText mandiName, mandiAddr, mandiPinCode;
        final Spinner mandiState, mandiDistt, mandiBlock;
        Button create;

        mandiName = innerDialog.findViewById(R.id.mandiName);
        mandiAddr = innerDialog.findViewById(R.id.mandiAddr);
        mandiPinCode = innerDialog.findViewById(R.id.mandiPinCode);
        mandiState = innerDialog.findViewById(R.id.mandiState);
        mandiDistt = innerDialog.findViewById(R.id.mandiDistt);
        mandiBlock = innerDialog.findViewById(R.id.mandiBlock);
        create = innerDialog.findViewById(R.id.create);

        stateList.clear();
        AppUtils.showProgressDialog(context);
        final AppRestClient client = AppRestClient.getInstance();
        Call<GetIdNames> call = client.getStateList();
        call.enqueue(new ApiCallBack<GetIdNames>() {
            @Override
            public void onResponse(Response<GetIdNames> response) {
                if (response.body() == null || response.body().getData() == null) return;
                GetIdNames.GetData getData = new GetIdNames.GetData();
                getData.setId(0);
                getData.setName(UrlConstant.SELECT);
                stateNames = response.body().getData();
                stateNames.add(0, getData);
                for (int i = 0; i < stateNames.size(); i++)
                    stateList.add(stateNames.get(i).getName());
                ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(
                        context, R.layout.template_spinner_text, stateList);
                stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mandiState.setAdapter(stateAdapter);
                AppUtils.hideProgressDialog();
            }

            @Override
            public void onResponse401(Response<GetIdNames> response) {

            }
        });

        mandiState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> adapterView, View view, int position, long id) {
                if (stateNames.get((int) id).getName().equals(UrlConstant.SELECT)) return;
                stateId = stateNames.get((int) id).getId();
                districtList.clear();
                AppUtils.showProgressDialog(context);
                Call<GetIdNames> callDistrict = client.getDistrictList(stateId);
                callDistrict.enqueue(new ApiCallBack<GetIdNames>() {
                    @Override
                    public void onResponse(Response<GetIdNames> response) {
                        if (response.body() == null || response.body().getData() == null) return;
                        GetIdNames.GetData getData = new GetIdNames.GetData();
                        getData.setId(0);
                        getData.setName(UrlConstant.SELECT);
                        districtNames = response.body().getData();
                        districtNames.add(0, getData);
                        for (int i = 0; i < districtNames.size(); i++)
                            districtList.add(districtNames.get(i).getName());
                        ArrayAdapter<String> disttAdapter = new ArrayAdapter<String>(
                                context, R.layout.template_spinner_text, districtList);
                        disttAdapter.setDropDownViewResource(
                                android.R.layout.simple_spinner_dropdown_item);
                        mandiDistt.setAdapter(disttAdapter);
                        AppUtils.hideProgressDialog();
                    }

                    @Override
                    public void onResponse401(Response<GetIdNames> response) {
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        mandiDistt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> adapterView, View view, int position, long id) {
                if (districtNames.get((int) id).getName().equals(UrlConstant.SELECT)) return;
                districtId = districtNames.get((int) id).getId();
                blockList.clear();
                AppUtils.showProgressDialog(context);
                Call<GetIdNames> callBlock = client.getBlockList(districtId);
                callBlock.enqueue(new ApiCallBack<GetIdNames>() {
                    @Override
                    public void onResponse(Response<GetIdNames> response) {
                        if (response.body() == null || response.body().getData() == null) return;
                        GetIdNames.GetData getData = new GetIdNames.GetData();
                        getData.setId(0);
                        getData.setName(UrlConstant.SELECT);
                        blockNames = response.body().getData();
                        blockNames.add(0, getData);
                        for (int i = 0; i < blockNames.size(); i++)
                            blockList.add(blockNames.get(i).getName());
                        ArrayAdapter<String> blockAdapter = new ArrayAdapter<String>(
                                context, R.layout.template_spinner_text, blockList);
                        blockAdapter.setDropDownViewResource(
                                android.R.layout.simple_spinner_dropdown_item);
                        mandiBlock.setAdapter(blockAdapter);
                        AppUtils.hideProgressDialog();
                    }

                    @Override
                    public void onResponse401(Response<GetIdNames> response) {
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        mandiBlock.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> adapterView, View view, int position, long id) {
                if (blockNames.get((int) id).getName().equals(UrlConstant.SELECT)) return;
                blockId = blockNames.get((int) id).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean toReturn = false;
                if (AppUtils.isNullCase(mandiName.getText().toString())) {
                    mandiName.setError("Market name is required");
                    toReturn = true;
                }
                if (stateId == 0 || districtId == 0 || blockId == 0) {
                    AppUtils.showToast("State, district and block are required");
                    toReturn = true;
                }
                if (AppUtils.isNullCase(mandiAddr.getText().toString())) {
                    mandiAddr.setError("Address is required");
                    toReturn = true;
                }
                if (AppUtils.isNullCase(mandiPinCode.getText().toString())) {
                    mandiPinCode.setError("Pincode is required");
                    toReturn = true;
                }
                if (toReturn) return;

                AppUtils.showProgressDialog(context);
                MandiCreate.Data data = new MandiCreate.Data(
                        mandiName.getText().toString(), Long.valueOf(stateId),
                        Long.valueOf(districtId), Long.valueOf(blockId),
                        mandiAddr.getText().toString(), mandiPinCode.getText().toString());
                Call<ResponseCreateEntity> mandiCall = client.createMandi(new MandiCreate(data));
                mandiCall.enqueue(new ApiCallBack<ResponseCreateEntity>() {
                    @Override
                    public void onResponse(Response<ResponseCreateEntity> response) {
                        if (response.body() == null
                                || response.body().getMetadata().getEntity_id() == null) return;
                        if (response.code() == 200) {
                            getSchemaData((Activity) context,
                                    UrlConstant.key_mapping.get(keyName).get("key"));
                            storeInMap(getViewFields.get(fieldPosition).getKey(),
                                    response.body().getMetadata().getEntity_id().toString());
                            innerDialog.dismiss();
                            dialog.dismiss();
                            AppUtils.hideProgressDialog();
                        } else if (response.code() == 500) {
                            AppUtils.showToast(UrlConstant.TECH_PROB);
                        }
                    }

                    @Override
                    public void onResponse401(Response<ResponseCreateEntity> response) {
                    }
                });
            }
        });
    }

    private void showAlertDialog(String info) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(info);
        builder.setCancelable(true);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(final DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void setTotalValue() {
        if (!TextUtils.isEmpty(qtyValue) && (!TextUtils.isEmpty(rateValue))) {
            if (!rateValue.equals("-")) {
                float qty = Float.parseFloat(qtyValue);
                float rate = Float.parseFloat(rateValue);
                float multiply = qty * rate;
                if (amountTextView != null)
                    amountTextView.setText("" + multiply);
            } else {
                if (amountTextView != null)
                    amountTextView.setText("0");
            }
        } else {
            if (amountTextView != null)
                amountTextView.setText("0");
        }
    }

    private void storeRequiredFields(int fieldPosition, TextView required) {
        if ((getViewFields.get(fieldPosition).getAttributes() != null) &&
                (getViewFields.get(fieldPosition).getAttributes().getRequired() != null))
            if (getReadEditState(getViewFields.get(fieldPosition).getAttributes().getRequired(),
                    postDataMap, entity_status)) {
                required.setVisibility(View.VISIBLE);
                requiredMap.add(getViewFields.get(fieldPosition).getKey());
                requiredField.requiredField(requiredMap);
            }
    }

    private String printUOM(long selectPos) {
        if ((uomPos != 0) && (selectPos != 0L)) {
            ArrayList<GetViewFields.MappingValues> arrayList;

            if (keyName.equals(UrlConstant.MANDI_PRICE)) {
                arrayList = getViewFields.get(uomPos).getMapping_values();
            } else {
                arrayList = getViewFields.get(
                        qtyPos).getSub_fields().get(uomPos).getMapping_values();
            }
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i).getId() == selectPos) {
                    storeInMap(uomKey, arrayList.get(i).getUom_id().get(0));
                    return arrayList.get(i).getUom_id().get(1);
                }
            }
        }
        return null;
    }

    private void displaySelectValues(int pos, RecyclerView recyclerView, TextView selection, String type) {
        recyclerAdapterSelectionValues = new RecyclerAdapterSelectionValues(context, selection, pos,
                getViewFields.get(pos).getSelection_values(), type, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(recyclerAdapterSelectionValues);
    }

    private String checkKey(String keyText) {
        if (postDataMap.isEmpty())
            return null;
        for (Map.Entry<String, String> entry : postDataMap.entrySet()) {
            if (entry.getKey().equals(keyText)) {
                return entry.getValue();
            }
        }
        return null;
    }

    private String getNameFromId(int fieldPos, int entityFieldValue, String type, int selectPos) {
        ArrayList<GetIdName> getIdNamesList = new ArrayList<>();
        if ((type.equals(UrlConstant.MANY2ONE)))
            getIdNamesList = getViewFields.get(fieldPos).getSelection_values();

        for (int i = 0; i < getIdNamesList.size(); i++) {
            if (entityFieldValue == getIdNamesList.get(i).getId()) {
                if (getViewFields.get(fieldPos).getKey().equals(UrlConstant.PARTNER_ID)) {
                    ifsc_code = getIdNamesList.get(i).getIfsc_code();
                    account_no = getIdNamesList.get(i).getAccount_no();
                    point_of_contact = getIdNamesList.get(i).getPoint_of_contact();
                }
                return getIdNamesList.get(i).getName();
            }
        }

        return null;
    }

    private int getPosFromMap(int fieldPos, String entityFieldValue, String type) {
        ArrayList<GetIdName> getIdNamesList = new ArrayList<>();
        if ((type.equals(UrlConstant.SELECTION)))
            getIdNamesList = getViewFields.get(fieldPos).getSelection_values();

        for (int i = 0; i < getIdNamesList.size(); i++) {
            if (entityFieldValue.equals(getIdNamesList.get(i).getName()))
                return i;
        }

        return 0;
    }

    private void onText(final int fieldPosition, final TextView textView) {
        int mday, mmonth, myear;
        if (textView.getText().toString().equals("")) {
            mday = myCalendar.get(Calendar.DAY_OF_MONTH);
            mmonth = myCalendar.get(Calendar.MONTH);
            myear = myCalendar.get(Calendar.YEAR);
        } else {
            ArrayList<String> list = new ArrayList<>(Arrays.asList(textView.getText().toString().split("/")));
            mday = Integer.parseInt(list.get(0));
            mmonth = Integer.parseInt(list.get(1));
            myear = Integer.parseInt(list.get(2));
        }
        DatePickerDialog StartTime = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int month = monthOfYear + 1;
                textView.setText(dayOfMonth + "/" + month + "/" + year);
                if (!textView.getText().toString().equals(postDataMap.get(getViewFields.get(fieldPosition).getKey())))
                    AppPreference.getInstance().setDATA_CHANGE_FLAG(true);

                storeInMap(getViewFields.get(fieldPosition).getKey(), textView.getText().toString());

            }
        }, myear, mmonth - 1, mday);
        StartTime.show();
    }

    @Override
    public int getItemCount() {
        return getViewFields.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private void storeInMap(String keyName, String data) {
        postDataMap.put(keyName, data);
        postJsonValues.onClick(false, postDataMap);
    }

    private void displayData(LinearLayout layout, String value, EditText editText) {
        layout.setVisibility(View.VISIBLE);
        if (!AppUtils.isNullCase(value))
            editText.setText(value);
    }

    private void displayValidations(String key, ImageView errorImg) {
        String error = "";
        if (validationMap.isEmpty())
            return;
        for (Map.Entry<String, String> entry : validationMap.entrySet()) {
            if (key.equals(entry.getKey()))
                error = entry.getValue();
        }
        if (!AppUtils.isNullCase(error)) {
            errorImg.setVisibility(View.VISIBLE);
        }
        final String finalError = error;
        errorImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.showErrorDialog(context, finalError);
            }
        });
    }

    private void getSchemaData(final Activity activity, final String keyName) {
        AppRestClient client = AppRestClient.getInstance();
        Call<com.dehaat.node.rest.response.EntityView> call = client.getEntitySchema(keyName);
        call.enqueue(new Callback<com.dehaat.node.rest.response.EntityView>() {
            @Override
            public void onResponse(Call<com.dehaat.node.rest.response.EntityView> call,
                                   Response<com.dehaat.node.rest.response.EntityView> response) {

                if (response.body() == null || response.body().getData() == null)
                    return;
                if (AppUtils.haveNetworkConnection(activity))
                    if (response.raw().networkResponse().code() == 200)
                        AppUtils.storeDataDB(activity, response.body().getData(), true);
                    else if (response.code() == 500)
                        AppUtils.showToast(UrlConstant.SERVER_ERROR);

                databaseHandler = new DatabaseHandler(context);
                update(response.body().getData().getSchema().getFields());

            }

            @Override
            public void onFailure(Call<com.dehaat.node.rest.response.EntityView> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (AppUtils.haveNetworkConnection(activity))
                        AppUtils.showToast(UrlConstant.SERVER_NOT_RES);
                } else
                    AppUtils.showToast(UrlConstant.TECH_PROB);
            }

        });

    }

    private void update(ArrayList<GetViewFields> temp) {
        this.getViewFields = temp;
        this.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(TextView selection, EditText receivingInput, int position,
                            String selectionId, String value, Object payload) {
        dialog.dismiss();
        if (selection != null && selectionId != null) {
            handleTextViewSelection(selection, position, selectionId, value);
        } else if (receivingInput != null && selectionId != null) {
            handleEditTextSelection(receivingInput, position, selectionId, value, payload);
        }
    }

    private void handleTextViewSelection(TextView selection, int position, String selectionId,
                                         String value) {
        if (keyName.equals(UrlConstant.PURCHASE_ORDER)) {
            ArrayList<GetIdName> getIdNames = getViewFields.get(position).getSelection_values();
            if (getViewFields.get(position).getKey().equals(UrlConstant.PARTNER_ID)) {
                for (int i = 0; i < getIdNames.size(); i++) {
                    if (getIdNames.get(i).getId() == Long.parseLong(selectionId)) {
                        ifsc_code = getIdNames.get(i).getIfsc_code();
                        account_no = getIdNames.get(i).getAccount_no();
                        point_of_contact = getIdNames.get(i).getPoint_of_contact();
                        break;
                    }
                }
            }
            if (ifscTextView != null)
                if (!AppUtils.isNullCase(ifsc_code))
                    ifscTextView.setText(ifsc_code);

            if (accountTextView != null)
                if (!AppUtils.isNullCase(account_no))
                    accountTextView.setText(account_no);
            if (pointOfContactTextView != null)
                if (!AppUtils.isNullCase(point_of_contact))
                    pointOfContactTextView.setText(point_of_contact);
        }
        if ((keyName.equals(UrlConstant.MANUFACTURING_ORDER)) &&
                (getViewFields.get(position).getKey().equals(UrlConstant.PRODUCT_ID))) {
            bomCallBack.onIdClick(Long.valueOf(selectionId));
            nestedSpinner = Long.parseLong(selectionId);
            qtyTextView.setText(printUOM(nestedSpinner));
        }
        selection.setText(value);
        if (getViewFields.get(position).getKey().equals(UrlConstant.PRODUCT_ID)) {
            commodPos = Long.parseLong(selectionId);
            String data = printUOM(commodPos);
            if (!AppUtils.isNullCase(data))
                qtyTextView.setText(data);
        }

        if (!selection.getText().toString().equals(postDataMap.get(getViewFields.get(position).getKey())))
            AppPreference.getInstance().setDATA_CHANGE_FLAG(true);

        storeInMap(getViewFields.get(position).getKey(), "" + selectionId);
    }

    private void handleEditTextSelection(EditText receivingInput, int position, String selectionId,
                                         String value, Object payload) {
        receivingInput.setText(value);
        if (!value.equals(postDataMap.get(getViewFields.get(position).getKey()))) {
            AppPreference.getInstance().setDATA_CHANGE_FLAG(true);
        }

        if (keyName.equals(UrlConstant.STORAGE_ORDER)) {
            selectedPayload = payload;
            String partnerName = ((GetEntityList.CreateBody) payload)
                    .getField(UrlConstant.NAME);
            storeInMap(UrlConstant.PARTNER_NAME, partnerName);
        }

        storeInMap(getViewFields.get(position).getKey(), selectionId);
    }

    public void setSelectedPayload(Object selectedPayload) {
        this.selectedPayload = selectedPayload;
    }

    public class EntityView extends RecyclerView.ViewHolder {
        private TextView textName, dateTime, required, qty_unit, lotText, selection,
                string_selection, qty_uom_unit;
        private LinearLayout mainBack, selectionBack, dateTimeBack, quantityBack, stringBack,
                floatBack, noteBack, lotBack, stringSelectionBack, quantityUOMBack, intBack;
        private EditText quantity, stringTxt, floatTxt, noteString, intTxt;
        private ImageView errorImg;

        public EntityView(View itemView) {
            super(itemView);
            mainBack = itemView.findViewById(R.id.mainBack);
            stringSelectionBack = itemView.findViewById(R.id.stringSelectionBack);
            selectionBack = itemView.findViewById(R.id.selectionBack);
            dateTimeBack = itemView.findViewById(R.id.dateTimeBack);
            quantityBack = itemView.findViewById(R.id.quantityBack);
            stringBack = itemView.findViewById(R.id.stringBack);
            floatBack = itemView.findViewById(R.id.floatBack);
            noteBack = itemView.findViewById(R.id.noteBack);
            lotBack = itemView.findViewById(R.id.lotBack);
            intBack = itemView.findViewById(R.id.intBack);

            textName = itemView.findViewById(R.id.textName);
            lotText = itemView.findViewById(R.id.lotText);
            selection = itemView.findViewById(R.id.selection);
            dateTime = itemView.findViewById(R.id.dateTime);
            required = itemView.findViewById(R.id.required);
            qty_unit = itemView.findViewById(R.id.qty_unit);
            string_selection = itemView.findViewById(R.id.string_selection);

            quantity = itemView.findViewById(R.id.quantity);
            floatTxt = itemView.findViewById(R.id.floatTxt);
            stringTxt = itemView.findViewById(R.id.stringTxt);
            noteString = itemView.findViewById(R.id.noteString);
            intTxt = itemView.findViewById(R.id.intTxt);

            errorImg = itemView.findViewById(R.id.errorImg);

            quantityUOMBack = itemView.findViewById(R.id.quantityUOMBack);
            qty_uom_unit = itemView.findViewById(R.id.qty_uom_unit);
        }
    }
}

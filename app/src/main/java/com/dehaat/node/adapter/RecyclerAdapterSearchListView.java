package com.dehaat.node.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.dehaat.node.R;
import com.dehaat.node.utilities.SearchParams;

import java.util.ArrayList;

public class RecyclerAdapterSearchListView extends
        RecyclerView.Adapter<RecyclerAdapterSearchListView.SelectionValuesList> implements Filterable {
    private Context context;
    private ArrayList<String> searchValuesList;
    private ArrayList<String> filterSearchValuesList;
    private SearchParams searchParams;


    public RecyclerAdapterSearchListView(Activity activity, ArrayList<String> searchValuesList,
                                         SearchParams searchParams) {
        this.context = activity;
        this.searchValuesList = searchValuesList;
        this.filterSearchValuesList = searchValuesList;
        this.searchParams = searchParams;
    }

    @Override
    public RecyclerAdapterSearchListView.SelectionValuesList onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_selection_values, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterSearchListView.SelectionValuesList(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterSearchListView.SelectionValuesList holder,
                                 final int position) {
        holder.setIsRecyclable(false);
        final int pos = holder.getAdapterPosition();
        holder.name.setText(searchValuesList.get(pos));
        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchParams.onClick(pos, searchValuesList.get(pos));
            }
        });
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    searchValuesList = filterSearchValuesList;
                } else {

                    ArrayList<String> filteredList = new ArrayList<>();

                    for (String list : filterSearchValuesList)
                        if (list.toLowerCase().contains(charString.toLowerCase()))
                            filteredList.add(list);

                    searchValuesList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = searchValuesList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                searchValuesList = (ArrayList<String>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemCount() {
        return searchValuesList.size();
    }

    public class SelectionValuesList extends RecyclerView.ViewHolder {
        private TextView name, number;
        private LinearLayout back;

        public SelectionValuesList(View itemView) {
            super(itemView);
            back = itemView.findViewById(R.id.back);
            name = itemView.findViewById(R.id.name);
            number = itemView.findViewById(R.id.number);
        }

    }


}

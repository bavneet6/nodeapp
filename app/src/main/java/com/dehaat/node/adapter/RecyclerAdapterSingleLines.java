package com.dehaat.node.adapter;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dehaat.node.R;
import com.dehaat.node.database.DatabaseHandler;
import com.dehaat.node.rest.response.GetIdName;
import com.dehaat.node.rest.response.GetViewFields;
import com.dehaat.node.utilities.AppPreference;
import com.dehaat.node.utilities.AppUtils;
import com.dehaat.node.utilities.PostJsonPositionValues;
import com.dehaat.node.utilities.RequiredField;
import com.dehaat.node.utilities.SearchParams;
import com.dehaat.node.utilities.SelectionId;
import com.dehaat.node.utilities.UrlConstant;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

import static com.dehaat.node.utilities.AppUtils.getReadEditState;

public class RecyclerAdapterSingleLines extends
        RecyclerView.Adapter<RecyclerAdapterSingleLines.EntityView> implements SelectionId {
    private Context context;
    private ArrayList<GetViewFields> getViewFields;
    private HashSet<String> requiredMap = new HashSet<>();
    private String valueStored, entity_status, uomKey, qtyValue, rateValue, keyName;
    private HashMap<String, String> postDataMap;
    private RequiredField requiredField;
    private Dialog dialog;
    private TextView qtyTextView;
    private Calendar myCalendar = Calendar.getInstance();
    private Long commodPos = 0L;
    private int uomPos = 0, fieldPos;
    private DatabaseHandler databaseHandler;
    private EditText amountTextView;
    private RecyclerAdapterSelectionValues recyclerAdapterSelectionValues;
    private PostJsonPositionValues postJsonPositionValues;
    private SearchParams searchParams;
    private HashMap<String, String> validationMap;

    public RecyclerAdapterSingleLines(Context context, String keyName, int fieldPos,
                                      ArrayList<GetViewFields> getViewFields,
                                      HashMap<String, String> postDataMap, String entity_status,
                                      HashMap<String, String> validationMap,
                                      RequiredField requiredField,
                                      PostJsonPositionValues postJsonPositionValues,
                                      SearchParams searchParams) {
        this.context = context;
        this.keyName = keyName;
        this.fieldPos = fieldPos;
        this.getViewFields = getViewFields;
        this.postDataMap = postDataMap;
        this.entity_status = entity_status;
        this.validationMap = validationMap;
        this.requiredField = requiredField;
        this.postJsonPositionValues = postJsonPositionValues;
        this.searchParams = searchParams;

    }

    @Override
    public RecyclerAdapterSingleLines.EntityView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_sale_fields, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterSingleLines.EntityView(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterSingleLines.EntityView holder, final int position) {
        holder.setIsRecyclable(false);
        final int fieldPosition = holder.getAdapterPosition();
        valueStored = checkKey(getViewFields.get(fieldPosition).getKey());
        if (!AppUtils.isNullCase(getViewFields.get(fieldPosition).getDisplay_Name()))
            holder.textName.setText(getViewFields.get(fieldPosition).getDisplay_Name());
        String compKey = getViewFields.get(fieldPosition).getKey();
        String compType = getViewFields.get(fieldPosition).getType();
        GetViewFields.Attributes attributes = getViewFields.get(fieldPosition).getAttributes();
        if ((attributes != null)
                && (attributes.getInvisible() != null))
            if (getReadEditState(attributes.getInvisible(),
                    postDataMap, entity_status)) {
                holder.mainBack.setVisibility(View.GONE);
            }
        //storing required field in hashmap
        storeRequiredFields(fieldPosition, holder.required);
        displayValidations(compKey, holder.errorImg);
        if (compType.equals(UrlConstant.MANY2ONE)) {
            holder.selectionBack.setVisibility(View.VISIBLE);
            if (attributes != null)
                if (attributes.getReadonly() != null)
                    if (getReadEditState(attributes.getReadonly(),
                            postDataMap, entity_status)) {
                        holder.selection.setClickable(false);
                        holder.selection.setEnabled(false);
                        holder.selection.setTextColor(context.getResources().getColor(R.color.lightgrey));
                    }
            if (!AppUtils.isNullCase(valueStored)) {
                commodPos = Long.valueOf(valueStored);
                String name = getNameFromId(fieldPosition, Integer.parseInt(valueStored),
                        compType, 0);
                holder.selection.setText(name);
            }

        } else if (compType.equals(UrlConstant.FLOAT)) {
            if (compKey.equals(UrlConstant.PRICE_TOTAL)) {
                amountTextView = holder.floatTxt;
                setTotalValue();
            }
            if (compKey.equals(UrlConstant.PRODUCT_QTY)) {
                qtyValue = postDataMap.get(UrlConstant.PRODUCT_QTY);
            } else if (compKey.equals(UrlConstant.PRICE_UNIT)) {
                rateValue = postDataMap.get(UrlConstant.PRICE_UNIT);
            }
            displayData(holder.floatBack, valueStored, holder.floatTxt);
            if (attributes != null)
                if (attributes.getReadonly() != null)
                    if (getReadEditState(attributes.getReadonly(),
                            postDataMap, entity_status)) {
                        holder.floatTxt.setFocusable(false);
                        holder.floatTxt.setEnabled(false);
                        holder.floatTxt.setTextColor(context.getResources().getColor(R.color.lightgrey));
                    }

        } else if (compType.equals(UrlConstant.STRING)) {
            displayData(holder.stringBack, valueStored, holder.stringTxt);
            if (attributes != null)
                if (attributes.getReadonly() != null)
                    if (getReadEditState(attributes.getReadonly(),
                            postDataMap, entity_status)) {
                        holder.stringTxt.setFocusable(false);
                        holder.stringTxt.setEnabled(false);
                        holder.stringTxt.setTextColor(context.getResources().getColor(R.color.lightgrey));
                    }
        } else if (compType.equals(UrlConstant.NOTE)) {
            displayData(holder.noteBack, valueStored, holder.noteString);
            if (attributes != null)
                if (attributes.getReadonly() != null)
                    if (getReadEditState(attributes.getReadonly(),
                            postDataMap, entity_status)) {
                        holder.noteString.setFocusable(false);
                        holder.noteString.setEnabled(false);
                        holder.noteString.setTextColor(context.getResources().getColor(R.color.lightgrey));
                        AppPreference.getInstance().setREADNOTES(true);
                    }
        } else if (compType.equals(UrlConstant.SELECTION)) {
            holder.stringSelectionBack.setVisibility(View.VISIBLE);
            displayValidations(compKey, holder.errorImg);
            if (attributes != null && attributes.getReadonly() != null)
                if (getReadEditState(attributes.getReadonly(), postDataMap, entity_status)) {
                    holder.string_selection.setClickable(false);
                    holder.string_selection.setEnabled(false);
                    holder.string_selection.setTextColor(context.getResources().getColor(R.color.lightgrey));
                }

            if (!AppUtils.isNullCase(valueStored)) {
                int pos = getPosFromMap(fieldPosition, valueStored, compType);
                holder.string_selection.setText(
                        getViewFields.get(fieldPosition).getSelection_values().get(
                                pos).getDisplay_name());
            }

        } else if (compType.equals(UrlConstant.DATETIME) ||
                compType.equals(UrlConstant.DATE)) {
            holder.dateTimeBack.setVisibility(View.VISIBLE);
            String date = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());

            if (entity_status.equals(UrlConstant.NEW))
                holder.dateTime.setText(date);

            if (!AppUtils.isNullCase(valueStored))
                holder.dateTime.setText(valueStored);
            if (attributes != null
                    && attributes.getReadonly() != null)
                if (getReadEditState(attributes.getReadonly(), postDataMap, entity_status)) {
                    holder.dateTime.setClickable(false);
                    holder.dateTime.setEnabled(false);
                    holder.dateTime.setTextColor(context.getResources().getColor(R.color.lightgrey));
                }
            storeInMap(compType, holder.dateTime.getText().toString());
        } else if (compType.equals(UrlConstant.UNIT_OF_MEAS)) {
            holder.quantityUOMBack.setVisibility(View.VISIBLE);
            uomPos = fieldPosition;
            qtyTextView = holder.qty_uom_unit;
            uomKey = compKey;
            String data = printUOM(commodPos);
            if (!AppUtils.isNullCase(data))
                qtyTextView.setText(data);
            if (attributes != null)
                if (attributes.getReadonly() != null)
                    if (getReadEditState(attributes.getReadonly(),
                            postDataMap, entity_status)) {
                        holder.qty_uom_unit.setFocusable(false);
                        holder.qty_uom_unit.setEnabled(false);
                        holder.qty_uom_unit.setTextColor(context.getResources().getColor(R.color.lightgrey));
                        AppPreference.getInstance().setREADNOTES(true);
                    }
        }
        holder.selection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(context);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.dialog_show_selection_values);
                final androidx.appcompat.widget.SearchView search = dialog.findViewById(R.id.search);
                search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        search.setIconified(false);
                    }
                });
                RecyclerView partList = dialog.findViewById(R.id.partList);

                displaySelectValues(fieldPosition, partList, holder.selection,
                        compType);
                search.setQueryHint("Search..");
                search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                    @Override
                    public boolean onQueryTextSubmit(String txt) {
                        // TODO Auto-generated method stub
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String txt) {
                        if (getViewFields.get(fieldPosition).getSelection_values().size() != 0) {
                            recyclerAdapterSelectionValues.getFilter().filter(txt);
                        }
                        return false;
                    }
                });
                dialog.getWindow().

                        setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.getWindow().

                        setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().

                        setGravity(Gravity.CENTER_VERTICAL);

                dialog.show();

            }
        });
        holder.floatTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (compKey.equals(UrlConstant.PRODUCT_QTY)) {
                    qtyValue = holder.floatTxt.getText().toString();
                    setTotalValue();
                } else if (compKey.equals(UrlConstant.PRICE_UNIT)) {
                    rateValue = holder.floatTxt.getText().toString();
                    setTotalValue();
                }
                if (!holder.floatTxt.getText().toString().equals(
                        postDataMap.get(compKey)))
                    AppPreference.getInstance().setDATA_CHANGE_FLAG(true);

                storeInMap(compKey, holder.floatTxt.getText().toString());
            }
        });
        holder.stringTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!holder.stringTxt.getText().toString().equals(
                        postDataMap.get(compKey)))
                    AppPreference.getInstance().setDATA_CHANGE_FLAG(true);

                storeInMap(compKey, holder.stringTxt.getText().toString());
            }
        });
        holder.noteString.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!holder.noteString.getText().toString().equals(
                        postDataMap.get(compKey)))
                    AppPreference.getInstance().setDATA_CHANGE_FLAG(true);

                storeInMap(compKey,
                        holder.noteString.getText().toString());
            }
        });
        holder.dateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onText(fieldPosition, holder.dateTime);
            }
        });
        holder.string_selection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(context);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.dialog_show_selection_values);
                Button create = dialog.findViewById(R.id.create);
                TextView selectLine = dialog.findViewById(R.id.selectLine);
                final SearchView search = dialog.findViewById(R.id.search);
                search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        search.setIconified(false);
                    }
                });

                selectLine.setVisibility(View.GONE);
                RecyclerView partList = dialog.findViewById(R.id.partList);
                databaseHandler = new DatabaseHandler(context);
                create.setVisibility(View.GONE);

                displaySelectValues(fieldPosition, partList, holder.string_selection,
                        getViewFields.get(fieldPosition).getType());

                search.setQueryHint("Search..");
                search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String txt) {
                        // TODO Auto-generated method stub
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String txt) {
                        if (getViewFields.get(fieldPosition).getSelection_values().size() != 0) {
                            recyclerAdapterSelectionValues.getFilter().filter(txt);
                        }
                        return false;
                    }
                });
                dialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.getWindow().setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                dialog.show();
            }
        });
    }

    private int getPosFromMap(int fieldPos, String entityFieldValue, String type) {
        ArrayList<GetIdName> getIdNamesList = new ArrayList<>();
        if ((type.equals(UrlConstant.SELECTION)))
            getIdNamesList = getViewFields.get(fieldPos).getSelection_values();

        for (int i = 0; i < getIdNamesList.size(); i++) {
            if (entityFieldValue.equals(getIdNamesList.get(i).getName()))
                return i;
        }

        return 0;
    }

    private void onText(final int fieldPosition, final TextView textView) {
        int mday, mmonth, myear;
        if (textView.getText().toString().equals("")) {
            mday = myCalendar.get(Calendar.DAY_OF_MONTH);
            mmonth = myCalendar.get(Calendar.MONTH);
            myear = myCalendar.get(Calendar.YEAR);
        } else {
            ArrayList<String> list = new ArrayList<>(Arrays.asList(textView.getText().toString().split("/")));
            mday = Integer.parseInt(list.get(0));
            mmonth = Integer.parseInt(list.get(1));
            myear = Integer.parseInt(list.get(2));
        }
        DatePickerDialog StartTime = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int month = monthOfYear + 1;
                textView.setText(dayOfMonth + "/" + month + "/" + year);
                if (!textView.getText().toString().equals(postDataMap.get(getViewFields.get(fieldPosition).getKey())))
                    AppPreference.getInstance().setDATA_CHANGE_FLAG(true);

                storeInMap(getViewFields.get(fieldPosition).getKey(), textView.getText().toString());

            }
        }, myear, mmonth - 1, mday);
        StartTime.show();
    }

    private String getNameFromId(int fieldPos, int entityFieldValue, String type, int selectPos) {
        ArrayList<GetIdName> getIdNamesList = new ArrayList<>();
        if ((type.equals(UrlConstant.MANY2ONE)))
            getIdNamesList = getViewFields.get(fieldPos).getSelection_values();

        for (int i = 0; i < getIdNamesList.size(); i++) {
            if (entityFieldValue == getIdNamesList.get(i).getId())
                return getIdNamesList.get(i).getName();
        }

        return null;
    }

    private void setTotalValue() {
        if (keyName.equals(UrlConstant.VENDOR_BILL)) {
            if (!TextUtils.isEmpty(qtyValue) && (!TextUtils.isEmpty(rateValue))) {
                if (!rateValue.equals("-") && !qtyValue.equals("-")) {
                    float qty = Float.parseFloat(qtyValue);
                    float rate = Float.parseFloat(rateValue);
                    float multiply = qty * rate;
                    if (amountTextView != null)
                        amountTextView.setText("" + multiply);
                } else {
                    if (amountTextView != null)
                        amountTextView.setText("0");
                }
            } else {
                if (amountTextView != null)
                    amountTextView.setText("0");
            }
            searchParams.onClick(fieldPos, amountTextView.getText().toString());
        }
    }

    private void displaySelectValues(int pos, RecyclerView recyclerView,
                                     TextView selection, String type) {
        recyclerAdapterSelectionValues = new RecyclerAdapterSelectionValues(context, selection, pos,
                getViewFields.get(pos).getSelection_values(), type, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(recyclerAdapterSelectionValues);
    }

    private void displayValidations(String key, ImageView errorImg) {
        String error = "";
        if (validationMap == null || validationMap.isEmpty())
            return;
        for (Map.Entry<String, String> entry : validationMap.entrySet()) {
            if (key.equals(entry.getKey()))
                error = entry.getValue();
        }
        if (!AppUtils.isNullCase(error)) {
            errorImg.setVisibility(View.VISIBLE);
        }
        final String finalError = error;
        errorImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.showErrorDialog(context, finalError);
            }
        });
    }


    private void displayData(LinearLayout layout, String value, EditText editText) {
        layout.setVisibility(View.VISIBLE);
        if (!AppUtils.isNullCase(value))
            editText.setText(value);
    }

    @Override
    public int getItemCount() {
        return getViewFields.size();
    }

    private String checkKey(String keyText) {
        if (postDataMap.isEmpty())
            return null;
        for (Map.Entry<String, String> entry : postDataMap.entrySet()) {

            if (entry.getKey().equals(keyText)) {
                return entry.getValue();
            }
        }
        return null;
    }

    private String printUOM(long selectPos) {
        if (selectPos != 0L) {
            ArrayList<GetViewFields.MappingValues> arrayList = getViewFields.get(uomPos).getMapping_values();
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i).getId() == selectPos) {
                    storeInMap(uomKey, arrayList.get(i).getUom_id().get(0));
                    return arrayList.get(i).getUom_id().get(1);
                }
            }
        }
        return null;

    }

    private void storeRequiredFields(int fieldPosition, TextView required) {
        if ((getViewFields.get(fieldPosition).getAttributes() != null) &&
                (getViewFields.get(fieldPosition).getAttributes().getRequired() != null))
            if (getReadEditState(getViewFields.get(fieldPosition).getAttributes().getRequired(),
                    postDataMap, entity_status)) {
                required.setVisibility(View.VISIBLE);
                requiredMap.add(getViewFields.get(fieldPosition).getKey());
                requiredField.requiredField(requiredMap);
            }
    }

    @Override
    public void onItemClick(TextView selection, EditText receivingInput, int position,
                            String selectionId, String value, Object payload) {
        dialog.dismiss();
        if (selectionId == null)
            return;
        selection.setText(value);
        if (getViewFields.get(position).getKey().equals(UrlConstant.PRODUCT_ID)) {
            commodPos = Long.valueOf(selectionId);
            String data = printUOM(commodPos);
            if (!AppUtils.isNullCase(data))
                qtyTextView.setText(data);
        }

        if (!selection.getText().toString().equals(postDataMap.get(getViewFields.get(position).getKey())))
            AppPreference.getInstance().setDATA_CHANGE_FLAG(true);

        storeInMap(getViewFields.get(position).getKey(), "" + selectionId);
    }

    private void storeInMap(String keyName, String data) {
        postDataMap.put(keyName, data);
        postJsonPositionValues.onClick(fieldPos, postDataMap);
    }

    public class EntityView extends RecyclerView.ViewHolder {
        private TextView required, textName, selection, qty_uom_unit, dateTime, string_selection;
        private LinearLayout selectionBack, quantityUOMBack, stringBack, floatBack, noteBack,
                dateTimeBack, mainBack, stringSelectionBack;
        private EditText stringTxt, noteString, floatTxt;
        private ImageView errorImg;


        public EntityView(View itemView) {
            super(itemView);
            required = itemView.findViewById(R.id.required);
            textName = itemView.findViewById(R.id.textName);
            selection = itemView.findViewById(R.id.selection);
            qty_uom_unit = itemView.findViewById(R.id.qty_uom_unit);

            mainBack = itemView.findViewById(R.id.mainBack);
            selectionBack = itemView.findViewById(R.id.selectionBack);
            quantityUOMBack = itemView.findViewById(R.id.quantityUOMBack);
            stringBack = itemView.findViewById(R.id.stringBack);
            floatBack = itemView.findViewById(R.id.floatBack);
            noteBack = itemView.findViewById(R.id.noteBack);
            dateTimeBack = itemView.findViewById(R.id.dateTimeBack);
            string_selection = itemView.findViewById(R.id.string_selection);
            stringSelectionBack = itemView.findViewById(R.id.stringSelectionBack);

            stringTxt = itemView.findViewById(R.id.stringTxt);
            noteString = itemView.findViewById(R.id.noteString);
            floatTxt = itemView.findViewById(R.id.floatTxt);
            dateTime = itemView.findViewById(R.id.dateTime);
            errorImg = itemView.findViewById(R.id.errorImg);
        }

    }
}


package com.dehaat.node.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.dehaat.node.R;
import com.dehaat.node.utilities.DeleteImages;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by DELL on 11/29/2017.
 */

public class RecyclerAdapterImageList extends RecyclerView.Adapter<RecyclerAdapterImageList.ImageList> {
    private Context context;
    private ArrayList<File> imagesList;
    private DeleteImages deleteImages;

    public RecyclerAdapterImageList(ArrayList<File> imagesList, DeleteImages deleteImages) {
        this.imagesList = imagesList;
        this.deleteImages = deleteImages;
    }

    @Override
    public RecyclerAdapterImageList.ImageList onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_imagelist, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterImageList.ImageList(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterImageList.ImageList holder, final int position) {
        holder.setIsRecyclable(false);
        final int pos = holder.getAdapterPosition();
        Bitmap bitmap = new BitmapDrawable(context.getResources(),
                imagesList.get(pos).getAbsolutePath()).getBitmap();
        holder.image_back.setImageBitmap(bitmap);
        holder.cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagesList.remove(holder.getAdapterPosition());
                deleteImages.onItemClick(String.valueOf(holder.getAdapterPosition()));
                notifyDataSetChanged();
            }
        });
        holder.image_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setCanceledOnTouchOutside(true);
                final ImageView imageView;
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.image_zoom);
                imageView = dialog.findViewById(R.id.imageView);

                Bitmap bitmap = new BitmapDrawable(context.getResources(),
                        imagesList.get(pos).getAbsolutePath()).getBitmap();
                imageView.setImageBitmap(bitmap);

                dialog.getWindow().setGravity(Gravity.CENTER);
                dialog.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return imagesList.size();
    }

    public class ImageList extends RecyclerView.ViewHolder {
        private TextView cross;
        private ImageView image_back;

        public ImageList(View itemView) {
            super(itemView);
            cross = itemView.findViewById(R.id.cross);
            image_back = itemView.findViewById(R.id.image_back);
        }

    }


}

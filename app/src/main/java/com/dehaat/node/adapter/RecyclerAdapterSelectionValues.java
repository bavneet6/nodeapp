package com.dehaat.node.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.dehaat.node.R;
import com.dehaat.node.rest.response.GetIdName;
import com.dehaat.node.utilities.SelectionId;
import com.dehaat.node.utilities.UrlConstant;

import java.util.ArrayList;

public class RecyclerAdapterSelectionValues extends RecyclerView.Adapter<RecyclerAdapterSelectionValues.SelectionValuesList> implements Filterable {
    private Context context;
    private ArrayList<GetIdName> selection_values;
    private ArrayList<GetIdName> filter_selection_values;
    private SelectionId selectionId;
    private TextView selection;
    private String type;
    private int position_;

    public RecyclerAdapterSelectionValues(Context context, TextView selection, int position_,
                                          ArrayList<GetIdName> selection_values, String type,
                                          SelectionId selectionId) {
        this.context = context;
        this.selection = selection;
        this.position_ = position_;
        this.selection_values = selection_values;
        this.filter_selection_values = selection_values;
        this.type = type;
        this.selectionId = selectionId;
    }

    @Override
    public RecyclerAdapterSelectionValues.SelectionValuesList onCreateViewHolder(
            ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_selection_values, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterSelectionValues.SelectionValuesList(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterSelectionValues.SelectionValuesList holder,
                                 final int position) {
        holder.setIsRecyclable(false);
        final int pos = holder.getAdapterPosition();
        if (type.equals(UrlConstant.SELECTION)) {
            holder.name.setText(selection_values.get(pos).getDisplay_name());
            holder.number.setText("");
        } else {
            holder.name.setText(selection_values.get(pos).getName());
            holder.number.setText(selection_values.get(pos).getMobile());
        }
        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.equals(UrlConstant.SELECTION))
                    selectionId.onItemClick(selection, null, position_, selection_values.get(pos).getName(),
                            selection_values.get(pos).getName(), null);
                else
                    selectionId.onItemClick(selection, null, position_,
                            String.valueOf(selection_values.get(pos).getId()),
                            selection_values.get(pos).getName(), null);
            }
        });
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    selection_values = filter_selection_values;
                } else {

                    ArrayList<GetIdName> filteredList = new ArrayList<>();

                    for (GetIdName list : filter_selection_values)
                        if ((list.getName().toLowerCase().contains(charString.toLowerCase()))
                                || ((list.getMobile() != null) &&
                                (list.getMobile().contains(charString))))
                            filteredList.add(list);

                    selection_values = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = selection_values;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                selection_values = (ArrayList<GetIdName>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemCount() {
        return selection_values.size();
    }

    public class SelectionValuesList extends RecyclerView.ViewHolder {
        private TextView name, number;
        private LinearLayout back;

        public SelectionValuesList(View itemView) {
            super(itemView);
            back = itemView.findViewById(R.id.back);
            name = itemView.findViewById(R.id.name);
            number = itemView.findViewById(R.id.number);
        }
    }
}
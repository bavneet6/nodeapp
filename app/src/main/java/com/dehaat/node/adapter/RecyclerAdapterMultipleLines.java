package com.dehaat.node.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dehaat.node.R;
import com.dehaat.node.rest.response.GetViewFields;
import com.dehaat.node.rest.response.MultipleLines;
import com.dehaat.node.utilities.AppPreference;
import com.dehaat.node.utilities.One2manyRequired;
import com.dehaat.node.utilities.PostJsonPositionValues;
import com.dehaat.node.utilities.PostJsonValues;
import com.dehaat.node.utilities.RequiredField;
import com.dehaat.node.utilities.SearchParams;
import com.dehaat.node.utilities.UrlConstant;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.dehaat.node.utilities.AppUtils.getReadEditState;

public class RecyclerAdapterMultipleLines extends
        RecyclerView.Adapter<RecyclerAdapterMultipleLines.EntityView> implements RequiredField,
        PostJsonPositionValues, SearchParams {
    private static HashMap<Integer, String> amount_total_map = new HashMap<>();
    private Context context;
    private ArrayList<GetViewFields> getViewFields;
    private List<List<MultipleLines.GetKeyValue>> multipleLines;
    private LinkedHashMap<Integer, HashMap<String, String>> postBaseDataMap = new LinkedHashMap<>();
    private String entity_status, keyName;
    private PostJsonValues postJsonValues;
    private HashSet<String> requiredField = new HashSet<>();
    private GetViewFields.Attributes attributes;
    private Float amount_total = 0f;
    private HashMap<Integer, HashMap<String, String>> invoiceValidationProcessMap;
    private One2manyRequired one2manyRequired;

    public RecyclerAdapterMultipleLines(Context context, String keyName, ArrayList<GetViewFields> getViewFields,
                                        List<List<MultipleLines.GetKeyValue>> multipleLines,
                                        String entity_status, HashMap<Integer,
            HashMap<String, String>> invoiceValidationProcessMap,
                                        PostJsonValues postJsonValues,
                                        One2manyRequired one2manyRequired) {
        this.context = context;
        this.keyName = keyName;
        this.getViewFields = getViewFields;
        this.multipleLines = multipleLines;
        this.entity_status = entity_status;
        this.invoiceValidationProcessMap = invoiceValidationProcessMap;
        this.postJsonValues = postJsonValues;
        this.one2manyRequired = one2manyRequired;


        for (int i = 0; i < getViewFields.size(); i++) {
            if (getViewFields.get(i).getType().equals(UrlConstant.ONE2MANY)) {
                this.getViewFields = getViewFields.get(i).getSub_fields();
                attributes = getViewFields.get(i).getAttributes();
            }
        }
        postBaseDataMap.clear();
        for (int i = 0; i < multipleLines.size(); i++) {
            HashMap<String, String> tempMap = new HashMap<>();
            for (int j = 0; j < multipleLines.get(i).size(); j++) {
                MultipleLines.GetKeyValue getKeyValue = multipleLines.get(i).get(j);
                tempMap.put(getKeyValue.getKey(), getKeyValue.getValue());
            }
            postBaseDataMap.put(i, tempMap);
        }
    }

    public RecyclerAdapterMultipleLines.EntityView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_mrp_lines_base, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterMultipleLines.EntityView(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterMultipleLines.EntityView holder, final int position) {
        holder.setIsRecyclable(false);
        final int fieldPos = holder.getAdapterPosition();
        holder.mrp_lines_list.setLayoutManager(new LinearLayoutManager(context));
        holder.mrp_lines_list.setAdapter(new RecyclerAdapterSingleLines(context, keyName, fieldPos,
                getViewFields, postBaseDataMap.get(fieldPos), entity_status,
                invoiceValidationProcessMap.get(fieldPos), this,
                this, this));

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppPreference.getInstance().setDATA_CHANGE_FLAG(true);
                HashMap<String, String> posData = postBaseDataMap.get(fieldPos);
                posData.put("is_deleted", "" + true);
                postBaseDataMap.put(fieldPos, posData);
                updateAdapter(postBaseDataMap);
                if (keyName.equals(UrlConstant.VENDOR_BILL)) {
                    prepareDataForInvoiceLines(postBaseDataMap);
                    calculateTotalCount(fieldPos);
                } else if (keyName.equals(UrlConstant.PAYMENT_REQUEST)) {
                    prepareDataForPaymentLines(postBaseDataMap);
                } else {
                    prepareDataForPriceLines(postBaseDataMap);
                }
            }
        });
        if (attributes != null)
            if (attributes.getReadonly() != null)
                if (getReadEditState(attributes.getReadonly(), null, entity_status)) {
                    holder.delete.setVisibility(View.GONE);
                }
        if (postBaseDataMap.get(fieldPos).get("is_deleted") != null &&
                postBaseDataMap.get(fieldPos).get("is_deleted").equals("" + true)) {
            holder.delete.setVisibility(View.GONE);
            holder.mrp_lines_list.setVisibility(View.GONE);
        }
    }

    private void updateAdapter(LinkedHashMap<Integer, HashMap<String, String>> postBaseDataMap) {
        this.postBaseDataMap = postBaseDataMap;
        notifyDataSetChanged();
    }

    private void calculateTotalCount(int fieldPos) {
        HashMap<String, String> tempHashMap = new HashMap<>();
        HashMap<String, String> deleteMap = new HashMap<>();
        deleteMap = postBaseDataMap.get(fieldPos);


        if (!deleteMap.isEmpty() && deleteMap.get("is_deleted") != null &&
                deleteMap.get("is_deleted").equals("true")) {
            amount_total_map.remove(fieldPos);
        }
        amount_total = 0f;

        for (Map.Entry<Integer, String> entry : amount_total_map.entrySet()) {
            amount_total = Float.valueOf(amount_total + Float.parseFloat(entry.getValue()));
        }
        tempHashMap.put(UrlConstant.AMOUNT_TOTAL, String.valueOf(amount_total));
        postJsonValues.onClick(true, tempHashMap);
    }

    @Override
    public int getItemCount() {
        return multipleLines.size();
    }

    @Override
    public void requiredField(HashSet<String> hashSet) {
        requiredField.addAll(hashSet);
        one2manyRequired.one2ManyRequiredField(requiredField);
    }

    @Override
    public void onClick(int pos, HashMap<String, String> value) {
        postBaseDataMap.put(pos, value);
        if (keyName.equals(UrlConstant.VENDOR_BILL))
            prepareDataForInvoiceLines(postBaseDataMap);
        else if (keyName.equals(UrlConstant.PAYMENT_REQUEST))
            prepareDataForPaymentLines(postBaseDataMap);
        else
            prepareDataForPriceLines(postBaseDataMap);
    }

    private void prepareDataForInvoiceLines(LinkedHashMap<Integer, HashMap<String, String>> postBaseDataMap) {
        HashMap<String, String> tempHashMap = new HashMap<>();
        List<List<MultipleLines.GetKeyValue>> getKeyValueList = new ArrayList<>();
        for (Map.Entry<Integer, HashMap<String, String>> entry : postBaseDataMap.entrySet()) {
            if (entry.getValue().get("id") == null && entry.getValue().get("is_deleted") != null &&
                    entry.getValue().get("is_deleted").equals("true")) {

            } else {
                List<MultipleLines.GetKeyValue> tempList = new ArrayList<>();
                for (Map.Entry<String, String> entry1 : entry.getValue().entrySet()) {
                    MultipleLines.GetKeyValue getKeyValue = new MultipleLines.GetKeyValue();
                    getKeyValue.setKey(entry1.getKey());
                    getKeyValue.setValue(entry1.getValue());
                    tempList.add(getKeyValue);
                }
                getKeyValueList.add(tempList);
            }
        }
        MultipleLines multipleLines = new MultipleLines();
        multipleLines.setInvoice_line_ids(getKeyValueList);
        Gson gson = new Gson();
        String jsonString = gson.toJson(multipleLines);
        tempHashMap.put(UrlConstant.INVOICE_LINE_IDS, jsonString);
        postJsonValues.onClick(false, tempHashMap);
    }

    private void prepareDataForPriceLines(LinkedHashMap<Integer, HashMap<String, String>> postBaseDataMap) {
        HashMap<String, String> tempHashMap = new HashMap<>();
        List<List<MultipleLines.GetKeyValue>> getKeyValueList = new ArrayList<>();
        for (Map.Entry<Integer, HashMap<String, String>> entry : postBaseDataMap.entrySet()) {
            if (entry.getValue().get("id") == null && entry.getValue().get("is_deleted") != null &&
                    entry.getValue().get("is_deleted").equals("true")) {

            } else {
                List<MultipleLines.GetKeyValue> tempList = new ArrayList<>();
                for (Map.Entry<String, String> entry1 : entry.getValue().entrySet()) {
                    MultipleLines.GetKeyValue getKeyValue = new MultipleLines.GetKeyValue();
                    getKeyValue.setKey(entry1.getKey());
                    getKeyValue.setValue(entry1.getValue());
                    tempList.add(getKeyValue);
                }
                getKeyValueList.add(tempList);
            }
        }
        MultipleLines multipleLines = new MultipleLines();
        multipleLines.setProduct_price(getKeyValueList);
        Gson gson = new Gson();
        String jsonString = gson.toJson(multipleLines);
        tempHashMap.put(UrlConstant.PRODUCT_PRICE_LINES, jsonString);
        postJsonValues.onClick(false, tempHashMap);
    }

    private void prepareDataForPaymentLines(LinkedHashMap<Integer, HashMap<String, String>> postBaseDataMap) {
        HashMap<String, String> tempHashMap = new HashMap<>();
        List<List<MultipleLines.GetKeyValue>> getKeyValueList = new ArrayList<>();
        for (Map.Entry<Integer, HashMap<String, String>> entry : postBaseDataMap.entrySet()) {
            if (entry.getValue().get("id") == null && entry.getValue().get("is_deleted") != null &&
                    entry.getValue().get("is_deleted").equals("true")) {

            } else {
                List<MultipleLines.GetKeyValue> tempList = new ArrayList<>();
                for (Map.Entry<String, String> entry1 : entry.getValue().entrySet()) {
                    MultipleLines.GetKeyValue getKeyValue = new MultipleLines.GetKeyValue();
                    getKeyValue.setKey(entry1.getKey());
                    getKeyValue.setValue(entry1.getValue());
                    tempList.add(getKeyValue);
                }
                getKeyValueList.add(tempList);
            }
        }
        MultipleLines multipleLines = new MultipleLines();
        multipleLines.setRequest_line_ids(getKeyValueList);
        Gson gson = new Gson();
        String jsonString = gson.toJson(multipleLines);
        tempHashMap.put(UrlConstant.REQUEST_LINE_IDS, jsonString);
        postJsonValues.onClick(false, tempHashMap);
    }

    @Override
    public void onClick(int id, String name) {
        amount_total_map.put(id, name);
        calculateTotalCount(id);
    }

    public class EntityView extends RecyclerView.ViewHolder {
        private RecyclerView mrp_lines_list;
        private ImageView delete;
        private LinearLayout back;


        public EntityView(View itemView) {
            super(itemView);
            mrp_lines_list = itemView.findViewById(R.id.mrp_lines_list);
            delete = itemView.findViewById(R.id.delete);
            back = itemView.findViewById(R.id.back);
            mrp_lines_list.setNestedScrollingEnabled(false);
        }

    }
}


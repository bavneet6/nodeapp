package com.dehaat.node.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.dehaat.node.R;
import com.dehaat.node.database.DatabaseHandler;
import com.dehaat.node.fragments.EntityCreationFragment;
import com.dehaat.node.rest.ApiCallBack;
import com.dehaat.node.rest.AppRestClient;
import com.dehaat.node.rest.response.EntityDataJson;
import com.dehaat.node.rest.response.JsonDataStore;
import com.dehaat.node.utilities.AppPreference;
import com.dehaat.node.utilities.AppUtils;
import com.dehaat.node.utilities.Unlink;
import com.dehaat.node.utilities.UrlConstant;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Response;

public class RecyclerAdapterBillPickingList extends
        RecyclerView.Adapter<RecyclerAdapterBillPickingList.EntityView> {
    private Context context;
    private ArrayList<EntityDataJson> getViewFields;
    private String keyName;
    private Long entity_id;
    private DatabaseHandler databaseHandler;
    private Unlink unlink;
    private HashMap<String, String> dataHashMap = new HashMap<>();


    public RecyclerAdapterBillPickingList(Context context, ArrayList<EntityDataJson> getViewFields,
                                          String keyName, Long entity_id, Unlink unlink) {
        this.context = context;
        this.getViewFields = getViewFields;
        this.keyName = keyName;
        this.entity_id = entity_id;
        this.unlink = unlink;
        databaseHandler = new DatabaseHandler(context);
    }

    @Override
    public RecyclerAdapterBillPickingList.EntityView onCreateViewHolder(ViewGroup parent,
                                                                        int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_picking_link, parent, false);
        return new RecyclerAdapterBillPickingList.EntityView(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterBillPickingList.EntityView holder,
                                 final int position) {
        final int fieldPos = holder.getAdapterPosition();
        if (keyName.equals(UrlConstant.SALES_ORDER) || keyName.equals(UrlConstant.PURCHASE_ORDER))
            holder.unlink.setVisibility(View.GONE);
        holder.setIsRecyclable(false);
        Gson gson = new Gson();
        ArrayList<JsonDataStore> arrayList = new ArrayList<>();

        if (getViewFields.get(fieldPos).getJson_payload() != null)
            arrayList = gson.fromJson(getViewFields.get(fieldPos).getJson_payload(),
                    new TypeToken<ArrayList<JsonDataStore>>() {
                    }.getType());
        if (arrayList != null && arrayList.size() > 0) {
            dataHashMap = AppUtils.convertToHashMap(context, arrayList);
        }
        String data = "";
        if (getViewFields.get(fieldPos).getEntity_type().equals(UrlConstant.EXPENSE_BILL)) {
            if (!dataHashMap.isEmpty()) {
                if (AppUtils.isNullCase(dataHashMap.get("number"))) {
                    data = databaseHandler.getStoreValues(getViewFields.get(fieldPos).getEntity_type(),
                            UrlConstant.PARTNER_ID, dataHashMap.get(UrlConstant.PARTNER_ID))
                            + " - " +
                            databaseHandler.getStoreValues(getViewFields.get(fieldPos).getEntity_type(),
                                    UrlConstant.PRODUCT_ID, dataHashMap.get(UrlConstant.PRODUCT_ID));
                } else {
                    data = dataHashMap.get("number") + " - " +
                            databaseHandler.getStoreValues(getViewFields.get(fieldPos).getEntity_type(),
                                    UrlConstant.PARTNER_ID, dataHashMap.get(UrlConstant.PARTNER_ID))
                            + " - " +
                            databaseHandler.getStoreValues(getViewFields.get(fieldPos).getEntity_type(),
                                    UrlConstant.PRODUCT_ID, dataHashMap.get(UrlConstant.PRODUCT_ID));
                }

            }
        } else if (getViewFields.get(fieldPos).getEntity_type().equals(UrlConstant.VENDOR_BILL)) {
            if (!dataHashMap.isEmpty()) {
                if (AppUtils.isNullCase(dataHashMap.get("number"))) {
                    data = databaseHandler.getStoreValues(getViewFields.get(fieldPos).getEntity_type(),
                            UrlConstant.PARTNER_ID, dataHashMap.get(UrlConstant.PARTNER_ID))
                            + " - ₹" +
                            dataHashMap.get(UrlConstant.AMOUNT_TOTAL);
                } else {
                    data = dataHashMap.get("number") + " - " +
                            databaseHandler.getStoreValues(getViewFields.get(fieldPos).getEntity_type(),
                                    UrlConstant.PARTNER_ID, dataHashMap.get(UrlConstant.PARTNER_ID))
                            + " - ₹" +
                            dataHashMap.get(UrlConstant.AMOUNT_TOTAL);
                }

            }
        } else {
            if (!dataHashMap.isEmpty()) {
                data = dataHashMap.get(UrlConstant.NAME) + " - " +
                        databaseHandler.getStoreValues(getViewFields.get(fieldPos).getEntity_type(),
                                UrlConstant.PARTNER_ID, dataHashMap.get(UrlConstant.PARTNER_ID)) +
                        " - " +
                        databaseHandler.getStoreValues(getViewFields.get(fieldPos).getEntity_type(),
                                UrlConstant.PRODUCT_ID, dataHashMap.get(UrlConstant.PRODUCT_ID));
            }
        }
        holder.poName.setText(data);

        holder.unlink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unlinkDialog(getViewFields.get(fieldPos).getEntity_id());
            }
        });
        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (keyName.equals(UrlConstant.SALES_DISPATCH))
                    keyName = UrlConstant.EXPENSE_BILL;
                else if (keyName.equals(UrlConstant.PURCHASE_RECEIPT))
                    keyName = UrlConstant.EXPENSE_BILL;
                else if (keyName.equals(UrlConstant.PURCHASE_ORDER))
                    keyName = UrlConstant.VENDOR_BILL;
                else if (keyName.equals(UrlConstant.EXPENSE_BILL)) {
                    if (getViewFields.get(fieldPos).getEntity_type().equals(UrlConstant.SALES_DISPATCH))
                        keyName = UrlConstant.SALES_DISPATCH;
                    else
                        keyName = UrlConstant.PURCHASE_RECEIPT;
                }

                EntityCreationFragment fragment = EntityCreationFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("KEY_NAME", keyName);
                bundle.putLong("ROW_ID", getViewFields.get(fieldPos).getRow_id());
                bundle.putBoolean("NOTES", false);
                fragment.setArguments(bundle);
                AppUtils.changeFragment((FragmentActivity) context, fragment);

            }
        });

    }

    private void unlinkDialog(final Long id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure you want to Delink?");
        builder.setCancelable(true);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(final DialogInterface dialog, int which) {
                AppUtils.showProgressDialog(context);
                AppRestClient client = AppRestClient.getInstance();
                com.dehaat.node.rest.body.Linking.Data data =
                        new com.dehaat.node.rest.body.Linking.Data(entity_id, id);
                com.dehaat.node.rest.body.Linking linking =
                        new com.dehaat.node.rest.body.Linking(data);

                Call<Void> call = null;

                if (keyName.equals(UrlConstant.SALES_DISPATCH))
                    call = client.postSalesVoucherBillUnLink(linking);
                else if (keyName.equals(UrlConstant.PURCHASE_RECEIPT))
                    call = client.postPurchaseVoucherBillUnLink(linking);
                else if (keyName.equals(UrlConstant.EXPENSE_BILL))
                    call = client.postBillUnLink(linking);

                call.enqueue(new ApiCallBack<Void>() {
                    @Override
                    public void onResponse(Response<Void> response) {
                        dialog.dismiss();
                        AppUtils.hideProgressDialog();
                        AppPreference.getInstance().setDATA_CHANGE_FLAG(false);
                        unlink.onClick(id, true);
                    }

                    @Override
                    public void onResponse401(Response<Void> response) throws JSONException {
                    }
                });

            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public int getItemCount() {
        return getViewFields.size();
    }

    public class EntityView extends RecyclerView.ViewHolder {
        private TextView poName, unlink;
        private LinearLayout back;


        public EntityView(View itemView) {
            super(itemView);
            back = itemView.findViewById(R.id.back);
            poName = itemView.findViewById(R.id.poName);
            unlink = itemView.findViewById(R.id.unlink);
            unlink.setVisibility(View.VISIBLE);
        }


    }

}
package com.dehaat.node.adapter;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.RecyclerView;

import com.dehaat.node.R;
import com.dehaat.node.database.DatabaseHandler;
import com.dehaat.node.rest.response.FinsihedProduct;
import com.dehaat.node.rest.response.GetIdName;
import com.dehaat.node.rest.response.GetViewFields;
import com.dehaat.node.rest.response.LotNumber;
import com.dehaat.node.utilities.AppPreference;
import com.dehaat.node.utilities.AppUtils;
import com.dehaat.node.utilities.BOMDataCallBack;
import com.dehaat.node.utilities.UrlConstant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.dehaat.node.utilities.AppUtils.getReadEditState;

public class RecyclerAdapterBOM extends RecyclerView.Adapter<RecyclerAdapterBOM.EntityView> {
    private Context context;
    private HashMap<Integer, FinsihedProduct.FinishedProd> moveLinesData = new HashMap<Integer, FinsihedProduct.FinishedProd>();
    private List<FinsihedProduct.FinishedProd> finishList;
    private BOMDataCallBack bomDataCallBack;
    private HashMap<String, String> postJsonData;
    private ArrayList<GetViewFields> getViewFields;
    private DatabaseHandler databaseHandler;
    private String entity_status, tracking;
    private HashMap<String, String> validationProcessMap;

    public RecyclerAdapterBOM(Context context, ArrayList<GetViewFields> getViewFields,
                              List<FinsihedProduct.FinishedProd> finishList,
                              HashMap<String, String> postJsonData, String entity_status,
                              HashMap<String, String> validationProcessMap,
                              BOMDataCallBack bomDataCallBack) {
        this.context = context;
        this.getViewFields = getViewFields;
        this.finishList = finishList;
        this.postJsonData = postJsonData;
        this.entity_status = entity_status;
        this.validationProcessMap = validationProcessMap;
        this.bomDataCallBack = bomDataCallBack;

        for (int i = 0; i < finishList.size(); i++) {
            moveLinesData.put(i, finishList.get(i));
        }
    }

    @Override
    public RecyclerAdapterBOM.EntityView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_bom, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterBOM.EntityView(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterBOM.EntityView holder, final int position) {
        holder.setIsRecyclable(false);
        final int fieldPos = holder.getAdapterPosition();
        checkValidation(holder.error, finishList.get(fieldPos).getProduct_id());
        for (int i = 0; i < getViewFields.size(); i++) {
            if (getViewFields.get(i).getType().equals(UrlConstant.MRP_LINES)) {
                ArrayList<GetViewFields> getViewFieldsArrayList = getViewFields.get(i).getSub_fields();
                for (int j = 0; j < getViewFieldsArrayList.size(); j++) {
                    if (getViewFieldsArrayList.get(j).getType().equals(UrlConstant.MANY2ONE))
                        holder.productName.setText(getName(getViewFields.get(i),
                                getViewFieldsArrayList.get(j).getKey(),
                                finishList.get(fieldPos).getProduct_id()));
                    else if (getViewFieldsArrayList.get(j).getType().equals(UrlConstant.UNIT_OF_MEAS))
                        holder.productUom.setText(getName(getViewFields.get(i),
                                getViewFieldsArrayList.get(j).getKey(),
                                finishList.get(fieldPos).getProduct_uom()));
                    else if (getViewFieldsArrayList.get(j).getType().equals(UrlConstant.FLOAT)) {
                        holder.productQty.setText("" + finishList.get(fieldPos).getProduct_qty());
                        if (getViewFieldsArrayList.get(j).getAttributes() != null)
                            if (getViewFieldsArrayList.get(j).getAttributes().getReadonly() != null)
                                if (getReadEditState(getViewFieldsArrayList.get(j).getAttributes().getReadonly(),
                                        postJsonData, entity_status)) {
                                    holder.productQty.setFocusable(false);
                                    holder.productQty.setEnabled(false);
                                    holder.productQty.setTextColor(context.getResources().getColor(R.color.lightgrey));
                                }
                    } else if (getViewFieldsArrayList.get(j).getType().equals(UrlConstant.LOT_NUMBER)) {
                        if ((AppUtils.isNullCase(tracking)) || (tracking.equals("none"))) {
                            holder.productLot.setVisibility(View.INVISIBLE);
                        } else {
                            holder.productLot.setVisibility(View.VISIBLE);
                            if (!AppUtils.isNullCase(finishList.get(fieldPos).getLot()))
                                holder.productLot.setText("" + finishList.get(fieldPos).getLot());
                            else {
                                holder.productLot.setText("T_" + AppUtils.getTimeStamp() + "_" +
                                        finishList.get(fieldPos).getProduct_id() + "_" +
                                        AppPreference.getInstance().getUSER_ID());
                                if (AppUtils.isNullCase(holder.productQty.getText().toString())) {
                                    storeInMap(fieldPos, 0f, holder.productLot.getText().toString());
                                } else {
                                    storeInMap(fieldPos, Float.valueOf(holder.productQty.getText().toString()),
                                            holder.productLot.getText().toString());
                                }
                            }
                        }

                        if (getViewFieldsArrayList.get(j).getAttributes() != null)
                            if (getViewFieldsArrayList.get(j).getAttributes().getReadonly() != null)
                                if (getReadEditState(getViewFieldsArrayList.get(j).getAttributes().getReadonly(),
                                        postJsonData, entity_status)) {
                                    holder.productLot.setClickable(false);
                                    holder.productLot.setEnabled(false);
                                    holder.productLot.setTextColor(context.getResources().getColor(R.color.lightgrey));
                                }
                    }
                }

                break;
            }
        }

        holder.productQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }


            @Override
            public void afterTextChanged(Editable s) {
                AppPreference.getInstance().setDATA_CHANGE_FLAG(true);
                if ((AppUtils.isNullCase(holder.productQty.getText().toString())) ||
                        holder.productQty.getText().toString().equals(".")) {
                    storeInMap(fieldPos, 0f, holder.productLot.getText().toString());
                } else {
                    storeInMap(fieldPos, Float.valueOf(holder.productQty.getText().toString()),
                            holder.productLot.getText().toString());
                }
            }
        });
        holder.productLot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setCancelable(true);
                dialog.setTitle("Select Lot Number");
                dialog.setContentView(R.layout.dialog_lot_numbers);
                androidx.appcompat.widget.SearchView lotSearch = dialog.findViewById(R.id.lotSearch);
                ListView lotList = dialog.findViewById(R.id.lotList);
                databaseHandler = new DatabaseHandler(context);
                ArrayList<LotNumber> lotNumbersList = databaseHandler.getLotNumbersData
                        (finishList.get(fieldPos).getProduct_id());
                List<String> perNameList = new ArrayList<>();
                for (int i = 0; i < lotNumbersList.size(); i++) {
                    if (!AppUtils.isNullCase(lotNumbersList.get(i).getPerm_lot_number()))
                        perNameList.add(lotNumbersList.get(i).getPerm_lot_number());
                    else
                        perNameList.add(lotNumbersList.get(i).getTemp_lot_number());
                }
                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                        android.R.layout.simple_list_item_1, perNameList);
                lotList.setAdapter(adapter);
                lotList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                        // TODO Auto-generated method stub
                        String value = adapter.getItem(position);
                        AppPreference.getInstance().setDATA_CHANGE_FLAG(true);
                        holder.productLot.setText(value);
                        dialog.dismiss();

                        if (AppUtils.isNullCase(holder.productQty.getText().toString())) {
                            storeInMap(fieldPos, 0f, holder.productLot.getText().toString());
                        } else {
                            storeInMap(fieldPos, Float.valueOf(holder.productQty.getText().toString()),
                                    holder.productLot.getText().toString());
                        }
                    }
                });
                lotSearch.setQueryHint("Search..");
                lotSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                    @Override
                    public boolean onQueryTextSubmit(String txt) {
                        // TODO Auto-generated method stub
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String txt) {
                        // TODO Auto-generated method stub

                        adapter.getFilter().filter(txt);
                        return false;
                    }
                });

                dialog.show();

            }
        });

    }

    private void checkValidation(ImageView error, Long product_id) {
        String errorText = "";
        if (validationProcessMap.isEmpty())
            return;
        for (Map.Entry<String, String> entry : validationProcessMap.entrySet()) {
            if (("" + product_id).equals(entry.getKey()))
                errorText = entry.getValue();
        }
        if (!AppUtils.isNullCase(errorText)) {
            error.setVisibility(View.VISIBLE);
        }
        final String finalErrorText = errorText;
        error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.showErrorDialog(context, finalErrorText);
            }
        });

    }

    private void storeInMap(int fieldPos, Float qty, String lot) {
        FinsihedProduct.FinishedProd finishedProd = new FinsihedProduct.FinishedProd();
        finishedProd.setProduct_id(finishList.get(fieldPos).getProduct_id());
        finishedProd.setProduct_qty(qty);
        finishedProd.setProduct_uom(finishList.get(fieldPos).getProduct_uom());
        if (AppUtils.isNullCase(lot))
            finishedProd.setLot_number(null);
        else
            finishedProd.setLot_number(lot);
        moveLinesData.put(fieldPos, finishedProd);
        bomDataCallBack.onDataCall(moveLinesData);
    }

    private String getName(GetViewFields getFields, String key, Long id) {

        for (int j = 0; j < getFields.getSub_fields().size(); j++) {
            GetViewFields getViewField = getFields.getSub_fields().get(j);
            if (getViewField.getKey().equals(key)) {
                if (getViewField.getType().equals(UrlConstant.MANY2ONE)) {
                    tracking = null;
                    ArrayList<GetIdName> getIdNames = getViewField.getSelection_values();
                    for (int k = 0; k < getIdNames.size(); k++) {
                        if (("" + getIdNames.get(k).getId()).equals("" + id)) {
                            tracking = getIdNames.get(k).getTracking();
                            return getIdNames.get(k).getName();
                        }
                    }
                } else if (getViewField.getType().equals(UrlConstant.UNIT_OF_MEAS)) {
                    for (int k = 0; k < getViewField.getMapping_values().size(); k++) {
                        if (("" + getViewField.getMapping_values().get(k).getUom_id().get(0))
                                .equals("" + id))
                            return getViewField.getMapping_values().get(k).getUom_id().get(1);
                    }
                }

            }
        }

        return null;
    }

    @Override
    public int getItemCount() {
        return finishList.size();
    }

    public class EntityView extends RecyclerView.ViewHolder {
        private TextView productName, productLot, productUom;
        private EditText productQty;
        private ImageView error;


        public EntityView(View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.productName);
            productLot = itemView.findViewById(R.id.productLot);
            productUom = itemView.findViewById(R.id.productUom);
            productQty = itemView.findViewById(R.id.productQty);
            error = itemView.findViewById(R.id.errorImg);
        }

    }
}


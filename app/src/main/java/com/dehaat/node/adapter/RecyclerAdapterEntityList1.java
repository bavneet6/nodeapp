package com.dehaat.node.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.dehaat.node.R;
import com.dehaat.node.database.DatabaseHandler;
import com.dehaat.node.fragments.EntityCreationFragment;
import com.dehaat.node.rest.response.EntityDataJson;
import com.dehaat.node.rest.response.JsonDataStore;
import com.dehaat.node.rest.response.SearchParameterTable;
import com.dehaat.node.utilities.AppUtils;
import com.dehaat.node.utilities.UrlConstant;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;

public class RecyclerAdapterEntityList1 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private Context context;
    private ArrayList<EntityDataJson> getViewFields;
    private String keyName;
    private ArrayList<JsonDataStore> dataList;
    private HashMap<String, String> postJsonData = new HashMap<>();
    private String notes = null;
    private DatabaseHandler databaseHandler;

    private boolean isLoadingAdded = false;

    public RecyclerAdapterEntityList1(Context context, ArrayList<EntityDataJson> getInfos,
                                      String keyName) {
        this.context = context;
        this.getViewFields = getInfos;
        this.keyName = keyName;
        databaseHandler = new DatabaseHandler(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.template_schema_list, parent, false);
        viewHolder = new EntityListVH(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {
            case ITEM:
                EntityListVH holder1 = (EntityListVH) holder;
                holder1.setIsRecyclable(false);
                final int fieldPos = holder1.getAdapterPosition();
                final String syncStatus = getViewFields.get(fieldPos).getSync_status();
                if (!AppUtils.isNullCase(getViewFields.get(fieldPos).getSync_status())) {
                    if (getViewFields.get(fieldPos).getSync_status().equals(UrlConstant.PENDING)) {
                        if (AppUtils.haveNetworkConnection(context)) {
                            holder1.imageStatus.setVisibility(View.GONE);
                            holder1.progressBar.setVisibility(View.VISIBLE);
                        } else {
                            holder1.imageStatus.setVisibility(View.VISIBLE);
                            holder1.progressBar.setVisibility(View.GONE);
                            holder1.imageStatus.setImageResource(R.drawable.pending);
                        }
                    } else if (syncStatus.equals(UrlConstant.ERRORS)) {
                        holder1.imageStatus.setImageResource(R.drawable.error1);
                    } else if (syncStatus.equals(UrlConstant.SUCCESS)) {
                        holder1.imageStatus.setImageResource(R.drawable.success);
                    }
                }
                storeInHashmap(fieldPos);
                if ((keyName.equals(UrlConstant.SALES_PAYMENT)) ||
                        (keyName.equals(UrlConstant.PURCHASE_PAYMENT))) {
                    holder1.paymentBack.setVisibility(View.VISIBLE);
                    holder1.orderBack.setVisibility(View.GONE);
                    holder1.dis3.setVisibility(View.GONE);
                    holder1.imageCom.setVisibility(View.VISIBLE);
                    holder1.imageCom.setImageResource(R.drawable.rupees_back);
                    holder1.dis6.setText(dataPrint(fieldPos, UrlConstant.PARTNER_ID));
                    holder1.dis7.setText(displayDataFromMap(UrlConstant.NAME));
                    holder1.dis8.setText(dataPrint(fieldPos, UrlConstant.ENTITY_STATUS));
                    holder1.dis9.setText(displayDataFromMap("payment_date"));
                    holder1.dis10.setText("Rs" + displayDataFromMap("amount"));
                    holder1.dis11.setText(dataPrint(fieldPos, "responsible"));
                    storeSearchValues(fieldPos, UrlConstant.NAME,
                            displayDataFromMap(UrlConstant.NAME));
                    storeSearchValues(fieldPos, UrlConstant.PARTNER_ID,
                            dataPrint(fieldPos, UrlConstant.PARTNER_ID));
                } else if (keyName.equals(UrlConstant.MANDI_PRICE)
                        || keyName.equals(UrlConstant.NODE_PRICE)) {
                    holder1.orderBack.setVisibility(View.GONE);
                    holder1.paymentBack.setVisibility(View.GONE);
                    holder1.notes.setVisibility(View.GONE);
                    holder1.pricesBack.setVisibility(View.VISIBLE);
                    holder1.commName.setVisibility(View.GONE);
                    if (keyName.equals(UrlConstant.MANDI_PRICE)) {
                        holder1.dis12.setText(dataPrint(fieldPos, UrlConstant.MANDI_ID));
                        holder1.dis15.setText(displayDataFromMap("applicable_date"));
                        storeSearchValues(
                                fieldPos, UrlConstant.MANDI_ID,
                                dataPrint(fieldPos, UrlConstant.MANDI_ID));
                    } else {
                        holder1.dis12.setText(dataPrint(fieldPos, UrlConstant.TEAM_ID));
                        holder1.dis15.setText(displayDataFromMap("from_date"));
                        holder1.dis16.setText(displayDataFromMap("to_date"));
                        holder1.dis16.setVisibility(View.VISIBLE);
                        storeSearchValues(
                                fieldPos, UrlConstant.TEAM_ID,
                                dataPrint(fieldPos, UrlConstant.TEAM_ID));
                    }

                    storeSearchValues(
                            fieldPos, UrlConstant.PRODUCT_ID,
                            dataPrint(fieldPos, UrlConstant.PRODUCT_ID));
                } else if (keyName.equals(UrlConstant.STORAGE_ORDER)) {

                    String entityStatus = databaseHandler.getEntitySingleListData(
                            getViewFields.get(position).getRow_id()).getEntity_status();
                    holder1.orderBack.setVisibility(View.GONE);
                    holder1.paymentBack.setVisibility(View.VISIBLE);
                    holder1.dis6.setText(displayDataFromMap(UrlConstant.NAME));
                    holder1.dis7.setText(
                            AppUtils.getDisplayNameStatus(context, entityStatus, keyName));
                    holder1.dis8.setText(displayDataFromMap(UrlConstant.PARTNER_NAME));
                    holder1.dis9.setText(dataPrint(fieldPos, UrlConstant.PRODUCT_ID));
                    holder1.dis10.setText(
                            AppUtils.generateDisplayData(
                                    context, "quantity", keyName, postJsonData));
                    holder1.dis11.setText(displayDataFromMap(UrlConstant.ORDER_DATE));

                    storeSearchValues(
                            fieldPos, UrlConstant.WAREHOUSE_ID,
                            dataPrint(fieldPos, UrlConstant.WAREHOUSE_ID));
                    storeSearchValues(
                            fieldPos, UrlConstant.PRODUCT_ID,
                            dataPrint(fieldPos, UrlConstant.PRODUCT_ID));
                } else if (keyName.equals(UrlConstant.NODE_PRICE)) {
                    holder1.orderBack.setVisibility(View.GONE);
                    holder1.paymentBack.setVisibility(View.GONE);
                    holder1.pricesBack.setVisibility(View.VISIBLE);
                    holder1.dis12.setText(dataPrint(fieldPos, UrlConstant.NODE_ID));
                } else if (keyName.equals(UrlConstant.COMMODITY_LOAN)) {
                    holder1.orderBack.setVisibility(View.GONE);
                    holder1.paymentBack.setVisibility(View.VISIBLE);
                    holder1.pricesBack.setVisibility(View.GONE);

                    holder1.dis6.setText(displayDataFromMap(UrlConstant.NAME));
                    holder1.dis7.setText(displayDataFromMap(UrlConstant.PARTNER_NAME));
                    holder1.dis8.setText(displayDataFromMap("application_date"));
                    holder1.dis9.setText("Rs. " + displayDataFromMap("requested_loan_amount"));
                    holder1.dis10.setText("Rs. " + displayDataFromMap("outstanding_loan_amount"));
                    holder1.dis11.setText(displayDataFromMap("rate_of_interest") + " %");

                    storeSearchValues(
                            fieldPos, UrlConstant.PARTNER_ID,
                            displayDataFromMap(UrlConstant.PARTNER_NAME));
                } else if (keyName.equals(UrlConstant.COMMODITY_LOAN_INVOICE)) {
                    holder1.orderBack.setVisibility(View.GONE);
                    holder1.paymentBack.setVisibility(View.GONE);
                    holder1.pricesBack.setVisibility(View.VISIBLE);

                    holder1.dis12.setText(displayDataFromMap(UrlConstant.PARTNER_NAME));
                    holder1.dis15.setVisibility(View.GONE);
                } else if (keyName.equals(UrlConstant.STORAGE_RECEIPT)
                        || keyName.equals(UrlConstant.STORAGE_DISPATCH)) {
                    holder1.orderBack.setVisibility(View.GONE);
                    holder1.paymentBack.setVisibility((View.VISIBLE));
                    holder1.pricesBack.setVisibility(View.GONE);

                    holder1.dis6.setText(displayDataFromMap(UrlConstant.NAME));
                    holder1.dis7.setText(displayDataFromMap(UrlConstant.ORIGIN));
                    holder1.dis8.setText(displayDataFromMap(UrlConstant.SCHEDULED_DATE));
                    holder1.dis9.setText(dataPrint(fieldPos, UrlConstant.WAREHOUSE_ID));
                    holder1.dis10.setText(displayDataFromMap(UrlConstant.PARTNER_NAME));
                    holder1.dis11.setText(dataPrint(fieldPos, UrlConstant.PRODUCT_ID));

                    storeSearchValues(
                            fieldPos, UrlConstant.PARTNER_ID,
                            displayDataFromMap(UrlConstant.PARTNER_NAME));
                    storeSearchValues(
                            fieldPos, UrlConstant.WAREHOUSE_ID,
                            dataPrint(fieldPos, UrlConstant.WAREHOUSE_ID));
                    storeSearchValues(
                            fieldPos, UrlConstant.ORIGIN,
                            displayDataFromMap(UrlConstant.ORIGIN));

                } else if (keyName.equals(UrlConstant.STORAGE_INVOICE)) {
                    holder1.orderBack.setVisibility(View.GONE);
                    holder1.paymentBack.setVisibility(View.VISIBLE);
                    holder1.pricesBack.setVisibility(View.GONE);

                    holder1.dis6.setText(displayDataFromMap(UrlConstant.NUMBER));
                    holder1.dis7.setText(displayDataFromMap(UrlConstant.PARTNER_NAME));
                    holder1.dis8.setText(AppUtils.getDisplayNameStatus(context,
                            getViewFields.get(position).getEntity_status(), keyName));
                    holder1.dis9.setText(dataPrint(fieldPos, UrlConstant.PRODUCT_ID));
                    holder1.dis10.setText(displayDataFromMap(UrlConstant.PRODUCT_QTY) + " qty");
                    holder1.dis11.setText(displayDataFromMap(UrlConstant.INVOICE_DATE));

                    storeSearchValues(
                            fieldPos, UrlConstant.PRODUCT_ID,
                            dataPrint(fieldPos, UrlConstant.PRODUCT_ID));
                } else {
                    holder1.imageCom.setVisibility(View.GONE);
                    holder1.commName.setVisibility(View.VISIBLE);
                    holder1.dis5.setText(dataPrint(fieldPos, UrlConstant.ENTITY_STATUS));

                    if ((keyName.equals(UrlConstant.SALES_ORDER)) ||
                            (keyName.equals(UrlConstant.PURCHASE_ORDER))) {
                        holder1.orderBack.setVisibility(View.VISIBLE);
                        holder1.paymentBack.setVisibility(View.GONE);
                        holder1.dis1.setText(dataPrint(fieldPos, UrlConstant.PARTNER_ID));
                        holder1.dis2.setText(displayDataFromMap(UrlConstant.NAME));
                        holder1.dis3.setText(displayDataFromMap("date_order"));
                        holder1.dis4.setText(AppUtils.generateDisplayData(context,
                                "quantity", keyName, postJsonData));
                        storeSearchValues(fieldPos, UrlConstant.NAME, displayDataFromMap(UrlConstant.NAME));
                        storeSearchValues(fieldPos, UrlConstant.PARTNER_ID, dataPrint(fieldPos, UrlConstant.PARTNER_ID));
                    } else if ((keyName.equals(UrlConstant.SALES_DISPATCH))
                            || (keyName.equals(UrlConstant.PURCHASE_RECEIPT))) {
                        holder1.orderBack.setVisibility(View.VISIBLE);
                        holder1.paymentBack.setVisibility(View.GONE);
                        holder1.dis1.setText(dataPrint(fieldPos, UrlConstant.PARTNER_ID));
                        holder1.dis3.setText(displayDataFromMap("lot_number"));
                        holder1.dis2.setText(displayDataFromMap(UrlConstant.NAME));
                        holder1.dis4.setText(AppUtils.generateDisplayData(context,
                                "quantity", keyName, postJsonData));

                        storeSearchValues(fieldPos, UrlConstant.NAME, displayDataFromMap(UrlConstant.NAME));
                        storeSearchValues(fieldPos, UrlConstant.PARTNER_ID,
                                dataPrint(fieldPos, UrlConstant.PARTNER_ID));
                        storeSearchValues(fieldPos, "origin", displayDataFromMap("origin"));

                    } else if ((keyName.equals(UrlConstant.PAYMENT_REQUEST))) {
                        holder1.orderBack.setVisibility(View.VISIBLE);
                        holder1.paymentBack.setVisibility(View.GONE);
                        holder1.dis1.setText(dataPrint(fieldPos, UrlConstant.PARTNER_ID));
                        holder1.dis3.setText(displayDataFromMap("payment_due_date"));
                        holder1.dis2.setText(displayDataFromMap(UrlConstant.NAME));
                        holder1.dis4.setText(AppUtils.generateDisplayData(context,
                                "quantity", keyName, postJsonData));

                        storeSearchValues(fieldPos, UrlConstant.NAME, displayDataFromMap(UrlConstant.NAME));
                        storeSearchValues(fieldPos, UrlConstant.PARTNER_ID,
                                dataPrint(fieldPos, UrlConstant.PARTNER_ID));

                    } else if (keyName.equals(UrlConstant.MANUFACTURING_ORDER)) {
                        holder1.orderBack.setVisibility(View.VISIBLE);
                        holder1.paymentBack.setVisibility(View.GONE);
                        holder1.dis1.setText(dataPrint(fieldPos, "warehouse_id"));
                        holder1.dis2.setText(displayDataFromMap(UrlConstant.NAME));
                        holder1.dis3.setText(displayDataFromMap("date_planned_start"));
                        holder1.dis4.setText(AppUtils.generateDisplayData(context, "quantity",
                                keyName, postJsonData));

                    } else if (keyName.equals(UrlConstant.VENDOR_BILL) ||
                            keyName.equals(UrlConstant.EXPENSE_BILL)) {
                        holder1.orderBack.setVisibility(View.VISIBLE);
                        holder1.paymentBack.setVisibility(View.GONE);
                        holder1.dis1.setText(dataPrint(fieldPos, UrlConstant.PARTNER_ID));
                        holder1.dis2.setText(displayDataFromMap("number"));
                        holder1.dis3.setText(displayDataFromMap("date_invoice"));
                        holder1.dis4.setText("₹" + displayDataFromMap(UrlConstant.AMOUNT_TOTAL));

                        storeSearchValues(fieldPos, "number", displayDataFromMap("number"));
                        storeSearchValues(fieldPos, UrlConstant.PARTNER_ID,
                                dataPrint(fieldPos, UrlConstant.PARTNER_ID));

                    }
                    printText(holder1.commName, fieldPos);
                    holder1.imageStatus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (getViewFields.get(fieldPos).getSync_status().equals(UrlConstant.PENDING))
                                if (AppUtils.haveNetworkConnection(context))
                                    AppUtils.showToast("Pending Data Syncing");
                                else
                                    AppUtils.showToast(UrlConstant.NO_INTERNET);
                            else if (getViewFields.get(fieldPos).getSync_status().equals(UrlConstant.ERRORS))
                                showErrorsDialog(getViewFields.get(fieldPos).getValidation_status());

                        }
                    });

                }

                holder1.backView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openEntityForm(fieldPos, false);
                    }
                });

                holder1.notes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        notes = null;
                        storeInHashmap(fieldPos);
                        if (AppUtils.isNullCase(notes))
                            AppUtils.showToast("No Notes Found.");
                        else {
                            final Dialog dialog = new Dialog(context);
                            final EditText notesText;
                            Button saveNotes;
                            dialog.setCanceledOnTouchOutside(true);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.dialog_notes);
                            notesText = dialog.findViewById(R.id.notesText);
                            saveNotes = dialog.findViewById(R.id.saveNotes);
                            saveNotes.setVisibility(View.VISIBLE);
                            saveNotes.setText("Edit");
                            notesText.setFocusable(false);
                            notesText.setEnabled(false);
                            saveNotes.setVisibility(View.VISIBLE);
                            notesText.setText(notes);
                            saveNotes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    openEntityForm(fieldPos, true);
                                }
                            });
                            Drawable d = new ColorDrawable(context.getResources().getColor(R.color.greytext));
                            d.setAlpha(100);
                            dialog.getWindow().setBackgroundDrawable(d);
                            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT);
                            dialog.getWindow().setGravity(Gravity.CENTER);
                            dialog.show();
                        }
                    }
                });
                break;
            case LOADING:
                break;
        }
    }

    private String displayDataFromMap(String key) {
        if (!AppUtils.isNullCase(postJsonData.get(key))) {
            return postJsonData.get(key);
        } else
            return "N/A";
    }

    private void storeSearchValues(int pos, String key, String value) {
        SearchParameterTable searchParameterTable = new SearchParameterTable();
        searchParameterTable.setEntity_type(keyName);
        searchParameterTable.setSearch_parameter(key);
        searchParameterTable.setSearch_value(value);
        searchParameterTable.setRow_id(getViewFields.get(pos).getRow_id());
        databaseHandler.insertSearchParameterValue(searchParameterTable);
    }

    @Override
    public int getItemCount() {
        return getViewFields == null ? 0 : getViewFields.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == getViewFields.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    private void printText(TextView textView, int pos) {
        String data = dataPrint(pos, UrlConstant.PRODUCT_ID);
        if (!data.equals("N/A"))
            textView.setText(data);
    }

    private String dataPrint(int pos, String key) {
        if (key.equals(UrlConstant.ENTITY_STATUS)) {
            if (!AppUtils.isNullCase(getViewFields.get(pos).getEntity_status())) {
                String displayStatusName = AppUtils.getDisplayNameStatus(context,
                        getViewFields.get(pos).getEntity_status(), keyName);
                if (!AppUtils.isNullCase(displayStatusName))
                    return displayStatusName;
                else
                    return "New";
            }
        } else {
            String name = databaseHandler.getStoreValues(keyName, key, postJsonData.get(key));
//            String name = AppUtils.generateDisplayData(context, key, keyName, postJsonData);
            if (AppUtils.isNullCase(name))
                return "N/A";
            else
                return name;
        }
        return null;
    }

    private void openEntityForm(int pos, boolean showNotes) {
        if ((AppUtils.haveNetworkConnection(context)) &&
                (getViewFields.get(pos).getSync_status().equals(UrlConstant.PENDING))) {
            AppUtils.showToast("Data is syncing.Please wait....");
        } else {
            EntityCreationFragment fragment = EntityCreationFragment.newInstance();
            Bundle bundle = new Bundle();
            bundle.putString("KEY_NAME", keyName);
            bundle.putLong("ROW_ID", getViewFields.get(pos).getRow_id());
            bundle.putBoolean("NOTES", showNotes);
            fragment.setArguments(bundle);
            AppUtils.changeFragment((FragmentActivity) context, fragment);
        }
    }

    private void showErrorsDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Errors");
        builder.setMessage(msg);
        builder.setCancelable(true);
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void storeInHashmap(int pos) {
        postJsonData.clear();
        Gson gson = new Gson();
        dataList = gson.fromJson(getViewFields.get(pos).getJson_payload(),
                new TypeToken<ArrayList<JsonDataStore>>() {
                }.getType());
        if (dataList != null)
            for (int i = 0; i < dataList.size(); i++) {
                postJsonData.put(dataList.get(i).getKey(), dataList.get(i).getValue());
            }
        if (!AppUtils.isNullCase(postJsonData.get("notes")))
            notes = postJsonData.get("notes");
    }

    public void add(EntityDataJson mc) {
        getViewFields.add(mc);
        notifyItemInserted(getViewFields.size() - 1);
    }

    public void addAll(ArrayList<EntityDataJson> mcList) {
        for (EntityDataJson mc : mcList) {
            add(mc);
        }
    }

    public void remove(EntityDataJson city) {
        int position = getViewFields.indexOf(city);
        if (position > -1) {
            getViewFields.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new EntityDataJson());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = getViewFields.size() - 1;
        EntityDataJson item = getItem(position);

        if (item != null) {
            getViewFields.remove(position);
            notifyItemRemoved(position);
        }
    }

    public EntityDataJson getItem(int position) {
        if (position < 0) return null;
        return getViewFields.get(position);
    }

    protected class EntityListVH extends RecyclerView.ViewHolder {
        private ProgressBar progressBar;
        private TextView dis1, dis2, dis3, dis4, dis5, dis6, dis7, dis8, dis9, dis10, dis11, dis12,
                dis15, dis16, commName;
        private ImageView imageStatus, imageCom, notes;
        private LinearLayout backView, paymentBack, orderBack, pricesBack, iconBack;

        public EntityListVH(View itemView) {
            super(itemView);
            iconBack = itemView.findViewById(R.id.iconBack);
            backView = itemView.findViewById(R.id.backView);
            orderBack = itemView.findViewById(R.id.orderBack);
            paymentBack = itemView.findViewById(R.id.paymentBack);
            pricesBack = itemView.findViewById(R.id.pricesBack);
            imageStatus = itemView.findViewById(R.id.imageStatus);
            notes = itemView.findViewById(R.id.notes);
            imageCom = itemView.findViewById(R.id.imageCom);
            progressBar = itemView.findViewById(R.id.progressbar);
            commName = itemView.findViewById(R.id.commName);
            dis1 = itemView.findViewById(R.id.dis1);
            dis2 = itemView.findViewById(R.id.dis2);
            dis3 = itemView.findViewById(R.id.dis3);
            dis4 = itemView.findViewById(R.id.dis4);
            dis5 = itemView.findViewById(R.id.dis5);
            dis6 = itemView.findViewById(R.id.dis6);
            dis7 = itemView.findViewById(R.id.dis7);
            dis8 = itemView.findViewById(R.id.dis8);
            dis9 = itemView.findViewById(R.id.dis9);
            dis10 = itemView.findViewById(R.id.dis10);
            dis11 = itemView.findViewById(R.id.dis11);
            dis12 = itemView.findViewById(R.id.dis12);
            dis15 = itemView.findViewById(R.id.dis15);
            dis16 = itemView.findViewById(R.id.dis16);
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder {
        public LoadingVH(View itemView) {
            super(itemView);
        }
    }
}
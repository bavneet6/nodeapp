package com.dehaat.node.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.dehaat.node.R;
import com.dehaat.node.fragments.EntityCreationFragment;
import com.dehaat.node.rest.response.GetEntityList;
import com.dehaat.node.utilities.AppUtils;
import com.dehaat.node.utilities.SelectionId;
import com.dehaat.node.utilities.UrlConstant;

import java.util.ArrayList;

public class RecyclerAdapterContactList
        extends RecyclerView.Adapter<RecyclerAdapterContactList.EntityView> {

    private Context context;
    private ArrayList<GetEntityList.CreateBody> contactList;
    private SelectionId selectionId;
    private EditText receivingInput;
    private int fieldPrintIndex;

    public RecyclerAdapterContactList(Context context,
                                      ArrayList<GetEntityList.CreateBody> contactList,
                                      SelectionId selectionId, EditText receivingInput,
                                      int fieldPrintIndex) {
        this.context = context;
        this.contactList = contactList;
        this.selectionId = selectionId;
        this.receivingInput = receivingInput;
        this.fieldPrintIndex = fieldPrintIndex;
    }

    @Override
    public RecyclerAdapterContactList.EntityView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_contact_list, parent, false);
        return new RecyclerAdapterContactList.EntityView(view);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapterContactList.EntityView holder, int position) {
        GetEntityList.CreateBody contact = contactList.get(position);

        final int fieldPosition = holder.getAdapterPosition();
        holder.setIsRecyclable(false);
        holder.contactName.setText(contact.getField(UrlConstant.NAME));
        holder.contactMobile.setText(contact.getField(UrlConstant.PRIMARY_NUMBER));

        holder.contactList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectionId.onItemClick(
                        null, receivingInput, fieldPrintIndex,
                        String.valueOf(contact.getEntity_id()),
                        contact.getField(UrlConstant.NAME), contact);
            }
        });

        holder.editContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EntityCreationFragment eCreateFragment = EntityCreationFragment.newInstance();
                Bundle args = new Bundle();
                args.putString("KEY_NAME", UrlConstant.PARTNER_CONTACT);
//              args.putLong("ROW_ID", contactList.get(position).getRow_id());
                args.putBoolean("NOTES", false);
                eCreateFragment.setArguments(args);
                AppUtils.changeFragment((FragmentActivity) context, eCreateFragment);
            }
        });
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }


    public class EntityView extends RecyclerView.ViewHolder {
        private ConstraintLayout contactList;
        private ImageView editContact;
        private TextView contactName, contactMobile;

        public EntityView(View itemView) {
            super(itemView);
            contactList = itemView.findViewById(R.id.contactList);
            editContact = itemView.findViewById(R.id.editContact);
            contactName = itemView.findViewById(R.id.contactName);
            contactMobile = itemView.findViewById(R.id.contactMobile);
        }
    }
}

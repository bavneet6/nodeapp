package com.dehaat.node.adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.dehaat.node.R;
import com.dehaat.node.database.DatabaseHandler;
import com.dehaat.node.rest.PicassoCallback;
import com.dehaat.node.rest.response.ProductFileTable;
import com.dehaat.node.utilities.AppUtils;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.dehaat.node.utilities.AppUtils.isNullCase;

public class RecyclerAdapterAttachedImages extends
        RecyclerView.Adapter<RecyclerAdapterAttachedImages.ImageList> {
    DatabaseHandler databaseHandler;
    private Context context;
    private String getPic;
    private List<String> arrayList;

    public RecyclerAdapterAttachedImages(Context context, List<String> farmerListInfo) {
        this.context = context;
        this.arrayList = farmerListInfo;
        databaseHandler = new DatabaseHandler(context);

    }

    @Override
    public RecyclerAdapterAttachedImages.ImageList onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_imagelist, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterAttachedImages.ImageList(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterAttachedImages.ImageList holder, final int position) {
        holder.cross.setVisibility(View.GONE);
        holder.setIsRecyclable(false);
        showImage(holder.getAdapterPosition(), holder.image_back);
        holder.image_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setCanceledOnTouchOutside(true);
                final ImageView imageView;
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.image_zoom);
                imageView = dialog.findViewById(R.id.imageView);
                showImage(holder.getAdapterPosition(), imageView);
                dialog.getWindow().setGravity(Gravity.CENTER);
                dialog.show();

            }
        });
    }

    private void showImage(int pos, final ImageView imageView) {
        generateAndCheckUrl(arrayList.get(pos));
        String photo = arrayList.get(pos);
        getPic = null;
        if (!isNullCase(photo)) {
            getPic = databaseHandler.getFileUrl(photo);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(imageView, new PicassoCallback("" +
                        photo, photo) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).into(imageView);
                    }
                });

            } else {
                imageView.setImageResource(0);
            }
        }

    }

    private void generateAndCheckUrl(String picFilePath) {
        databaseHandler = new DatabaseHandler(context);
        String fileName = databaseHandler.getFileName("" + picFilePath);
        if ((isNullCase(fileName)) || (!picFilePath.equals(fileName))) {
            ProductFileTable productFileTable = new ProductFileTable();
            productFileTable.setProduct_id("" + picFilePath);
            productFileTable.setProd_file(picFilePath);
            URL url = null;
            try {
                url = new AppUtils.FetchPicture(picFilePath).execute().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            productFileTable.setProd_url("" + url);
            databaseHandler.inserPicturesFile(productFileTable);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ImageList extends RecyclerView.ViewHolder {
        private TextView cross;
        private ImageView image_back;

        public ImageList(View itemView) {
            super(itemView);
            cross = itemView.findViewById(R.id.cross);
            image_back = itemView.findViewById(R.id.image_back);
        }

    }


}

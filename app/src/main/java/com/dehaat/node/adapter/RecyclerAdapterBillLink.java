package com.dehaat.node.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.dehaat.node.R;
import com.dehaat.node.database.DatabaseHandler;
import com.dehaat.node.rest.response.EntityDataJson;
import com.dehaat.node.rest.response.JsonDataStore;
import com.dehaat.node.utilities.AppUtils;
import com.dehaat.node.utilities.BillLinking;
import com.dehaat.node.utilities.UrlConstant;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RecyclerAdapterBillLink extends
        RecyclerView.Adapter<RecyclerAdapterBillLink.EntityView> {
    private Context context;
    private ArrayList<EntityDataJson> getViewFields;
    private BillLinking linking;
    private List<Long> ids = new ArrayList<>();
    private DatabaseHandler databaseHandler;
    private HashMap<String, String> dataHashMap = new HashMap<>();

    public RecyclerAdapterBillLink(Context context,
                                   ArrayList<EntityDataJson> getViewFields, BillLinking linking) {
        this.context = context;
        this.getViewFields = getViewFields;
        this.linking = linking;
        databaseHandler = new DatabaseHandler(context);
    }

    @Override
    public RecyclerAdapterBillLink.EntityView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_link, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterBillLink.EntityView(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterBillLink.EntityView holder,
                                 final int position) {
        final int fieldPos = holder.getAdapterPosition();
        holder.setIsRecyclable(false);
        if (ids.size() != 0)
            for (Long id : ids) {
                if (id == getViewFields.get(fieldPos).getEntity_id()) {
                    holder.tick.setVisibility(View.VISIBLE);
                }
            }

        Gson gson = new Gson();
        ArrayList<JsonDataStore> arrayList = gson.fromJson(getViewFields.get(fieldPos).
                        getJson_payload(),
                new TypeToken<ArrayList<JsonDataStore>>() {
                }.getType());
        if (arrayList != null && arrayList.size() > 0) {
            dataHashMap = AppUtils.convertToHashMap(context, arrayList);
        }
        String data = "";
        if (getViewFields.get(fieldPos).getEntity_type().equals(UrlConstant.EXPENSE_BILL)) {
            if (!dataHashMap.isEmpty()) {
                if (AppUtils.isNullCase(dataHashMap.get("number"))) {
                    data = databaseHandler.getStoreValues(getViewFields.get(fieldPos).getEntity_type(),
                            UrlConstant.PARTNER_ID, dataHashMap.get(UrlConstant.PARTNER_ID))
                            + " - " +
                            databaseHandler.getStoreValues(getViewFields.get(fieldPos).getEntity_type(),
                                    UrlConstant.PRODUCT_ID, dataHashMap.get(UrlConstant.PRODUCT_ID));
                } else {
                    data = dataHashMap.get("number") + " - " +
                            databaseHandler.getStoreValues(getViewFields.get(fieldPos).getEntity_type(),
                                    UrlConstant.PARTNER_ID, dataHashMap.get(UrlConstant.PARTNER_ID))
                            + " - " +
                            databaseHandler.getStoreValues(getViewFields.get(fieldPos).getEntity_type(),
                                    UrlConstant.PRODUCT_ID, dataHashMap.get(UrlConstant.PRODUCT_ID));
                }

            }
        } else {
            if (!dataHashMap.isEmpty()) {
                data = dataHashMap.get(UrlConstant.NAME) + " - " +
                        databaseHandler.getStoreValues(getViewFields.get(fieldPos).getEntity_type(),
                                UrlConstant.PARTNER_ID, dataHashMap.get(UrlConstant.PARTNER_ID))
                        + " - " +
                        databaseHandler.getStoreValues(getViewFields.get(fieldPos).getEntity_type(),
                                UrlConstant.PRODUCT_ID, dataHashMap.get(UrlConstant.PRODUCT_ID));
            }
        }
        holder.poName.setText(data);

        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ids.contains(getViewFields.get(fieldPos).getEntity_id())) {
                    ids.remove(getViewFields.get(fieldPos).getEntity_id());
                    holder.tick.setVisibility(View.GONE);

                } else {
                    ids.add(getViewFields.get(fieldPos).getEntity_id());
                    holder.tick.setVisibility(View.VISIBLE);

                }
                linking.onBillClick(ids);
            }
        });


    }

    @Override
    public int getItemCount() {
        return getViewFields.size();
    }

    public class EntityView extends RecyclerView.ViewHolder {
        private TextView poName;
        private LinearLayout back;
        private ImageView tick;


        public EntityView(View itemView) {
            super(itemView);
            back = itemView.findViewById(R.id.back);
            tick = itemView.findViewById(R.id.tick);
            poName = itemView.findViewById(R.id.poName);
        }


    }


}
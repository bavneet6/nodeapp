package com.dehaat.node.utilities;

public interface ShowMultipleLines {
    void show(boolean b, boolean button);
}

package com.dehaat.node.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.dehaat.node.Node;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class AppPreference {
    private static AppPreference mInstance;
    private final String PREF_NAME = "pref";
    private final String AUTH_TOKEN = "auth_token";
    private final String APP_LOGIN = "app_login";
    private final String DATA_CHANGE_FLAG = "data_change_flag";
    private final String USER_ID = "user_id";
    private final String WAREHOUSE_ID = "warehouse_id";
    private final String TEAM_ID = "team_id";
    private final String IP = "ip";
    private final String NOTES = "notes";
    private final String SEARCHED_TEXT = "search_text";
    private final String FCM_TOKEN = "fcm_token";
    private SharedPreferences sharedPref;

    public AppPreference() {
        sharedPref = Node.getInstance().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static AppPreference getInstance() {
        if (mInstance == null)
            mInstance = new AppPreference();

        return mInstance;
    }

    public void clearData() {
        SharedPreferences.Editor mEditor = sharedPref.edit();
        mEditor.clear();
        mEditor.commit();
    }

    private void saveData(String key, String val) {
        SharedPreferences.Editor mEditor = sharedPref.edit();
        mEditor.putString(key, val);
        mEditor.commit();
    }

    private void saveData(String key, boolean val) {
        SharedPreferences.Editor mEditor = sharedPref.edit();
        mEditor.putBoolean(key, val);
        mEditor.commit();
    }

    private void saveData(String key, long val) {
        SharedPreferences.Editor mEditor = sharedPref.edit();
        mEditor.putLong(key, val);
        mEditor.commit();
    }

    private String getStringData(String key, String defValue) {
        return sharedPref.getString(key, defValue);
    }

    private boolean getBooleanData(String key, boolean defValue) {
        return sharedPref.getBoolean(key, defValue);
    }

    private long getLongData(String key, long defValue) {
        return sharedPref.getLong(key, defValue);
    }

    public String getAuthToken() {
        return getStringData(AUTH_TOKEN, null);
    }
    //****************************************************************888

    public void setAuthToken(String token) {
        saveData(AUTH_TOKEN, token);
    }

    public boolean getAPP_LOGIN() {
        return getBooleanData(APP_LOGIN, false);
    }

    public void setAPP_LOGIN(boolean b) {
        saveData(APP_LOGIN, b);
    }

    public Long getUSER_ID() {
        return getLongData(USER_ID, 0);
    }

    public void setUSER_ID(Long value) {
        saveData(USER_ID, value);
    }

    public List<Long> getWAREHOUSE_ID() {
        Gson gson = new Gson();
        String signInJson = getStringData(WAREHOUSE_ID, null);
        if (signInJson != null) {
            Type type = new TypeToken<List<String>>() {
            }.getType();
            List<Long> allCardResponse = gson.fromJson(signInJson, type);
            return allCardResponse;
        }
        return null;
    }

    public void setWAREHOUSE_ID(List<Long> token) {
        Gson gson = new Gson();
        String signInJson = null;
        if (token != null) {
            signInJson = gson.toJson(token);
        }
        saveData(WAREHOUSE_ID, signInJson);
    }

    public boolean getDATA_CHANGE_FLAG() {
        return getBooleanData(DATA_CHANGE_FLAG, false);
    }

    public void setDATA_CHANGE_FLAG(boolean b) {
        saveData(DATA_CHANGE_FLAG, b);
    }

    public boolean getREADNOTES() {
        return getBooleanData(NOTES, false);
    }

    public void setREADNOTES(boolean b) {
        saveData(NOTES, b);
    }

    public Long getTEAM_ID() {
        return getLongData(TEAM_ID, 0);
    }

    public void setTEAM_ID(Long data) {
        saveData(TEAM_ID, data);
    }

    public String getSEARCHED_TEXT() {
        return getStringData(SEARCHED_TEXT, null);
    }

    public void setSEARCHED_TEXT(String data) {
        saveData(SEARCHED_TEXT, data);
    }

    public String getIP() {
        return getStringData(IP, UrlConstant.PERM_BASE_URL);
    }

    public void setIP(String ip) {
        saveData(IP, ip);
    }

    public String getFCM_TOKEN() {
        return getStringData(FCM_TOKEN, null);
    }

    public void setFCM_TOKEN(String token) {
        saveData(FCM_TOKEN, token);
    }

}

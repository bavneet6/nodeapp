package com.dehaat.node.utilities;

import android.text.TextUtils;

import com.dehaat.node.BuildConfig;

import java.util.HashMap;
import java.util.Map;

public interface UrlConstant {
    //    String PERM_BASE_URL = "http://13.126.85.106:5000";
    String TEMP_BASE_URL = BuildConfig.BASE_URL;
    //    String PERM_BASE_URL = "http://35.154.53.102:5000";
    String PERM_BASE_URL = BuildConfig.BASE_URL;

    String KEY_NAME = "KEY_NAME";
    String ROW_ID = "ROW_ID";
    String NOTES = "NOTES";
    String JSON_DATA = "JSON_DATA";

    String SELECTION = "Selection";
    String MANY2ONE = "Many2One";
    String DATETIME = "Datetime";
    String DATE = "Date";
    String QUANTITY = "Quantity";
    String STRING = "String";
    String INTEGER = "Integer";
    String FLOAT = "Float";
    String NOTE = "Note";
    String UNIT_OF_MEAS = "Unit_Of_Measurement";
    String LOT_NUMBER = "Lot_Number";
    String HOME = "Home";
    String ONE2MANY = "One2Many";
    String BILL_OF_MATERIAL = "Bill_Of_Material";
    String MRP_LINES = "MRP_lines";

    String PRODUCT_ID = "product_id";
    String APPROVED_AMOUNT = "approved_amount";
    String PRODUCT_QTY = "product_qty";
    String PRODUCT_UOM = "product_uom";
    String PRODUCT_UOM_ID = "product_uom_id";
    String PICKING_COUNT = "picking_count";
    String PICKING_IDS = "picking_ids";
    String INVOICE_IDS = "invoice_ids";
    String INVOICE_COUNT = "invoice_count";
    String PARTNER_ID = "partner_id";
    String PARTNER_NAME = "partner_name";
    String LINKED_ORDER_ID = "linked_order_id";
    String STORAGE_ORDER_IDS = "storage_order_ids";
    String LINKED_LOAN_ID = "linked_loan_id";
    String PURCHASE_LINE_ID = "purchase_line_id";
    String AMOUNT_TOTAL = "amount_total";
    String PRICE_TOTAL = "price_total";
    String PRICE_UNIT = "price_unit";
    String NAME = "name";
    String ORIGIN = "origin";
    String STREET = "street";
    String NUMBER = "number";
    String PRIMARY_NUMBER = "primary_no";
    String SECONDARY_NUMBER = "secondary_no";
    String ZIP = "zip";
    String POINT_OF_CONTACT = "point_of_contact";
    String MANDI_ID = "mandi_id";
    String UNIT_RENT = "unit_rent";
    String ORDER_DATE = "order_date";
    String INVOICE_DATE = "date_invoice";
    String SCHEDULED_DATE = "scheduled_date";
    String NODE_ID = "node_id";
    String INC_PICKING_IDS = "incoming_picking_ids";
    String OUT_PICKING_IDS = "outgoing_picking_ids";
    String LOAN_IDS = "loan_ids";
    String TEAM_ID = "team_id";

    String SALESORDER = "SalesOrder";
    String SALES_ORDER = "sales_order";

    String SALESDISPATCH = "SalesDispatch";
    String SALES_DISPATCH = "sales_dispatch";

    String SALESPAYMENT = "SalesPayment";
    String SALES_PAYMENT = "sales_payment";

    String PURCHASEORDER = "PurchaseOrder";
    String PURCHASE_ORDER = "purchase_order";

    String PURCHASERECEIPT = "PurchaseReceipt";
    String PURCHASE_RECEIPT = "purchase_receipt";

    String PURCHASEPAYMENT = "PurchasePayment";
    String PURCHASE_PAYMENT = "purchase_payment";

    String MANUFACTURING_ORDER = "manufacturing_order";
    String MANUFACTURINGORDER = "ManufacturingOrder";

    String VENDOR_BILL = "vendor_bill";
    String VENDORBILL = "VendorBill";

    String EXPENSE_BILL = "expense_bill";
    String EXPENSEBILL = "ExpenseBill";

    String MANDI_PRICE = "mandi_price";
    String MANDIPRICE = "MandiPrice";

    String STORAGE_ORDER = "storage_order";
    String STORAGEORDER = "StorageOrder";

    String NODE_PRICE = "node_price";
    String NODEPRICE = "NodePrice";

    String PARTNER_CONTACT = "contact";
    String PARTNERCONTACT = "Contact";

    String COMMODITY_LOAN = "commodity_loan";
    String COMMODITYLOAN = "CommodityLoan";

    String COMMODITY_LOAN_INVOICE = "commodity_loan_invoice";
    String COMMODITYLOANINVOICE = "CommodityLoanInvoice";

    String STORAGE_INVOICE = "storage_invoice";
    String STORAGEINVOICE = "StorageInvoice";

    String STORAGE_RECEIPT = "storage_receipt";
    String STORAGERECEIPT = "StorageReceipt";

    String STORAGE_DISPATCH = "storage_dispatch";
    String STORAGEDISPATCH = "StorageDispatch";

    String PAYMENT_REQUEST = "payment_request";
    String PAYMENTREQUEST = "PaymentRequest";


    String ENTITY_STATUS = "entity_status";
    String PENDING = "Pending";
    String ERRORS = "Errors";
    String SUCCESS = "Success";
    String TRUE = "True";
    String FALSE = "False";

    String CONFIRM = "confirm";
    String CANCEL = "cancel";
    String FORCE_AVAIL = "force_availability";
    String RECHECK_AVAIL = "recheck_availability";
    String SETTODRAFT = "set_to_draft";
    String RESERVE = "reserve";
    String APPROVE = "approve";
    String REJECT = "reject";
    String NEW = "new";
    String SHOW_VALIDATE = "show_validate";

    String SERVER_NOT_RES = "Server Is Not Responding";
    String NO_INTERNET = "No Internet Connection";
    String TECH_PROB = "Technical Problem Occured. Please Try Again After Sometime";
    String NO_INTERNET_ACTION = "Internet Connection is required to perform this action.";
    String SERVER_ERROR = "Server Error";
    String INVOICE_LINE_IDS = "invoice_line_ids";
    String PRODUCT_PRICE_LINES = "product_price_lines";
    String REQUEST_LINE_IDS = "request_line_ids";

    String OR = "|";
    String AND = "&";
    String IN = "IN";
    String EQUALS = "=";
    String NOTEQUALS = "!=";
    String GREATER = ">";
    String LESS = "<";
    String LESSEQUAL = "<=";
    String GREATEREQUAL = ">=";
    String NOT_IN = "NOT_IN";

    String UNHANDLED_EXCEPTION = "unhandled_exception";
    String REQ_FIELD = "Required Field";
    String WAREHOUSE_ID = "warehouse_id";
    String FNF_DB = BuildConfig.TEMP_FNF_DB;
    String BACK_ORDER = "stock.backorder.confirmation";
    String OVERPROCESSED = "stock.overprocessed.transfer";

    String LINKED_STORAGE_ORDER_ID = "linked_storage_order_id";

    String PURCHASE_ORDER_SEARCH = "Name,Vendor";
    String PURCHASE_RECEIPT_SEARCH = "PO Name,Vendor,Name";
    String SALES_DISPATCH_SEARCH = "SO Name,Vendor,Name";
    String BILL_SEARCH = "Number,Vendor";
    String PURCHASE_PAYMENT_SEARCH = "Name,Vendor";
    String MANDI_PRICE_SEARCH = "Market";
    String NODE_PRICE_SEARCH = "Node";
    String PAYMENT_REQUEST_SEARCH = "Name,Partner";
    String STORAGE_ORDER_SEARCH = "Warehouse,Product"; // Product ID,Warehouse ID";
    String STORAGE_INVOICE_SEARCH = "Product";
    String COMMODITY_LOAN_SEARCH = "Partner";
    String STORAGE_RECEIPT_SEARCH = "Warehouse,Storage Order,Partner";
    String STORAGE_DISPATCH_SEARCH = "Warehouse,Storage Order,Partner";

    String SELECT = "Select";

    String LIST_GET_METHOD = "listGetMethod";
    String CREATE_METHOD = "createMethod";
    String UPDATE_METHOD = "updateMethod";
    String ACTION_METHOD = "actionMethod";
    String LINKED_ENTITIES = "linkedKeyNames";
    String PAGE_HEAD = "pageHead";

    Map<String, String> SALES_ORDER_MAPPING = new HashMap<String, String>() {{
        put("key", SALESORDER);
        put("search", PURCHASE_ORDER_SEARCH);
        put(LIST_GET_METHOD, "getSalesOrderList");
        put(CREATE_METHOD, "createSalesOrder");
        put(UPDATE_METHOD, "updateSalesOrder");
        put(ACTION_METHOD, "actionSalesOrder");
        put(PAGE_HEAD, "Sales Order");
    }};
    Map<String, String> SALES_DISPATCH_MAPPING = new HashMap<String, String>() {{
        put("key", SALESDISPATCH);
        put("search", SALES_DISPATCH_SEARCH);
        put(LIST_GET_METHOD, "getSalesDispatchList");
        put(CREATE_METHOD, "createSalesDispatch");
        put(UPDATE_METHOD, "updateSalesDispatch");
        put(ACTION_METHOD, "actionSalesDispatch");
        put(PAGE_HEAD, "Dispatch Voucher");
    }};
    Map<String, String> SALES_PAYMENT_MAPPING = new HashMap<String, String>() {{
        put("key", SALESPAYMENT);
        put("search", PURCHASE_PAYMENT_SEARCH);
        put(LIST_GET_METHOD, "getSalesPaymentList");
        put(ACTION_METHOD, "actionSalesPayment");
        put(PAGE_HEAD, "Sales Payment");
    }};
    Map<String, String> PURCHASE_ORDER_MAPPING = new HashMap<String, String>() {{
        put("key", PURCHASEORDER);
        put("search", PURCHASE_ORDER_SEARCH);
        put(LIST_GET_METHOD, "getPurchaseOrderList");
        put(CREATE_METHOD, "createPurchaseOrder");
        put(UPDATE_METHOD, "updatePurchaseOrder");
        put(ACTION_METHOD, "actionPurchaseOrder");
        put(PAGE_HEAD, "Purchase Order");
    }};
    Map<String, String> PURCHASE_RECEIPT_MAPPING = new HashMap<String, String>() {{
        put("key", PURCHASERECEIPT);
        put("search", PURCHASE_RECEIPT_SEARCH);
        put(LIST_GET_METHOD, "getPurchaseRecieptList");
        put(CREATE_METHOD, "createPurchaseReceipt");
        put(UPDATE_METHOD, "updatePurchaseReceipt");
        put(ACTION_METHOD, "actionPurchaseReceipt");
        put(PAGE_HEAD, "GRN");
    }};
    Map<String, String> PURCHASE_PAYMENT_MAPPING = new HashMap<String, String>() {{
        put("key", PURCHASEPAYMENT);
        put("search", PURCHASE_PAYMENT_SEARCH);
        put(LIST_GET_METHOD, "getPurchasePaymentList");
        put(CREATE_METHOD, "createPurchasePayment");
        put(UPDATE_METHOD, "updatePurchasePayment");
        put(ACTION_METHOD, "actionPurchasePayment");
        put(PAGE_HEAD, "Purchase Payment");
    }};
    Map<String, String> VENDOR_BILL_MAPPING = new HashMap<String, String>() {{
        put("key", VENDORBILL);
        put("search", BILL_SEARCH);
        put(LIST_GET_METHOD, "getVendorBillList");
        put(CREATE_METHOD, "createVendorBill");
        put(UPDATE_METHOD, "updateVendorBill");
        put(ACTION_METHOD, "actionVendorBill");
        put(PAGE_HEAD, "Vendor Bill");
    }};
    Map<String, String> EXPENSE_BILL_MAPPING = new HashMap<String, String>() {{
        put("key", EXPENSEBILL);
        put("search", BILL_SEARCH);
        put(LIST_GET_METHOD, "getExpenseBillList");
        put(CREATE_METHOD, "createExpenseBill");
        put(UPDATE_METHOD, "updateExpenseBill");
        put(ACTION_METHOD, "actionExpenseBill");
        put(PAGE_HEAD, "Expense Bill");
    }};
    Map<String, String> MANDI_PRICE_MAPPING = new HashMap<String, String>() {{
        put("key", MANDIPRICE);
        put("search", MANDI_PRICE_SEARCH);
        put(LIST_GET_METHOD, "getMandiPriceList");
        put(CREATE_METHOD, "createMandiPrice");
        put(UPDATE_METHOD, "updateMandiPrice");
        put(PAGE_HEAD, "Mandi Price");
    }};
    Map<String, String> NODE_PRICE_MAPPING = new HashMap<String, String>() {{
        put("key", NODEPRICE);
        put("search", NODE_PRICE_SEARCH);
        put(LIST_GET_METHOD, "getNodePriceList");
        put(CREATE_METHOD, "createNodePrice");
        put(UPDATE_METHOD, "updateNodePrice");
        put(PAGE_HEAD, "Node Price");
    }};
    Map<String, String> PAYMENT_REQUEST_MAPPING = new HashMap<String, String>() {{
        put("key", PAYMENTREQUEST);
        put("search", PAYMENT_REQUEST_SEARCH);
        put(LIST_GET_METHOD, "getPaymentRequestList");
        put(CREATE_METHOD, "createPaymentRequest");
        put(UPDATE_METHOD, "updatePaymentRequest");
        put(ACTION_METHOD, "actionPaymentRequest");
        put(PAGE_HEAD, "Payment Request");
    }};
    Map<String, String> STORAGE_ORDER_MAPPING = new HashMap<String, String>() {{
        put("key", STORAGEORDER);
        put("search", STORAGE_ORDER_SEARCH);
        put(LIST_GET_METHOD, "getStorageOrders");
        put(CREATE_METHOD, "createStorageOrder");
        put(UPDATE_METHOD, "updateStorageOrder");
        put(ACTION_METHOD, "actionStorageOrder");
        put(LINKED_ENTITIES,
                TextUtils.join(",", new String[]{STORAGE_INVOICE, STORAGE_RECEIPT}));
        put(PAGE_HEAD, "Storage Record");
    }};
    Map<String, String> CONTACT_MAPPING = new HashMap<String, String>() {{
        put("key", PARTNERCONTACT);
        put("search", "");
        put(LIST_GET_METHOD, "getContacts");
        put(CREATE_METHOD, "createPartnerContact");
        put(UPDATE_METHOD, "updatePartnerContact");
        put(PAGE_HEAD, "Partner Contact");
    }};
    Map<String, String> COMMODITY_LOAN_MAPPING = new HashMap<String, String>() {{
        put("key", COMMODITYLOAN);
        put("search", COMMODITY_LOAN_SEARCH);
        put(LIST_GET_METHOD, "getCommodityLoanList");
        put(CREATE_METHOD, "createCommodityLoan");
        put(UPDATE_METHOD, "updateCommodityLoan");
        put(PAGE_HEAD, "Commodity Loan");
    }};
    Map<String, String> LOAN_INVOICE_MAPPING = new HashMap<String, String>() {{
        put("key", COMMODITYLOANINVOICE);
        put("search", "");
        put(LIST_GET_METHOD, "getCommodityLoanInvoiceList");
        put(CREATE_METHOD, "createCommodityLoanInvoice");
        put(UPDATE_METHOD, "updateCommodityLoanInvoice");
        put(PAGE_HEAD, "Commmodity Loan Invoice");
    }};
    Map<String, String> STORAGE_INVOICE_MAPPING = new HashMap<String, String>() {{
        put("key", STORAGEINVOICE);
        put("search", STORAGE_INVOICE_SEARCH);
        put(LIST_GET_METHOD, "getStorageInvoiceList");
        put(CREATE_METHOD, "createStorageInvoice");
        put(UPDATE_METHOD, "updateStorageInvoice");
        put(PAGE_HEAD, "Storage Bill");
    }};
    Map<String, String> STORAGE_RECEIPT_MAPPING = new HashMap<String, String>() {{
        put("key", STORAGERECEIPT);
        put("search", STORAGE_RECEIPT_SEARCH);
        put(LIST_GET_METHOD, "getStorageReceiptList");
        put(CREATE_METHOD, "createStorageReceipt");
        put(UPDATE_METHOD, "updateStorageReceipt");
        put(PAGE_HEAD, "Storage GRN");
    }};
    Map<String, String> STORAGE_DISPATCH_MAPPING = new HashMap<String, String>() {{
        put("key", STORAGEDISPATCH);
        put("search", STORAGE_DISPATCH_SEARCH);
        put(LIST_GET_METHOD, "getStorageDispatchList");
        put(CREATE_METHOD, "createStorageDispatch");
        put(UPDATE_METHOD, "updateStorageDispatch");
        put(PAGE_HEAD, "Storage Dispatch Voucher");
    }};

    Map<String, Map<String, String>> key_mapping = new HashMap<String, Map<String, String>>() {{
        put(SALES_ORDER, SALES_ORDER_MAPPING);
        put(SALES_DISPATCH, SALES_DISPATCH_MAPPING);
        put(SALES_PAYMENT, SALES_PAYMENT_MAPPING);
        put(PURCHASE_ORDER, PURCHASE_ORDER_MAPPING);
        put(PURCHASE_RECEIPT, PURCHASE_RECEIPT_MAPPING);
        put(PURCHASE_PAYMENT, PURCHASE_PAYMENT_MAPPING);
        put(VENDOR_BILL, VENDOR_BILL_MAPPING);
        put(EXPENSE_BILL, EXPENSE_BILL_MAPPING);
        put(MANDI_PRICE, MANDI_PRICE_MAPPING);
        put(NODE_PRICE, NODE_PRICE_MAPPING);
        put(PAYMENT_REQUEST, PAYMENT_REQUEST_MAPPING);
//        put(STORAGE_ORDER, STORAGE_ORDER_MAPPING);
//        put(PARTNER_CONTACT, CONTACT_MAPPING);
//        put(COMMODITY_LOAN, COMMODITY_LOAN_MAPPING);
//        put(COMMODITY_LOAN_INVOICE, LOAN_INVOICE_MAPPING);
//        put(STORAGE_INVOICE, STORAGE_INVOICE_MAPPING);
//        put(STORAGE_RECEIPT, STORAGE_RECEIPT_MAPPING);
//        put(STORAGE_DISPATCH, STORAGE_DISPATCH_MAPPING);
    }};

    Map<String, String> pricesMapping = new HashMap<String, String>() {{
        put("Min Price", "min_price");
        put("Max Price", "max_price");
        put("Best Price", "best_price");
    }};

    Map<String, String> nodePricesMapping = new HashMap<String, String>() {{
        put("DeHaat Price", "dehaat_price");
    }};
}
package com.dehaat.node.utilities;

import java.util.HashSet;

public interface RequiredField {
    void requiredField(HashSet<String> hashSet);
}

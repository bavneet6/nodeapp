package com.dehaat.node.utilities;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.Nullable;

import com.dehaat.node.database.DatabaseHandler;
import com.dehaat.node.rest.AppRestClient;
import com.dehaat.node.rest.body.EntityCreateDataBody;
import com.dehaat.node.rest.response.EntityDataJson;
import com.dehaat.node.rest.response.JsonDataStore;
import com.dehaat.node.rest.response.ResponseCreateEntity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainService extends Service {
    private static final String TAG = "FORM_UPDATE";
    private DatabaseHandler databaseHandler;
    private Handler handler;
    private Runnable runnable;
    private ArrayList<EntityDataJson> entityDataJson;
    private int i = 0, millisDelayTime = 600000, toChangeData;
    private String keyName = null, schema_version = null, entityType = null;
    private ArrayList<JsonDataStore> dataList;
    private boolean isRunning = false, toConfirm = false;
    private Long rowId, entity_id;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //called only once just for initialize
    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "onCreate");
        databaseHandler = new DatabaseHandler(this);
    }

    @Override
    public int onStartCommand(final Intent intent, final int flags, final int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        if (intent != null) {
            keyName = intent.getStringExtra("DATA");
            millisDelayTime = intent.getIntExtra("TIME", 600000);
        }
        handler = new Handler();
        runnable = new Runnable() {
            public void run() {
                handler.postDelayed(runnable, millisDelayTime);
                Log.e(TAG, "Runmethod");

                if (!isRunning) {
                    i = 0;
                    if (AppUtils.haveNetworkConnection(getApplication()))
                        if (AppUtils.isNullCase(keyName))
                            entityDataJson = databaseHandler.getEntityPendingList(null);
                        else
                            entityDataJson = databaseHandler.getEntityPendingList(keyName);

                    if ((AppUtils.haveNetworkConnection(getApplication())) && (entityDataJson.size() != 0))
                        prepareData(i);
                }
            }
        };

        handler.postDelayed(runnable, 10000);
        return START_STICKY;
    }

    private void sendMessageToActivity() {
        this.sendBroadcast(new Intent().setAction("DATA_SYNC"));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        // this is used if we remove the app from the recent tabs and then also we want our service should work
        noTaskRemoveMethod();
    }

    private void noTaskRemoveMethod() {
        Intent restartServiceTask = new Intent(getApplicationContext(), this.getClass());
        restartServiceTask.setPackage(getPackageName());
        PendingIntent restartPendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceTask, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager myAlarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        myAlarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartPendingIntent);
    }

    /**
     * preparing data to send through retrofit
     */
    private void prepareData(int i) {
        if (i < entityDataJson.size()) {
            isRunning = true;
            //update the lock column
            databaseHandler.updateLockData(entityDataJson.get(i).getRow_id());
            // get the json payload
            Gson gson = new Gson();
            dataList = gson.fromJson(entityDataJson.get(i).getJson_payload(), new TypeToken<ArrayList<JsonDataStore>>() {
            }.getType());

            if ((dataList != null) && (dataList.size() != 0)) {
                entityType = entityDataJson.get(i).getEntity_type();
                schema_version = entityDataJson.get(i).getPayload_version();
                entity_id = entityDataJson.get(i).getEntity_id();
                rowId = Long.valueOf(entityDataJson.get(i).getRow_id());
                toChangeData = entityDataJson.get(i).getAction_required();
                toConfirm = toChangeData == 1;
                //retrofit call to send the data
                sendData();
            }
        }
    }

    private void sendData() {
        AppRestClient client = AppRestClient.getInstance();
        EntityCreateDataBody.CreateBody createBody = new EntityCreateDataBody.CreateBody();
        createBody.setFields(dataList);
        EntityCreateDataBody.MetaData metaData = new EntityCreateDataBody.MetaData();
        metaData.setClient_id(rowId);
        if (entity_id == 0L)
            metaData.setEntity_id(0L);
        else
            metaData.setEntity_id(entity_id);
        metaData.setVersion(schema_version);

        EntityCreateDataBody entityCreateDataBody = new EntityCreateDataBody(createBody, metaData);

        Call<ResponseCreateEntity> call = null;
        if (!AppUtils.isNullCase(entityType))
            if (entityType.equals(UrlConstant.SALES_ORDER))
                if (entity_id == 0L)
                    call = client.createSalesOrder(toConfirm, entityCreateDataBody);
                else
                    call = client.updateSalesOrder(toConfirm, entityCreateDataBody);
            else if (entityType.equals(UrlConstant.SALES_DISPATCH))
                if (entity_id == 0L)
                    call = client.createSalesDispatch(toConfirm, entityCreateDataBody);
                else
                    call = client.updateSalesDispatch(toConfirm, entityCreateDataBody);
            else if (entityType.equals(UrlConstant.SALES_PAYMENT))
                if (entity_id == 0L)
                    call = client.createSalesPayment(toConfirm, entityCreateDataBody);
                else
                    call = client.updateSalesPayment(toConfirm, entityCreateDataBody);
            else if (entityType.equals(UrlConstant.PURCHASE_ORDER))
                if (entity_id == 0L)
                    call = client.createPurchaseOrder(toConfirm, entityCreateDataBody);
                else
                    call = client.updatePurchaseOrder(toConfirm, entityCreateDataBody);
            else if (entityType.equals(UrlConstant.PURCHASE_RECEIPT))
                if (entity_id == 0L)
                    call = client.createPurchaseReceipt(toConfirm, entityCreateDataBody);
                else
                    call = client.updatePurchaseReceipt(toConfirm, entityCreateDataBody);
            else if (entityType.equals(UrlConstant.PURCHASE_PAYMENT))
                if (entity_id == 0L)
                    call = client.createPurchasePayment(toConfirm, entityCreateDataBody);
                else
                    call = client.updatePurchasePayment(toConfirm, entityCreateDataBody);
            else if (entityType.equals(UrlConstant.MANUFACTURING_ORDER))
                if (entity_id == 0L)
                    call = client.createManufacturingOrder(toConfirm, entityCreateDataBody);
                else
                    call = client.updateManufacturingOrder(toConfirm, entityCreateDataBody);
            else if (entityType.equals(UrlConstant.VENDOR_BILL))
                if (entity_id == 0L)
                    call = client.createVendorBill(toConfirm, entityCreateDataBody);
                else
                    call = client.updateVendorBill(toConfirm, entityCreateDataBody);
            else if (entityType.equals(UrlConstant.EXPENSE_BILL))
                if (entity_id == 0L)
                    call = client.createExpenseBill(toConfirm, entityCreateDataBody);
                else
                    call = client.updateExpenseBill(toConfirm, entityCreateDataBody);

        call.enqueue(new Callback<ResponseCreateEntity>() {
            @Override
            public void onResponse(Call<ResponseCreateEntity> call, Response<ResponseCreateEntity> response) {
                if (response == null)
                    return;
                Gson gson = new Gson();
                String jsonString = gson.toJson(dataList);
                EntityDataJson entityDataJsonTemp = new EntityDataJson();
                entityDataJsonTemp.setJson_payload(jsonString);
                if (response.code() == 200) {
                    entityDataJsonTemp.setEntity_id(response.body().getMetadata().getEntity_id());

                    if (!response.body().isDialog_occured()) {
                        entityDataJsonTemp.setSync_status(UrlConstant.SUCCESS);
                        entityDataJsonTemp.setValidation_status("");
                    } else {
                        entityDataJsonTemp.setSync_status(UrlConstant.ERRORS);
                        entityDataJsonTemp.setValidation_status("");
                        entityDataJsonTemp.setDialog_id(response.body().getDialog_id());
                    }

                } else if (response.code() == 500) {
                    entityDataJsonTemp.setSync_status(UrlConstant.ERRORS);
                    entityDataJsonTemp.setEntity_id(entityDataJson.get(i).getEntity_id());
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        if (jObjError.get("error_type").equals(UrlConstant.UNHANDLED_EXCEPTION)) {
                            entityDataJsonTemp.setValidation_status("Technical Problem");
                        } else
                            entityDataJsonTemp.setValidation_status("" + jObjError.get("error_message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                databaseHandler.updateServiceEntityData(entityDataJsonTemp, entityDataJson.get(i).getRow_id());

                if (i == entityDataJson.size() - 1) {
                    isRunning = false;
                    sendMessageToActivity();
                } else
                    prepareData(++i);
            }

            @Override
            public void onFailure(Call<ResponseCreateEntity> call, Throwable t) {
                EntityDataJson entityDataJsonTemp = new EntityDataJson();
                entityDataJsonTemp.setSync_status(UrlConstant.PENDING);
                entityDataJsonTemp.setValidation_status("");
                entityDataJsonTemp.setEntity_id(entityDataJson.get(i).getEntity_id());
                databaseHandler.updateServiceEntityData(entityDataJsonTemp, entityDataJson.get(i).getRow_id());

                if (i == entityDataJson.size() - 1) {
                    isRunning = false;
                    sendMessageToActivity();
                } else
                    prepareData(++i);
            }
        });
    }
}

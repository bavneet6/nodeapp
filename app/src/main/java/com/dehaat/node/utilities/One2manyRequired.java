package com.dehaat.node.utilities;

import java.util.HashSet;

public interface One2manyRequired {
    void one2ManyRequiredField(HashSet<String> hashSet);
}

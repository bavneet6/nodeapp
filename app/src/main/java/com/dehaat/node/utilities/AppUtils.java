package com.dehaat.node.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.amazonaws.HttpMethod;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.dehaat.node.Node;
import com.dehaat.node.R;
import com.dehaat.node.activity.LoginActivity;
import com.dehaat.node.database.DatabaseHandler;
import com.dehaat.node.rest.ApiCallBack;
import com.dehaat.node.rest.AppRestClient;
import com.dehaat.node.rest.response.EntityView;
import com.dehaat.node.rest.response.GetEntityList;
import com.dehaat.node.rest.response.GetIdName;
import com.dehaat.node.rest.response.GetViewFields;
import com.dehaat.node.rest.response.JsonDataStore;
import com.dehaat.node.rest.response.LotNumber;
import com.dehaat.node.rest.response.MultipleLines;
import com.dehaat.node.rest.response.StoreValues;
import com.dehaat.node.rest.response.VersionName;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;
import com.google.gson.Gson;

import org.json.JSONException;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import retrofit2.Call;
import retrofit2.Response;

public class AppUtils {
    private static ProgressDialog progressDialog;
    private static DatabaseHandler databaseHandler;

    public static void showToast(String msg) {
        Context context = Node.getInstance();
        if (context != null && msg != null)
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void changeFragment(FragmentActivity activity, Fragment fragment) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frag_container, fragment).addToBackStack("").commit();
    }

    public static void changeNoStackFragment(FragmentActivity activity, Fragment fragment) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frag_container, fragment).commit();
    }

    public static void showProgressDialog(Context context) {
        if (context == null)
            return;

        if (progressDialog != null)
            return;
        progressDialog = new ProgressDialog(context);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void checkNetwork(final Activity activity, final TextView networkBar,
                                    final Boolean showText) {
        ConnectionLiveData connectionLiveData = new ConnectionLiveData(activity);
        connectionLiveData.observe((LifecycleOwner) activity, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean connection) {
                if (connection) {
                    networkBar.setBackgroundColor(Color.parseColor("#489a29"));
                    if (showText)
                        networkBar.setText("ONLINE");
                    else
                        networkBar.setText("");
                } else {
                    networkBar.setBackgroundColor(Color.parseColor("#d05656"));
                    if (showText)
                        networkBar.setText("OFFLINE");
                    else
                        networkBar.setText("");
                }
            }
        });
    }

    public static void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        progressDialog = null;
    }

    public static boolean haveNetworkConnection(Context activity) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        if (activity == null) {
            activity = Node.getInstance().getApplicationContext();
        }
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {

            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public static void imageSelection(androidx.fragment.app.Fragment fragment, int count) {
        Options options = Options.init()
                .setRequestCode(100)
                .setCount(count)
                .setFrontfacing(false)
                .setImageQuality(ImageQuality.HIGH)
                .setScreenOrientation(Options.SCREEN_ORIENTATION_FULL_SENSOR)
                .setImageResolution(1024, 800)
                .setPath("/OPS/new");
        Pix.start(fragment, options);
    }


    public static HashMap<String, String> convertToHashMap(Context context,
                                                           ArrayList<JsonDataStore> arrayList) {
        HashMap<String, String> tempMap = new HashMap<>();
        for (int i = 0; i < arrayList.size(); i++) {
            tempMap.put(arrayList.get(i).getKey(), arrayList.get(i).getValue());
        }

        return tempMap;
    }

    private static String getValue(String key, HashMap<String, String> dataList) {
        for (Map.Entry<String, String> entry : dataList.entrySet()) {
            if (entry.getKey().equals(key))
                return entry.getValue();
        }
        return null;
    }

    public static String generateDisplayData(Context context, String key, String
            keyName, HashMap<String, String> dataList) {

        databaseHandler = new DatabaseHandler(context);
        EntityView.GetView getView = databaseHandler.getSchemaSingleView(keyName);
        if (getView == null)
            return null;
        if (getView.getSchema() == null)
            return null;
        ArrayList<GetViewFields> getViewFields = getView.getSchema().getFields();
        for (int i = 0; i < getViewFields.size(); i++) {
            if (getViewFields.get(i).getType().equals(UrlConstant.MANY2ONE)) {
                if (!getViewFields.get(i).getKey().equals(key))
                    continue;
                if (getViewFields.get(i).getSelection_values() == null)
                    continue;
                String value = getValue(key, dataList);
                if (isNullCase(value))
                    return null;
                for (int j = 0; j < getViewFields.get(i).getSelection_values().size(); j++) {
                    if (getViewFields.get(i).getSelection_values().get(j).getId() ==
                            (Integer.parseInt(value)))
                        return getViewFields.get(i).getSelection_values().get(j).getName();
                }

            } else if (getViewFields.get(i).getType().equals(UrlConstant.ONE2MANY)) {
                if (!getViewFields.get(i).getKey().equals(key))
                    continue;
                ArrayList<GetViewFields> list = getViewFields.get(i).getSub_fields();
                Gson gson = new Gson();
                List<List<MultipleLines.GetKeyValue>> getKeyValueList = new ArrayList<>();

                if (keyName.equals(UrlConstant.VENDOR_BILL))
                    getKeyValueList = gson.fromJson(dataList.get(UrlConstant.INVOICE_LINE_IDS),
                            MultipleLines.class).getInvoice_line_ids();
                else if (keyName.equals(UrlConstant.PAYMENT_REQUEST)) {
                    if (dataList.get(UrlConstant.REQUEST_LINE_IDS) != null)
                        getKeyValueList = gson.fromJson(dataList.get(UrlConstant.REQUEST_LINE_IDS),
                                MultipleLines.class).getRequest_line_ids();
                } else
                    getKeyValueList = gson.fromJson(dataList.get(UrlConstant.PRODUCT_PRICE_LINES),
                            MultipleLines.class).getProduct_price();

                String result = "", unit = "";
                Long product = 0L;
                if (getKeyValueList != null)
                    for (int k = 0; k < getKeyValueList.size(); k++) {
                        for (int l = 0; l < getKeyValueList.get(k).size(); l++) {
                            for (int j = 0; j < list.size(); j++) {
                                if (list.get(j).getType().equals(UrlConstant.MANY2ONE)) {
                                    if (list.get(j).getKey().equals(getKeyValueList.get(k).get(l).getKey())) {
                                        ArrayList<GetIdName> getIdNames = list.get(j).getSelection_values();
                                        for (int m = 0; m < getIdNames.size(); m++) {
                                            if (getKeyValueList.get(k).get(l).getValue().equals("" +
                                                    getIdNames.get(m).getId())) {
                                                product = getIdNames.get(m).getId();
                                                result = result + "Product: " + getIdNames.get(m).getName() + " , ";
                                                if (!isNullCase(unit))
                                                    result = result + "Unit: " + unit + " , ";
                                            }
                                        }
                                    }
                                } else if (list.get(j).getType().equals(UrlConstant.FLOAT)) {
                                    if (list.get(j).getKey().equals(getKeyValueList.get(k).get(l).getKey())) {
                                        if (list.get(j).getKey().equals(UrlConstant.PRODUCT_QTY))
                                            result = result + "Quantity: " + getKeyValueList.get(k).get(l).getValue() + " , ";
                                        else if (list.get(j).getKey().equals(UrlConstant.PRICE_UNIT))
                                            result = result + "Price:₹ " + getKeyValueList.get(k).get(l).getValue() + " , ";
                                        else if (list.get(j).getKey().equals("max_price"))
                                            result = result + "Price:₹ " + getKeyValueList.get(k).get(l).getValue() + " , ";
                                        else if (list.get(j).getKey().equals("min_price"))
                                            result = result + "Price:₹ " + getKeyValueList.get(k).get(l).getValue() + " , ";
                                        else if (list.get(j).getKey().equals("best_price"))
                                            result = result + "Price:₹ " + getKeyValueList.get(k).get(l).getValue() + " , ";
                                        else if (list.get(j).getKey().equals("dehaat_price"))
                                            result = result + "Price:₹ " + getKeyValueList.get(k).get(l).getValue() + " , ";
                                        else if (list.get(j).getKey().equals(UrlConstant.PRICE_TOTAL))
                                            result = result + "Sub Total:₹ " + getKeyValueList.get(k).get(l).getValue() + " , ";
                                    }
                                } else if (list.get(j).getType().equals(UrlConstant.UNIT_OF_MEAS)) {
                                    if (list.get(j).getKey().equals(getKeyValueList.get(k).get(l).getKey())) {
                                        ArrayList<GetViewFields.MappingValues> getIdNames = list.get(j).getMapping_values();
                                        for (int m = 0; m < getIdNames.size(); m++) {
                                            if (getKeyValueList.get(k).get(l).getValue().
                                                    equals("" + getIdNames.get(m).getUom_id().get(0)))
                                                unit = getIdNames.get(m).getUom_id().get(1);
                                                if (product == getIdNames.get(m).getId())
                                                    result = result + "Unit: " + getIdNames.get(m).getUom_id().get(1) + " , ";


                                        }
                                    }
                                }
                            }
                        }
                    }
                return result;

            } else if (getViewFields.get(i).getType().equals(UrlConstant.QUANTITY)) {
                if (!getViewFields.get(i).getKey().equals(key))
                    continue;
                ArrayList<GetViewFields> list = getViewFields.get(i).getSub_fields();
                String uom = "", price = "";

                for (int j = 0; j < list.size(); j++) {
                    //check for at what field Position is selection and edittext
                    if (list.get(j).getType().equals(UrlConstant.UNIT_OF_MEAS)) {
                        String value = getValue(list.get(j).getKey(), dataList);
                        if (!AppUtils.isNullCase(value))
                            for (int k = 0; k < list.get(j).getMapping_values().size(); k++) {
                                if (list.get(j).getMapping_values().get(k).getUom_id().get(0).equals(value))
                                    uom = list.get(j).getMapping_values().get(k).getUom_id().get(1);
                            }

                    } else if (list.get(j).getType().equals(UrlConstant.FLOAT)) {
                        String value = getValue(list.get(j).getKey(), dataList);
                        price = value;
                    }
                }
                return price + " " + uom;

            } else if (getViewFields.get(i).getType().equals(UrlConstant.MRP_LINES)) {
                if (!getViewFields.get(i).getKey().equals(key))
                    continue;
            } else {
                if (!getViewFields.get(i).getKey().equals(key))
                    continue;
                String value = getValue(key, dataList);
                if (isNullCase(value))
                    return null;
                else
                    return value;
            }
        }
        return null;
    }

    public static String getDisplayNameStatus(Context context, String status, String keyName) {
        databaseHandler = new DatabaseHandler(context);
        EntityView.GetView getView = databaseHandler.getSchemaSingleView(keyName);
        if (getView == null)
            return null;
        if (getView.getSchema() == null)
            return null;
        if (getView.getSchema().getStatuses() == null)
            return null;
        ArrayList<EntityView.GetView.GetSchema.GetKeyValue> getKeyValueArrayList =
                getView.getSchema().getStatuses();
        for (int i = 0; i < getKeyValueArrayList.size(); i++) {
            if (getKeyValueArrayList.get(i).getKey().equals(status)) {
                return getKeyValueArrayList.get(i).getValue();
            }
        }

        return null;
    }

    public static void showSessionExpiredDialog(Activity context) {
        databaseHandler = new DatabaseHandler(context);
        AppPreference.getInstance().clearData();
        Intent i = new Intent(context, LoginActivity.class);
        databaseHandler.clearEntityDataTable();
        databaseHandler.clearFormSchemaTable();
        databaseHandler.clearLotNumbersTable();
        databaseHandler.clearSearchTable();
        databaseHandler.clearStoreValueTable();
        databaseHandler.clearURlsData();
        Node.getInstance().clearApplicationData();
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
        context.finish();
    }

    public static boolean isNullCase(String test) {
        return (test == null) || (test.equals("")) || (test.equals("false"));
    }

    public static boolean isNullWith0Case(String test) {
        return (test == null) || (test.equals("")) || (test.equals("false")) ||
                (test.equals("0")) || (test.equals("0.0"));
    }

    public static String getTimeStamp() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String tmstp = dateFormat.format(new Date());
        tmstp = tmstp.replace("-", "");
        tmstp = tmstp.replace(":", "");
        tmstp = tmstp.replace(" ", "_");
        return tmstp;
    }

    public static void showErrorDialog(Context context, String error) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("" + error);
        builder.setCancelable(true);
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void storeLotNumbers(Context context,
                                       ArrayList<GetViewFields> getViewFieldsArrayList) {
        if (context == null)
            return;
        databaseHandler = new DatabaseHandler(context);
        for (int i = 0; i < getViewFieldsArrayList.size(); i++) {
            if (getViewFieldsArrayList.get(i).getLot_numbers() != null) {
                ArrayList<GetViewFields.LotNumbers> lotNumberArrayList =
                        getViewFieldsArrayList.get(i).getLot_numbers();
                for (int j = 0; j < lotNumberArrayList.size(); j++) {
                    LotNumber lotNumber = new LotNumber();
                    lotNumber.setLot_number_id(lotNumberArrayList.get(j).getLot_id());
                    lotNumber.setProduct_id(lotNumberArrayList.get(j).getProduct_id());
                    lotNumber.setTemp_lot_number(lotNumberArrayList.get(j).getRef());
                    lotNumber.setPerm_lot_number(lotNumberArrayList.get(j).getName());

                    databaseHandler.insertOrUpdateLotNumbers(lotNumber);
                }

            }
        }
    }


    public static void showBanner(Activity activity, TextView networkBar) {
        if (activity == null)
            return;
        if (AppUtils.haveNetworkConnection(activity)) {
            networkBar.setBackgroundColor(Color.parseColor("#489a29"));
            networkBar.setText("ONLINE");
        } else {
            networkBar.setBackgroundColor(Color.parseColor("#d05656"));
            networkBar.setText("OFFLINE");
        }
    }

    public static void storeDataDB(Activity activity, EntityView.GetView data, boolean store) {
        if (activity == null)
            return;
        databaseHandler = new DatabaseHandler(activity);
        EntityView.GetView fieldsData = new EntityView.GetView();
        fieldsData.setKey(data.getKey());
        fieldsData.setVersion(data.getVersion());
        fieldsData.setSchema(data.getSchema());
        fieldsData.setDisplay_name(data.getDisplay_name());
        databaseHandler.insertSchemaData(fieldsData);

        if (data.getKey().equals(UrlConstant.PURCHASE_RECEIPT) ||
                data.getKey().equals(UrlConstant.SALES_DISPATCH)) {
            ArrayList<GetViewFields> getViewFieldsArrayList = data.getSchema().getFields();
            AppUtils.storeLotNumbers(activity, getViewFieldsArrayList);
        }
        if (store) {
            ArrayList<GetViewFields> getViewFields = data.getSchema().getFields();
            for (GetViewFields getViewFields1 : getViewFields) {
                if (getViewFields1.getType().equals(UrlConstant.MANY2ONE)) {
                    for (GetIdName getIdName : getViewFields1.getSelection_values()) {
                        StoreValues storeValues = new StoreValues();
                        storeValues.setEntityType(data.getKey());
                        storeValues.setKeyName(getViewFields1.getKey());
                        storeValues.setDataId("" + getIdName.getId());
                        storeValues.setDataValue(getIdName.getName());
                        databaseHandler.insertStoreValues(storeValues);
                    }
                }
            }

        }
    }

    public static boolean getReadEditState(String readonly, HashMap<String, String> postDataMap,
                                           String entity_status) {
        if (readonly.equals("true")) {
            return true;
        } else {
            readonly = readonly.substring(1, readonly.length() - 1);
            String tempData = readonly.replace(" ", "");
            readonly = tempData.trim();

            for (int i = 0; i < readonly.length(); i++) {

                String temp = String.valueOf(readonly.charAt(i));

                if (temp.equals("(")) {
                    return solveExpression(readonly, postDataMap, entity_status);

                } else if ((temp.equals(UrlConstant.OR)) || (temp.equals(UrlConstant.AND))) {
                    Boolean exp1 = null, exp2 = null;
                    Stack stack = new Stack();
                    List<String> expressionList = new ArrayList<String>(Arrays.asList(readonly.split(";")));
                    for (int j = 0; j < expressionList.size(); j++) {
                        String data = removeFirstAndLast(expressionList.get(j));
                        if (data.equals(UrlConstant.OR) || (data.equals(UrlConstant.AND)))
                            stack.push(data);
                        else {
                            String temp1 = String.valueOf(expressionList.get(j).charAt(0));
                            if (temp1.equals("(")) {
                                if (solveExpression(expressionList.get(j), postDataMap, entity_status)) {
                                    if (exp1 != null)
                                        exp2 = true;
                                    else
                                        exp1 = true;
                                } else {
                                    if (exp1 != null)
                                        exp2 = false;
                                    else
                                        exp1 = false;
                                }
                            }
                            if ((exp1 != null) && (exp2 != null)) {
                                String popData = String.valueOf(stack.pop());
                                if (popData.equals(UrlConstant.OR))
                                    exp1 = exp1 || exp2;
                                else if (popData.equals(UrlConstant.AND))
                                    exp1 = exp1 && exp2;

                                exp2 = null;
                            }

                        }

                    }
                    return exp1;

                }
            }
        }

        return false;

    }

    public static void getVersionName(final Activity activity) {
        final String appPackageName;
        String versionName = null;
        appPackageName = activity.getPackageName();
        try {
            versionName = activity.getPackageManager()
                    .getPackageInfo(activity.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        AppRestClient client = AppRestClient.getInstance();
        Call<VersionName> call = client.getVersionName();
        final String finalVersionName = versionName;
        call.enqueue(new ApiCallBack<VersionName>() {
            @Override
            public void onResponse(Response<VersionName> response) {
                if (response.body() == null)
                    return;
                String version = response.body().getVersion();
                if (!version.equals(finalVersionName)) {
                    openUpdateDialog(activity, appPackageName);
                }
            }

            @Override
            public void onResponse401(Response<VersionName> response) throws JSONException {

            }
        });
    }

    private static void openUpdateDialog(final Activity activity, final String appPackageName) {
        final Dialog dialog = new Dialog(activity);
        TextView okay;
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_update);
        okay = dialog.findViewById(R.id.okay);
        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                dialog.dismiss();
            }
        });
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }

    private static boolean solveExpression(String readonly, HashMap<String, String> postDataMap,
                                           String entity_status) {
        String fieldValue = null, schemaValue = null, schemaFields, schemaOperator;
        List<String> expressionList = new ArrayList<String>(Arrays.asList(readonly.substring(1,
                readonly.length() - 1).split(",", 3)));

        schemaFields = removeFirstAndLast(expressionList.get(0));
        schemaOperator = removeFirstAndLast(expressionList.get(1));

        if (schemaFields.equals("state"))
            fieldValue = entity_status;
        else
            fieldValue = checkKey(schemaFields, postDataMap);

        if (fieldValue == null)
            return false;

        if ((schemaOperator.equals(UrlConstant.IN)) ||
                (schemaOperator.equals(UrlConstant.NOT_IN))) {
            schemaValue = removeFirstAndLast(expressionList.get(2));
            List<String> list = new ArrayList<String>(Arrays.asList(schemaValue.substring(1,
                    schemaValue.length() - 1).split(",")));

            if (schemaOperator.equals(UrlConstant.IN)) {
                for (int j = 0; j < list.size(); j++) {
                    String dataTemp = removeFirstAndLast(list.get(j));
                    if (dataTemp.equals(fieldValue))
                        return true;
                }
                return false;
            } else if (schemaOperator.equals(UrlConstant.NOT_IN)) {
                for (int j = 0; j < list.size(); j++) {
                    String dataTemp = removeFirstAndLast(list.get(j));
                    if (dataTemp.equals(fieldValue))
                        return false;
                }
                return true;
            }


        } else if ((schemaOperator.equals(UrlConstant.EQUALS)) ||
                (schemaOperator.equals(UrlConstant.NOTEQUALS))) {
            schemaValue = expressionList.get(2);
            schemaValue = schemaValue.replaceAll("^\"|\"$", "");
            if (schemaOperator.equals(UrlConstant.EQUALS)) {

                return fieldValue.equals(schemaValue);

            } else if (schemaOperator.equals(UrlConstant.NOTEQUALS)) {

                return !fieldValue.equals(schemaValue);
            }

        } else {
            float schemaValues;
            schemaValue = expressionList.get(2);
            schemaValues = Float.parseFloat((schemaValue));
            float tempFieldValue = Float.parseFloat(((fieldValue)));

            if (schemaOperator.equals(UrlConstant.GREATER)) {
                return tempFieldValue > schemaValues;
            } else if (schemaOperator.equals(UrlConstant.LESS)) {
                return tempFieldValue < schemaValues;
            } else if (schemaOperator.equals(UrlConstant.GREATEREQUAL)) {
                return tempFieldValue >= schemaValues;
            } else if (schemaOperator.equals(UrlConstant.LESSEQUAL)) {
                return tempFieldValue <= schemaValues;
            }
        }
        return false;
    }

    private static String removeFirstAndLast(String tempData) {
        tempData = tempData.replaceAll("^\"|\"$", "");
        return tempData;
    }

    private static String checkKey(String keyText, HashMap<String, String> postDataMap) {
        if (postDataMap.isEmpty())
            return null;
        for (Map.Entry<String, String> entry : postDataMap.entrySet()) {

            if (entry.getKey().equals(keyText)) {
                return entry.getValue();
            }
        }
        return null;
    }

    public static String getLinkedKey(String keyName) {
        if (keyName.equals(UrlConstant.STORAGE_INVOICE))
            return UrlConstant.STORAGE_ORDER;
        else if (keyName.equals(UrlConstant.STORAGE_DISPATCH))
            return UrlConstant.STORAGE_ORDER;
        else if (keyName.equals(UrlConstant.STORAGE_RECEIPT))
            return UrlConstant.STORAGE_ORDER;
        else if (keyName.equals(UrlConstant.COMMODITY_LOAN))
            return UrlConstant.STORAGE_ORDER;
        else if (keyName.equals(UrlConstant.COMMODITY_LOAN_INVOICE))
            return UrlConstant.COMMODITY_LOAN;

        return null;
    }

    public static Call<GetEntityList> getEntityList(String keyName, String ids,
                                                    int limit, int limitSize,
                                                    String searchKey, String search_text) {
        AppRestClient client = AppRestClient.getInstance();
        try {
            Object Call = client.getClass()
                    .getDeclaredMethod(
                            UrlConstant.key_mapping.get(keyName).get(UrlConstant.LIST_GET_METHOD),
                            String.class, int.class, int.class, String.class, String.class)
                    .invoke(client, ids, limit, limitSize, searchKey, search_text);
            return (retrofit2.Call) Call;
        } catch (Exception e) {
            return null;
        }
    }


    public static String[] getLinkedEntities(String keyName) {
        Map<String, String> keyMap = UrlConstant.key_mapping.get(keyName);
        if (keyMap != null) {
            String linkedEntities = keyMap.get(UrlConstant.LINKED_ENTITIES);
            if (linkedEntities != null) {
                return linkedEntities.split(",");
            } else {
                return new String[]{};
            }
        } else {
            return null;
        }
    }


    public static class FetchPicture extends android.os.AsyncTask<String, String, URL> {
        URL url = null;
        String photo;

        public FetchPicture(String photo1) {
            photo = photo1;
        }

        @Override
        protected URL doInBackground(String... strings) {
            AmazonS3 s3Client = new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider());
            GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(UrlConstant.FNF_DB, photo);
            generatePresignedUrlRequest.setMethod(HttpMethod.GET);
            url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
            return url;
        }
    }
}

package com.dehaat.node.utilities;

import com.dehaat.node.rest.response.FinsihedProduct;

import java.util.HashMap;

public interface BOMDataCallBack {
    void onDataCall(HashMap<Integer, FinsihedProduct.FinishedProd> data);

}

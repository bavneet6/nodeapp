package com.dehaat.node.utilities;

import java.util.HashMap;

public interface PostJsonPositionValues {

    void onClick(int pos, HashMap<String, String> value);
}

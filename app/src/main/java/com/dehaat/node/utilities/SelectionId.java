package com.dehaat.node.utilities;

import android.widget.EditText;
import android.widget.TextView;

public interface SelectionId {

    void onItemClick(TextView selection, EditText receivingInput, int position, String selectionId,
                     String string, Object payload);
}

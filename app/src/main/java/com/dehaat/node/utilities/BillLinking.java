package com.dehaat.node.utilities;

import java.util.List;

public interface BillLinking {
    void onBillClick(List<Long> id);
}

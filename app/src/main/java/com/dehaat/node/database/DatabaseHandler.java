package com.dehaat.node.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.dehaat.node.rest.response.EntityDataJson;
import com.dehaat.node.rest.response.EntityView;
import com.dehaat.node.rest.response.GetEntityList;
import com.dehaat.node.rest.response.LotNumber;
import com.dehaat.node.rest.response.ProductFileTable;
import com.dehaat.node.rest.response.SearchParameterTable;
import com.dehaat.node.rest.response.StoreValues;
import com.dehaat.node.utilities.AppUtils;
import com.dehaat.node.utilities.UrlConstant;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 6;
    private static final String DATABASE_NAME = "nodeDB_1";

    private static final String FORM_TABLE = "formTable";
    private static final String FORM_NAME = "form_name";
    private static final String FORM_VERSION = "form_version";
    private static final String FORM_DEFINITION = "form_definition";
    private static final String FORM_HEADING = "form_heading";

    private static final String ENTITY_DATA_TABLE = "schemaDataTable";
    private static final String ROW_ID = "row_id";
    private static final String ENTITY_TYPE = "entity_type";
    private static final String JSON_PAYLOAD = "json_payload";
    private static final String SYNC_STATUS = "sync_status";
    private static final String VALIDATION_STATUS = "validation_status";
    private static final String ENTITY_STATUS = "entity_status";
    private static final String ENTITY_ID = "entity_id";
    private static final String PAYLOAD_VERSION = "payload_version";
    private static final String WRITE_DATE = "write_date";
    private static final String LOCK_DATA = "lock_data";
    private static final String ACTION_REQUIRED = "action_required";
    private static final String ATTACHMENTS = "attachments";
    private static final String DIALOG_ID = "dialog_id";


    private static final String TEMP_LOT_NUMBER = "temp_lot_number";
    private static final String PERM_LOT_NUMBER = "perm_lot_number";
    private static final String PRODUCT_ID = "product_id";
    private static final String LOT_NUMBER_TABLE = "lot_number_table";
    private static final String LOT_NUMBER_ID = "lot_number_id";

    private static final String PRODUCT_PIC_TABLE = "product_pic_table";
    private static final String PRODUCT_PIC_FILE = "product_pic_file";
    private static final String PRODUCT_PIC_URL = "product_pic_url";


    private static final String SEARCH_TABLE = "search_table";
    private static final String SEARCH_PARAMETER = "search_parameter";
    private static final String SEARCH_VALUE = "search_value";


    private static final String VALUE_STORE_TABLE = "value_store_table";
    private static final String KEYNAME = "keyname";
    private static final String STORE_ID = "store_id";
    private static final String STORE_VALUE = "store_value";


    private SQLiteDatabase database;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String FORM_VIEW = "CREATE TABLE " + FORM_TABLE + "("
                + FORM_NAME + " TEXT PRIMARY KEY ," + FORM_VERSION + " TEXT,"
                + FORM_DEFINITION + " TEXT ," + FORM_HEADING + " TEXT" + ")";

        String ENTITY_DATA_VIEW = "CREATE TABLE " + ENTITY_DATA_TABLE + "("
                + ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," + ENTITY_TYPE + " TEXT ," +
                JSON_PAYLOAD + " TEXT ," + SYNC_STATUS + " TEXT ,"
                + VALIDATION_STATUS + " TEXT ," + ENTITY_STATUS + " TEXT ," + ENTITY_ID + " TEXT ,"
                + PAYLOAD_VERSION + " TEXT ," + WRITE_DATE + " TEXT ," + LOCK_DATA + " TEXT ,"
                + ACTION_REQUIRED + " TEXT ," + ATTACHMENTS + " TEXT ," + DIALOG_ID + " TEXT " + ")";

        String LOT_NUMBER_VIEW = "CREATE TABLE " + LOT_NUMBER_TABLE + "("
                + ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," + LOT_NUMBER_ID + " LONG ,"
                + PRODUCT_ID + " LONG ," + TEMP_LOT_NUMBER + " TEXT ," + PERM_LOT_NUMBER +
                " TEXT " + ")";

        String PRO_URL_TABLE = "CREATE TABLE " + PRODUCT_PIC_TABLE + "("
                + PRODUCT_ID + " TEXT PRIMARY KEY ," + PRODUCT_PIC_FILE + " TEXT ," +
                PRODUCT_PIC_URL + " TEXT " + ")";


        String SEARCH_VIEW_TABLE = "CREATE TABLE " + SEARCH_TABLE + "("
                + ENTITY_TYPE + " TEXT ," + SEARCH_PARAMETER + " TEXT ," + SEARCH_VALUE +
                " TEXT ," + ROW_ID + " INTEGER ,"
                + "PRIMARY KEY" + "(" + ENTITY_TYPE + "," + SEARCH_PARAMETER + "," +
                SEARCH_VALUE + "," + ROW_ID + "),"
                + " FOREIGN KEY (" + ROW_ID + ") REFERENCES " + ENTITY_DATA_TABLE +
                "(" + ROW_ID + ")" + "ON DELETE CASCADE" + ")";

        String VALUE_TABLE = "CREATE TABLE " + VALUE_STORE_TABLE + "("
                + ENTITY_TYPE + " TEXT ," + KEYNAME + " TEXT ," + STORE_ID + " TEXT ," +
                STORE_VALUE + " TEXT ," +
                "PRIMARY KEY" + "(" + ENTITY_TYPE + "," + KEYNAME + "," + STORE_ID + "," +
                STORE_VALUE + "))";

        db.execSQL(PRO_URL_TABLE);
        db.execSQL(FORM_VIEW);
        db.execSQL(ENTITY_DATA_VIEW);
        db.execSQL(LOT_NUMBER_VIEW);
        db.execSQL(SEARCH_VIEW_TABLE);
        db.execSQL(VALUE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e("DATABASE", "" + oldVersion + "+" + newVersion);
        db.execSQL("DROP TABLE IF EXISTS '" + PRODUCT_PIC_TABLE + "'");
        String PRO_URL_TABLE = "CREATE TABLE " + PRODUCT_PIC_TABLE + "("
                + PRODUCT_ID + " TEXT PRIMARY KEY ," + PRODUCT_PIC_FILE + " TEXT ," +
                PRODUCT_PIC_URL + " TEXT " + ")";

        db.execSQL("DROP TABLE IF EXISTS '" + ENTITY_DATA_TABLE + "'");
        String ENTITY_DATA_VIEW = "CREATE TABLE " + ENTITY_DATA_TABLE + "("
                + ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," + ENTITY_TYPE + " TEXT ," +
                JSON_PAYLOAD + " TEXT ," + SYNC_STATUS + " TEXT ,"
                + VALIDATION_STATUS + " TEXT ," + ENTITY_STATUS + " TEXT ," + ENTITY_ID + " TEXT ," +
                PAYLOAD_VERSION + " TEXT ," + WRITE_DATE + " TEXT ," + LOCK_DATA + " TEXT ," +
                ACTION_REQUIRED + " TEXT ,"
                + ATTACHMENTS + " TEXT ," + DIALOG_ID + " TEXT " + ")";
        db.execSQL("DROP TABLE IF EXISTS '" + SEARCH_TABLE + "'");
        String SEARCH_VIEW_TABLE = "CREATE TABLE " + SEARCH_TABLE + "("
                + ENTITY_TYPE + " TEXT ," + SEARCH_PARAMETER + " TEXT ," + SEARCH_VALUE + " TEXT ," +
                ROW_ID + " INTEGER ,"
                + "PRIMARY KEY" + "(" + ENTITY_TYPE + "," + SEARCH_PARAMETER + "," + SEARCH_VALUE + ","
                + ROW_ID + "),"
                + " FOREIGN KEY (" + ROW_ID + ") REFERENCES " + ENTITY_DATA_TABLE + "(" + ROW_ID + ")" +
                "ON DELETE CASCADE" + ")";

        String VALUE_TABLE = "CREATE TABLE " + VALUE_STORE_TABLE + "("
                + ENTITY_TYPE + " TEXT ," + KEYNAME + " TEXT ," + STORE_ID + " TEXT ," + STORE_VALUE
                + " TEXT ," +
                "PRIMARY KEY" + "(" + ENTITY_TYPE + "," + KEYNAME + "," + STORE_ID + ","
                + STORE_VALUE + "))";
        db.execSQL(PRO_URL_TABLE);
        db.execSQL(ENTITY_DATA_VIEW);
        db.execSQL(SEARCH_VIEW_TABLE);
        db.execSQL(VALUE_TABLE);
        // Create tables again
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        db.execSQL("PRAGMA foreign_keys=ON");
    }

    /**
     * store the schema data
     * contact is the data json to be stored
     *
     * @param contact
     */
    public void insertSchemaData(EntityView.GetView contact) {
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FORM_NAME, contact.getKey());
        contentValues.put(FORM_VERSION, contact.getVersion());
        Gson gson = new Gson();
        String jsonString = gson.toJson(contact.getSchema());
        contentValues.put(FORM_DEFINITION, jsonString);
        contentValues.put(FORM_HEADING, contact.getDisplay_name());
        database.insertWithOnConflict(FORM_TABLE, null, contentValues,
                SQLiteDatabase.CONFLICT_REPLACE);
        database.close();
    }

    public void insertOrUpdateLotNumbers(LotNumber lotNumber) {
        database = this.getWritableDatabase();
        Long data = 0L;
        String selectQuery;
        Cursor cursor;
        selectQuery = "SELECT " + ROW_ID + " FROM " + LOT_NUMBER_TABLE + " WHERE " +
                LOT_NUMBER_ID + "=" + "'" + lotNumber.getLot_number_id() + "'";
        cursor = database.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            if (cursor.moveToFirst())
                data = Long.valueOf(cursor.getString(0));
        } else {
            if (!AppUtils.isNullCase(lotNumber.getTemp_lot_number())) {
                selectQuery = "SELECT " + ROW_ID + " FROM " + LOT_NUMBER_TABLE +
                        " WHERE " + TEMP_LOT_NUMBER + "=" + "'" + lotNumber.getTemp_lot_number() + "'";
                cursor = database.rawQuery(selectQuery, null);
                if (cursor != null && cursor.getCount() > 0)
                    if (cursor.moveToFirst())
                        data = Long.valueOf(cursor.getString(0));
            }

        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(LOT_NUMBER_ID, lotNumber.getLot_number_id());
        contentValues.put(PRODUCT_ID, lotNumber.getProduct_id());
        contentValues.put(TEMP_LOT_NUMBER, lotNumber.getTemp_lot_number());
        contentValues.put(PERM_LOT_NUMBER, lotNumber.getPerm_lot_number());
        if (data == 0L)
            database.insertWithOnConflict(LOT_NUMBER_TABLE, null,
                    contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        else
            database.update(LOT_NUMBER_TABLE, contentValues, ROW_ID + "=" + data,
                    null);
        database.close();
    }

    public void insertLotNumbers(LotNumber lotNumber) {
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(LOT_NUMBER_ID, lotNumber.getLot_number_id());
        contentValues.put(PRODUCT_ID, lotNumber.getProduct_id());
        contentValues.put(TEMP_LOT_NUMBER, lotNumber.getTemp_lot_number());
        contentValues.put(PERM_LOT_NUMBER, lotNumber.getPerm_lot_number());
        database.insertWithOnConflict(LOT_NUMBER_TABLE, null, contentValues,
                SQLiteDatabase.CONFLICT_REPLACE);
        database.close();
    }

    public void insertStoreValues(StoreValues storeValues) {
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ENTITY_TYPE, storeValues.getEntityType());
        contentValues.put(KEYNAME, storeValues.getKeyName());
        contentValues.put(STORE_ID, storeValues.getDataId());
        contentValues.put(STORE_VALUE, storeValues.getDataValue());
        database.insertWithOnConflict(VALUE_STORE_TABLE, null, contentValues,
                SQLiteDatabase.CONFLICT_REPLACE);
        database.close();
    }

    public ArrayList<LotNumber> getLotNumbersData(Long id) {
        database = this.getWritableDatabase();
        String selectQuery = "SELECT  * FROM " + LOT_NUMBER_TABLE + " WHERE " +
                PRODUCT_ID + " =" + "'" + id + "'";
        ArrayList<LotNumber> lotNumbersList = new ArrayList<>();
        LotNumber lotNumbers;

        Cursor cursor = database.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                lotNumbers = new LotNumber();
                lotNumbers.setLot_number_id(Long.valueOf(cursor.getString(1)));
                lotNumbers.setProduct_id(Long.valueOf(cursor.getString(2)));
                lotNumbers.setTemp_lot_number(cursor.getString(3));
                lotNumbers.setPerm_lot_number(cursor.getString(4));
                lotNumbersList.add(lotNumbers);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return lotNumbersList;
    }

    public ArrayList<String> getSearchValuesForDropDown(String keyname, String search_parameter) {
        database = this.getWritableDatabase();
        String selectQuery;
        selectQuery = "SELECT DISTINCT " + SEARCH_VALUE + "  FROM " + SEARCH_TABLE + " WHERE " +
                SEARCH_PARAMETER + " =" + "'" + search_parameter + "'" + " AND " + ENTITY_TYPE +
                " =" + "'" + keyname + "'";

        ArrayList<String> searchParameterTablesList = new ArrayList<>();

        Cursor cursor = database.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                searchParameterTablesList.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return searchParameterTablesList;
    }

    public EntityView.GetView getSchemaSingleView(String keyName) {
        database = this.getWritableDatabase();

        EntityView.GetView contactModel = null;
        // Select All Query
        String selectQuery = "SELECT  * FROM " + FORM_TABLE + " WHERE " + FORM_NAME + " =" + "'" +
                keyName + "'";

        Cursor cursor = database.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                contactModel = new EntityView.GetView();
                contactModel.setKey(cursor.getString(0));
                contactModel.setVersion(cursor.getString(1));
                Gson gson = new Gson();
                EntityView.GetView.GetSchema getSchema = gson.fromJson(cursor.getString(2),
                        EntityView.GetView.GetSchema.class);
                contactModel.setSchema(getSchema);
                contactModel.setDisplay_name(cursor.getString(3));
                // Adding contact to list
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();

        // return contact list
        return contactModel;
    }

    public ArrayList<EntityDataJson> getEntityListData(String keyName) {
        database = this.getWritableDatabase();

        ArrayList<EntityDataJson> dataList = new ArrayList<>();
        EntityDataJson contactModel = null;
        // Select All Query
        String selectQuery = "SELECT  * FROM " + ENTITY_DATA_TABLE + " WHERE " + ENTITY_TYPE +
                " =" + "'" +
                keyName + "'" + " ORDER BY " + WRITE_DATE + " DESC";

        Cursor cursor = database.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                contactModel = new EntityDataJson();
                contactModel.setRow_id(Long.valueOf(cursor.getString(0)));
                contactModel.setEntity_type(cursor.getString(1));
                contactModel.setJson_payload(cursor.getString(2));
                contactModel.setSync_status(cursor.getString(3));
                contactModel.setValidation_status(cursor.getString(4));
                contactModel.setEntity_status(cursor.getString(5));
                contactModel.setEntity_id(Long.valueOf(cursor.getString(6)));
                contactModel.setPayload_version(cursor.getString(7));
                contactModel.setWrite_date(cursor.getString(8));
                contactModel.setLock_data(cursor.getString(9));
                contactModel.setAction_required(Integer.parseInt((cursor.getString(10))));
                dataList.add(contactModel);
                // Adding contact to list
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        // return contact list
        return dataList;
    }

    String makePlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }

    public ArrayList<EntityDataJson> getEntityDataForLink(String[] list) {
        database = this.getWritableDatabase();

        ArrayList<EntityDataJson> dataList = new ArrayList<>();
        EntityDataJson contactModel = null;

        // Select All Query
        String selectQuery = "SELECT  * FROM " + ENTITY_DATA_TABLE + " WHERE " + ENTITY_TYPE +
                " IN " + "(" + makePlaceholders(list.length) + ")" + " ORDER BY " + WRITE_DATE + " DESC";

        Cursor cursor = database.rawQuery(selectQuery, list);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                contactModel = new EntityDataJson();
                contactModel.setRow_id(Long.valueOf(cursor.getString(0)));
                contactModel.setEntity_type(cursor.getString(1));
                contactModel.setJson_payload(cursor.getString(2));
                contactModel.setSync_status(cursor.getString(3));
                contactModel.setValidation_status(cursor.getString(4));
                contactModel.setEntity_status(cursor.getString(5));
                contactModel.setEntity_id(Long.valueOf(cursor.getString(6)));
                contactModel.setPayload_version(cursor.getString(7));
                contactModel.setWrite_date(cursor.getString(8));
                contactModel.setLock_data(cursor.getString(9));
                contactModel.setAction_required(Integer.parseInt((cursor.getString(10))));
                dataList.add(contactModel);
                // Adding contact to list
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        // return contact list
        return dataList;
    }

    public EntityDataJson getEntityIdData(String[] list, Long entityId) {
        database = this.getWritableDatabase();

        EntityDataJson contactModel = null;
        // Select All Query
        String selectQuery = "SELECT  * FROM " + ENTITY_DATA_TABLE + " WHERE " + ENTITY_ID + " =" + "'" +
                entityId + "'" + " AND " + ENTITY_TYPE + " IN " + "(" + makePlaceholders(list.length) + ")";

        Cursor cursor = database.rawQuery(selectQuery, list);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                contactModel = new EntityDataJson();
                contactModel.setRow_id(Long.valueOf(cursor.getString(0)));
                contactModel.setEntity_type(cursor.getString(1));
                contactModel.setJson_payload(cursor.getString(2));
                contactModel.setSync_status(cursor.getString(3));
                contactModel.setValidation_status(cursor.getString(4));
                contactModel.setEntity_status(cursor.getString(5));
                contactModel.setEntity_id(Long.valueOf(cursor.getString(6)));
                contactModel.setPayload_version(cursor.getString(7));
                contactModel.setWrite_date(cursor.getString(8));
                contactModel.setLock_data(cursor.getString(9));
                contactModel.setAction_required(Integer.parseInt((cursor.getString(10))));
                // Adding contact to list
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();

        // return contact list
        return contactModel;
    }

    public ArrayList<EntityDataJson> getEntityPendingList(String keyName) {
        database = this.getWritableDatabase();

        ArrayList<EntityDataJson> dataList = new ArrayList<>();
        EntityDataJson contactModel = null;
        // Select All Query
        String selectQuery = null;
        if (AppUtils.isNullCase(keyName)) {
            selectQuery = "SELECT  * FROM " + ENTITY_DATA_TABLE + " WHERE " + SYNC_STATUS + " =" + "'" +
                    UrlConstant.PENDING + "'" + " AND " + LOCK_DATA + " =" + "'" + UrlConstant.FALSE + "'";
        } else {
            selectQuery = "SELECT  * FROM " + ENTITY_DATA_TABLE + " WHERE " + SYNC_STATUS + " =" + "'" +
                    UrlConstant.PENDING + "'" + " AND " + ENTITY_TYPE + " =" + "'" + keyName + "'"
                    + " AND " + LOCK_DATA + " =" + "'" + UrlConstant.FALSE + "'";
        }
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0)
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    contactModel = new EntityDataJson();
                    contactModel.setRow_id(Long.valueOf(cursor.getString(0)));
                    contactModel.setEntity_type(cursor.getString(1));
                    contactModel.setJson_payload(cursor.getString(2));
                    contactModel.setSync_status(cursor.getString(3));
                    contactModel.setValidation_status(cursor.getString(4));
                    contactModel.setEntity_status(cursor.getString(5));
                    contactModel.setEntity_id(Long.valueOf(cursor.getString(6)));
                    contactModel.setPayload_version(cursor.getString(7));
                    contactModel.setWrite_date(cursor.getString(8));
                    contactModel.setLock_data(cursor.getString(9));
                    contactModel.setAction_required(Integer.parseInt((cursor.getString(10))));
                    dataList.add(contactModel);
                    // Adding contact to list
                } while (cursor.moveToNext());
            }

        // this gets called even if there is an exception somewhere above
        if (cursor != null)
            cursor.close();

        // return contact list
        return dataList;
    }

    public ArrayList<EntityDataJson> getStoredSearchedValues(boolean byrow, String keyName,
                                                             String[] list) {
        database = this.getWritableDatabase();

        ArrayList<EntityDataJson> dataList = new ArrayList<>();
        EntityDataJson contactModel = null;
        // Select All Query
        String selectQuery = null;
        if (byrow)
            selectQuery = "SELECT  * FROM " + ENTITY_DATA_TABLE + " WHERE " + ENTITY_TYPE + " ="
                    + "'" +
                    keyName + "'" + " AND " + ROW_ID + " IN " + "(" + makePlaceholders(list.length)
                    + ")";
        else
            selectQuery = "SELECT  * FROM " + ENTITY_DATA_TABLE + " WHERE " + ENTITY_TYPE + " =" +
                    "'" +
                    keyName + "'" + " AND " + ENTITY_ID + " IN " + "(" + makePlaceholders(list.length)
                    + ")";

        Cursor cursor = database.rawQuery(selectQuery, list);
        if (cursor != null && cursor.getCount() > 0)
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    contactModel = new EntityDataJson();
                    contactModel.setRow_id(Long.valueOf(cursor.getString(0)));
                    contactModel.setEntity_type(cursor.getString(1));
                    contactModel.setJson_payload(cursor.getString(2));
                    contactModel.setSync_status(cursor.getString(3));
                    contactModel.setValidation_status(cursor.getString(4));
                    contactModel.setEntity_status(cursor.getString(5));
                    contactModel.setEntity_id(Long.valueOf(cursor.getString(6)));
                    contactModel.setPayload_version(cursor.getString(7));
                    contactModel.setWrite_date(cursor.getString(8));
                    contactModel.setLock_data(cursor.getString(9));
                    contactModel.setAction_required(Integer.parseInt((cursor.getString(10))));
                    dataList.add(contactModel);
                    // Adding contact to list
                } while (cursor.moveToNext());
            }

        // this gets called even if there is an exception somewhere above
        if (cursor != null)
            cursor.close();

        // return contact list
        return dataList;
    }

    public ArrayList<Long> getSearchValuesIDS(String keyName, String name) {
        database = this.getWritableDatabase();

        ArrayList<Long> searchParameterTablesList = new ArrayList<>();
        // Select All Query
        String selectQuery = null;
        selectQuery = "SELECT  " + ROW_ID + " FROM " + SEARCH_TABLE + " WHERE " + ENTITY_TYPE +
                " =" + "'" +
                keyName + "'" + " AND " + SEARCH_VALUE + " = " + "'" + name + "'";

        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0)
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    searchParameterTablesList.add(Long.valueOf(cursor.getString(0)));
                } while (cursor.moveToNext());
            }
        if (cursor != null)
            cursor.close();

        return searchParameterTablesList;
    }

    public EntityDataJson getEntitySingleListData(long rowID) {
        database = this.getWritableDatabase();

        EntityDataJson contactModel = null;
        // Select All Query
        String selectQuery = "SELECT  * FROM " + ENTITY_DATA_TABLE + " WHERE " + ROW_ID +
                " =" + "'" +
                rowID + "'";

        Cursor cursor = database.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                contactModel = new EntityDataJson();
                contactModel.setRow_id(Long.valueOf(cursor.getString(0)));
                contactModel.setEntity_type(cursor.getString(1));
                contactModel.setJson_payload(cursor.getString(2));
                contactModel.setSync_status(cursor.getString(3));
                contactModel.setValidation_status(cursor.getString(4));
                contactModel.setEntity_status(cursor.getString(5));
                contactModel.setEntity_id(Long.valueOf(cursor.getString(6)));
                contactModel.setPayload_version(cursor.getString(7));
                contactModel.setWrite_date(cursor.getString(8));
                contactModel.setLock_data(cursor.getString(9));
                contactModel.setAction_required(Integer.parseInt((cursor.getString(10))));
                contactModel.setAttachments(cursor.getString(11));
                contactModel.setDialog_id(cursor.getString(12));
                // Adding contact to list
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();


        // return contact list
        return contactModel;
    }

    public long insertEntityData(EntityDataJson contact) {
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ENTITY_TYPE, contact.getEntity_type());
        contentValues.put(JSON_PAYLOAD, contact.getJson_payload());
        contentValues.put(SYNC_STATUS, contact.getSync_status());
        contentValues.put(VALIDATION_STATUS, contact.getValidation_status());
        contentValues.put(ENTITY_STATUS, contact.getEntity_status());
        contentValues.put(ENTITY_ID, contact.getEntity_id());
        contentValues.put(PAYLOAD_VERSION, contact.getPayload_version());
        contentValues.put(WRITE_DATE, contact.getWrite_date());
        contentValues.put(LOCK_DATA, contact.getLock_data());
        contentValues.put(ACTION_REQUIRED, contact.getAction_required());
        contentValues.put(ATTACHMENTS, contact.getAttachments());
        contentValues.put(DIALOG_ID, contact.getDialog_id());

        long id = database.insertWithOnConflict(ENTITY_DATA_TABLE, null,
                contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        database.close();
        return id;
    }

    public void updateEntityData(EntityDataJson contact, long rowID) {
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ENTITY_TYPE, contact.getEntity_type());
        contentValues.put(JSON_PAYLOAD, contact.getJson_payload());
        contentValues.put(SYNC_STATUS, contact.getSync_status());
        contentValues.put(VALIDATION_STATUS, contact.getValidation_status());
        contentValues.put(ENTITY_STATUS, contact.getEntity_status());
        contentValues.put(ENTITY_ID, contact.getEntity_id());
        contentValues.put(PAYLOAD_VERSION, contact.getPayload_version());
        contentValues.put(WRITE_DATE, contact.getWrite_date());
        contentValues.put(LOCK_DATA, contact.getLock_data());
        contentValues.put(ACTION_REQUIRED, contact.getAction_required());
        contentValues.put(ATTACHMENTS, contact.getAttachments());
        contentValues.put(DIALOG_ID, contact.getDialog_id());

        database.update(ENTITY_DATA_TABLE, contentValues, ROW_ID + "=" + rowID,
                null);
        database.close();

    }

    public Long updateServiceEntityData(EntityDataJson contact, long rowID) {
        Long id = 0L;
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        if (rowID == 0L) {
            contentValues.put(ENTITY_TYPE, contact.getEntity_type());
            contentValues.put(JSON_PAYLOAD, contact.getJson_payload());
            contentValues.put(SYNC_STATUS, contact.getSync_status());
            contentValues.put(VALIDATION_STATUS, contact.getValidation_status());
            contentValues.put(ENTITY_STATUS, contact.getEntity_status());
            contentValues.put(ENTITY_ID, contact.getEntity_id());
            contentValues.put(PAYLOAD_VERSION, contact.getPayload_version());
            contentValues.put(WRITE_DATE, contact.getWrite_date());
            contentValues.put(LOCK_DATA, contact.getLock_data());
            contentValues.put(ACTION_REQUIRED, contact.getAction_required());
            contentValues.put(ATTACHMENTS, contact.getAttachments());
            contentValues.put(DIALOG_ID, contact.getDialog_id());
            id = database.insertWithOnConflict(ENTITY_DATA_TABLE, null,
                    contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        } else {
            contentValues.put(SYNC_STATUS, contact.getSync_status());
            contentValues.put(VALIDATION_STATUS, contact.getValidation_status());
            contentValues.put(ENTITY_ID, contact.getEntity_id());
            contentValues.put(LOCK_DATA, UrlConstant.FALSE);
            contentValues.put(JSON_PAYLOAD, contact.getJson_payload());
            contentValues.put(ACTION_REQUIRED, 0);
            contentValues.put(DIALOG_ID, contact.getDialog_id());
            Long.valueOf(database.update(ENTITY_DATA_TABLE, contentValues,
                    ROW_ID + "=" + rowID, null));
            id = rowID;
        }
        database.close();
        return id;

    }

    public void updateAttachments(long rowID, List<String> attachments) {
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ATTACHMENTS, String.valueOf(attachments));
        database.update(ENTITY_DATA_TABLE, contentValues,
                ROW_ID + "=" + rowID, null);
        database.close();

    }

    public void deleteEntityData(long rowID) {
        database = this.getWritableDatabase();
        database.delete(ENTITY_DATA_TABLE, ROW_ID +
                "=" + rowID, null);
        database.close();

    }

    public void updateLockData(long rowID) {
        database = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(LOCK_DATA, UrlConstant.TRUE);
        database.update(ENTITY_DATA_TABLE, cv, ROW_ID + "=" +
                rowID, null);
        database.close();

    }

    public void inserPicturesFile(ProductFileTable productFileTable) {
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PRODUCT_ID, productFileTable.getProduct_id());
        contentValues.put(PRODUCT_PIC_FILE, productFileTable.getProd_file());
        contentValues.put(PRODUCT_PIC_URL, productFileTable.getProd_url());
        database.insertWithOnConflict(PRODUCT_PIC_TABLE, null,
                contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        database.close();
    }

    public void insertSearchParameterValue(SearchParameterTable searchParameterTable) {
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ENTITY_TYPE, searchParameterTable.getEntity_type());
        contentValues.put(SEARCH_PARAMETER, searchParameterTable.getSearch_parameter());
        contentValues.put(SEARCH_VALUE, searchParameterTable.getSearch_value());
        contentValues.put(ROW_ID, searchParameterTable.getRow_id());
        database.insertWithOnConflict(SEARCH_TABLE, null,
                contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        database.close();
    }

    public String getFileUrl(String productId) {
        String strR = null;
        String selectQuery = "SELECT " + PRODUCT_PIC_URL + " FROM " +
                PRODUCT_PIC_TABLE + " WHERE " + PRODUCT_ID + " =" + "'" +
                productId + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            strR = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        return strR;
    }

    public String getStoreValues(String entityType, String keyName, String id) {
        String strR = null;
        String selectQuery = "SELECT " + STORE_VALUE + " FROM " + VALUE_STORE_TABLE +
                " WHERE " + ENTITY_TYPE + " =" + "'" +
                entityType + "'" + " AND " + KEYNAME + " =" + "'" + keyName + "'" +
                " AND " + STORE_ID + " =" + "'" + id + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            strR = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        return strR;
    }

    public String getFileName(String productId) {
        String strR = null;
        String selectQuery = "SELECT " + PRODUCT_PIC_FILE + " FROM " + PRODUCT_PIC_TABLE +
                " WHERE " + PRODUCT_ID + " =" + "'" +
                productId + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            strR = cursor.getString(0);
        }
        cursor.close();

        return strR;
    }

    public void updateLockDataFalse(long rowID) {
        database = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(LOCK_DATA, UrlConstant.FALSE);
        database.update(ENTITY_DATA_TABLE, cv, ROW_ID + "=" + rowID, null);
        database.close();

    }

    public void deleteAllEntityData(String keyName) {
        database = this.getWritableDatabase();
        database.delete(ENTITY_DATA_TABLE, ENTITY_TYPE + "=" + "'" + keyName +
                "'" + " AND " + SYNC_STATUS + "=" + "'" + UrlConstant.SUCCESS + "'", null);
        database.close();
    }

    public Long insertOrUpdateEntityData(String keyName, GetEntityList.CreateBody response,
                                         long entityId) {
        Long rowId = 0L;
        database = this.getWritableDatabase();
        String data = null;
        String selectQuery = "SELECT " + SYNC_STATUS + " FROM " + ENTITY_DATA_TABLE +
                " WHERE " + ENTITY_ID + " =" + "'" +
                entityId + "'" + " AND " + ENTITY_TYPE + " =" + "'" + keyName + "'";

        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            if (cursor.moveToFirst())
                data = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();

        ContentValues contentValues = new ContentValues();
        contentValues.put(ENTITY_TYPE, keyName);
        Gson gson = new Gson();
        String jsonString = gson.toJson(response.getFields());
        String attachments = gson.toJson(response.getAttachments());
        contentValues.put(JSON_PAYLOAD, jsonString);
        contentValues.put(SYNC_STATUS, UrlConstant.SUCCESS);
        contentValues.put(VALIDATION_STATUS, "");
        contentValues.put(ENTITY_STATUS, response.getEntity_status());
        contentValues.put(ENTITY_ID, entityId);
        contentValues.put(PAYLOAD_VERSION, response.getVersion());
        contentValues.put(WRITE_DATE, response.getWrite_date());
        contentValues.put(LOCK_DATA, UrlConstant.FALSE);
        contentValues.put(ACTION_REQUIRED, 0);
        contentValues.put(ATTACHMENTS, attachments);
        contentValues.put(DIALOG_ID, "");
        if (AppUtils.isNullCase(data))
            rowId = database.insertWithOnConflict(ENTITY_DATA_TABLE, null,
                    contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        else if (!((data.equals(UrlConstant.PENDING)) || (data.equals(UrlConstant.ERRORS)))) {
            Long.valueOf(database.update(ENTITY_DATA_TABLE, contentValues,
                    ENTITY_ID + "=" + entityId, null));

            selectQuery = "SELECT " + ROW_ID + " FROM " + ENTITY_DATA_TABLE +
                    " WHERE " + ENTITY_ID + " =" + "'" +
                    entityId + "'" + " AND " + ENTITY_TYPE + " =" + "'" + keyName + "'";
            cursor = database.rawQuery(selectQuery, null);
            if (cursor != null && cursor.getCount() > 0) {
                if (cursor.moveToFirst())
                    rowId = Long.valueOf(cursor.getString(0));
            }
            if (cursor != null)
                cursor.close();
        }

        database.close();
        return rowId;
    }


    public void clearFormSchemaTable() {
        database = this.getWritableDatabase();
        database.delete(FORM_TABLE, null, null);
    }

    public void clearEntityDataTable() {
        database = this.getWritableDatabase();
        database.delete(ENTITY_DATA_TABLE, null, null);
    }

    public void clearLotNumbersTable() {
        database = this.getWritableDatabase();
        database.delete(LOT_NUMBER_TABLE, null, null);
    }

    public void clearURlsData() {
        database = this.getWritableDatabase();
        database.delete(PRODUCT_PIC_TABLE, null, null);
    }

    public void clearSearchTable() {
        database = this.getWritableDatabase();
        database.delete(SEARCH_TABLE, null, null);
    }

    public void clearStoreValueTable() {
        database = this.getWritableDatabase();
        database.delete(VALUE_STORE_TABLE, null, null);
    }
}
package com.dehaat.node.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dehaat.node.R;
import com.dehaat.node.database.DatabaseHandler;
import com.dehaat.node.rest.ApiCallBack;
import com.dehaat.node.rest.AppRestClient;
import com.dehaat.node.rest.response.EntityView;
import com.dehaat.node.rest.response.FcmToken;
import com.dehaat.node.rest.response.GetIdName;
import com.dehaat.node.rest.response.GetViewFields;
import com.dehaat.node.rest.response.StoreValues;
import com.dehaat.node.rest.response.Token;
import com.dehaat.node.rest.response.UserInfo;
import com.dehaat.node.utilities.AppPreference;
import com.dehaat.node.utilities.AppUtils;
import com.dehaat.node.utilities.MainService;
import com.dehaat.node.utilities.UrlConstant;
import com.google.firebase.iid.FirebaseInstanceId;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = HomeFragment.class.getSimpleName();
    private TextView networkBar;
    private ImageView logout;
    private LinearLayout salesOrder, dispatchVoucher, paymentReciept, purchaseOrder,
            purchaseVoucher, paymentVoucher, processingMod, vendorBill, expenseBill, progressBack,
            mandiPrice, nodePrice, storageOrder, loanCredit, loanInvoice, storageInvoice,
            incomingGRN, outgoingGRN, paymentRequest;
    private String sendType = null;
    private Dialog actionDialog;
    private DatabaseHandler databaseHandler;
    private boolean ip = false;
    private int count = 0;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, null);
        showActionBar(false);
        logout = v.findViewById(R.id.logout);
        networkBar = v.findViewById(R.id.networkBar);
        salesOrder = v.findViewById(R.id.salesOrder);
        dispatchVoucher = v.findViewById(R.id.dispatchVoucher);
        paymentReciept = v.findViewById(R.id.paymentReciept);
        purchaseOrder = v.findViewById(R.id.purchaseOrder);
        purchaseVoucher = v.findViewById(R.id.purchaseVoucher);
        paymentVoucher = v.findViewById(R.id.paymentVoucher);
        processingMod = v.findViewById(R.id.processingMod);
        vendorBill = v.findViewById(R.id.vendorBill);
        expenseBill = v.findViewById(R.id.expenseBill);
        progressBack = v.findViewById(R.id.progressBack);
        mandiPrice = v.findViewById(R.id.mandiPrice);
        storageOrder = v.findViewById(R.id.storageOrder);
        nodePrice = v.findViewById(R.id.nodePrice);
        loanCredit = v.findViewById(R.id.loanCredit);
        loanInvoice = v.findViewById(R.id.loanInvoice);
        storageInvoice = v.findViewById(R.id.storageInvoice);
        incomingGRN = v.findViewById(R.id.incomingGRN);
        outgoingGRN = v.findViewById(R.id.outgoingGRN);
        paymentRequest = v.findViewById(R.id.paymentRequest);
        logout.setOnClickListener(this);

        salesOrder.setOnClickListener(this);
        dispatchVoucher.setOnClickListener(this);
        paymentReciept.setOnClickListener(this);
        purchaseOrder.setOnClickListener(this);
        purchaseVoucher.setOnClickListener(this);
        paymentVoucher.setOnClickListener(this);
        processingMod.setOnClickListener(this);
        vendorBill.setOnClickListener(this);
        expenseBill.setOnClickListener(this);
        mandiPrice.setOnClickListener(this);
        storageOrder.setOnClickListener(this);
        nodePrice.setOnClickListener(this);
        loanCredit.setOnClickListener(this);
        loanInvoice.setOnClickListener(this);
        storageInvoice.setOnClickListener(this);
        incomingGRN.setOnClickListener(this);
        outgoingGRN.setOnClickListener(this);
        paymentRequest.setOnClickListener(this);
        databaseHandler = new DatabaseHandler(getActivity());

        fetchUserInfo();
        sendFCMToken();

        return v;
    }

    private void sendFCMToken() {
        final String token = FirebaseInstanceId.getInstance().getToken();
        if (token == null)
            return;
        if (!token.equals(AppPreference.getInstance().getFCM_TOKEN())) {
            // Add custom implementation, as needed.
            Token token1 = new Token();
            token1.setFcm_token(token);
            AppRestClient client = AppRestClient.getInstance();
            FcmToken fcmToken = new FcmToken(token1);
            Call<Void> call = client.fcmToken(fcmToken);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.code() == 200)
                        AppPreference.getInstance().setFCM_TOKEN(token);
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {

                }
            });
        }

    }
    private void fetchUserInfo() {
        progressBack.setVisibility(View.VISIBLE);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        AppRestClient client = AppRestClient.getInstance();
        Call<UserInfo> call = client.getInfo();
        call.enqueue(new ApiCallBack<UserInfo>() {
            @Override
            public void onResponse(Response<UserInfo> response) {
                if (response.body() == null || response.body().getData() == null) return;
                if (response.code() == 200) {
                    Long storedId = AppPreference.getInstance().getTEAM_ID();
                    Long teamId = response.body().getData().getTeam_id();
                    if (teamId != storedId) {
                        databaseHandler.clearEntityDataTable();
                        databaseHandler.clearFormSchemaTable();
                        databaseHandler.clearLotNumbersTable();
                        databaseHandler.clearSearchTable();
                        databaseHandler.clearURlsData();
                        databaseHandler.clearStoreValueTable();
                        AppPreference.getInstance().setWAREHOUSE_ID(
                                response.body().getData().getWarehouse_id());
                        AppPreference.getInstance().setTEAM_ID(
                                response.body().getData().getTeam_id());
                    }
                }
                count = 0;
                //getSchema
                if (AppUtils.haveNetworkConnection(getActivity())) {
                    for (Map.Entry<String, Map<String, String>> entry : UrlConstant.key_mapping.entrySet()) {
                        getSchemaData(getActivity(), entry.getValue().get("key"));
                    }
                } else {
                    progressBack.setVisibility(View.GONE);
                    getActivity().getWindow().clearFlags(
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
            }

            @Override
            public void onResponse401(Response<UserInfo> response) {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });
    }

    public void getSchemaData(final Activity activity, final String keyName) {
        AppRestClient client = AppRestClient.getInstance();
        Call<EntityView> call = client.getEntitySchema(keyName);
        call.enqueue(new Callback<EntityView>() {
            @Override
            public void onResponse(Call<EntityView> call, Response<EntityView> response) {
                if (response.body() == null || response.body().getData() == null) return;
                if (AppUtils.haveNetworkConnection(activity))
                    if (response.raw().networkResponse().code() != 304) {
                        if (response.code() == 200)
                            AppUtils.storeDataDB(activity, response.body().getData(),
                                    false);
                        else if (response.code() == 500)
                            AppUtils.showToast(UrlConstant.SERVER_ERROR);

                        AsyncTaskExample asyncTask = new AsyncTaskExample(
                                response.body().getData().getKey(),
                                response.body().getData().getSchema());
                        asyncTask.execute("");
                    } else {
                        ++count;
                        if (count == UrlConstant.key_mapping.size()) {
                            progressBack.setVisibility(View.GONE);
                            if (getActivity() == null)
                                return;
                            getActivity().getWindow().clearFlags
                                    (WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        }
                    }
            }

            @Override
            public void onFailure(Call<EntityView> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (AppUtils.haveNetworkConnection(activity))
                        AppUtils.showToast(UrlConstant.SERVER_NOT_RES);
                } else
                    AppUtils.showToast(UrlConstant.TECH_PROB);

                ++count;
                if (count == UrlConstant.key_mapping.size()) {
                    progressBack.setVisibility(View.GONE);
                    getActivity().getWindow().clearFlags(
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.logout:
                showMenuDialog();
                break;
            case R.id.salesOrder:
                sendType = UrlConstant.PAYMENT_REQUEST;
                openFragment(sendType);
                break;
            case R.id.dispatchVoucher:
                sendType = UrlConstant.SALES_DISPATCH;
                openFragment(sendType);
                break;
            case R.id.paymentReciept:
                sendType = UrlConstant.SALES_PAYMENT;
                openFragment(sendType);
                break;
            case R.id.purchaseOrder:
                sendType = UrlConstant.PURCHASE_ORDER;
                openFragment(sendType);
                break;
            case R.id.purchaseVoucher:
                sendType = UrlConstant.PURCHASE_RECEIPT;
                openFragment(sendType);
                break;
            case R.id.paymentVoucher:
                sendType = UrlConstant.PURCHASE_PAYMENT;
                openFragment(sendType);
                break;
            case R.id.processingMod:
                sendType = UrlConstant.MANUFACTURING_ORDER;
                openFragment(sendType);
                break;
            case R.id.vendorBill:
                sendType = UrlConstant.VENDOR_BILL;
                openFragment(sendType);
                break;
            case R.id.expenseBill:
                sendType = UrlConstant.EXPENSE_BILL;
                openFragment(sendType);
                break;
            case R.id.mandiPrice:
                sendType = UrlConstant.MANDI_PRICE;
                openFragment(sendType);
                break;
            case R.id.storageOrder:
                sendType = UrlConstant.STORAGE_ORDER;
                openFragment(sendType);
                break;
            case R.id.nodePrice:
                sendType = UrlConstant.NODE_PRICE;
                openFragment(sendType);
                break;
            case R.id.loanCredit:
                sendType = UrlConstant.COMMODITY_LOAN;
                openFragment(sendType);
                break;
            case R.id.loanInvoice:
                sendType = UrlConstant.COMMODITY_LOAN_INVOICE;
                openFragment(sendType);
                break;
            case R.id.storageInvoice:
                sendType = UrlConstant.STORAGE_INVOICE;
                openFragment(sendType);
                break;
            case R.id.incomingGRN:
                sendType = UrlConstant.STORAGE_RECEIPT;
                openFragment(sendType);
                break;
            case R.id.outgoingGRN:
                sendType = UrlConstant.STORAGE_DISPATCH;
                openFragment(sendType);
                break;
            case R.id.paymentRequest:
                sendType = UrlConstant.PAYMENT_REQUEST;
                openFragment(sendType);
                break;
        }
    }

    private void openFragment(String sendType) {
        Bundle bundle = new Bundle();
        bundle.putString("KEY_NAME", sendType);
        EntityListFragment fragment = EntityListFragment.newInstance();
        fragment.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(
                R.id.frag_container, fragment).addToBackStack("").commit();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void showMenuDialog() {
        actionDialog = new Dialog(getActivity());
        actionDialog.setCanceledOnTouchOutside(true);
        actionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        actionDialog.setContentView(R.layout.dialog_menu);
        Button logout = actionDialog.findViewById(R.id.logoutB);
        Button ipVisible = actionDialog.findViewById(R.id.ipVisible);

        if (ip)
            ipVisible.setVisibility(View.VISIBLE);
        else
            ipVisible.setVisibility(View.GONE);

        if (AppPreference.getInstance().getIP().equals(UrlConstant.TEMP_BASE_URL))
            ipVisible.setText("Switch To Prod");
        else if (AppPreference.getInstance().getIP().equals(UrlConstant.PERM_BASE_URL))
            ipVisible.setText("Switch To Temp");

        logout.setOnClickListener(v -> showLogoutConfirmDialog());
        ipVisible.setOnClickListener(v -> {
            actionDialog.dismiss();
            AppUtils.showSessionExpiredDialog(getActivity());
            Intent intent = new Intent(getActivity(), MainService.class);
            getActivity().stopService(intent);
        });

        actionDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        actionDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT
                , ViewGroup.LayoutParams.WRAP_CONTENT);
        actionDialog.getWindow().setGravity(Gravity.TOP | Gravity.LEFT);
        actionDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        AppUtils.checkNetwork(getActivity(), networkBar, false);
        Intent intent = new Intent(getActivity(), MainService.class);
        getActivity().startService(intent);

        if (AppUtils.haveNetworkConnection(getActivity()))
            AppUtils.getVersionName(activity);
    }

    private void showLogoutConfirmDialog() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(),
                    android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setTitle("Logout")
                .setMessage("Are you sure you want to logout? Your unsaved data will be deleted.")
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        AppUtils.showSessionExpiredDialog(getActivity());
                        Intent intent = new Intent(getActivity(), MainService.class);
                        getActivity().stopService(intent);

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        actionDialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private class AsyncTaskExample extends AsyncTask<String, String, EntityView.GetView.GetSchema> {
        String key_name = null;
        EntityView.GetView.GetSchema getSchema1 = null;

        public AsyncTaskExample(String key, EntityView.GetView.GetSchema getSchema) {
            key_name = key;
            getSchema1 = getSchema;
        }

        @Override
        protected EntityView.GetView.GetSchema doInBackground(String... strings) {
            ArrayList<GetViewFields> getViewFields = getSchema1.getFields();
            for (GetViewFields getViewFields1 : getViewFields) {
                if (getViewFields1.getType().equals(UrlConstant.MANY2ONE)) {
                    for (GetIdName getIdName : getViewFields1.getSelection_values()) {
                        StoreValues storeValues = new StoreValues();
                        storeValues.setEntityType(key_name);
                        storeValues.setKeyName(getViewFields1.getKey());
                        storeValues.setDataId("" + getIdName.getId());
                        storeValues.setDataValue(getIdName.getName());
                        databaseHandler.insertStoreValues(storeValues);
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(EntityView.GetView.GetSchema bitmap) {
            super.onPostExecute(bitmap);
            ++count;
            if (count == UrlConstant.key_mapping.size()) {
                progressBack.setVisibility(View.GONE);
                if (getActivity() == null) return;
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }
    }
}
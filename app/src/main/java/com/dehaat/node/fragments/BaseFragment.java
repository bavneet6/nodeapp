package com.dehaat.node.fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

public class BaseFragment extends Fragment {
    public FragmentActivity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        setHasOptionsMenu(true);
    }

    /**
     * This function is for showing action bar in fragments
     *
     * @param value - true or false
     */

    protected void showActionBar(boolean value) {
        if (value)
            ((AppCompatActivity) activity).getSupportActionBar().show();
        else
            ((AppCompatActivity) activity).getSupportActionBar().hide();


    }

}

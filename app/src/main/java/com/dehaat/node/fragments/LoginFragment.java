package com.dehaat.node.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dehaat.node.R;
import com.dehaat.node.activity.LoginActivity;
import com.dehaat.node.activity.MainActivity;
import com.dehaat.node.database.DatabaseHandler;
import com.dehaat.node.rest.ApiCallBack;
import com.dehaat.node.rest.AppRestClient;
import com.dehaat.node.rest.body.LoginBody;
import com.dehaat.node.rest.response.EntityView;
import com.dehaat.node.rest.response.GetEntityList;
import com.dehaat.node.rest.response.LoginResponse;
import com.dehaat.node.rest.response.UserInfo;
import com.dehaat.node.utilities.AppPreference;
import com.dehaat.node.utilities.AppUtils;
import com.dehaat.node.utilities.UrlConstant;

import org.json.JSONException;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends BaseFragment implements View.OnClickListener, TextWatcher {
    public static final String TAG = LoginFragment.class.getSimpleName();
    private ImageView next, ipchange;
    private TextView userError, passError;
    private EditText username, password;
    private DatabaseHandler databaseHandler;
    private String key;
    private ArrayList<String> list = new ArrayList<>();
    private int i = 0, timer = 0;
    private ProgressDialog pd;
    private Handler handler;
    private Runnable runnable;
    private boolean ip = false;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, null);
        showActionBar(false);
        databaseHandler = new DatabaseHandler(getActivity());
        next = v.findViewById(R.id.next);
        ipchange = v.findViewById(R.id.ipchange);
        username = v.findViewById(R.id.username);
        password = v.findViewById(R.id.password);
        userError = v.findViewById(R.id.userError);
        passError = v.findViewById(R.id.passError);
        username.addTextChangedListener(this);
        password.addTextChangedListener(this);
        next.setOnClickListener(this);
        ipchange.setOnClickListener(this);
        for (Map.Entry<String, Map<String, String>> entry : UrlConstant.key_mapping.entrySet()) {
            list.add(entry.getKey());
        }
        if (ip)
            ipchange.setVisibility(View.VISIBLE);
        else
            ipchange.setVisibility(View.INVISIBLE);

        return v;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.next:
                if (!AppUtils.isNullCase(username.getText().toString())
                        && isValidEmail(username.getText().toString())) {
                    if (AppUtils.isNullCase(password.getText().toString())){
                        passError.setVisibility(View.VISIBLE);
                    } else {
                        if (ip) {
                            if (AppUtils.isNullCase(AppPreference.getInstance().getIP())) {
                                AppUtils.showToast("Please Select Server First");
                            }
                        } else {
                            AppPreference.getInstance().setIP(UrlConstant.PERM_BASE_URL);
                        }

                        if (!AppUtils.isNullCase(AppPreference.getInstance().getIP())) {
                            sendLoginData();
                        }
                    }
                } else {
                    userError.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.ipchange:
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setCancelable(true);
                builder.setNegativeButton("PRODUCTION SERVER",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                AppPreference.getInstance().setIP(UrlConstant.PERM_BASE_URL);
                            }
                        });
                builder.setPositiveButton("TEST SERVER", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        AppPreference.getInstance().setIP(UrlConstant.TEMP_BASE_URL);

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
                break;
        }
    }

    private void sendLoginData() {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        LoginBody.DataBody dataBody = new LoginBody.DataBody();
        dataBody.setUsername(username.getText().toString());
        dataBody.setPassword(password.getText().toString());

        LoginBody loginBody = new LoginBody(dataBody);

        Call<LoginResponse> call = client.sendLogin(loginBody);
        call.enqueue(new ApiCallBack<LoginResponse>() {
            @Override
            public void onResponse(Response<LoginResponse> response) throws JSONException {
                AppUtils.hideProgressDialog();

                if (response.code() == 200) {
                    if (response.body() == null)
                        return;
                    AppPreference.getInstance().setUSER_ID(response.body().getUser_id());
                    AppPreference.getInstance().setAuthToken(response.body().getAuth_token());
                    AppPreference.getInstance().setAPP_LOGIN(true);
                    fetchUserInfo();
                }
            }

            @Override
            public void onResponse401(Response<LoginResponse> response) throws JSONException {
                showWrongCredDialog();
            }
        });
    }

    private void fetchUserInfo() {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        Call<UserInfo> call = client.getInfo();
        call.enqueue(new ApiCallBack<UserInfo>() {
            @Override
            public void onResponse(Response<UserInfo> response) throws JSONException {
                AppUtils.hideProgressDialog();

                if (response.code() == 200) {
                    if (response.body() != null) {
                        AppPreference.getInstance().setWAREHOUSE_ID(response.body().getData().getWarehouse_id());
                        AppPreference.getInstance().setTEAM_ID(response.body().getData().getTeam_id());
                    }
                    pd = new ProgressDialog(getActivity());
                    pd.setMessage("Fetching Schema and Data...");
                    pd.setCancelable(false);
                    pd.show();
                    fetchData();
                }
            }

            @Override
            public void onResponse401(Response<UserInfo> response) throws JSONException {
                showWrongCredDialog();
            }
        });
    }

    private void fetchData() {
        handler = new Handler();
        runnable = new Runnable() {
            public void run() {
                if (timer == 2 * UrlConstant.key_mapping.size()) {
                    pd.dismiss();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    getContext().startActivity(intent);
                    ((LoginActivity) getContext()).finish();
                }

                handler.postDelayed(this, 1000);
            }
        };
        runnable.run();
        for (Map.Entry<String, Map<String, String>> entry : UrlConstant.key_mapping.entrySet()) {
            getSchemaData(entry.getValue().get("key"));
        }
        if (i < list.size()) {
            getListData(list.get(i));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (AppUtils.haveNetworkConnection(getActivity()))
            AppUtils.getVersionName(activity);
    }

    private void getListData(String keyName) {
        AppRestClient client = AppRestClient.getInstance();
        key = keyName;
        Call<GetEntityList> call;
        try{
            Object Call = client.getClass()
                    .getDeclaredMethod(
                            UrlConstant.key_mapping.get(keyName).get(UrlConstant.LIST_GET_METHOD),
                            String.class, int.class, int.class, String.class, String.class)
                    .invoke(client, null, 0, 10, null, null);
            call = (retrofit2.Call) Call;
        } catch (Exception ex){
            call = null;
        }

        call.enqueue(new Callback<GetEntityList>() {
            @Override
            public void onResponse(Call<GetEntityList> call, Response<GetEntityList> response) {
                ++timer;
                if ((response.code() == 200) || (response.code() == 500)) {
                    if (response.body() != null) {
                        if ((response.body().getCreateBody() != null) &&
                                (response.body().getCreateBody().size() != 0))
                            storeListDataDB(response.body().getCreateBody());
                    }
                    ++i;
                    if (i != list.size())
                        getListData(list.get(i));
                } else if (response.code() == 401) {
                    AppUtils.showSessionExpiredDialog(getActivity());
                }
            }

            @Override
            public void onFailure(Call<GetEntityList> call, Throwable t) {
                ++timer;
                if (t instanceof ConnectException)
                    if (!AppUtils.haveNetworkConnection(getActivity()))
                        AppUtils.showToast(UrlConstant.NO_INTERNET);
                    else
                        AppUtils.showToast(UrlConstant.SERVER_NOT_RES);
                else
                    AppUtils.showToast(UrlConstant.TECH_PROB);
            }
        });
    }

    private void storeListDataDB(ArrayList<GetEntityList.CreateBody> data) {
        for (int i = 0; i < data.size(); i++) {
            databaseHandler.insertOrUpdateEntityData(key, data.get(i), data.get(i).getEntity_id());
        }
    }

    private void getSchemaData(final String keyName) {
        AppRestClient client = AppRestClient.getInstance();
        Call<EntityView> call = client.getEntitySchema(keyName);
        call.enqueue(new ApiCallBack<EntityView>() {
            @Override
            public void onResponse(Response<EntityView> response) {
                ++timer;
                if (response.body() == null || response.body().getData() == null)
                    return;
                if (AppUtils.haveNetworkConnection(getActivity()))
                    if (response.code() == 200)
                        AppUtils.storeDataDB(getActivity(), response.body().getData(), true);
            }

            @Override
            public void onResponse401(Response<EntityView> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });
    }

    private void showWrongCredDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Information");
        builder.setMessage("Incorrect Login Details");
        builder.setCancelable(true);
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (handler != null)
            handler.removeCallbacks(runnable);

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (isValidEmail(username.getText().toString()))
            userError.setVisibility(View.GONE);
        else
            userError.setVisibility(View.VISIBLE);

        if (AppUtils.isNullCase(password.getText().toString()))
            passError.setVisibility(View.VISIBLE);
        else
            passError.setVisibility(View.GONE);
    }
}

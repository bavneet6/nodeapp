package com.dehaat.node.fragments;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dehaat.node.R;
import com.dehaat.node.adapter.RecyclerAdapterEntityList1;
import com.dehaat.node.adapter.RecyclerAdapterSearchListView;
import com.dehaat.node.database.DatabaseHandler;
import com.dehaat.node.rest.response.EntityDataJson;
import com.dehaat.node.rest.response.GetEntityList;
import com.dehaat.node.utilities.AppPreference;
import com.dehaat.node.utilities.AppUtils;
import com.dehaat.node.utilities.PaginationScrollListener;
import com.dehaat.node.utilities.SearchParams;
import com.dehaat.node.utilities.UrlConstant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchingFragment extends BaseFragment implements Observer, View.OnClickListener,
        SearchParams, SearchView.OnQueryTextListener, AdapterView.OnItemSelectedListener {

    public static final String TAG = SearchingFragment.class.getSimpleName();
    private ImageView back;
    private ProgressBar progressBar;
    private Spinner searchValues;
    private TextView networkBar, no_item, searched_text, searchText;
    private RecyclerView searchList;
    private Button search;
    private ArrayList<EntityDataJson> entityDataList = new ArrayList<>();
    private ArrayList<String> searchParameters = new ArrayList<>();
    private ArrayList<String> searchValuesList = new ArrayList<>();
    private ArrayList<String> searchParameterTables = new ArrayList<>();
    private DatabaseHandler databaseHandler;
    private String keyName, search_text, dropDownSelectedParam, searchKey;
    private Dialog dialog;
    private androidx.appcompat.widget.SearchView searchView;
    private RecyclerAdapterSearchListView recyclerAdapterSearchListView;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerAdapterEntityList1 recyclerAdapterEntityList;

    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int limit = 0;
    private String[] searchTextIdList;
    private int limitSize = 10;

    public static SearchingFragment newInstance() {
        return new SearchingFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search_list, null);
        showActionBar(false);
        back = v.findViewById(R.id.back);
        searchValues = v.findViewById(R.id.searchValues);
        networkBar = v.findViewById(R.id.networkBar);
        searched_text = v.findViewById(R.id.searched_text);
        no_item = v.findViewById(R.id.no_item);
        linearLayoutManager = new LinearLayoutManager(
                getActivity(), RecyclerView.VERTICAL, false);
        searchList = v.findViewById(R.id.searchList);
        searchList.setNestedScrollingEnabled(false);
        searchText = v.findViewById(R.id.searchText);
        progressBar = v.findViewById(R.id.progressBar);
        searchText.setOnClickListener(this);
        back.setOnClickListener(this);
        databaseHandler = new DatabaseHandler(getActivity());
        searchValues.setOnItemSelectedListener(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            keyName = bundle.getString("KEY_NAME");
        }
        setSearchData();
        AppUtils.showBanner(getActivity(), networkBar);
        return v;
    }

    private void setSearchData() {
        searchParameters.clear();
        List<String> myList;
        myList = new ArrayList<>(
                Arrays.asList(UrlConstant.key_mapping.get(keyName).get("search").split(",")));
        searchParameters.addAll(myList);
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(
                getActivity(), android.R.layout.simple_list_item_1, searchParameters);
        searchValues.setAdapter(adapter);
    }

    @Override
    public void update(Observable observable, Object o) {
        AppUtils.showBanner(getActivity(), networkBar);
    }

    private void prepareDataForSelection() {
        searchValuesList.clear();
        if (dropDownSelectedParam.equals("Vendor") || dropDownSelectedParam.equals("Partner")) {
            searchKey = UrlConstant.PARTNER_ID;
        } else if (dropDownSelectedParam.equals("Name"))
            searchKey = UrlConstant.NAME;
        else if (dropDownSelectedParam.equals("PO Name") ||
                dropDownSelectedParam.equals("SO Name"))
            searchKey = "origin";
        else if (dropDownSelectedParam.equals("Number"))
            searchKey = "number";
        else if (dropDownSelectedParam.equals("Market"))
            searchKey = UrlConstant.MANDI_ID;
        else if (dropDownSelectedParam.equals("Product"))
            searchKey = UrlConstant.PRODUCT_ID;
        else if (dropDownSelectedParam.equals("Warehouse")) {
            searchKey = UrlConstant.WAREHOUSE_ID;
        } else if (dropDownSelectedParam.equals("Storage Order")) {
            searchKey = UrlConstant.ORIGIN;
        } else if (dropDownSelectedParam.equals("Node"))
            searchKey = UrlConstant.TEAM_ID;
        searchParameterTables = databaseHandler.getSearchValuesForDropDown(keyName, searchKey);
        if (searchParameterTables != null && searchParameterTables.size() != 0) {
            for (int i = 0; i < searchParameterTables.size(); i++) {
                if (!searchParameterTables.get(i).equals("N/A"))
                    searchValuesList.add(searchParameterTables.get(i));
            }
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.searchText:
                prepareDataForSelection();
                dialog = new Dialog(getActivity());
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.dialog_search_values_list);
                searchView = dialog.findViewById(R.id.searchView);
                search = dialog.findViewById(R.id.search);
                search.setOnClickListener(this);
                RecyclerView partList = dialog.findViewById(R.id.partList);
                partList.setNestedScrollingEnabled(false);
                recyclerAdapterSearchListView = new RecyclerAdapterSearchListView(
                        getActivity(), searchValuesList, this);
                partList.setLayoutManager(new LinearLayoutManager(getActivity()));
                partList.setAdapter(recyclerAdapterSearchListView);
                searchView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        searchView.setIconified(false);
                    }
                });
                searchView.setOnQueryTextListener(this);
                dialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                dialog.show();
                break;
            case R.id.search:
                progressBar.setVisibility(View.VISIBLE);
                entityDataList.clear();
                dialog.dismiss();
                if (!TextUtils.isEmpty(search_text)) {
                    searched_text.setVisibility(View.VISIBLE);
                    searched_text.setText("Searched By: " + search_text);
                    AppPreference.getInstance().setSEARCHED_TEXT(search_text);
                    if (AppUtils.haveNetworkConnection(getActivity()))
                        showSearchDataList();
                    else {
                        List<Long> ids = databaseHandler.getSearchValuesIDS(keyName, search_text);
                        searchTextIdList = new String[ids.size()];

                        for (int i = 0; i < ids.size(); i++) {
                            searchTextIdList[i] = String.valueOf(ids.get(i));
                        }
                        if (searchTextIdList.length > 0)
                            entityDataList = databaseHandler.getStoredSearchedValues(true, keyName, searchTextIdList);
                        if (entityDataList.size() > 0)
                            no_item.setVisibility(View.GONE);
                        else
                            no_item.setVisibility(View.VISIBLE);
                        setData();
                    }
                } else
                    AppUtils.showToast("Please enter something for searching.");
                break;
            case R.id.back:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!AppUtils.isNullCase(AppPreference.getInstance().getSEARCHED_TEXT())) {
            search_text = AppPreference.getInstance().getSEARCHED_TEXT();
            if (search != null)
                search.callOnClick();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        AppPreference.getInstance().setSEARCHED_TEXT(null);
    }

    private Call<GetEntityList> getListAPI() {
        return AppUtils.getEntityList(keyName, null, limit, limitSize, searchKey, search_text);
    }

    private void storeListDataDB(ArrayList<GetEntityList.CreateBody> data) {
        for (int i = 0; i < data.size(); i++) {
            databaseHandler.insertOrUpdateEntityData(keyName, data.get(i), data.get(i).getEntity_id());
        }
    }

    private void loadFirstPage() {
        limit = 0;
        entityDataList.clear();
        getListAPI().enqueue(new Callback<GetEntityList>() {
            @Override
            public void onResponse(Call<GetEntityList> call, Response<GetEntityList> response) {
                no_item.setVisibility(View.GONE);
                if (response.body() != null && response.body().getCreateBody() != null && response.body().getCreateBody().size() != 0) {
                    storeListDataDB(response.body().getCreateBody());
                    ArrayList<GetEntityList.CreateBody> data = response.body().getCreateBody();
                    searchTextIdList = new String[data.size()];

                    for (int i = 0; i < data.size(); i++)
                        searchTextIdList[i] = String.valueOf(data.get(i).getEntity_id());

                    entityDataList = databaseHandler.getStoredSearchedValues(false, keyName, searchTextIdList);
                    recyclerAdapterEntityList.addAll(entityDataList);
                }
                if (entityDataList.size() == 0) {
                    isLastPage = true;
                    no_item.setVisibility(View.VISIBLE);
                } else {
                    isLastPage = false;
                    recyclerAdapterEntityList.addLoadingFooter();
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<GetEntityList> call, Throwable t) {
                progressBar.setVisibility(View.GONE);

            }
        });
    }

    private void loadNextPage() {
        entityDataList.clear();
        limit = limit + limitSize;
        getListAPI().enqueue(new Callback<GetEntityList>() {
            @Override
            public void onResponse(Call<GetEntityList> call, Response<GetEntityList> response) {
                recyclerAdapterEntityList.removeLoadingFooter();
                isLoading = false;
                if (response.body() != null && response.body().getCreateBody() != null &&
                        response.body().getCreateBody().size() != 0) {
                    storeListDataDB(response.body().getCreateBody());
                    ArrayList<GetEntityList.CreateBody> data = response.body().getCreateBody();
                    searchTextIdList = new String[data.size()];

                    for (int i = 0; i < data.size(); i++)
                        searchTextIdList[i] = String.valueOf(data.get(i).getEntity_id());

                    entityDataList = databaseHandler.getStoredSearchedValues(false, keyName, searchTextIdList);
                    recyclerAdapterEntityList.addAll(entityDataList);
                }
                if (entityDataList.size() == 0) {
                    isLastPage = true;
                } else {
                    isLastPage = false;
                    recyclerAdapterEntityList.addLoadingFooter();
                }
            }

            @Override
            public void onFailure(Call<GetEntityList> call, Throwable t) {

            }
        });
    }

    private void showSearchDataList() {
        setData();
        searchList.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;

                // mocking network delay for API call
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPage();
                    }
                }, 3000);
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        loadFirstPage();
    }

    @Override
    public void onClick(int id, String name) {
        searchView.setQuery(name, false);
        searchView.setIconified(false);
    }

    private void setData() {
        recyclerAdapterEntityList = new RecyclerAdapterEntityList1(getActivity(), entityDataList, keyName);
        searchList.setLayoutManager(linearLayoutManager);
        searchList.setItemAnimator(new DefaultItemAnimator());
        searchList.setAdapter(recyclerAdapterEntityList);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        search_text = s;
        if (searchValuesList.size() != 0) {
            recyclerAdapterSearchListView.getFilter().filter(s);
        }
        return false;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String label = adapterView.getItemAtPosition(i).toString();
        dropDownSelectedParam = label;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}

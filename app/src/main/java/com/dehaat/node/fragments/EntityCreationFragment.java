package com.dehaat.node.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dehaat.node.R;
import com.dehaat.node.adapter.RecyclerAdapterAttachedImages;
import com.dehaat.node.adapter.RecyclerAdapterBOM;
import com.dehaat.node.adapter.RecyclerAdapterBillLink;
import com.dehaat.node.adapter.RecyclerAdapterBillPickingList;
import com.dehaat.node.adapter.RecyclerAdapterFields;
import com.dehaat.node.adapter.RecyclerAdapterImageList;
import com.dehaat.node.adapter.RecyclerAdapterLinkedOrders;
import com.dehaat.node.adapter.RecyclerAdapterMultipleLines;
import com.dehaat.node.adapter.RecyclerAdapterPickingLink;
import com.dehaat.node.adapter.RecyclerAdapterStorageDispatch;
import com.dehaat.node.adapter.RecyclerAdapterStorageInvoice;
import com.dehaat.node.adapter.RecyclerAdapterStorageLoan;
import com.dehaat.node.adapter.RecyclerAdapterStorageReceipt;
import com.dehaat.node.database.DatabaseHandler;
import com.dehaat.node.rest.ApiCallBack;
import com.dehaat.node.rest.AppRestClient;
import com.dehaat.node.rest.body.Attachments;
import com.dehaat.node.rest.body.EntityCreateDataBody;
import com.dehaat.node.rest.response.AttachmentResponse;
import com.dehaat.node.rest.response.EntityDataJson;
import com.dehaat.node.rest.response.EntityView;
import com.dehaat.node.rest.response.FinsihedProduct;
import com.dehaat.node.rest.response.GetEntityList;
import com.dehaat.node.rest.response.GetIdName;
import com.dehaat.node.rest.response.GetViewFields;
import com.dehaat.node.rest.response.JsonDataStore;
import com.dehaat.node.rest.response.LotNumber;
import com.dehaat.node.rest.response.MultipleLines;
import com.dehaat.node.rest.response.ResponseCreateEntity;
import com.dehaat.node.rest.response.VendorBill;
import com.dehaat.node.utilities.AppPreference;
import com.dehaat.node.utilities.AppUtils;
import com.dehaat.node.utilities.BOMCallBack;
import com.dehaat.node.utilities.BOMDataCallBack;
import com.dehaat.node.utilities.BillLinking;
import com.dehaat.node.utilities.DeleteImages;
import com.dehaat.node.utilities.One2manyRequired;
import com.dehaat.node.utilities.PostJsonValues;
import com.dehaat.node.utilities.RequiredField;
import com.dehaat.node.utilities.ShowMultipleLines;
import com.dehaat.node.utilities.Unlink;
import com.dehaat.node.utilities.UrlConstant;
import com.fxn.pix.Pix;
import com.fxn.utility.PermUtil;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dehaat.node.utilities.AppUtils.getLinkedKey;
import static com.dehaat.node.utilities.AppUtils.getTimeStamp;
import static com.dehaat.node.utilities.AppUtils.storeDataDB;

public class EntityCreationFragment extends BaseFragment implements View.OnClickListener,
        PostJsonValues, RequiredField, BOMCallBack, BOMDataCallBack, BillLinking, DeleteImages,
        Unlink, ShowMultipleLines, One2manyRequired {
    public static final String TAG = EntityCreationFragment.class.getSimpleName();
    private static RecyclerView.LayoutManager layoutManager1;
    private static RecyclerView entityView, attachedImages, addImages;
    private static RecyclerAdapterFields recyclerAdapterFields;
    private static EntityView.GetView fieldsData;
    private static EntityDataJson dbFieldsData;
    private static String keyName, fileData = "";
    private static ArrayList<GetViewFields> getViewFields;
    private static long rowid = 0L, entity_id = 0L, rowIdStorageOrder = 0L;
    private static boolean creatingNew = false;
    private static HashMap<String, String> postJsonData = new HashMap<>();
    private static DatabaseHandler databaseHandler;
    private static HashMap<String, String> validationMap = new HashMap<>();
    private static HashMap<String, String> validationProcessMap = new HashMap<>();
    private static HashMap<Integer, HashMap<String, String>> invoiceValidationProcessMap = new HashMap<Integer, HashMap<String, String>>();
    private static List<FinsihedProduct.FinishedProd> finishProds = new ArrayList<FinsihedProduct.FinishedProd>();
    private static List<List<MultipleLines.GetKeyValue>> multipleLines = new ArrayList<>();
    private static View v;
    private static ArrayList<EntityDataJson> entityDataJsonArrayList = new ArrayList<>();
    private static ArrayList<EntityDataJson> storageDispatchArraylist = new ArrayList<>();
    private static ArrayList<EntityDataJson> storageReceiptArraylist = new ArrayList<>();
    private static ArrayList<EntityDataJson> storageInvoiceArraylist = new ArrayList<>();
    private static ArrayList<EntityDataJson> commodityLoanArrayList = new ArrayList<>();
    private static ArrayList<EntityDataJson> linkedOrdersArrayList = new ArrayList<>();
    private static ArrayList<EntityDataJson> billEntityDataJsonArrayList = new ArrayList<>();
    private static List<String> pickingIds, linkedOrderIds, linkedList, billLinkedList, storageDisLinkedList,
            commodityLoanLinkedList, storageInvoiceLinkedList, storageReceiptLinkedList,
            linkedOrdersLinkedList;
    private static LinearLayout errorsBack, linkedEntitiesLayout,
            manufHead, billsLinkLayout, amount_total_back, status_back, storageDispatchLayout,
            commodityLoanLayout, storageInvoiceLayout, storageReceiptLayout, linkedOrderLayout;
    private static ImageView back, menuOp;
    private static TextView mainHead, done, networkBar, viewErrors, status,
            home, notes, share, confirm, no_records, linkedText, billLinkedText,
            billAddLinks, bill_no_records, attachment, tech_error, bill_tech_error,
            linkedRecordsNumber, amount_total, storageDisAddLinks,
            storage_dis_no_records, storage_dis_tech_error, commodityLoan_tech_error,
            commodityLoan_no_records, commodityLoanAddLinks,
            storageInvoiceAddLinks, storageInvoice_no_records, storageInvoice_tech_error,
            storageReceipt_no_records, storageReceipt_tech_error, linkedOrder_no_records,
            linkedOrder_tech_error, linkedOrderTextView;
    private static String tempNotesKey, schema_version,
            entity_status = UrlConstant.NEW, sync_status = "", tempLotNo;
    ArrayList<String> returnValue = new ArrayList<>();
    private Dialog linkDialog, actionDialog;
    private Long rawProdId = 0L;
    private Button setToDraft, cancel, forceAvail, rollback, recheckAvail, reserve, addProduct,
            reject, approve;
    private static RecyclerView linked_recy, move_lines_recy, bill_linked_recy, multipleItemList,
            storage_dis_linked_recy, commodityLoan_linked_recy, storageInvoice_linked_recy,
            storageReceipt_linked_recy, linkedOrder_linked_recy;
    private boolean actionData = false, openNotes = false;
    private RecyclerView.LayoutManager layoutManager, layoutManagerBOM, layoutManagerMultiple,
            layoutManagerStorageDis, layoutManagerCommodityLoan, layoutManagerStorageInvoice,
            layoutManagerStorageReceipt, layoutManagerLinkedOrders;
    private RecyclerAdapterPickingLink recyclerAdapterPickingLink;
    private RecyclerAdapterStorageDispatch recyclerAdapterStorageDispatch;
    private RecyclerAdapterStorageLoan recyclerAdapterStorageLoan;
    private RecyclerAdapterLinkedOrders recyclerAdapterLinkedOrders;
    private RecyclerAdapterStorageInvoice recyclerAdapterStorageInvoice;
    private RecyclerAdapterStorageReceipt recyclerAdapterStorageReceipt;
    private RecyclerAdapterMultipleLines recyclerAdapterMultipleLines;
    private RecyclerAdapterBillPickingList recyclerAdapterBillPickingList;
    private RecyclerAdapterBOM recyclerAdapterBOM;
    private HashSet<String> requiredField = new HashSet<>();
    private HashSet<String> one2ManyrequiredField = new HashSet<>();
    private boolean actionMenu = false;
    private String[] linkKeyName = null, linkBills = null, list = null;
    private String keyLinkName = null, reason = null;
    private List<Long> billIds = new ArrayList<>();
    private List<String> attachments = new ArrayList<>();
    private ArrayList<String> imageList = new ArrayList<>();
    private Dialog dialog;
    private HashMap<Integer, File> imageFileList = new HashMap<>();
    private ArrayList<File> imageFileList1 = new ArrayList<>();
    private EntityCreationFragment receivingFragment;
    private Object selectedPayload;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ProgressBar progressBar;

    public static EntityCreationFragment newInstance() {
        return new EntityCreationFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_entity_create, null);
        showActionBar(false);
        databaseHandler = new DatabaseHandler(getActivity());
        AppPreference.getInstance().setREADNOTES(false);
        back = v.findViewById(R.id.back);
        menuOp = v.findViewById(R.id.options);
        progressBar = v.findViewById(R.id.progressBar);

        viewErrors = v.findViewById(R.id.viewErrors);
        viewErrors.setPaintFlags(viewErrors.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mainHead = v.findViewById(R.id.mainHead);
        networkBar = v.findViewById(R.id.networkBar);
        done = v.findViewById(R.id.done);
        status = v.findViewById(R.id.status);
        home = v.findViewById(R.id.home);
        notes = v.findViewById(R.id.notes);
        share = v.findViewById(R.id.share);
        no_records = v.findViewById(R.id.no_records);
        linkedText = v.findViewById(R.id.linkedText);
        linkedRecordsNumber = v.findViewById(R.id.linkedRecordsNumber);
        billLinkedText = v.findViewById(R.id.billLinkedText);
        billAddLinks = v.findViewById(R.id.billAddLinks);
        bill_no_records = v.findViewById(R.id.bill_no_records);
        bill_tech_error = v.findViewById(R.id.bill_tech_error);
        tech_error = v.findViewById(R.id.tech_error);
        attachment = v.findViewById(R.id.attachment);
        confirm = v.findViewById(R.id.confirm);
        amount_total = v.findViewById(R.id.amount_total);

        rollback = v.findViewById(R.id.rollback);
        addProduct = v.findViewById(R.id.addProduct);

        errorsBack = v.findViewById(R.id.errorsBack);
        linkedEntitiesLayout = v.findViewById(R.id.linkedEntitiesLayout);
        manufHead = v.findViewById(R.id.manufHead);
        billsLinkLayout = v.findViewById(R.id.billsLinkLayout);
        amount_total_back = v.findViewById(R.id.amount_total_back);
        status_back = v.findViewById(R.id.status_back);

        entityView = v.findViewById(R.id.entityView);
        multipleItemList = v.findViewById(R.id.multipleItemList);
        linked_recy = v.findViewById(R.id.linked_recy);
        move_lines_recy = v.findViewById(R.id.move_lines_recy);
        bill_linked_recy = v.findViewById(R.id.bill_linked_recy);

        storageDispatchLayout = v.findViewById(R.id.storageDispatchLayout);
        storageDisAddLinks = v.findViewById(R.id.storageDisAddLinks);
        storage_dis_no_records = v.findViewById(R.id.storage_dis_no_records);
        storage_dis_tech_error = v.findViewById(R.id.storage_dis_tech_error);
        storage_dis_linked_recy = v.findViewById(R.id.storage_dis_linked_recy);

        commodityLoanLayout = v.findViewById(R.id.commodityLoanLayout);
        commodityLoanAddLinks = v.findViewById(R.id.commodityLoanAddLinks);
        commodityLoan_no_records = v.findViewById(R.id.commodityLoan_no_records);
        commodityLoan_tech_error = v.findViewById(R.id.commodityLoan_tech_error);
        commodityLoan_linked_recy = v.findViewById(R.id.commodityLoan_linked_recy);

        storageInvoiceLayout = v.findViewById(R.id.storageInvoiceLayout);
        storageInvoiceAddLinks = v.findViewById(R.id.storageInvoiceAddLinks);
        storageInvoice_no_records = v.findViewById(R.id.storageInvoice_no_records);
        storageInvoice_tech_error = v.findViewById(R.id.storageInvoice_tech_error);
        storageInvoice_linked_recy = v.findViewById(R.id.storageInvoice_linked_recy);

        storageReceiptLayout = v.findViewById(R.id.storageReceiptLayout);
        storageReceipt_no_records = v.findViewById(R.id.storageReceipt_no_records);

        storageReceipt_tech_error = v.findViewById(R.id.storageReceipt_tech_error);
        storageReceipt_linked_recy = v.findViewById(R.id.storageReceipt_linked_recy);

        linkedOrderLayout = v.findViewById(R.id.linkedOrderLayout);
        linkedOrder_no_records = v.findViewById(R.id.linkedOrder_no_records);
        linkedOrder_tech_error = v.findViewById(R.id.linkedOrder_tech_error);
        linkedOrder_linked_recy = v.findViewById(R.id.linkedOrder_linked_recy);
        linkedOrderTextView = v.findViewById(R.id.linkedOrderTextView);

        linkedOrder_linked_recy.setNestedScrollingEnabled(false);
        storageReceipt_linked_recy.setNestedScrollingEnabled(false);
        storageInvoice_linked_recy.setNestedScrollingEnabled(false);
        commodityLoan_linked_recy.setNestedScrollingEnabled(false);
        storage_dis_linked_recy.setNestedScrollingEnabled(false);
        linked_recy.setNestedScrollingEnabled(false);
        entityView.setNestedScrollingEnabled(false);
        move_lines_recy.setNestedScrollingEnabled(false);
        bill_linked_recy.setNestedScrollingEnabled(false);
        multipleItemList.setNestedScrollingEnabled(false);
        layoutManager1 = new LinearLayoutManager(getActivity());

        storageDisAddLinks.setOnClickListener(this);
        commodityLoanAddLinks.setOnClickListener(this);
        storageInvoiceAddLinks.setOnClickListener(this);

        back.setOnClickListener(this);
        addProduct.setOnClickListener(this);
        menuOp.setOnClickListener(this);
        done.setOnClickListener(this);
        home.setOnClickListener(this);
        notes.setOnClickListener(this);
        share.setOnClickListener(this);
        confirm.setOnClickListener(this);
        rollback.setOnClickListener(this);
        billAddLinks.setOnClickListener(this);
        attachment.setOnClickListener(this);
        viewErrors.setOnClickListener(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            keyName = bundle.getString("KEY_NAME");
            rowid = bundle.getLong("ROW_ID");
            openNotes = bundle.getBoolean("NOTES");
            creatingNew = bundle.getBoolean("CREATING_NEW");
            postJsonData = (HashMap<String, String>) bundle.getSerializable("JSON_DATA");
        }
        if (keyName.equals(UrlConstant.VENDOR_BILL)) {
            addProduct.setText("Less Deduction");
        } else if (keyName.equals(UrlConstant.MANDI_PRICE) || keyName.equals(UrlConstant.NODE_PRICE)) {
            addProduct.setText("Add Product");
            addProduct.setVisibility(View.VISIBLE);
        } else if (keyName.equals(UrlConstant.PAYMENT_REQUEST)) {
            addProduct.setText("Add Payment Line");
        }
        linkedList = new ArrayList<>();
        billLinkedList = new ArrayList<>();
        storageDisLinkedList = new ArrayList<>();
        commodityLoanLinkedList = new ArrayList<>();
        storageInvoiceLinkedList = new ArrayList<>();
        storageReceiptLinkedList = new ArrayList<>();
        linkedOrdersLinkedList = new ArrayList<>();
        tempLotNo = "T_" + getTimeStamp() + "_" + AppPreference.getInstance().getUSER_ID();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        AppUtils.showBanner(getActivity(), networkBar);
        entity_id = 0L;
        if (AppUtils.haveNetworkConnection(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            fetchSchema();
        } else {
            loadData(getActivity());
        }
        AppPreference.getInstance().setDATA_CHANGE_FLAG(false);

        if (openNotes)
            notes.performClick();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        return v;
    }

    private void fetchSchema() {
        String key = UrlConstant.key_mapping.get(keyName).get("key");
        AppRestClient client = AppRestClient.getInstance();
        Call<EntityView> call = client.getEntitySchema(key);
        call.enqueue(new Callback<EntityView>() {
            @Override
            public void onResponse(Call<EntityView> call, Response<EntityView> response) {
                if (response.body() == null || response.body().getData() == null) return;
                if (AppUtils.haveNetworkConnection(activity))
                    if (response.code() == 200)
                        storeDataDB(activity, response.body().getData(), true);
                    else if (response.code() == 500)
                        AppUtils.showToast(UrlConstant.SERVER_ERROR);
                loadData(getActivity());
            }

            @Override
            public void onFailure(Call<EntityView> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                if (t instanceof ConnectException) {
                    if (AppUtils.haveNetworkConnection(activity))
                        AppUtils.showToast(UrlConstant.SERVER_NOT_RES);
                } else
                    AppUtils.showToast(UrlConstant.TECH_PROB);
            }
        });
    }

    private void prepareActions(Activity activity) {
        if (activity == null)
            return;
        actionDialog = new Dialog(activity);
        actionDialog.setCanceledOnTouchOutside(true);
        actionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        actionDialog.setContentView(R.layout.dialog_actions);

        setToDraft = actionDialog.findViewById(R.id.setToDraft);
        forceAvail = actionDialog.findViewById(R.id.forceAvail);
        recheckAvail = actionDialog.findViewById(R.id.recheckAvail);
        cancel = actionDialog.findViewById(R.id.cancel);
        reserve = actionDialog.findViewById(R.id.reserve);
        approve = actionDialog.findViewById(R.id.approve);
        reject = actionDialog.findViewById(R.id.reject);
        setToDraft.setOnClickListener(this);
        cancel.setOnClickListener(this);
        reserve.setOnClickListener(this);
        forceAvail.setOnClickListener(this);
        recheckAvail.setOnClickListener(this);
        approve.setOnClickListener(this);
        reject.setOnClickListener(this);

        if (fieldsData.getSchema().getActions() != null)
            visibleActions();

        actionDialog.getWindow().setBackgroundDrawable(new ColorDrawable
                (android.graphics.Color.TRANSPARENT));
        actionDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        actionDialog.getWindow().setGravity(Gravity.TOP | Gravity.RIGHT);
    }

    private void loadData(Activity activity) {
        billLinkedList = new ArrayList<>();
        linkedList = new ArrayList<>();
        entity_status = UrlConstant.NEW;
        status_back.setVisibility(View.VISIBLE);
        if (postJsonData == null) {
            postJsonData = new HashMap<>();
            postJsonData.clear();
        }
        validationMap.clear();
        validationProcessMap.clear();
        invoiceValidationProcessMap.clear();
        if (rowid == 0) {
            attachment.setVisibility(View.GONE);
            if (keyName.equals(UrlConstant.SALES_ORDER) ||
                    keyName.equals(UrlConstant.PURCHASE_ORDER)) {
                if (AppPreference.getInstance().getWAREHOUSE_ID() != null &&
                        AppPreference.getInstance().getWAREHOUSE_ID().size() > 0)
                    postJsonData.put(UrlConstant.WAREHOUSE_ID,
                            String.valueOf(AppPreference.getInstance().getWAREHOUSE_ID().get(0)));
            }
        }
        if (keyName.equals(UrlConstant.VENDOR_BILL)) {
            amount_total_back.setVisibility(View.VISIBLE);
        }
        dbFieldsData = null;
        if (keyName.equals(UrlConstant.PURCHASE_RECEIPT)) {
            postJsonData.put("lot_number", tempLotNo);
        }

        dbFieldsData = databaseHandler.getEntitySingleListData(rowid);
        attachment.setText("0 Attachments(s)");
        //locking the data for the row
        databaseHandler.updateLockData(rowid);
        if (dbFieldsData != null) {
            if (dbFieldsData.getAttachments() != null) {
                Gson gson = new Gson();
                attachments = gson.fromJson(dbFieldsData.getAttachments(),
                        new TypeToken<ArrayList<String>>() {
                        }.getType());
            }
            if (attachments != null && attachments.size() > 0)
                attachment.setText(attachments.size() + " Attachments(s)");
            if (dbFieldsData.getJson_payload() != null) {
                Gson gson = new Gson();
                ArrayList<JsonDataStore> arrayList = gson.fromJson(
                        dbFieldsData.getJson_payload(),
                        new TypeToken<ArrayList<JsonDataStore>>() {
                        }.getType());
                if (arrayList != null)
                    for (int i = 0; i < arrayList.size(); i++) {
                        postJsonData.put(arrayList.get(i).getKey(), arrayList.get(i).getValue());
                    }
            }
            if (!AppUtils.isNullCase(postJsonData.get(UrlConstant.PICKING_COUNT))) {
                int count = Integer.parseInt(postJsonData.get(UrlConstant.PICKING_COUNT));
                if (keyName.equals(UrlConstant.PURCHASE_RECEIPT) ||
                        (keyName.equals(UrlConstant.SALES_DISPATCH)) ||
                        keyName.equals(UrlConstant.EXPENSE_BILL)) {
                    billsLinkLayout.setVisibility(View.VISIBLE);
                    billLinkedText.setVisibility(View.VISIBLE);
                    if (keyName.equals(UrlConstant.EXPENSE_BILL)) {
                        billLinkedText.setText("Linked Voucher/GRN");
                        billAddLinks.setText("Add Link");
                    } else
                        billLinkedText.setText("Linked Voucher");
                    if (count > 0)
                        if (!AppUtils.isNullCase(postJsonData.get(UrlConstant.PICKING_IDS))) {
                            String value = postJsonData.get(UrlConstant.PICKING_IDS);
                            String tempData = value.replace(" ", "");
                            value = tempData.trim();
                            billLinkedList = Arrays.asList(value.substring(1,
                                    value.length() - 1).split(","));
                        }
                } else {
                    linkedEntitiesLayout.setVisibility(View.VISIBLE);
                    linkedText.setVisibility(View.VISIBLE);
                    if (keyName.equals(UrlConstant.PURCHASE_ORDER))
                        linkedText.setText("Linked GRN");
                    else
                        linkedText.setText("Linked Voucher");

                    if (count > 0) {
                        linkedRecordsNumber.setText(count + " Record(s)");
                        if (!AppUtils.isNullCase(postJsonData.get(UrlConstant.PICKING_IDS))) {
                            String value = postJsonData.get(UrlConstant.PICKING_IDS);
                            String tempData = value.replace(" ", "");
                            value = tempData.trim();
                            linkedList = Arrays.asList(value.substring(1,
                                    value.length() - 1).split(","));
                        }
                    }
                }
            }
            if (!AppUtils.isNullCase(postJsonData.get(UrlConstant.INVOICE_COUNT))) {
                int count = Integer.parseInt(postJsonData.get(UrlConstant.INVOICE_COUNT));
                billsLinkLayout.setVisibility(View.VISIBLE);
                billLinkedText.setVisibility(View.VISIBLE);
                if (count > 0)
                    if (!AppUtils.isNullCase(postJsonData.get(UrlConstant.INVOICE_IDS))) {
                        String value = postJsonData.get(UrlConstant.INVOICE_IDS);
                        String tempData = value.replace(" ", "");
                        value = tempData.trim();
                        billLinkedList = Arrays.asList(value.substring(1,
                                value.length() - 1).split(","));
                    }
                if (keyName.equals(UrlConstant.PURCHASE_ORDER)) {
                    billLinkedText.setText("Vendor Bill");
                    billAddLinks.setText("Create");
                    if (dbFieldsData.getEntity_status().equals("draft") && count <= 0) {
                        billsLinkLayout.setVisibility(View.GONE);
                        billLinkedText.setVisibility(View.GONE);
                    }
                } else {
                    billLinkedText.setText("Linked Expenses");
                    billAddLinks.setText("Add Link");
                }
            }
            if (!AppUtils.isNullCase(postJsonData.get(UrlConstant.LINKED_ORDER_ID))) {
                if (keyName.equals(UrlConstant.VENDOR_BILL)) {
                    linkedEntitiesLayout.setVisibility(View.VISIBLE);
                    linkedText.setVisibility(View.VISIBLE);
                    linkedText.setText("Linked Order");
                }
                linkedList.add(postJsonData.get(UrlConstant.LINKED_ORDER_ID));
                linkedRecordsNumber.setText("1" + " Record(s)");
            }

            if (dbFieldsData.getSync_status().equals(UrlConstant.SUCCESS) ||
                    dbFieldsData.getSync_status().equals(UrlConstant.PENDING))
                errorsBack.setVisibility(View.GONE);
            else
                errorsBack.setVisibility(View.VISIBLE);

            entity_id = dbFieldsData.getEntity_id();
            if (dbFieldsData.getEntity_status() != null)
                entity_status = dbFieldsData.getEntity_status();
            sync_status = dbFieldsData.getSync_status();

            if (!AppUtils.isNullCase(dbFieldsData.getEntity_status())) {
                String displayStatusName = AppUtils.getDisplayNameStatus(
                        activity, dbFieldsData.getEntity_status(), keyName);
                if (!AppUtils.isNullCase(displayStatusName))
                    status.setText(displayStatusName);
            }
        }

        fieldsData = databaseHandler.getSchemaSingleView(keyName);
        if (fieldsData == null)
            return;
        ArrayList<GetViewFields> getViewFieldsArrayList = fieldsData.getSchema().getFields();
        for (int temp = 0; temp < getViewFieldsArrayList.size(); temp++) {
            if (rowid == 0)
                if (getViewFieldsArrayList.get(temp).getDefaultText() != null) {
                    postJsonData.put(getViewFieldsArrayList.get(temp).getKey(),
                            getViewFieldsArrayList.get(temp).getDefaultText());
                }

            if (getViewFieldsArrayList.get(temp).getType().equals(UrlConstant.NOTE))
                tempNotesKey = getViewFieldsArrayList.get(temp).getKey();
        }

        schema_version = fieldsData.getVersion();
        getViewFields = fieldsData.getSchema().getFields();
        mainHead.setText(fieldsData.getDisplay_name());

        if ((keyName.equals(UrlConstant.PURCHASE_RECEIPT)) ||
                (keyName.equals(UrlConstant.SALES_DISPATCH))) {
            if (!entity_status.equals(UrlConstant.NEW)) {
                //order layout in transfers
                linkedEntitiesLayout.setVisibility(View.VISIBLE);
                linkedText.setVisibility(View.VISIBLE);
                if (keyName.equals(UrlConstant.PURCHASE_RECEIPT))
                    linkedText.setText("Linked Purchase Order");
                else
                    linkedText.setText("Linked Sales Order");
            }

        }

        if (keyName.equals(UrlConstant.SALES_ORDER)) {
            linkKeyName = new String[]{UrlConstant.SALES_DISPATCH};
            keyLinkName = UrlConstant.SALES_DISPATCH;
        } else if (keyName.equals(UrlConstant.PURCHASE_ORDER)) {
            linkKeyName = new String[]{UrlConstant.PURCHASE_RECEIPT};
            keyLinkName = UrlConstant.PURCHASE_RECEIPT;
            linkBills = new String[]{UrlConstant.VENDOR_BILL};
        } else if (keyName.equals(UrlConstant.SALES_DISPATCH)) {
            linkKeyName = new String[]{UrlConstant.SALES_ORDER};
            keyLinkName = UrlConstant.SALES_ORDER;
            linkBills = new String[]{UrlConstant.EXPENSE_BILL};
        } else if (keyName.equals(UrlConstant.PURCHASE_RECEIPT)) {
            linkKeyName = new String[]{UrlConstant.PURCHASE_ORDER};
            keyLinkName = UrlConstant.PURCHASE_ORDER;
            linkBills = new String[]{UrlConstant.EXPENSE_BILL};
        } else if (keyName.equals(UrlConstant.VENDOR_BILL)) {
            linkKeyName = new String[]{UrlConstant.PURCHASE_ORDER};
            keyLinkName = UrlConstant.PURCHASE_ORDER;
        } else if (keyName.equals(UrlConstant.EXPENSE_BILL)) {
            linkBills = new String[]{UrlConstant.PURCHASE_RECEIPT,
                    UrlConstant.SALES_DISPATCH};
        } else if (keyName.equals(UrlConstant.STORAGE_INVOICE)
                || keyName.equals(UrlConstant.STORAGE_RECEIPT)
                || keyName.equals(UrlConstant.STORAGE_DISPATCH)
                || keyName.equals(UrlConstant.COMMODITY_LOAN_INVOICE)) {

            if (!AppUtils.isNullCase(postJsonData.get(UrlConstant.LINKED_ORDER_ID))) {
                linkedOrderLayout.setVisibility(View.VISIBLE);
                linkedOrdersLinkedList.add(postJsonData.get(UrlConstant.LINKED_ORDER_ID));
                if (linkedOrdersLinkedList.size() == 0 ||
                        AppUtils.isNullCase(linkedOrdersLinkedList.get(0)))
                    linkedOrder_no_records.setVisibility(View.VISIBLE);
                else {
                    getLinkedOrdersList(linkedOrdersLinkedList);
                }
            }

            if (keyName.equals(UrlConstant.COMMODITY_LOAN_INVOICE)) {
                linkedOrderTextView.setText("Linked Commodity Loan");
            }
        } else if (keyName.equals(UrlConstant.COMMODITY_LOAN)) {

            if (!AppUtils.isNullCase(postJsonData.get("storage_order_ids"))) {
                linkedOrderLayout.setVisibility(View.VISIBLE);
                String value = postJsonData.get("storage_order_ids");
                String tempData = value.replace(" ", "");
                value = tempData.trim();
                linkedOrdersLinkedList = Arrays.asList(value.substring(1,
                        value.length() - 1).split(","));

                if (linkedOrdersLinkedList.size() == 0 ||
                        AppUtils.isNullCase(linkedOrdersLinkedList.get(0)))
                    linkedOrder_no_records.setVisibility(View.VISIBLE);
                else {
                    getLinkedOrdersList(linkedOrdersLinkedList);
                }
            }
            if (!AppUtils.isNullCase(postJsonData.get(UrlConstant.INVOICE_IDS))) {
                storageInvoiceLayout.setVisibility(View.VISIBLE);
                String value = postJsonData.get(UrlConstant.INVOICE_IDS);
                String tempData = value.replace(" ", "");
                value = tempData.trim();
                storageInvoiceLinkedList = Arrays.asList(value.substring(1,
                        value.length() - 1).split(","));

                if (storageInvoiceLinkedList.size() == 0 || AppUtils.isNullCase(storageInvoiceLinkedList.get(0)))
                    storageInvoice_no_records.setVisibility(View.VISIBLE);
                else {
                    getStorageInvoiceList(storageInvoiceLinkedList);
                }
            }

            linkKeyName = new String[]{UrlConstant.STORAGE_ORDER};
        } else if (keyName.equals(UrlConstant.STORAGE_ORDER)) {

            if (!AppUtils.isNullCase(postJsonData.get(UrlConstant.OUT_PICKING_IDS))) {
                storageDispatchLayout.setVisibility(View.VISIBLE);
                String value = postJsonData.get(UrlConstant.OUT_PICKING_IDS);
                String tempData = value.replace(" ", "");
                value = tempData.trim();
                storageDisLinkedList = Arrays.asList(value.substring(1,
                        value.length() - 1).split(","));

                if (storageDisLinkedList.size() == 0 || AppUtils.isNullCase(storageDisLinkedList.get(0)))
                    storage_dis_no_records.setVisibility(View.VISIBLE);
                else {
                    getStorageDispatchList(storageDisLinkedList);
                }
            }
            if (!AppUtils.isNullCase(postJsonData.get(UrlConstant.LOAN_IDS))) {
                commodityLoanLayout.setVisibility(View.VISIBLE);
                String value = postJsonData.get(UrlConstant.LOAN_IDS);
                String tempData = value.replace(" ", "");
                value = tempData.trim();
                commodityLoanLinkedList = Arrays.asList(value.substring(1,
                        value.length() - 1).split(","));

                if (commodityLoanLinkedList.size() == 0 || AppUtils.isNullCase(commodityLoanLinkedList.get(0)))
                    commodityLoan_no_records.setVisibility(View.VISIBLE);
                else {
                    getCommodityLoanList(commodityLoanLinkedList);
                }
            }
            if (!AppUtils.isNullCase(postJsonData.get(UrlConstant.INVOICE_IDS))) {
                storageInvoiceLayout.setVisibility(View.VISIBLE);
                String value = postJsonData.get(UrlConstant.INVOICE_IDS);
                String tempData = value.replace(" ", "");
                value = tempData.trim();
                storageInvoiceLinkedList = Arrays.asList(value.substring(1,
                        value.length() - 1).split(","));

                if (storageInvoiceLinkedList.size() == 0 || AppUtils.isNullCase(storageInvoiceLinkedList.get(0)))
                    storageInvoice_no_records.setVisibility(View.VISIBLE);
                else {
                    getStorageInvoiceList(storageInvoiceLinkedList);
                }
            }
            if (!AppUtils.isNullCase(postJsonData.get(UrlConstant.INC_PICKING_IDS))) {
                storageReceiptLayout.setVisibility(View.VISIBLE);
                String value = postJsonData.get(UrlConstant.INC_PICKING_IDS);
                String tempData = value.replace(" ", "");
                value = tempData.trim();
                storageReceiptLinkedList = Arrays.asList(value.substring(1,
                        value.length() - 1).split(","));

                if (storageReceiptLinkedList.size() == 0 || AppUtils.isNullCase(storageReceiptLinkedList.get(0)))
                    storageReceipt_no_records.setVisibility(View.VISIBLE);
                else {
                    getStorageReceiptList(storageReceiptLinkedList);
                }
            }
        }
        if (linkedList.size() == 0)
            no_records.setVisibility(View.VISIBLE);
        else {
            openPickingListDialog(linkedList);
        }
        if (billLinkedList.size() == 0)
            bill_no_records.setVisibility(View.VISIBLE);
        else {
            openBillListDialog(billLinkedList);
        }

        ArrayList<EntityView.GetView.GetSchema.GetActions> getActionsArrayList =
                fieldsData.getSchema().getActions();
        if (getActionsArrayList != null)
            for (int i = 0; i < getActionsArrayList.size(); i++) {
                if (getActionsArrayList.get(i).getKey().equals(UrlConstant.CONFIRM))
                    if (!AppUtils.isNullCase(entity_status))
                        if (!AppUtils.getReadEditState(getActionsArrayList.get(i).getAttributes().getInvisible(),
                                postJsonData, entity_status)) {
                            confirm.setVisibility(View.VISIBLE);
                            confirm.setText(getActionsArrayList.get(i).getDisplay_name());
                        }

            }
        displayData(activity);
        prepareActions(activity);
        if (progressBar != null)
            progressBar.setVisibility(View.GONE);
    }

    private void visibleActions() {
        ArrayList<EntityView.GetView.GetSchema.GetActions> getActionsArrayList =
                fieldsData.getSchema().getActions();
        for (int i = 0; i < getActionsArrayList.size(); i++) {
            if (AppUtils.isNullCase(entity_status))
                return;

            if (getActionsArrayList.get(i).getKey().equals(UrlConstant.CANCEL)) {
                if (!AppUtils.getReadEditState(getActionsArrayList.get(i).getAttributes().getInvisible(),
                        postJsonData, entity_status)) {
                    cancel.setVisibility(View.VISIBLE);
                    actionMenu = true;
                    cancel.setText(getActionsArrayList.get(i).getDisplay_name());
                }
            } else if (getActionsArrayList.get(i).getKey().equals(UrlConstant.SETTODRAFT)) {
                if (!AppUtils.getReadEditState(getActionsArrayList.get(i).getAttributes().getInvisible(),
                        postJsonData, entity_status)) {
                    setToDraft.setVisibility(View.VISIBLE);
                    actionMenu = true;
                    setToDraft.setText(getActionsArrayList.get(i).getDisplay_name());
                }
            } else if (getActionsArrayList.get(i).getKey().equals(UrlConstant.FORCE_AVAIL)) {
                if (!AppUtils.getReadEditState(getActionsArrayList.get(i).getAttributes().getInvisible(),
                        postJsonData, entity_status)) {
                    forceAvail.setVisibility(View.VISIBLE);
                    actionMenu = true;
                    forceAvail.setText(getActionsArrayList.get(i).getDisplay_name());
                }
            } else if (getActionsArrayList.get(i).getKey().equals(UrlConstant.RECHECK_AVAIL)) {
                if (!AppUtils.getReadEditState(getActionsArrayList.get(i).getAttributes().getInvisible(),
                        postJsonData, entity_status)) {
                    recheckAvail.setVisibility(View.VISIBLE);
                    actionMenu = true;
                    recheckAvail.setText(getActionsArrayList.get(i).getDisplay_name());
                }
            } else if (getActionsArrayList.get(i).getKey().equals(UrlConstant.RESERVE)) {
                if (!AppUtils.getReadEditState(getActionsArrayList.get(i).getAttributes().getInvisible(),
                        postJsonData, entity_status)) {
                    reserve.setVisibility(View.VISIBLE);
                    actionMenu = true;
                    reserve.setText(getActionsArrayList.get(i).getDisplay_name());
                }
            } else if (getActionsArrayList.get(i).getKey().equals(UrlConstant.APPROVE)) {
                if (!AppUtils.getReadEditState(getActionsArrayList.get(i).getAttributes().getInvisible(),
                        postJsonData, entity_status)) {
                    approve.setVisibility(View.VISIBLE);
                    actionMenu = true;
                    approve.setText(getActionsArrayList.get(i).getDisplay_name());
                }
            } else if (getActionsArrayList.get(i).getKey().equals(UrlConstant.REJECT)) {
                if (!AppUtils.getReadEditState(getActionsArrayList.get(i).getAttributes().getInvisible(),
                        postJsonData, entity_status)) {
                    reject.setVisibility(View.VISIBLE);
                    actionMenu = true;
                    reject.setText(getActionsArrayList.get(i).getDisplay_name());
                }
            }
        }
        if (actionMenu) {
            menuOp.setVisibility(View.VISIBLE);
        }
    }

    private void displayData(Activity activity) {
        fieldsData = databaseHandler.getSchemaSingleView(keyName);
        if (keyName.equals(UrlConstant.PARTNER_CONTACT)) {
            Bundle args = getArguments();
            if (args.get(UrlConstant.PRIMARY_NUMBER) != null) {
                postJsonData.put(UrlConstant.PRIMARY_NUMBER,
                        args.get(UrlConstant.PRIMARY_NUMBER).toString());
            }
        }

        recyclerAdapterFields = new RecyclerAdapterFields(
                activity, keyName, fieldsData.getSchema().getFields(),
                postJsonData, validationMap, entity_status, this, this,
                this, this);
        if (selectedPayload != null) {
            recyclerAdapterFields.setSelectedPayload(selectedPayload);
        }
        entityView.setLayoutManager(layoutManager1);
        entityView.setAdapter(recyclerAdapterFields);
        if (keyName.equals(UrlConstant.MANUFACTURING_ORDER)) {
            move_lines_recy.setVisibility(View.VISIBLE);
            displayBomData();
        }
    }

    private void displayBomData() {
        manufHead.setVisibility(View.VISIBLE);
        finishProds = new ArrayList<>();
        finishProds.clear();
        String finished_moves = postJsonData.get("finished_moves");
        if ((rowid != 0) || (!AppUtils.isNullCase(finished_moves))) {
            if (!AppUtils.isNullCase(finished_moves)) {
                String finLines = finished_moves;
                Gson gson = new Gson();
                finishProds = gson.fromJson(finLines, FinsihedProduct.class).getFinished_moves();
            }
        } else {
            ArrayList<GetViewFields.MappingValues> bomList;
            for (int i = 0; i < getViewFields.size(); i++) {
                if (getViewFields.get(i).getType().equals(UrlConstant.BILL_OF_MATERIAL)) {
                    bomList = getViewFields.get(i).getMapping_values();
                    for (int j = 0; j < bomList.size(); j++) {
                        if (("" + rawProdId).equals("" + bomList.get(j).getRaw_product_id())) {
                            for (int k = 0; k < bomList.get(j).getFinished_products().size(); k++) {
                                ArrayList<GetViewFields.MappingValues.FinishProd> finishProdArrayList =
                                        bomList.get(j).getFinished_products();
                                FinsihedProduct.FinishedProd finishedProd =
                                        new FinsihedProduct.FinishedProd();
                                finishedProd.setProduct_id(finishProdArrayList.get(k).getProduct_id());
                                finishedProd.setLot_number(null);
                                finishedProd.setProduct_qty(0f);
                                finishedProd.setProduct_uom(finishProdArrayList.get(k).getProduct_uom_id());
                                finishProds.add(finishedProd);
                            }
                            FinsihedProduct finsihedProduct = new FinsihedProduct();
                            finsihedProduct.setFinished_moves(finishProds);
                            Gson gson = new Gson();
                            String jsonData = gson.toJson(finsihedProduct);
                            postJsonData.put("finished_moves", jsonData);
                            postJsonData.put(getViewFields.get(i).getKey(),
                                    String.valueOf(bomList.get(j).getId()));
                        }
                    }
                    break;
                }
            }
        }
        layoutManagerBOM = new LinearLayoutManager(getActivity());
        recyclerAdapterBOM = new RecyclerAdapterBOM(getActivity(), getViewFields,
                finishProds, postJsonData, entity_status, validationProcessMap,
                this);
        move_lines_recy.setLayoutManager(layoutManagerBOM);
        move_lines_recy.setAdapter(recyclerAdapterBOM);
    }

    private void displayMultipleItems(boolean add, boolean button) {
        if (button)
            addProduct.setVisibility(View.VISIBLE);
        else
            addProduct.setVisibility(View.GONE);
        if (add) {
            Gson gson = new Gson();
            String jsonString = null;
            List<List<MultipleLines.GetKeyValue>> getKeyValueList = new ArrayList<>();
            List<MultipleLines.GetKeyValue> list = new ArrayList<>();
            if (postJsonData.get(UrlConstant.INVOICE_LINE_IDS) == null) {
                MultipleLines.GetKeyValue getKeyValue = new MultipleLines.GetKeyValue();
                getKeyValue.setKey(UrlConstant.PRODUCT_ID);
                getKeyValue.setValue("");
                list.add(getKeyValue);
                getKeyValueList.add(list);
                MultipleLines multipleLines = new MultipleLines();
                multipleLines.setInvoice_line_ids(getKeyValueList);
                jsonString = gson.toJson(multipleLines);
            } else {
                String invoice_line_ids = postJsonData.get(UrlConstant.INVOICE_LINE_IDS);
                getKeyValueList = gson.fromJson(invoice_line_ids,
                        MultipleLines.class).getInvoice_line_ids();
                MultipleLines.GetKeyValue getKeyValue = new MultipleLines.GetKeyValue();
                getKeyValue.setKey(UrlConstant.PRODUCT_ID);
                getKeyValue.setValue("");
                list.add(getKeyValue);
                getKeyValueList.add(list);
                MultipleLines multipleLines = new MultipleLines();
                multipleLines.setInvoice_line_ids(getKeyValueList);
                jsonString = gson.toJson(multipleLines);
            }
            postJsonData.put(UrlConstant.INVOICE_LINE_IDS, jsonString);
            AppPreference.getInstance().setDATA_CHANGE_FLAG(true);
        }
        multipleLines.clear();
        String invoice_line_ids = postJsonData.get(UrlConstant.INVOICE_LINE_IDS);
        if ((rowid != 0) || (!AppUtils.isNullCase(invoice_line_ids))) {
            if (!AppUtils.isNullCase(invoice_line_ids)) {
                Gson gson = new Gson();
                multipleLines = gson.fromJson(invoice_line_ids,
                        MultipleLines.class).getInvoice_line_ids();
            }
        }
        multipleItemList.setVisibility(View.VISIBLE);
        layoutManagerMultiple = new LinearLayoutManager(getActivity());
        recyclerAdapterMultipleLines = new RecyclerAdapterMultipleLines(
                getActivity(), keyName, getViewFields, multipleLines, entity_status,
                invoiceValidationProcessMap, this, this);
        multipleItemList.setLayoutManager(layoutManagerMultiple);
        multipleItemList.setAdapter(recyclerAdapterMultipleLines);
    }

    private void displayPriceData(boolean add, boolean button) {
        if (addProduct != null)
            if (button)
                addProduct.setVisibility(View.VISIBLE);
            else
                addProduct.setVisibility(View.GONE);
        if (add) {
            Gson gson = new Gson();
            String jsonString = null;
            List<List<MultipleLines.GetKeyValue>> getKeyValueList = new ArrayList<>();
            List<MultipleLines.GetKeyValue> list = new ArrayList<>();
            if (postJsonData.get(UrlConstant.PRODUCT_PRICE_LINES) == null) {
                MultipleLines.GetKeyValue getKeyValue = new MultipleLines.GetKeyValue();
                getKeyValue.setKey(UrlConstant.PRODUCT_ID);
                getKeyValue.setValue("");
                list.add(getKeyValue);
                getKeyValueList.add(list);
                MultipleLines multipleLines = new MultipleLines();
                multipleLines.setProduct_price(getKeyValueList);
                jsonString = gson.toJson(multipleLines);
            } else {
                String product_prices = postJsonData.get(UrlConstant.PRODUCT_PRICE_LINES);
                getKeyValueList = gson.fromJson(product_prices,
                        MultipleLines.class).getProduct_price();
                MultipleLines.GetKeyValue getKeyValue = new MultipleLines.GetKeyValue();
                getKeyValue.setKey(UrlConstant.PRODUCT_ID);
                getKeyValue.setValue("");
                list.add(getKeyValue);
                getKeyValueList.add(list);
                MultipleLines multipleLines = new MultipleLines();
                multipleLines.setProduct_price(getKeyValueList);
                jsonString = gson.toJson(multipleLines);
            }
            postJsonData.put(UrlConstant.PRODUCT_PRICE_LINES, jsonString);
            AppPreference.getInstance().setDATA_CHANGE_FLAG(true);
        }
        multipleLines.clear();
        String product_prices = postJsonData.get(UrlConstant.PRODUCT_PRICE_LINES);
        if ((rowid != 0) || (!AppUtils.isNullCase(product_prices))) {
            if (!AppUtils.isNullCase(product_prices)) {
                Gson gson = new Gson();
                multipleLines = gson.fromJson(product_prices,
                        MultipleLines.class).getProduct_price();
            }
        }
        multipleItemList.setVisibility(View.VISIBLE);
        layoutManagerMultiple = new LinearLayoutManager(getActivity());
        recyclerAdapterMultipleLines = new RecyclerAdapterMultipleLines(getActivity(), keyName,
                getViewFields, multipleLines, entity_status, invoiceValidationProcessMap, this, this);
        multipleItemList.setLayoutManager(layoutManagerMultiple);
        multipleItemList.setAdapter(recyclerAdapterMultipleLines);
    }

    private void displayPaymentData(boolean add, boolean button) {
        if (addProduct != null)
            if (button)
                addProduct.setVisibility(View.VISIBLE);
            else
                addProduct.setVisibility(View.GONE);
        if (add) {
            Gson gson = new Gson();
            String jsonString = null;
            List<List<MultipleLines.GetKeyValue>> getKeyValueList = new ArrayList<>();
            List<MultipleLines.GetKeyValue> list = new ArrayList<>();
            if (postJsonData.get(UrlConstant.REQUEST_LINE_IDS) == null) {
                MultipleLines.GetKeyValue getKeyValue = new MultipleLines.GetKeyValue();
                getKeyValue.setKey(UrlConstant.APPROVED_AMOUNT);
                getKeyValue.setValue(postJsonData.get("requested_amount"));
                list.add(getKeyValue);
                getKeyValueList.add(list);
                MultipleLines multipleLines = new MultipleLines();
                multipleLines.setRequest_line_ids(getKeyValueList);
                jsonString = gson.toJson(multipleLines);
            } else {
                String product_prices = postJsonData.get(UrlConstant.REQUEST_LINE_IDS);
                getKeyValueList = gson.fromJson(product_prices,
                        MultipleLines.class).getRequest_line_ids();
                float amount_left;
                if (getKeyValueList.size() == 0)
                    amount_left = Float.parseFloat(postJsonData.get("requested_amount"));
                else {
                    amount_left = 0f;
                    for (int i = 0; i < getKeyValueList.size(); i++)
                        for (int j = 0; j < getKeyValueList.get(i).size(); j++)
                            if (getKeyValueList.get(i).get(j).getKey().equals(UrlConstant.APPROVED_AMOUNT))
                                amount_left += Float.parseFloat(getKeyValueList.get(i).get(j).getValue());
                    float final_amt = Float.parseFloat(postJsonData.get("requested_amount")) - amount_left;
                    if (final_amt <= 0)
                        amount_left = 0f;
                    else
                        amount_left = final_amt;
                }
                MultipleLines.GetKeyValue getKeyValue = new MultipleLines.GetKeyValue();
                getKeyValue.setKey(UrlConstant.APPROVED_AMOUNT);
                getKeyValue.setValue("" + amount_left);
                list.add(getKeyValue);
                getKeyValueList.add(list);
                MultipleLines multipleLines = new MultipleLines();
                multipleLines.setRequest_line_ids(getKeyValueList);
                jsonString = gson.toJson(multipleLines);
            }
            postJsonData.put(UrlConstant.REQUEST_LINE_IDS, jsonString);
            AppPreference.getInstance().setDATA_CHANGE_FLAG(true);
        }
        multipleLines.clear();
        String product_prices = postJsonData.get(UrlConstant.REQUEST_LINE_IDS);
        if ((rowid != 0) || (!AppUtils.isNullCase(product_prices))) {
            if (!AppUtils.isNullCase(product_prices)) {
                Gson gson = new Gson();
                multipleLines = gson.fromJson(product_prices,
                        MultipleLines.class).getRequest_line_ids();
            }
        }
        multipleItemList.setVisibility(View.VISIBLE);
        layoutManagerMultiple = new LinearLayoutManager(getActivity());
        recyclerAdapterMultipleLines = new RecyclerAdapterMultipleLines(getActivity(), keyName,
                getViewFields, multipleLines, entity_status, invoiceValidationProcessMap, this, this);
        multipleItemList.setLayoutManager(layoutManagerMultiple);
        multipleItemList.setAdapter(recyclerAdapterMultipleLines);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        EntityCreationFragment fragment = EntityCreationFragment.newInstance();
        Bundle bundle = new Bundle();
        HashMap<String, String> tempMapData = new HashMap<>();
        Gson gson = new Gson();
        String jsonString = null;
        Float product;
        List<List<MultipleLines.GetKeyValue>> getKeyValueList = new ArrayList<>();
        List<MultipleLines.GetKeyValue> list = new ArrayList<>();
        MultipleLines.GetKeyValue getKeyValue = new MultipleLines.GetKeyValue();
        MultipleLines multipleLines = new MultipleLines();
        Float total;

        switch (id) {
            case R.id.back:
                checkData(getActivity(), null);
                break;
            case R.id.storageDisAddLinks:
                bundle.putString(UrlConstant.KEY_NAME, UrlConstant.STORAGE_DISPATCH);
                bundle.putLong(UrlConstant.ROW_ID, 0);
                bundle.putBoolean(UrlConstant.NOTES, false);

                tempMapData.put(UrlConstant.PRODUCT_ID, postJsonData.get(UrlConstant.PRODUCT_ID));
                tempMapData.put(UrlConstant.PRODUCT_UOM_ID, postJsonData.get(UrlConstant.PRODUCT_UOM));
                tempMapData.put(UrlConstant.PRODUCT_QTY, postJsonData.get(UrlConstant.PRODUCT_QTY));
                tempMapData.put(UrlConstant.ORIGIN, postJsonData.get(UrlConstant.NAME));
                tempMapData.put(UrlConstant.LINKED_ORDER_ID, String.valueOf(entity_id));
                tempMapData.put(UrlConstant.PARTNER_ID, postJsonData.get(UrlConstant.PARTNER_ID));
                tempMapData.put(UrlConstant.PARTNER_NAME, postJsonData.get(UrlConstant.PARTNER_NAME));
                tempMapData.put(UrlConstant.WAREHOUSE_ID, postJsonData.get(UrlConstant.WAREHOUSE_ID));

                bundle.putSerializable(UrlConstant.JSON_DATA, tempMapData);
                fragment.setArguments(bundle);
                AppUtils.changeFragment(getActivity(), fragment);
                AppPreference.getInstance().setDATA_CHANGE_FLAG(true);

                break;
            case R.id.storageInvoiceAddLinks:
                bundle.putLong(UrlConstant.ROW_ID, 0);
                bundle.putBoolean(UrlConstant.NOTES, false);

                tempMapData.put(UrlConstant.PRODUCT_QTY, postJsonData.get(UrlConstant.PRODUCT_QTY));
                tempMapData.put(UrlConstant.LINKED_ORDER_ID, String.valueOf(entity_id));
                tempMapData.put(UrlConstant.PARTNER_ID, postJsonData.get(UrlConstant.PARTNER_ID));
                tempMapData.put(UrlConstant.PARTNER_NAME, postJsonData.get(UrlConstant.PARTNER_NAME));
                tempMapData.put(UrlConstant.WAREHOUSE_ID, postJsonData.get(UrlConstant.WAREHOUSE_ID));

                if (keyName.equals(UrlConstant.COMMODITY_LOAN)) {
                    bundle.putString(UrlConstant.KEY_NAME, UrlConstant.COMMODITY_LOAN_INVOICE);
                    tempMapData.put(UrlConstant.NUMBER, postJsonData.get(UrlConstant.NAME));
                } else if (keyName.equals(UrlConstant.STORAGE_ORDER)) {
                    bundle.putString(UrlConstant.KEY_NAME, UrlConstant.STORAGE_INVOICE);
                }

                bundle.putSerializable(UrlConstant.JSON_DATA, tempMapData);
                fragment.setArguments(bundle);
                AppUtils.changeFragment(getActivity(), fragment);
                AppPreference.getInstance().setDATA_CHANGE_FLAG(true);
                break;
            case R.id.commodityLoanAddLinks:
                bundle.putString(UrlConstant.KEY_NAME, UrlConstant.COMMODITY_LOAN);
                bundle.putLong(UrlConstant.ROW_ID, 0);
                bundle.putBoolean(UrlConstant.NOTES, false);

                tempMapData.put(UrlConstant.PARTNER_ID, postJsonData.get(UrlConstant.PARTNER_ID));
                tempMapData.put(UrlConstant.PARTNER_NAME, postJsonData.get(UrlConstant.PARTNER_NAME));
                tempMapData.put(UrlConstant.PRODUCT_ID, postJsonData.get(UrlConstant.PRODUCT_ID));
                tempMapData.put(
                        UrlConstant.STORAGE_ORDER_IDS,
                        Arrays.toString(new Long[]{entity_id}));

                bundle.putSerializable(UrlConstant.JSON_DATA, tempMapData);
                fragment.setArguments(bundle);
                AppUtils.changeFragment(getActivity(), fragment);
                AppPreference.getInstance().setDATA_CHANGE_FLAG(true);
                break;
            case R.id.done:
                Gson gson1 = new Gson();

                List<List<MultipleLines.GetKeyValue>> getKeyValueList1 = new ArrayList<>();
                if (keyName.equals(UrlConstant.VENDOR_BILL)) {
                    String invoice_line_ids = postJsonData.get(UrlConstant.INVOICE_LINE_IDS);
                    if (invoice_line_ids != null)
                        getKeyValueList1 = gson1.fromJson(invoice_line_ids,
                                MultipleLines.class).getInvoice_line_ids();
                } else if ((keyName.equals(UrlConstant.MANDI_PRICE))
                        || (keyName.equals(UrlConstant.NODE_PRICE))) {
                    String invoice_line_ids = postJsonData.get(UrlConstant.PRODUCT_PRICE_LINES);
                    if (invoice_line_ids != null)
                        getKeyValueList1 = gson1.fromJson(invoice_line_ids,
                                MultipleLines.class).getProduct_price();
                } else if ((keyName.equals(UrlConstant.PAYMENT_REQUEST))) {
                    String invoice_line_ids = postJsonData.get(UrlConstant.REQUEST_LINE_IDS);
                    if (invoice_line_ids != null)
                        getKeyValueList1 = gson1.fromJson(invoice_line_ids,
                                MultipleLines.class).getRequest_line_ids();
                }
                if (keyName.equals(UrlConstant.VENDOR_BILL) && getKeyValueList1 != null &&
                        getKeyValueList1.size() <= 0) {
                    AppUtils.showToast("Please add some Products");
                } else if (keyName.equals(UrlConstant.MANDI_PRICE) && getKeyValueList1 != null &&
                        getKeyValueList1.size() <= 0) {
                    AppUtils.showToast("Please add some Products");
                } else if (keyName.equals(UrlConstant.NODE_PRICE) && getKeyValueList1 != null &&
                        getKeyValueList1.size() <= 0) {
                    AppUtils.showToast("Please add some Products");
                } else if ((entity_id == 0L) || (sync_status.equals(UrlConstant.ERRORS)))
                    checkValidations(getActivity(), null, 0, false,
                            UrlConstant.PENDING);
                else {
                    if (AppPreference.getInstance().getDATA_CHANGE_FLAG())
                        checkValidations(getActivity(), null, 0, false,
                                UrlConstant.PENDING);
                    else
                        AppUtils.showToast("No data changed.");
                }
                break;
            case R.id.options:
                openActionsDialog();
                break;
            case R.id.home:
                checkData(getActivity(), UrlConstant.HOME);
                break;
            case R.id.share:
                createFileData();
                break;
            case R.id.notes:
                if (AppUtils.isNullCase(tempNotesKey))
                    AppUtils.showToast("No notes for this section.");
                else
                    openNotesDialog();
                break;
            case R.id.billAddLinks:
                if (keyName.equals(UrlConstant.PURCHASE_ORDER)) {
                    bundle.putString("KEY_NAME", UrlConstant.VENDOR_BILL);
                    bundle.putLong("ROW_ID", 0);
                    bundle.putBoolean("NOTES", false);
                    tempMapData.put(UrlConstant.PARTNER_ID, postJsonData.get(UrlConstant.PARTNER_ID));
                    getKeyValue.setKey(UrlConstant.PRODUCT_ID);
                    getKeyValue.setValue(postJsonData.get(UrlConstant.PRODUCT_ID));
                    list.add(getKeyValue);

                    getKeyValue = new MultipleLines.GetKeyValue();
                    getKeyValue.setKey(UrlConstant.PRODUCT_QTY);
                    product = Float.parseFloat(postJsonData.get("qty_received")) -
                            Float.parseFloat(postJsonData.get("qty_invoiced"));
                    if (product <= 0) {
                        product = 0f;
                        getKeyValue.setValue("0");
                    } else
                        getKeyValue.setValue("" + product);
                    list.add(getKeyValue);

                    getKeyValue = new MultipleLines.GetKeyValue();
                    getKeyValue.setKey(UrlConstant.PRODUCT_UOM);
                    getKeyValue.setValue(postJsonData.get(UrlConstant.PRODUCT_UOM));
                    list.add(getKeyValue);

                    getKeyValue = new MultipleLines.GetKeyValue();
                    getKeyValue.setKey(UrlConstant.PRICE_UNIT);
                    getKeyValue.setValue(postJsonData.get(UrlConstant.PRICE_UNIT));
                    list.add(getKeyValue);

                    getKeyValue = new MultipleLines.GetKeyValue();
                    getKeyValue.setKey(UrlConstant.PRICE_TOTAL);
                    total = product * Float.parseFloat(postJsonData.get(UrlConstant.PRICE_UNIT));
                    getKeyValue.setValue("" + total);
                    list.add(getKeyValue);

                    getKeyValue = new MultipleLines.GetKeyValue();
                    getKeyValue.setKey(UrlConstant.PURCHASE_LINE_ID);
                    getKeyValue.setValue(postJsonData.get(UrlConstant.PURCHASE_LINE_ID));
                    list.add(getKeyValue);

                    getKeyValueList.add(list);

                    multipleLines.setInvoice_line_ids(getKeyValueList);
                    jsonString = gson.toJson(multipleLines);

                    tempMapData.put(UrlConstant.INVOICE_LINE_IDS, jsonString);
                    bundle.putSerializable("JSON_DATA", tempMapData);
                    fragment.setArguments(bundle);
                    AppUtils.changeFragment(getActivity(), fragment);
                    AppPreference.getInstance().setDATA_CHANGE_FLAG(true);
                } else {
                    AppUtils.showProgressDialog(getActivity());
                    getBillDataList();
                }
                break;
            case R.id.setToDraft:
                actionDialog.dismiss();
                if (AppUtils.haveNetworkConnection(getActivity()))
                    showActionDialog("Do you want to change the state to Draft?",
                            UrlConstant.SETTODRAFT);
                else
                    AppUtils.showToast(UrlConstant.NO_INTERNET_ACTION);
                break;
            case R.id.confirm:
                confirmActionDialog();
                break;
            case R.id.viewErrors:
                if (dbFieldsData.getDialog_id() != null) {
                    if (dbFieldsData.getDialog_id().equals(UrlConstant.BACK_ORDER)) {
                        showBackOrderDialog();
                    } else if (dbFieldsData.getDialog_id().equals(UrlConstant.OVERPROCESSED)) {
                        showOverProcessedDialog();
                    }
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(dbFieldsData.getValidation_status());
                    builder.setCancelable(true);
                    AlertDialog alert = builder.create();
                    alert.show();
                }

                break;
            case R.id.rollback:
                showErrorCleanDataDialog();
                break;
            case R.id.attachment:
                showImageDialog();
                break;
            case R.id.cancel:
                actionDialog.dismiss();
                if (AppUtils.haveNetworkConnection(getActivity()))
                    showActionDialog("Do you want to Cancel this " +
                            fieldsData.getDisplay_name() + "?", UrlConstant.CANCEL);
                else
                    AppUtils.showToast(UrlConstant.NO_INTERNET_ACTION);
                break;
            case R.id.forceAvail:
                actionDialog.dismiss();
                if (AppUtils.haveNetworkConnection(getActivity()))
                    showActionDialog("Do you want to Force this product's stock availability?",
                            UrlConstant.FORCE_AVAIL);
                else
                    AppUtils.showToast(UrlConstant.NO_INTERNET_ACTION);
                break;
            case R.id.recheckAvail:
                actionDialog.dismiss();
                if (AppUtils.haveNetworkConnection(getActivity()))
                    showActionDialog("Do you want to Recheck this product's stock availability?",
                            UrlConstant.RECHECK_AVAIL);
                else
                    AppUtils.showToast(UrlConstant.NO_INTERNET_ACTION);
                break;
            case R.id.approve:
                actionDialog.dismiss();
                if (AppUtils.haveNetworkConnection(getActivity()))
                    showActionDialog("Do you want to Approve this Payment Request?",
                            UrlConstant.APPROVE);
                else
                    AppUtils.showToast(UrlConstant.NO_INTERNET_ACTION);
                break;
            case R.id.reject:
                actionDialog.dismiss();
                if (AppUtils.haveNetworkConnection(getActivity()))
                    showActionDialog("Do you want to Reject this Payment Request?",
                            UrlConstant.REJECT);
                else
                    AppUtils.showToast(UrlConstant.NO_INTERNET_ACTION);
                break;
            case R.id.reserve:
                actionDialog.dismiss();
                if (AppUtils.haveNetworkConnection(getActivity()))
                    showActionDialog("Do you want to Reserve this product?",
                            UrlConstant.RESERVE);
                else
                    AppUtils.showToast(UrlConstant.NO_INTERNET_ACTION);
                break;
            case R.id.addProduct:
                if (keyName.equals(UrlConstant.VENDOR_BILL))
                    displayMultipleItems(true, true);
                else if (keyName.equals(UrlConstant.MANDI_PRICE)
                        || keyName.equals(UrlConstant.NODE_PRICE))
                    displayPriceData(true, true);
                else if (keyName.equals(UrlConstant.PAYMENT_REQUEST))
                    displayPaymentData(true, true);
                break;
        }
    }

    private void showBackOrderDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_back_order_issue);
        Button createBackOrder, noBackOrder, cancel;

        createBackOrder = dialog.findViewById(R.id.createBackOrder);
        noBackOrder = dialog.findViewById(R.id.noBackOrder);
        cancel = dialog.findViewById(R.id.cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        createBackOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                callDialogAPI(UrlConstant.BACK_ORDER, "create_backorder");
            }
        });
        noBackOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                callDialogAPI(UrlConstant.BACK_ORDER, "cancel_backorder");
            }
        });
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
        dialog.show();
    }

    private void callDialogAPI(String dialog_id, String actionName) {
        AppRestClient client = AppRestClient.getInstance();
        EntityCreateDataBody.CreateBody createBody = new EntityCreateDataBody.CreateBody();
        ArrayList<JsonDataStore> jsonDataStoreArrayList = mapToList(postJsonData);
        AppUtils.showProgressDialog(activity);
        createBody.setFields(jsonDataStoreArrayList);
        EntityCreateDataBody.MetaData metaData = new EntityCreateDataBody.MetaData();
        metaData.setClient_id(rowid);
        if (entity_id == 0L)
            metaData.setEntity_id(0L);
        else
            metaData.setEntity_id(entity_id);
        metaData.setVersion(schema_version);

        EntityCreateDataBody entityCreateDataBody = null;
        Call<ResponseCreateEntity> call = null;
        if (keyName.equals(UrlConstant.SALES_DISPATCH)) {
            entityCreateDataBody = new EntityCreateDataBody(createBody, metaData);
            call = client.dialogActionDispatchVoucher(AppPreference.getInstance().getDATA_CHANGE_FLAG(),
                    entityCreateDataBody, actionName, dialog_id);
        } else if (keyName.equals(UrlConstant.PURCHASE_RECEIPT)) {
            entityCreateDataBody = new EntityCreateDataBody(createBody, metaData);
            call = client.dialogActionPurchaseReceipt(AppPreference.getInstance().getDATA_CHANGE_FLAG(),
                    entityCreateDataBody, actionName, dialog_id);
        } else if (keyName.equals(UrlConstant.VENDOR_BILL)) {
            VendorBill.Data dataNote = new VendorBill.Data();
            dataNote.setReason(reason);
            VendorBill vendorBill = new VendorBill(dataNote, metaData);
            call = client.dialogActionVendorBill(
                    vendorBill, actionName, dialog_id);
        } else if (keyName.equals(UrlConstant.STORAGE_RECEIPT)) {
            entityCreateDataBody = new EntityCreateDataBody(createBody, metaData);
            call = client.dialogActionStorageReceipt(
                    AppPreference.getInstance().getDATA_CHANGE_FLAG(), entityCreateDataBody,
                    actionName, dialog_id);
        }
        call.enqueue(new Callback<ResponseCreateEntity>() {
            @Override
            public void onResponse(Call<ResponseCreateEntity> call,
                                   Response<ResponseCreateEntity> response) {
                AppUtils.hideProgressDialog();
                EntityDataJson entityDataJsonTemp = new EntityDataJson();
                if (response.code() == 200) {
                    entityDataJsonTemp.setSync_status(UrlConstant.SUCCESS);
                    entityDataJsonTemp.setValidation_status("");
                    entityDataJsonTemp.setEntity_id(response.body().getMetadata().getEntity_id());

                } else if (response.code() == 500) {
                    entityDataJsonTemp.setSync_status(UrlConstant.ERRORS);
                    entityDataJsonTemp.setEntity_id(entity_id);
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        if (jObjError.get("error_type").equals(UrlConstant.UNHANDLED_EXCEPTION)) {
                            entityDataJsonTemp.setValidation_status("Technical Problem");
                        } else
                            entityDataJsonTemp.setValidation_status("" +
                                    jObjError.get("error_message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 401)
                    AppUtils.showSessionExpiredDialog(getActivity());

                databaseHandler.updateServiceEntityData(entityDataJsonTemp, rowid);

                AppPreference.getInstance().setDATA_CHANGE_FLAG(false);
                getEntityData(getActivity());
            }

            @Override
            public void onFailure(Call<ResponseCreateEntity> call, Throwable t) {
                AppUtils.hideProgressDialog();

                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(getActivity()))
                        AppUtils.showToast(UrlConstant.NO_INTERNET);
                    else
                        AppUtils.showToast(UrlConstant.SERVER_NOT_RES);

                } else {
                    AppUtils.showToast(UrlConstant.TECH_PROB);
                }
            }
        });
    }

    private void showOverProcessedDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_over_processed);
        Button confirm, cancel;

        confirm = dialog.findViewById(R.id.confirm);
        cancel = dialog.findViewById(R.id.cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                callDialogAPI(UrlConstant.OVERPROCESSED, "confirm");
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
        dialog.show();
    }

    private void showImageDialog() {
        dialog = new Dialog(getActivity());
        TextView done, cancel;
        ImageView addImageButton;
        dialog.setCanceledOnTouchOutside(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_images);
        attachedImages = dialog.findViewById(R.id.attachedImages);
        addImages = dialog.findViewById(R.id.addImages);
        addImageButton = dialog.findViewById(R.id.addImageButton);
        attachedImages.setNestedScrollingEnabled(false);
        addImages.setNestedScrollingEnabled(false);
        done = dialog.findViewById(R.id.done);
        cancel = dialog.findViewById(R.id.cancel);
        if (attachments != null && attachments.size() != 0) {
            attachedImages.setAdapter(new RecyclerAdapterAttachedImages(getActivity(), attachments));
            attachedImages.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        }
        addImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onImageClick();
            }
        });
        imageFileList1.clear();
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageList.clear();
                imageFileList.clear();
                dialog.dismiss();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageFileList.isEmpty()) {
                    AppUtils.showToast("No new attachment added.");
                } else {
                    imageFileList1.clear();
                    for (Map.Entry<Integer, File> entry : imageFileList.entrySet()) {
                        imageFileList1.add(entry.getValue());
                    }
                    sendDataToServer();
                }
            }
        });
        Drawable d = new ColorDrawable(getActivity().getResources().getColor(R.color.greytext));
        d.setAlpha(100);
        dialog.getWindow().setBackgroundDrawable(d);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }

    private void onImageClick() {
        AppUtils.imageSelection(this, 5);
    }

    private void sendDataToServer() {
        AppUtils.showProgressDialog(getActivity());
        final Attachments data = new Attachments(entity_id, keyName);
        AppRestClient client = AppRestClient.getInstance();
        Call<AttachmentResponse> call = client.postAttacthments(imageFileList1,
                data);
        call.enqueue(new ApiCallBack<AttachmentResponse>() {
            @Override
            public void onResponse(Response<AttachmentResponse> response) {
                dialog.dismiss();
                imageFileList.clear();
                imageList.clear();
                if (attachments == null)
                    attachments = new ArrayList<>();

                for (int i = 0; i < response.body().getData().size(); i++) {
                    attachments.add(response.body().getData().get(i));
                }
                databaseHandler.updateAttachments(rowid, attachments);
                AppUtils.hideProgressDialog();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.detach(EntityCreationFragment.this).attach(EntityCreationFragment.this).commit();
            }

            @Override
            public void onResponse401(Response<AttachmentResponse> response) throws JSONException {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("val", "requestCode ->  " + requestCode + "  resultCode " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (100): {
                if (resultCode == Activity.RESULT_OK) {
                    returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                    storeImagesInList(returnValue);
                }
            }
            break;
        }
    }

    private void storeImagesInList(ArrayList<String> returnValue) {
        for (int i = 0; i < returnValue.size(); i++) {
            File file = new File(returnValue.get(i));
            imageFileList1.add(file);
        }
        for (int i = 0; i < imageFileList1.size(); i++) {
            imageFileList.put(i, imageFileList1.get(i));
        }
        addImages.setAdapter(new RecyclerAdapterImageList(imageFileList1, this));
        addImages.setLayoutManager(new GridLayoutManager(getActivity(), 3));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    AppUtils.imageSelection(this, 5);
                } else {
                }
                return;
            }
        }
    }

    private void getBillDataList() {
        String keyNameLinked = null;
        AppRestClient client = AppRestClient.getInstance();
        Call<GetEntityList> call = null;
        if (keyName.equals(UrlConstant.SALES_DISPATCH)
                || keyName.equals(UrlConstant.PURCHASE_RECEIPT)) {
            keyNameLinked = UrlConstant.EXPENSE_BILL;
            list = new String[]{UrlConstant.EXPENSE_BILL};
            call = client.getExpenseBillList(null, 0, 0,
                    null, null);
        } else if (keyName.equals(UrlConstant.EXPENSE_BILL)) {
            keyNameLinked = null;
            call = client.getDispatchRecieptList(null, 0, 0,
                    null, null);
            list = new String[]{UrlConstant.SALES_DISPATCH, UrlConstant.PURCHASE_RECEIPT};
        }

        list = AppUtils.getLinkedEntities(keyName);

        final String finalKeyNameLinked = keyNameLinked;
        call.enqueue(new ApiCallBack<GetEntityList>() {
            @Override
            public void onResponse(Response<GetEntityList> response) {
                AppUtils.showProgressDialog(getActivity());
                if (response.body() == null)
                    return;
                if ((response.body().getCreateBody() != null) &&
                        (response.body().getCreateBody().size() != 0))
                    storeBillData(finalKeyNameLinked, response.body().getCreateBody(),
                            0);

                ArrayList<EntityDataJson> linkArraylist =
                        databaseHandler.getEntityDataForLink(list);
                openBillLinkDialog(linkArraylist);
                AppUtils.hideProgressDialog();
            }

            @Override
            public void onResponse401(Response<GetEntityList> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });
    }

    private void openActionsDialog() {
        actionDialog.show();
    }


    private void openPickingListDialog(List<String> linkList) {
        entityDataJsonArrayList.clear();
        pickingIds = new ArrayList<>();
        for (int i = 0; i < linkList.size(); i++) {
            if (AppUtils.haveNetworkConnection(getActivity())) {
                pickingIds.add(linkList.get(i));
            } else {
                EntityDataJson tempDataList;
                tempDataList = databaseHandler.getEntityIdData(linkKeyName,
                        Long.valueOf(linkList.get(i)));
                if (tempDataList == null)
                    pickingIds.add(linkList.get(i));
                else
                    entityDataJsonArrayList.add(tempDataList);
            }
        }

        if (pickingIds.size() != 0)
            getDataForPickingIdsLinked(pickingIds);
        else
            printPickingIds();

    }


    private void getStorageDispatchList(List<String> linkList) {
        storageDispatchArraylist.clear();
        pickingIds = new ArrayList<>();
        for (int i = 0; i < linkList.size(); i++) {
            if (AppUtils.haveNetworkConnection(getActivity())) {
                pickingIds.add(linkList.get(i));
            } else {
                EntityDataJson tempDataList;
                tempDataList = databaseHandler.getEntityIdData(
                        new String[]{UrlConstant.STORAGE_DISPATCH},
                        Long.valueOf(linkList.get(i)));
                if (tempDataList == null)
                    pickingIds.add(linkList.get(i));
                else
                    storageDispatchArraylist.add(tempDataList);
            }
        }

        if (pickingIds.size() != 0)
            getStorageDispatchData(pickingIds);
        else
            printStorageDisIds();
    }

    private void getLinkedOrdersList(List<String> linkList) {
        linkedOrdersArrayList.clear();
        linkedOrderIds = new ArrayList<>();
        for (int i = 0; i < linkList.size(); i++) {
            if (AppUtils.haveNetworkConnection(getActivity())) {
                linkedOrderIds.add(linkList.get(i));
            } else {
                EntityDataJson tempDataList;
                tempDataList = databaseHandler.getEntityIdData(new String[]
                                {getLinkedKey(keyName)},
                        Long.valueOf(linkList.get(i)));
                if (tempDataList == null)
                    linkedOrderIds.add(linkList.get(i));
                else
                    linkedOrdersArrayList.add(tempDataList);
            }
        }

        if (linkedOrderIds.size() != 0)
            getLinkedOrderData(linkedOrderIds);
        else
            printLinkedOrderIds();

    }

    private void getCommodityLoanList(List<String> linkList) {
        commodityLoanArrayList.clear();
        pickingIds = new ArrayList<>();
        for (int i = 0; i < linkList.size(); i++) {
            if (AppUtils.haveNetworkConnection(getActivity())) {
                pickingIds.add(linkList.get(i));
            } else {
                EntityDataJson tempDataList;
                tempDataList = databaseHandler.getEntityIdData(new String[]
                                {UrlConstant.COMMODITY_LOAN},
                        Long.valueOf(linkList.get(i)));
                if (tempDataList == null)
                    pickingIds.add(linkList.get(i));
                else
                    commodityLoanArrayList.add(tempDataList);
            }
        }

        if (pickingIds.size() != 0)
            getCommodityLoanData(pickingIds);
        else
            printCommodityLoanIds();

    }

    private String getInvoiceKey() {
        if (keyName.equals(UrlConstant.COMMODITY_LOAN))
            return UrlConstant.COMMODITY_LOAN_INVOICE;
        else
            return UrlConstant.STORAGE_INVOICE;
    }

    private void getStorageInvoiceList(List<String> linkList) {
        storageInvoiceArraylist.clear();
        pickingIds = new ArrayList<>();
        for (int i = 0; i < linkList.size(); i++) {
            if (AppUtils.haveNetworkConnection(getActivity())) {
                pickingIds.add(linkList.get(i));
            } else {


                EntityDataJson tempDataList;
                tempDataList = databaseHandler.getEntityIdData(new String[]
                                {getInvoiceKey()},
                        Long.valueOf(linkList.get(i)));
                if (tempDataList == null)
                    pickingIds.add(linkList.get(i));
                else
                    storageInvoiceArraylist.add(tempDataList);
            }
        }

        if (pickingIds.size() != 0)
            getStorageInvoiceData(pickingIds);
        else
            printStorageInvoiceIds();

    }

    private void getStorageReceiptList(List<String> linkList) {
        storageReceiptArraylist.clear();
        pickingIds = new ArrayList<>();
        for (int i = 0; i < linkList.size(); i++) {
            if (AppUtils.haveNetworkConnection(getActivity())) {
                pickingIds.add(linkList.get(i));
            } else {
                EntityDataJson tempDataList;
                tempDataList = databaseHandler.getEntityIdData(new String[]
                                {UrlConstant.STORAGE_RECEIPT},
                        Long.valueOf(linkList.get(i)));
                if (tempDataList == null)
                    pickingIds.add(linkList.get(i));
                else
                    storageReceiptArraylist.add(tempDataList);
            }
        }

        if (pickingIds.size() != 0)
            getStorageReceiptData(pickingIds);
        else
            printStorageReceiptIds();

    }

    private void openBillListDialog(List<String> linkList) {
        billEntityDataJsonArrayList.clear();
        pickingIds = new ArrayList<>();

        for (int i = 0; i < linkList.size(); i++) {
            if (AppUtils.haveNetworkConnection(getActivity())) {
                pickingIds.add(linkList.get(i));
            } else {
                EntityDataJson tempDataList;
                tempDataList = databaseHandler.getEntityIdData(linkBills,
                        Long.valueOf(linkList.get(i)));
                if (tempDataList == null)
                    pickingIds.add(linkList.get(i));
                else
                    billEntityDataJsonArrayList.add(tempDataList);
            }
        }

        if (pickingIds.size() != 0)
            getDataForBillIdsLinked(pickingIds);
        else
            printBillIds();
    }

    private void printStorageDisIds() {
        if (getActivity() == null)
            return;
        layoutManagerStorageDis = new LinearLayoutManager(getActivity());
        recyclerAdapterStorageDispatch = new RecyclerAdapterStorageDispatch(
                getActivity(), storageDispatchArraylist, keyName);
        storage_dis_linked_recy.setLayoutManager(layoutManagerStorageDis);
        storage_dis_linked_recy.setAdapter(recyclerAdapterStorageDispatch);
    }

    private void printCommodityLoanIds() {
        if (getActivity() == null)
            return;
        layoutManagerCommodityLoan = new LinearLayoutManager(getActivity());
        recyclerAdapterStorageLoan = new RecyclerAdapterStorageLoan(getActivity(),
                commodityLoanArrayList, keyName);
        commodityLoan_linked_recy.setLayoutManager(layoutManagerCommodityLoan);
        commodityLoan_linked_recy.setAdapter(recyclerAdapterStorageLoan);
    }

    private void printLinkedOrderIds() {
        if (getActivity() == null)
            return;
        layoutManagerLinkedOrders = new LinearLayoutManager(getActivity());
        recyclerAdapterLinkedOrders = new RecyclerAdapterLinkedOrders(getActivity(),
                linkedOrdersArrayList, keyName);
        linkedOrder_linked_recy.setLayoutManager(layoutManagerLinkedOrders);
        linkedOrder_linked_recy.setAdapter(recyclerAdapterLinkedOrders);
    }

    private void printStorageInvoiceIds() {
        if (getActivity() == null)
            return;
        layoutManagerStorageInvoice = new LinearLayoutManager(getActivity());
        recyclerAdapterStorageInvoice = new RecyclerAdapterStorageInvoice(
                getActivity(), storageInvoiceArraylist, keyName);
        storageInvoice_linked_recy.setLayoutManager(layoutManagerStorageInvoice);
        storageInvoice_linked_recy.setAdapter(recyclerAdapterStorageInvoice);
    }

    private void printStorageReceiptIds() {
        if (getActivity() == null)
            return;
        layoutManagerStorageReceipt = new LinearLayoutManager(getActivity());
        recyclerAdapterStorageReceipt = new RecyclerAdapterStorageReceipt(getActivity(),
                storageReceiptArraylist, keyName);
        storageReceipt_linked_recy.setLayoutManager(layoutManagerStorageReceipt);
        storageReceipt_linked_recy.setAdapter(recyclerAdapterStorageReceipt);
    }

    private void printPickingIds() {
        if (getActivity() == null)
            return;
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerAdapterPickingLink = new RecyclerAdapterPickingLink(getActivity(),
                entityDataJsonArrayList, keyName);
        linked_recy.setLayoutManager(layoutManager);
        linked_recy.setAdapter(recyclerAdapterPickingLink);
    }

    private void printBillIds() {
        if (getActivity() == null)
            return;
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerAdapterBillPickingList = new RecyclerAdapterBillPickingList(getActivity(),
                billEntityDataJsonArrayList, keyName, entity_id, this);
        bill_linked_recy.setLayoutManager(layoutManager);
        bill_linked_recy.setAdapter(recyclerAdapterBillPickingList);
    }

    private void getDataForBillIdsLinked(List<String> pickingIds) {
        String keyNameLinked = null;
        String iDs = "";
        for (int i = 0; i < pickingIds.size(); i++) {
            if (i == pickingIds.size() - 1)
                iDs = iDs + pickingIds.get(i);
            else
                iDs = iDs + pickingIds.get(i) + ",";
        }
        AppRestClient client = AppRestClient.getInstance();
        Call<GetEntityList> call = null;
        if (keyName.equals(UrlConstant.SALES_DISPATCH)) {
            keyNameLinked = UrlConstant.EXPENSE_BILL;
            call = client.getExpenseBillList(iDs, 0, 0, null,
                    null);
        } else if (keyName.equals(UrlConstant.PURCHASE_RECEIPT)) {
            keyNameLinked = UrlConstant.EXPENSE_BILL;
            call = client.getExpenseBillList(iDs, 0, 0, null,
                    null);
        } else if (keyName.equals(UrlConstant.EXPENSE_BILL)) {
            keyNameLinked = UrlConstant.PURCHASE_RECEIPT;
            call = client.getDispatchRecieptList(iDs, 0, 0, null,
                    null);
        } else if (keyName.equals(UrlConstant.PURCHASE_ORDER)) {
            keyNameLinked = UrlConstant.VENDOR_BILL;
            call = client.getVendorBillList(iDs, 0, 0, null,
                    null);
        }
        final String finalKeyNameLinked = keyNameLinked;
        call.enqueue(new ApiCallBack<GetEntityList>() {
            @Override
            public void onResponse(Response<GetEntityList> response) {
                if (response.body() == null)
                    return;
                if ((response.body().getCreateBody() != null) &&
                        (response.body().getCreateBody().size() != 0))
                    storeBillData(finalKeyNameLinked, response.body().getCreateBody(), 1);
                else
                    bill_tech_error.setVisibility(View.VISIBLE);

            }

            @Override
            public void onResponse401(Response<GetEntityList> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });
    }

    private void getDataForPickingIdsLinked(List<String> pickingIds) {
        String iDs = "";
        for (int i = 0; i < pickingIds.size(); i++) {
            if (i == pickingIds.size() - 1)
                iDs = iDs + pickingIds.get(i);
            else
                iDs = iDs + pickingIds.get(i) + ",";
        }
        Call<GetEntityList> call;
        call = AppUtils.getEntityList(keyName, iDs, 0, 0,
                null, null);
        call.enqueue(new ApiCallBack<GetEntityList>() {
            @Override
            public void onResponse(Response<GetEntityList> response) {
                if (response.body() == null)
                    return;
                if ((response.body().getCreateBody() != null) &&
                        (response.body().getCreateBody().size() != 0))
                    storeListDataDB(response.body().getCreateBody(), 1);
                else
                    tech_error.setVisibility(View.VISIBLE);
            }

            @Override
            public void onResponse401(Response<GetEntityList> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });
    }

    private void getStorageDispatchData(List<String> pickingIds) {
        String iDs = "";
        for (int i = 0; i < pickingIds.size(); i++) {
            if (i == pickingIds.size() - 1)
                iDs = iDs + pickingIds.get(i);
            else
                iDs = iDs + pickingIds.get(i) + ",";
        }
        Call<GetEntityList> call;
        call = AppUtils.getEntityList(UrlConstant.STORAGE_DISPATCH, iDs, 0, 0,
                null, null);
        call.enqueue(new ApiCallBack<GetEntityList>() {
            @Override
            public void onResponse(Response<GetEntityList> response) {
                if (response.body() == null)
                    return;
                if ((response.body().getCreateBody() != null) &&
                        (response.body().getCreateBody().size() != 0))
                    storeStorageDisDataDB(response.body().getCreateBody());
                else
                    storage_dis_tech_error.setVisibility(View.VISIBLE);
            }

            @Override
            public void onResponse401(Response<GetEntityList> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });
    }

    private void getCommodityLoanData(List<String> pickingIds) {
        String iDs = "";
        for (int i = 0; i < pickingIds.size(); i++) {
            if (i == pickingIds.size() - 1)
                iDs = iDs + pickingIds.get(i);
            else
                iDs = iDs + pickingIds.get(i) + ",";
        }
        Call<GetEntityList> call;
        call = AppUtils.getEntityList(UrlConstant.COMMODITY_LOAN, iDs, 0, 0,
                null, null);
        call.enqueue(new ApiCallBack<GetEntityList>() {
            @Override
            public void onResponse(Response<GetEntityList> response) {
                if (response.body() == null)
                    return;
                if ((response.body().getCreateBody() != null) &&
                        (response.body().getCreateBody().size() != 0))
                    storeCommodityLoanData(response.body().getCreateBody());
                else
                    commodityLoan_tech_error.setVisibility(View.VISIBLE);
            }

            @Override
            public void onResponse401(Response<GetEntityList> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });
    }

    private void getLinkedOrderData(List<String> pickingIds) {
        String iDs = "";
        for (int i = 0; i < pickingIds.size(); i++) {
            if (i == pickingIds.size() - 1)
                iDs = iDs + pickingIds.get(i);
            else
                iDs = iDs + pickingIds.get(i) + ",";
        }
        Call<GetEntityList> call;
        call = AppUtils.getEntityList(getLinkedKey(keyName), iDs, 0, 0,
                null, null);
        call.enqueue(new ApiCallBack<GetEntityList>() {
            @Override
            public void onResponse(Response<GetEntityList> response) {
                if (response.body() == null)
                    return;
                if ((response.body().getCreateBody() != null) &&
                        (response.body().getCreateBody().size() != 0))
                    storeLinkedOrderData(response.body().getCreateBody());
                else
                    linkedOrder_tech_error.setVisibility(View.VISIBLE);
            }

            @Override
            public void onResponse401(Response<GetEntityList> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });
    }

    private void getStorageInvoiceData(List<String> pickingIds) {
        String iDs = "";
        for (int i = 0; i < pickingIds.size(); i++) {
            if (i == pickingIds.size() - 1)
                iDs = iDs + pickingIds.get(i);
            else
                iDs = iDs + pickingIds.get(i) + ",";
        }
        Call<GetEntityList> call;
        call = AppUtils.getEntityList(getInvoiceKey(), iDs, 0, 0,
                null, null);
        call.enqueue(new ApiCallBack<GetEntityList>() {
            @Override
            public void onResponse(Response<GetEntityList> response) {
                if (response.body() == null)
                    return;
                if ((response.body().getCreateBody() != null) &&
                        (response.body().getCreateBody().size() != 0))
                    storeStorageInvoiceData(response.body().getCreateBody());
                else
                    storageInvoice_tech_error.setVisibility(View.VISIBLE);
            }

            @Override
            public void onResponse401(Response<GetEntityList> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });
    }

    private void getStorageReceiptData(List<String> pickingIds) {
        String iDs = "";
        for (int i = 0; i < pickingIds.size(); i++) {
            if (i == pickingIds.size() - 1)
                iDs = iDs + pickingIds.get(i);
            else
                iDs = iDs + pickingIds.get(i) + ",";
        }
        Call<GetEntityList> call;
        call = AppUtils.getEntityList(UrlConstant.STORAGE_RECEIPT, iDs, 0, 0,
                null, null);
        call.enqueue(new ApiCallBack<GetEntityList>() {
            @Override
            public void onResponse(Response<GetEntityList> response) {
                if (response.body() == null)
                    return;
                if ((response.body().getCreateBody() != null) &&
                        (response.body().getCreateBody().size() != 0))
                    storeStorageReceiptData(response.body().getCreateBody());
                else
                    storageReceipt_tech_error.setVisibility(View.VISIBLE);
            }

            @Override
            public void onResponse401(Response<GetEntityList> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });
    }

    private void storeListDataDB(ArrayList<GetEntityList.CreateBody> data, int justStore) {
        if (getActivity() == null)
            return;
        for (int i = 0; i < data.size(); i++) {
            databaseHandler.insertOrUpdateEntityData(keyLinkName, data.get(i),
                    data.get(i).getEntity_id());
            if (justStore == 1) {
                entityDataJsonArrayList.add(databaseHandler.getEntityIdData(linkKeyName,
                        Long.valueOf(data.get(i).getEntity_id())));
                printPickingIds();
            }
        }
    }

    private void storeStorageDisDataDB(ArrayList<GetEntityList.CreateBody> data) {
        if (getActivity() == null)
            return;
        for (int i = 0; i < data.size(); i++) {
            databaseHandler.insertOrUpdateEntityData(
                    UrlConstant.STORAGE_DISPATCH, data.get(i), data.get(i).getEntity_id());
            storageDispatchArraylist.add(
                    databaseHandler.getEntityIdData(new String[]{UrlConstant.STORAGE_DISPATCH},
                            Long.valueOf(data.get(i).getEntity_id())));
            printStorageDisIds();
        }
    }

    private void storeCommodityLoanData(ArrayList<GetEntityList.CreateBody> data) {
        if (getActivity() == null)
            return;
        for (int i = 0; i < data.size(); i++) {
            databaseHandler.insertOrUpdateEntityData(UrlConstant.COMMODITY_LOAN, data.get(i),
                    data.get(i).getEntity_id());
            commodityLoanArrayList.add(databaseHandler.getEntityIdData(new String[]
                            {UrlConstant.COMMODITY_LOAN},
                    Long.valueOf(data.get(i).getEntity_id())));
            printCommodityLoanIds();
        }
    }

    private void storeLinkedOrderData(ArrayList<GetEntityList.CreateBody> data) {
        if (getActivity() == null)
            return;
        for (int i = 0; i < data.size(); i++) {
            databaseHandler.insertOrUpdateEntityData(getLinkedKey(keyName), data.get(i),
                    data.get(i).getEntity_id());
            linkedOrdersArrayList.add(databaseHandler.getEntityIdData(new String[]
                            {getLinkedKey(keyName)},
                    Long.valueOf(data.get(i).getEntity_id())));
            printLinkedOrderIds();
        }
    }

    private void storeStorageInvoiceData(ArrayList<GetEntityList.CreateBody> data) {
        if (getActivity() == null)
            return;
        for (int i = 0; i < data.size(); i++) {
            databaseHandler.insertOrUpdateEntityData(getInvoiceKey(), data.get(i),
                    data.get(i).getEntity_id());
            storageInvoiceArraylist.add(databaseHandler.getEntityIdData(new String[]
                            {getInvoiceKey()},
                    Long.valueOf(data.get(i).getEntity_id())));
            printStorageInvoiceIds();
        }
    }

    private void storeStorageReceiptData(ArrayList<GetEntityList.CreateBody> data) {
        if (getActivity() == null)
            return;
        for (int i = 0; i < data.size(); i++) {
            databaseHandler.insertOrUpdateEntityData(UrlConstant.STORAGE_RECEIPT, data.get(i),
                    data.get(i).getEntity_id());
            storageReceiptArraylist.add(databaseHandler.getEntityIdData(new String[]
                            {UrlConstant.STORAGE_RECEIPT},
                    Long.valueOf(data.get(i).getEntity_id())));
            printStorageReceiptIds();
        }
    }

    private void storeBillData(String finalKeyNameLinked,
                               ArrayList<GetEntityList.CreateBody> data, int justStore) {
        if (getActivity() == null)
            return;
        for (int i = 0; i < data.size(); i++) {
            if (keyName.equals(UrlConstant.EXPENSE_BILL))
                databaseHandler.insertOrUpdateEntityData(data.get(i).getEntity_type(),
                        data.get(i), data.get(i).getEntity_id());
            else
                databaseHandler.insertOrUpdateEntityData(finalKeyNameLinked, data.get(i),
                        data.get(i).getEntity_id());
            if (justStore == 1) {
                billEntityDataJsonArrayList.add(databaseHandler.getEntityIdData(linkBills,
                        Long.valueOf(data.get(i).getEntity_id())));
            }
        }
        printBillIds();
    }

    private void openBillLinkDialog(ArrayList<EntityDataJson> billEntityDataJsonArrayList) {
        billIds.clear();
        linkDialog = new Dialog(getActivity());
        RecyclerView linkRecy;
        Button save, cancel, createNew;
        TextView no_orders;
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        RecyclerAdapterBillLink recyclerAdapterBillLink;
        linkDialog.setCanceledOnTouchOutside(true);
        linkDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        linkDialog.setContentView(R.layout.dialog_link);
        linkRecy = linkDialog.findViewById(R.id.linkRec);
        save = linkDialog.findViewById(R.id.save);
        cancel = linkDialog.findViewById(R.id.cancel);
        createNew = linkDialog.findViewById(R.id.createNew);
        no_orders = linkDialog.findViewById(R.id.no_orders);
        linkRecy.setNestedScrollingEnabled(false);
        if (billEntityDataJsonArrayList.size() == 0) {
            save.setVisibility(View.GONE);
            no_orders.setVisibility(View.VISIBLE);
        }

        recyclerAdapterBillLink = new RecyclerAdapterBillLink(getActivity(),
                billEntityDataJsonArrayList, this);
        linkRecy.setLayoutManager(layoutManager);
        linkRecy.setAdapter(recyclerAdapterBillLink);
        AppUtils.hideProgressDialog();
        createNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linkDialog.dismiss();
                Bundle bundle = new Bundle();
                bundle.putString("KEY_NAME", UrlConstant.EXPENSE_BILL);
                bundle.putLong("ROW_ID", 0);
                bundle.putBoolean("NOTES", false);
                EntityCreationFragment fragment = EntityCreationFragment.newInstance();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.frag_container, fragment).
                        addToBackStack("").commit();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linkDialog.dismiss();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (billIds.size() == 0)
                    AppUtils.showToast("Please Select One Record.");
                else
                    postDataForBillLinking();
            }
        });
        Drawable d = new ColorDrawable(getActivity().getResources().getColor(R.color.greytext));
        d.setAlpha(100);
        linkDialog.getWindow().setBackgroundDrawable(d);
        linkDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        linkDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        linkDialog.getWindow().setGravity(Gravity.CENTER);
        linkDialog.show();
    }

    // to post the selected bill
    private void postDataForBillLinking() {
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        com.dehaat.node.rest.body.Linking.Data data =
                new com.dehaat.node.rest.body.Linking.Data(dbFieldsData.getEntity_id(), billIds);
        com.dehaat.node.rest.body.Linking linking = new com.dehaat.node.rest.body.Linking(data);

        Call<Void> call = null;

        if (keyName.equals(UrlConstant.SALES_DISPATCH))
            call = client.postSalesVoucherBillLink(linking);
        else if ((keyName.equals(UrlConstant.PURCHASE_RECEIPT)))
            call = client.postPurchaseVoucherBillLink(linking);
        else if (keyName.equals(UrlConstant.EXPENSE_BILL))
            call = client.postBillLink(linking);
        call.enqueue(new ApiCallBack<Void>() {
            @Override
            public void onResponse(Response<Void> response) {
                linkDialog.dismiss();
                showLinkCreatedDialog();
                AppUtils.hideProgressDialog();
            }

            @Override
            public void onResponse401(Response<Void> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });
    }

    private void showLinkCreatedDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Link Created");
        builder.setCancelable(false);

        builder.setPositiveButton("OKAY", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                AppPreference.getInstance().setDATA_CHANGE_FLAG(false);
                getEntityData(getActivity());
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private ArrayList<JsonDataStore> mapToList(HashMap<String, String> values) {
        ArrayList<JsonDataStore> jsonDataStoreArrayList = new ArrayList<>();
        for (Map.Entry<String, String> entry : values.entrySet()) {
            JsonDataStore jsonDataStore = new JsonDataStore();
            jsonDataStore.setKey(entry.getKey());
            jsonDataStore.setValue(entry.getValue());
            jsonDataStoreArrayList.add(jsonDataStore);
        }
        return jsonDataStoreArrayList;
    }

    private void callActionApi(String actionName) {
        AppRestClient client = AppRestClient.getInstance();
        EntityCreateDataBody.CreateBody createBody = new EntityCreateDataBody.CreateBody();
        final ArrayList<JsonDataStore> jsonDataStoreArrayList = mapToList(postJsonData);
        checkValidations(getActivity(), null, 0, true, UrlConstant.SUCCESS);
        if ((actionData) || (actionName.equals(UrlConstant.CANCEL)) ||
                (actionName.equals(UrlConstant.SETTODRAFT))) {
            AppUtils.showProgressDialog(activity);
            createBody.setFields(jsonDataStoreArrayList);
            EntityCreateDataBody.MetaData metaData = new EntityCreateDataBody.MetaData();
            metaData.setClient_id(rowid);
            if (entity_id == 0L)
                metaData.setEntity_id(0L);
            else
                metaData.setEntity_id(entity_id);
            metaData.setVersion(schema_version);

            final EntityCreateDataBody entityCreateDataBody = new EntityCreateDataBody(createBody, metaData);
            Call<ResponseCreateEntity> call;
            try {
                Object Call = client.getClass()
                        .getDeclaredMethod(
                                UrlConstant.key_mapping.get(keyName).get(UrlConstant.ACTION_METHOD),
                                boolean.class, EntityCreateDataBody.class, String.class)
                        .invoke(client, AppPreference.getInstance().getDATA_CHANGE_FLAG(),
                                entityCreateDataBody, actionName);
                call = (retrofit2.Call) Call;
            } catch (Exception e) {
                call = null;
            }

            call.enqueue(new Callback<ResponseCreateEntity>() {
                @Override
                public void onResponse(Call<ResponseCreateEntity> call, Response<ResponseCreateEntity> response) {
                    AppUtils.hideProgressDialog();
                    Gson gson = new Gson();
                    String jsonString = gson.toJson(jsonDataStoreArrayList);
                    EntityDataJson entityDataJsonTemp = new EntityDataJson();
                    entityDataJsonTemp.setJson_payload(jsonString);
                    if (response.code() == 200) {
                        entityDataJsonTemp.setSync_status(UrlConstant.SUCCESS);
                        entityDataJsonTemp.setValidation_status("");
                        entityDataJsonTemp.setEntity_id(response.body().getMetadata().getEntity_id());
                    } else if (response.code() == 500) {
                        entityDataJsonTemp.setSync_status(UrlConstant.ERRORS);
                        entityDataJsonTemp.setEntity_id(entity_id);
                        JSONObject jObjError = null;
                        try {
                            jObjError = new JSONObject(response.errorBody().string());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (jObjError.get("error_type").equals(UrlConstant.UNHANDLED_EXCEPTION)) {
                                entityDataJsonTemp.setValidation_status("Technical Problem");
                            } else
                                entityDataJsonTemp.setValidation_status("" + jObjError.get("error_message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else if (response.code() == 401)
                        AppUtils.showSessionExpiredDialog(getActivity());

                    databaseHandler.updateServiceEntityData(entityDataJsonTemp, rowid);

                    AppPreference.getInstance().setDATA_CHANGE_FLAG(false);
                    getEntityData(getActivity());
                }

                @Override
                public void onFailure(Call<ResponseCreateEntity> call, Throwable t) {
                    if (t instanceof ConnectException) {
                        if (!AppUtils.haveNetworkConnection(getActivity()))
                            AppUtils.showToast(UrlConstant.NO_INTERNET);
                        else
                            AppUtils.showToast(UrlConstant.SERVER_NOT_RES);

                    } else {
                        AppUtils.showToast(UrlConstant.TECH_PROB);
                    }
                }

            });
        }
    }

    private void getEntityData(final Activity activity) {
        AppUtils.showProgressDialog(activity);
        Call<GetEntityList> call;
        call = AppUtils.getEntityList(
                keyName, "" + entity_id, 0, 0, null, null);
        call.enqueue(new ApiCallBack<GetEntityList>() {
            @Override
            public void onResponse(Response<GetEntityList> response) {
                if (response.body() == null)
                    return;
                if ((response.body().getCreateBody() != null) &&
                        (response.body().getCreateBody().size() != 0))
                    for (int i = 0; i < response.body().getCreateBody().size(); i++) {
                        databaseHandler.insertOrUpdateEntityData(keyName,
                                response.body().getCreateBody().get(i), entity_id);
                    }
                EntityCreationFragment fragment = EntityCreationFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putLong("ROW_ID", rowid);
                bundle.putBoolean("NOTES", false);

                if (keyName.equals(UrlConstant.PARTNER_CONTACT)) {
                    fragment = receivingFragment;
                    bundle.putString("KEY_NAME", UrlConstant.STORAGE_ORDER);
                    long rowId = (creatingNew) ? 0L : rowIdStorageOrder;
                    bundle.putLong("ROW_ID", rowId);
                    HashMap<String, String> jsonData = new HashMap<>();
                    jsonData.put(
                            UrlConstant.PARTNER_ID,
                            response.body().getCreateBody().get(0).getEntity_id().toString()
                    );
                    bundle.putSerializable("JSON_DATA", jsonData);
                    bundle.putBoolean("CREATING_NEW", creatingNew);
                    fragment.setArguments(bundle);
                    fragment.setSelectedPayload(response.body().getCreateBody().get(0));

                    FragmentTransaction transaction = ((FragmentActivity) activity)
                            .getSupportFragmentManager()
                            .beginTransaction();
                    transaction.replace(R.id.frag_container, fragment).commit();
                } else {
                    bundle.putString("KEY_NAME", keyName);
                    bundle.putSerializable("JSON_DATA", postJsonData);
                    fragment.setArguments(bundle);
                    FragmentTransaction transaction = ((FragmentActivity) activity)
                            .getSupportFragmentManager()
                            .beginTransaction();
                    transaction.replace(R.id.frag_container, fragment).commit();
                }
            }

            @Override
            public void onResponse401(Response<GetEntityList> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(activity);
            }
        });
    }


    private void openNotesDialog() {
        final Dialog dialog = new Dialog(getActivity());
        final EditText notesText;
        Button saveNotes;
        dialog.setCanceledOnTouchOutside(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_notes);
        notesText = dialog.findViewById(R.id.notesText);
        saveNotes = dialog.findViewById(R.id.saveNotes);

        if (AppPreference.getInstance().getREADNOTES()) {
            notesText.setFocusable(false);
            notesText.setEnabled(false);
            saveNotes.setVisibility(View.GONE);
        }
        if (!AppUtils.isNullCase(postJsonData.get(tempNotesKey)))
            notesText.setText(postJsonData.get(tempNotesKey));
        saveNotes.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                postJsonData.put(tempNotesKey, notesText.getText().toString());
                displayData(getActivity());
                dialog.dismiss();
                AppPreference.getInstance().setDATA_CHANGE_FLAG(true);

            }
        });
        Drawable d = new ColorDrawable(getActivity().getResources().getColor(R.color.greytext));
        d.setAlpha(100);
        dialog.getWindow().setBackgroundDrawable(d);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }


    public void showCleanDataDialog(final Activity activity, final String data) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Information");
        builder.setMessage("Do you want to save this " + fieldsData.getDisplay_name() + "?");
        builder.setCancelable(true);
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                AppPreference.getInstance().setDATA_CHANGE_FLAG(false);
                dialog.dismiss();
                onBackHomePressed(activity, data);
            }
        });
        builder.setPositiveButton("YES", (dialog, which) -> {
            dialog.dismiss();
            checkValidations(activity, data, 0, false, UrlConstant.PENDING);

        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void showActionDialog(String message, final String action) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Information");
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setNegativeButton("NO", (dialog, which) -> dialog.dismiss());
        builder.setPositiveButton("YES", (dialog, which) -> {
            dialog.dismiss();
            callActionApi(action);
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void confirmActionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Information");
        builder.setMessage("Do you want to Confirm this " + fieldsData.getDisplay_name() + "?");
        builder.setCancelable(true);
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                checkValidations(getActivity(), null, 1, false,
                        UrlConstant.PENDING);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void showErrorCleanDataDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Information");
        builder.setMessage("Are you sure you want to Rollback to your previous data?");
        builder.setCancelable(true);
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                AppPreference.getInstance().setDATA_CHANGE_FLAG(false);
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                if (AppUtils.haveNetworkConnection(getActivity())) {
                    databaseHandler.deleteEntityData(rowid);
                    AppPreference.getInstance().setDATA_CHANGE_FLAG(false);
                    dialog.dismiss();
                    getActivity().onBackPressed();
                } else {
                    dialog.dismiss();
                    AppUtils.showToast(UrlConstant.NO_INTERNET_ACTION);
                }

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void createJSON(HashMap<String, String> values, String status, int toChange) {
        ArrayList<JsonDataStore> jsonDataStoreArrayList = mapToList(values);
        insertOrUpdateEntityDB(jsonDataStoreArrayList, status, toChange);
    }

    private String checkKeyValueValid(String key) {
        for (Map.Entry<String, String> entry : postJsonData.entrySet()) {
            if (key.equals(entry.getKey()))
                if (AppUtils.isNullCase(entry.getValue()))
                    return "";
                else
                    return entry.getValue();
        }
        return null;
    }

    private void checkValidations(Activity activity, String data, int toChange,
                                  boolean action, String status) {
        validationMap.clear();
        validationProcessMap.clear();
        invoiceValidationProcessMap.clear();
        for (int i = 0; i < getViewFields.size(); i++) {
            String value = checkKeyValueValid(getViewFields.get(i).getKey());

            if (getViewFields.get(i).getType().equals(UrlConstant.MANY2ONE)) {
                if (isReq(getViewFields.get(i).getKey()))
                    if ((AppUtils.isNullCase(value) || (value.equals("0"))))
                        validationMap.put(getViewFields.get(i).getKey(), UrlConstant.REQ_FIELD);
            } else if (getViewFields.get(i).getType().equals(UrlConstant.STRING)) {
                if (isReq(getViewFields.get(i).getKey()))
                    if (AppUtils.isNullCase(value))
                        validationMap.put(getViewFields.get(i).getKey(), UrlConstant.REQ_FIELD);
                checkPattern(getViewFields.get(i), value);
            } else if (getViewFields.get(i).getType().equals(UrlConstant.SELECTION)) {
                if (isReq(getViewFields.get(i).getKey()))
                    if (AppUtils.isNullCase(value))
                        validationMap.put(getViewFields.get(i).getKey(), UrlConstant.REQ_FIELD);
            } else if (getViewFields.get(i).getType().equals(UrlConstant.LOT_NUMBER)) {
                if (isReq(getViewFields.get(i).getKey()))
                    if (AppUtils.isNullCase(value))
                        validationMap.put(getViewFields.get(i).getKey(), UrlConstant.REQ_FIELD);

            } else if (getViewFields.get(i).getType().equals(UrlConstant.FLOAT)) {
                if (isReq(getViewFields.get(i).getKey()))
                    if (AppUtils.isNullCase(value))
                        validationMap.put(getViewFields.get(i).getKey(), UrlConstant.REQ_FIELD);

                checkPattern(getViewFields.get(i), value);
            } else if (getViewFields.get(i).getType().equals(UrlConstant.NOTE)) {
                if (isReq(getViewFields.get(i).getKey()))
                    if (AppUtils.isNullCase(value))
                        validationMap.put(getViewFields.get(i).getKey(), UrlConstant.REQ_FIELD);

                checkPattern(getViewFields.get(i), value);
            } else if (getViewFields.get(i).getType().equals(UrlConstant.QUANTITY)) {
                for (int j = 0; j < getViewFields.get(i).getSub_fields().size(); j++) {
                    if (getViewFields.get(i).getSub_fields().get(j).getType().equals(UrlConstant.FLOAT)) {
                        value = checkKeyValueValid(getViewFields.get(i).getSub_fields().get(j).getKey());

                        if (isReq(getViewFields.get(i).getSub_fields().get(j).getKey()))
                            if (AppUtils.isNullCase(value))
                                validationMap.put(getViewFields.get(i).getKey(), UrlConstant.REQ_FIELD);

                        checkPattern(getViewFields.get(i).getSub_fields().get(j), value);
                    }
                }

            } else if (getViewFields.get(i).getType().equals(UrlConstant.MRP_LINES)) {
                value = checkKeyValueValid(getViewFields.get(i).getKey());
                if (!AppUtils.isNullCase(value)) {
                    Gson gson = new Gson();
                    List<FinsihedProduct.FinishedProd> finishedProdsList =
                            gson.fromJson(value, FinsihedProduct.class).getFinished_moves();
                    ArrayList<GetViewFields> getViewFieldsList = getViewFields.get(i).getSub_fields();
                    for (int j = 0; j < getViewFieldsList.size(); j++) {
                        if (getViewFieldsList.get(j).getType().equals(UrlConstant.MANY2ONE))
                            for (int k = 0; k < getViewFieldsList.get(j).getSelection_values().size(); k++) {
                                GetIdName getIdName = getViewFieldsList.get(j).getSelection_values().get(k);
                                for (int l = 0; l < finishedProdsList.size(); l++) {
                                    if (("" + getIdName.getId()).equals("" +
                                            finishedProdsList.get(l).getProduct_id()))
                                        if (getIdName.getTracking().equals("lot"))
                                            if (AppUtils.isNullCase(finishedProdsList.get(l).getLot()))
                                                validationProcessMap.put("" +
                                                                finishedProdsList.get(l).getProduct_id(),
                                                        "Enter Lot Number");

                                }
                            }
                        break;
                    }
                }
            } else if (((keyName.equals(UrlConstant.VENDOR_BILL))
                    || (keyName.equals(UrlConstant.MANDI_PRICE))
                    || (keyName.equals(UrlConstant.NODE_PRICE))
                    || (keyName.equals(UrlConstant.PAYMENT_REQUEST))) &&
                    getViewFields.get(i).getType().equals(UrlConstant.ONE2MANY)) {
                value = checkKeyValueValid(getViewFields.get(i).getKey());
                if (!AppUtils.isNullCase(value)) {
                    Gson gson = new Gson();
                    List<List<MultipleLines.GetKeyValue>> invoiceLineIds;
                    if (keyName.equals(UrlConstant.VENDOR_BILL))
                        invoiceLineIds =
                                gson.fromJson(value, MultipleLines.class).getInvoice_line_ids();
                    else if (keyName.equals(UrlConstant.PAYMENT_REQUEST))
                        invoiceLineIds =
                                gson.fromJson(value, MultipleLines.class).getRequest_line_ids();
                    else
                        invoiceLineIds =
                                gson.fromJson(value, MultipleLines.class).getProduct_price();
                    ArrayList<GetViewFields> getViewFieldsList = getViewFields.get(i).getSub_fields();
                    for (int l = 0; l < getViewFieldsList.size(); l++) {

                        for (int j = 0; j < invoiceLineIds.size(); j++) {
                            HashMap<String, String> valuesMap = new HashMap<>();
                            HashMap<String, String> tempMap = new HashMap<>();
                            for (int k = 0; k < invoiceLineIds.get(j).size(); k++) {
                                valuesMap.put(invoiceLineIds.get(j).get(k).getKey(),
                                        invoiceLineIds.get(j).get(k).getValue());
                            }
                            if (getViewFieldsList.get(l).getType().equals(UrlConstant.MANY2ONE)) {
                                if (one2manyisReq(getViewFieldsList.get(l).getKey())) {
                                    if (valuesMap.containsKey(getViewFieldsList.get(l).getKey())) {
                                        if (AppUtils.isNullCase(valuesMap.get(getViewFieldsList.get(l).getKey()))
                                                || valuesMap.get(getViewFieldsList.get(l).getKey()).equals("0")) {
                                            tempMap.put(getViewFieldsList.get(l).getKey(),
                                                    UrlConstant.REQ_FIELD);
                                            invoiceValidationProcessMap.put(j, tempMap);
                                        }
                                    }
                                }
                            } else if (getViewFieldsList.get(l).getType().
                                    equals(UrlConstant.FLOAT)) {
                                if (getViewFieldsList.get(l).getKey().
                                        equals(UrlConstant.APPROVED_AMOUNT)) {
                                    if (AppUtils.isNullWith0Case(valuesMap.get(UrlConstant.APPROVED_AMOUNT))) {
                                        if (AppUtils.isNullCase(getViewFieldsList.get(l).
                                                getInput_validation_error()))
                                            tempMap.put(getViewFieldsList.get(l).getKey(),
                                                    UrlConstant.REQ_FIELD);
                                        else
                                            tempMap.put(getViewFieldsList.get(l).getKey(),
                                                    getViewFieldsList.get(l).getInput_validation_error());
                                        invoiceValidationProcessMap.put(j, tempMap);
                                    }
                                } else {
                                    if (one2manyisReq(getViewFieldsList.get(l).getKey())) {
                                        if (valuesMap.containsKey(getViewFieldsList.get(l).getKey())) {
                                            if (AppUtils.isNullCase(valuesMap.get(
                                                    getViewFieldsList.get(l).getKey()))) {
                                                tempMap.put(getViewFieldsList.get(l).getKey(),
                                                        UrlConstant.REQ_FIELD);
                                                invoiceValidationProcessMap.put(j, tempMap);
                                                checkInvoicePattern(j, getViewFieldsList.get(l),
                                                        valuesMap.get(getViewFieldsList.get(l).getKey()));
                                            }
                                        } else {
                                            if (AppUtils.isNullCase(getViewFieldsList.get(l).
                                                    getInput_validation_error()))
                                                tempMap.put(getViewFieldsList.get(l).getKey(),
                                                        UrlConstant.REQ_FIELD);
                                            else
                                                tempMap.put(getViewFieldsList.get(l).getKey(),
                                                        getViewFieldsList.get(l).getInput_validation_error());
                                            invoiceValidationProcessMap.put(j, tempMap);
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }

        if ((!validationMap.isEmpty()) || (!validationProcessMap.isEmpty()) ||
                (!invoiceValidationProcessMap.isEmpty()))
            displayData(activity);
        else {
            if (AppUtils.haveNetworkConnection(activity)) {
                AppUtils.showProgressDialog(activity);
                if (!action)
                    sendSyncEntityData(activity, toChange);
            } else
                createJSON(postJsonData, status, toChange);
            if (action) {
                actionData = true;
                return;
            } else {
                if (!AppUtils.haveNetworkConnection(activity)) {
                    AppPreference.getInstance().setDATA_CHANGE_FLAG(false);
                    onBackHomePressed(activity, data);
                }
            }
        }
    }

    private void sendSyncEntityData(final Activity activity, final int toChange) {
        final ArrayList<JsonDataStore> jsonDataStoreArrayList = mapToList(postJsonData);

        boolean toConfirm = false;
        AppRestClient client = AppRestClient.getInstance();
        EntityCreateDataBody.CreateBody createBody = new EntityCreateDataBody.CreateBody();
        createBody.setFields(jsonDataStoreArrayList);
        EntityCreateDataBody.MetaData metaData = new EntityCreateDataBody.MetaData();
        metaData.setClient_id(rowid);
        if (entity_id == 0L)
            metaData.setEntity_id(0L);
        else
            metaData.setEntity_id(entity_id);
        metaData.setVersion(schema_version);

        EntityCreateDataBody entityCreateDataBody = new EntityCreateDataBody(createBody, metaData);
        toConfirm = toChange == 1;
        Call<ResponseCreateEntity> call = null;
        if (!AppUtils.isNullCase(keyName)) {
            String method = entity_id == 0L ? UrlConstant.CREATE_METHOD : UrlConstant.UPDATE_METHOD;
            try {
                Object Call = client.getClass()
                        .getDeclaredMethod(UrlConstant.key_mapping.get(keyName).get(method),
                                boolean.class, EntityCreateDataBody.class)
                        .invoke(client, toConfirm, entityCreateDataBody);
                call = (retrofit2.Call) Call;
            } catch (Exception e) {
                call = null;
            }
        }
        call.enqueue(new Callback<ResponseCreateEntity>() {
            @Override
            public void onResponse(Call<ResponseCreateEntity> call,
                                   Response<ResponseCreateEntity> response) {
                if (response == null)
                    return;
                Date date = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                EntityDataJson entityDataJsonTemp = new EntityDataJson();

                Gson gson = new Gson();
                String jsonString = gson.toJson(jsonDataStoreArrayList);
                entityDataJsonTemp.setEntity_type(keyName);
                entityDataJsonTemp.setJson_payload(jsonString);
                entityDataJsonTemp.setEntity_status(UrlConstant.NEW);
                entityDataJsonTemp.setPayload_version(fieldsData.getVersion());
                entityDataJsonTemp.setWrite_date(dateFormat.format(date));
                entityDataJsonTemp.setLock_data(UrlConstant.FALSE);
                entityDataJsonTemp.setAction_required(0);

                if (response.code() == 200) {
                    entity_id = response.body().getMetadata().getEntity_id();
                    entityDataJsonTemp.setEntity_id(entity_id);

                    if (!response.body().isDialog_occured()) {
                        entityDataJsonTemp.setSync_status(UrlConstant.SUCCESS);
                        entityDataJsonTemp.setValidation_status("");
                    } else {
                        entityDataJsonTemp.setSync_status(UrlConstant.ERRORS);
                        entityDataJsonTemp.setValidation_status("");
                        entityDataJsonTemp.setDialog_id(response.body().getDialog_id());
                    }

                } else if (response.code() == 500) {
                    entityDataJsonTemp.setSync_status(UrlConstant.ERRORS);
                    entityDataJsonTemp.setEntity_id(entity_id);
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        if (jObjError.get("error_type").equals(UrlConstant.UNHANDLED_EXCEPTION)) {
                            entityDataJsonTemp.setValidation_status("Technical Problem");
                        } else
                            entityDataJsonTemp.setValidation_status("" + jObjError.get("error_message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                rowid = databaseHandler.updateServiceEntityData(entityDataJsonTemp, rowid);
                if (response.code() == 200)
                    getEntityData(activity);
                else {
                    loadData(activity);
                }
                AppUtils.hideProgressDialog();
            }

            @Override
            public void onFailure(Call<ResponseCreateEntity> call, Throwable t) {

                AppUtils.hideProgressDialog();

                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(activity))
                        AppUtils.showToast(UrlConstant.NO_INTERNET);
                    else
                        AppUtils.showToast(UrlConstant.SERVER_NOT_RES);
                } else {
                    AppUtils.showToast(UrlConstant.TECH_PROB);
                }
            }
        });

    }

    private boolean isReq(String key) {
        for (String tempKey : requiredField) {
            if (tempKey.equals(key))
                return true;
        }
        return false;
    }

    private boolean one2manyisReq(String key) {
        for (String tempKey : one2ManyrequiredField) {
            if (tempKey.equals(key))
                return true;
        }
        return false;
    }

    private void checkPattern(GetViewFields getViewFields, String value) {
        if (AppUtils.isNullCase(getViewFields.getInput_validations()))
            return;
        if (AppUtils.isNullCase(value))
            value = "";
        Pattern p = Pattern.compile(getViewFields.getInput_validations());
        Matcher m = p.matcher(value);
        if (!m.find())
            validationMap.put(getViewFields.getKey(), getViewFields.getInput_validation_error());
    }

    private void checkInvoicePattern(int pos, GetViewFields getViewFields, String value) {
        if (AppUtils.isNullCase(getViewFields.getInput_validations()))
            return;
        if (AppUtils.isNullCase(value))
            value = "";
        Pattern p = Pattern.compile(getViewFields.getInput_validations());
        Matcher m = p.matcher(value);
        if (!m.find()) {
            HashMap<String, String> tempMap = new HashMap<>();
            tempMap.put(getViewFields.getKey(), getViewFields.getInput_validation_error());
            invoiceValidationProcessMap.put(pos, tempMap);
        }
    }

    private void insertOrUpdateEntityDB(ArrayList<JsonDataStore> jsonObject, String
            status, int toChange) {
        if (keyName.equals(UrlConstant.PURCHASE_RECEIPT))
            if (rowid == 0) {
                LotNumber lotNumber = new LotNumber();
                String prod = checkKeyValue(UrlConstant.PRODUCT_ID, postJsonData);
                lotNumber.setLot_number_id(0L);
                lotNumber.setProduct_id(Long.valueOf(prod));

                String lot_num = checkKeyValue("lot_number", postJsonData);
                lotNumber.setTemp_lot_number(lot_num);
                lotNumber.setPerm_lot_number(lot_num);

                databaseHandler.insertLotNumbers(lotNumber);
            }

        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Gson gson = new Gson();
        String jsonString = gson.toJson(jsonObject);
        EntityDataJson entityDataJson = new EntityDataJson();
        entityDataJson.setEntity_type(keyName);
        entityDataJson.setJson_payload(jsonString);
        entityDataJson.setSync_status(status);
        if (rowid == 0) {
            entityDataJson.setValidation_status(null);
            entityDataJson.setEntity_status(UrlConstant.NEW);
            entityDataJson.setEntity_id(0L);
            entityDataJson.setPayload_version(fieldsData.getVersion());
            entityDataJson.setWrite_date(dateFormat.format(date));
            entityDataJson.setLock_data(UrlConstant.FALSE);
            entityDataJson.setAction_required(toChange);
            databaseHandler.insertEntityData(entityDataJson);
        } else {
            entityDataJson.setValidation_status(dbFieldsData.getValidation_status());
            entityDataJson.setEntity_status(dbFieldsData.getEntity_status());
            entityDataJson.setEntity_id(dbFieldsData.getEntity_id());
            entityDataJson.setPayload_version(fieldsData.getVersion());
            entityDataJson.setWrite_date(dateFormat.format(date));
            entityDataJson.setLock_data(UrlConstant.FALSE);
            entityDataJson.setAction_required(toChange);
            databaseHandler.updateEntityData(entityDataJson, rowid);

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        if (rowid != 0)
            databaseHandler.updateLockDataFalse(rowid);

        if (actionDialog != null)
            actionDialog.dismiss();

        Bundle params = new Bundle();
        params.putString("mobile", "" + AppPreference.getInstance().getUSER_ID());
        mFirebaseAnalytics.logEvent(keyName, params);
    }


    @Override
    public void onResume() {
        super.onResume();
        AppUtils.checkNetwork(getActivity(), networkBar, true);
        InputMethodManager imm = (InputMethodManager) getActivity().
                getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onClick(boolean refresh, HashMap<String, String> value) {
        postJsonData.putAll(value);
        if (refresh)
            amount_total.setText("₹" + postJsonData.get(UrlConstant.AMOUNT_TOTAL));
    }

    public void checkData(Activity activity, String data) {
        if (AppPreference.getInstance().getDATA_CHANGE_FLAG()) {
            if (!postJsonData.isEmpty()) {
                showCleanDataDialog(activity, data);
            } else {
                AppPreference.getInstance().setDATA_CHANGE_FLAG(false);
                activity.onBackPressed();
            }
        } else {
            onBackHomePressed(activity, data);
        }
    }

    private void createFileData() {
        fileData = "";
        fileData = "DeHaat " + fieldsData.getDisplay_name() + "\n" + "\n";
        for (int i = 0; i < getViewFields.size(); i++) {
            String value = AppUtils.generateDisplayData(getActivity(), getViewFields.get(i).getKey(),
                    keyName, postJsonData);
            storeValueInFile(getViewFields.get(i).getDisplay_Name(), value);
        }
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, fileData);
        try {
            activity.startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex) {
        }
    }

    private void storeValueInFile(String displayName, String value) {
        if (!AppUtils.isNullCase(value))
            fileData = fileData + displayName + " : " + value + "\n";
    }

    private String checkKeyValue(String keyNme, HashMap<String, String> dataList) {
        for (Map.Entry<String, String> entry : dataList.entrySet()) {
            if (entry.getKey().equals(keyNme))
                return entry.getValue();
        }
        return null;
    }

    private void onBackHomePressed(Activity activity, String data) {
        if (AppUtils.isNullCase(data))
            activity.onBackPressed();
        else {
            getActivity().getSupportFragmentManager().popBackStack(null,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
            getFragmentManager().beginTransaction().replace(R.id.frag_container,
                    HomeFragment.newInstance()).commit();
        }
    }

    @Override
    public void requiredField(HashSet<String> hashSet) {
        requiredField.addAll(hashSet);
    }

    @Override
    public void onIdClick(Long id) {
        rawProdId = id;
        loadData(getActivity());
    }

    @Override
    public void onDataCall(HashMap<Integer, FinsihedProduct.FinishedProd> data) {
        storeLinesIntoMap(data);
    }

    private void storeLinesIntoMap(HashMap<Integer, FinsihedProduct.FinishedProd> data) {
        ArrayList<FinsihedProduct.FinishedProd> finishedProds = new ArrayList<>();

        if (!data.isEmpty())
            for (Map.Entry<Integer, FinsihedProduct.FinishedProd> entry : data.entrySet()) {
                finishedProds.add(entry.getValue());
            }

        FinsihedProduct finsihedProduct = new FinsihedProduct();
        finsihedProduct.setFinished_moves(finishedProds);
        Gson gson = new Gson();
        String jsonData = gson.toJson(finsihedProduct);
        postJsonData.put("finished_moves", jsonData);
    }

    @Override
    public void onBillClick(List<Long> id) {
        billIds = id;
    }

    @Override
    public void onItemClick(String s) {
        imageFileList.remove(Integer.parseInt(s));
    }

    @Override
    public void onClick(Long id, boolean dlink) {
        if (dlink) {
            getEntityData(getActivity());
        }
    }

    @Override
    public void show(boolean b, boolean button) {
        if (keyName.equals(UrlConstant.VENDOR_BILL))
            displayMultipleItems(false, button);
        else if (keyName.equals(UrlConstant.PAYMENT_REQUEST)) {
            displayPaymentData(false, button);
        } else if (keyName.equals(UrlConstant.MANDI_PRICE)
                || keyName.equals(UrlConstant.NODE_PRICE)) {
            displayPriceData(false, button);
        }

    }

    @Override
    public void one2ManyRequiredField(HashSet<String> hashSet) {
        one2ManyrequiredField.addAll(hashSet);
    }

    public void setReceivingFragment(EntityCreationFragment receivingFragment) {
        this.receivingFragment = receivingFragment;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setSelectedPayload(Object selectedPayload) {
        this.selectedPayload = selectedPayload;
    }
}

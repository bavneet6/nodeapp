package com.dehaat.node.fragments;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dehaat.node.R;
import com.dehaat.node.adapter.RecyclerAdapterEntityList1;
import com.dehaat.node.database.DatabaseHandler;
import com.dehaat.node.rest.response.EntityDataJson;
import com.dehaat.node.rest.response.GetEntityList;
import com.dehaat.node.rest.response.MultipleLines;
import com.dehaat.node.utilities.AppPreference;
import com.dehaat.node.utilities.AppUtils;
import com.dehaat.node.utilities.MainService;
import com.dehaat.node.utilities.PaginationScrollListener;
import com.dehaat.node.utilities.UrlConstant;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EntityListFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = EntityListFragment.class.getSimpleName();
    private ImageView back, refreshData, search, info, priceGraph;
    private TextView heading, createNew, networkBar, no_item, listSize;
    private String keyName, keyNameList;
    private RecyclerView listData;
    private DatabaseHandler databaseHandler;
    private LinearLayoutManager linearLayoutManager;
    private ProgressBar progressBar;
    private ArrayList<EntityDataJson> entityDataList = new ArrayList<>();
    private RecyclerAdapterEntityList1 recyclerAdapterEntityList;
    private String[] searchTextIdList;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int limit = 0;
    private final int limitSize = 10;
    private FirebaseAnalytics mFirebaseAnalytics;
    // limiting to 5 for this tutorial, since total pages in actual API is very large. Feel free to modify.
    BroadcastReceiver broadCastNewMessage = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // here you can update your db with new messages and update the ui (chat message list)
            progressBar.setVisibility(View.VISIBLE);
            displayListData();
        }
    };

    public static EntityListFragment newInstance() {
        return new EntityListFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_entity_list, null);
        showActionBar(false);
        databaseHandler = new DatabaseHandler(getActivity());
        linearLayoutManager = new LinearLayoutManager(
                getActivity(), RecyclerView.VERTICAL, false);
        back = v.findViewById(R.id.back);
        refreshData = v.findViewById(R.id.refreshData);
        search = v.findViewById(R.id.search);
        info = v.findViewById(R.id.info);
        priceGraph = v.findViewById(R.id.priceGraph);
        heading = v.findViewById(R.id.heading);
        createNew = v.findViewById(R.id.createNew);
        networkBar = v.findViewById(R.id.networkBar);
        no_item = v.findViewById(R.id.no_item);
        listSize = v.findViewById(R.id.listSize);
        listData = v.findViewById(R.id.listData);
        progressBar = v.findViewById(R.id.progressBar);
        listData.setNestedScrollingEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        back.setOnClickListener(this);
        createNew.setOnClickListener(this);
        refreshData.setOnClickListener(this);
        search.setOnClickListener(this);
        info.setOnClickListener(this);

        Bundle bundle = getArguments();
        if (bundle != null) {
            keyName = bundle.getString("KEY_NAME");
            keyNameList = UrlConstant.key_mapping.get(keyName).get(UrlConstant.PAGE_HEAD);

            if (keyName.equals(UrlConstant.MANDI_PRICE) || keyName.equals(UrlConstant.NODE_PRICE)) {
                priceGraph.setVisibility(View.VISIBLE);
                priceGraph.setOnClickListener(this);
            }
        }
        //calling service
        Intent serviceIntent = new Intent(getActivity(), MainService.class);
        serviceIntent.putExtra("DATA", keyName);
        serviceIntent.putExtra("TIME", 10000);
        getActivity().startService(serviceIntent);
        AppUtils.showBanner(getActivity(), networkBar);
        if (keyName.equals(UrlConstant.SALES_DISPATCH)
                || keyName.equals(UrlConstant.VENDOR_BILL)
                || keyName.equals(UrlConstant.PURCHASE_RECEIPT)
                || keyName.equals(UrlConstant.STORAGE_INVOICE)
                || keyName.equals(UrlConstant.COMMODITY_LOAN_INVOICE)
                || keyName.equals(UrlConstant.STORAGE_RECEIPT)
                || keyName.equals(UrlConstant.COMMODITY_LOAN)
                || keyName.equals(UrlConstant.STORAGE_DISPATCH)) {
            createNew.setVisibility(View.GONE);
        } else {
            createNew.setVisibility(View.VISIBLE);
        }
        displayListData();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        return v;
    }

    private void displayListData() {
        recyclerAdapterEntityList = new RecyclerAdapterEntityList1(getActivity(),
                entityDataList, keyName);
        listData.setLayoutManager(linearLayoutManager);
        listData.setItemAnimator(new DefaultItemAnimator());
        listData.setAdapter(recyclerAdapterEntityList);
        listData.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;

                // mocking network delay for API call
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPage();
                    }
                }, 3000);
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        loadFirstPage();
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadCastNewMessage);
        Bundle params = new Bundle();
        params.putString("mobile", "" + AppPreference.getInstance().getUSER_ID());
        mFirebaseAnalytics.logEvent(keyName + "List", params);
    }

    @Override
    public void onResume() {
        super.onResume();
        AppUtils.checkNetwork(getActivity(), networkBar, true);
        getActivity().registerReceiver(this.broadCastNewMessage,
                new IntentFilter("DATA_SYNC"));

    }

    private void storeListDataDB(ArrayList<GetEntityList.CreateBody> data) {
        for (int i = 0; i < data.size(); i++) {
            databaseHandler.insertOrUpdateEntityData(keyName, data.get(i),
                    data.get(i).getEntity_id());
        }
    }

    private void displaySchmeaListData() {
        heading.setText(keyNameList);
        listSize.setText(entityDataList.size() + " Records ");
        if (entityDataList.size() != 0) {
            no_item.setVisibility(View.GONE);
            listData.setVisibility(View.VISIBLE);
            // to display the data as per the schema
        } else {
            no_item.setVisibility(View.VISIBLE);
            listData.setVisibility(View.GONE);
        }
    }

    private void loadFirstPage() {
        limit = 0;
        entityDataList.clear();
        getListAPI().enqueue(new Callback<GetEntityList>() {
            @Override
            public void onResponse(Call<GetEntityList> call, Response<GetEntityList> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null && response.body().getCreateBody() != null &&
                        response.body().getCreateBody().size() != 0) {
                    databaseHandler.deleteAllEntityData(keyName);
                    storeListDataDB(response.body().getCreateBody());
                    entityDataList = databaseHandler.getEntityListData(keyName);
                    recyclerAdapterEntityList.addAll(entityDataList);
                }
                displaySchmeaListData();
                if (entityDataList.size() == 0)
                    isLastPage = true;
                else {
                    isLastPage = false;
                    recyclerAdapterEntityList.addLoadingFooter();
                }
            }

            @Override
            public void onFailure(Call<GetEntityList> call, Throwable t) {

            }
        });
    }

    private void loadNextPage() {
        entityDataList.clear();
        limit = limit + limitSize;
        getListAPI().enqueue(new Callback<GetEntityList>() {
            @Override
            public void onResponse(Call<GetEntityList> call, Response<GetEntityList> response) {
                recyclerAdapterEntityList.removeLoadingFooter();
                isLoading = false;
                if (response.body() != null && response.body().getCreateBody() != null &&
                        response.body().getCreateBody().size() != 0) {
                    storeListDataDB(response.body().getCreateBody());
                    ArrayList<GetEntityList.CreateBody> data = response.body().getCreateBody();
                    searchTextIdList = new String[data.size()];

                    for (int i = 0; i < data.size(); i++)
                        searchTextIdList[i] = String.valueOf(data.get(i).getEntity_id());

                    entityDataList = databaseHandler.getStoredSearchedValues(false,
                            keyName, searchTextIdList);
                    recyclerAdapterEntityList.addAll(entityDataList);
                    listSize.setText(databaseHandler.getEntityListData(keyName).size() + " Records ");
                }
                if ((response.body().getCreateBody() == null) ||
                        (response.body().getCreateBody() != null &&
                                response.body().getCreateBody().size() == 0))
                    isLastPage = true;
                else {
                    isLastPage = false;
                    recyclerAdapterEntityList.addLoadingFooter();
                }
            }

            @Override
            public void onFailure(Call<GetEntityList> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.info:
                final Dialog dialog = new Dialog(getActivity());
                dialog.setCanceledOnTouchOutside(true);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_information);
                dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setGravity(Gravity.CENTER);
                dialog.show();
                break;
            case R.id.refreshData:
                if (AppUtils.haveNetworkConnection(getActivity())) {
                    progressBar.setVisibility(View.VISIBLE);
                    displayListData();
                } else {
                    AppUtils.showToast(UrlConstant.NO_INTERNET);
                }
                break;
            case R.id.createNew:
                Bundle bundle = new Bundle();
                bundle.putString("KEY_NAME", keyName);
                bundle.putLong("ROW_ID", 0);
                bundle.putBoolean("NOTES", false);
                bundle.putBoolean("CREATING_NEW", true);
                if (keyName.equals(UrlConstant.MANDI_PRICE)) {
                    HashMap<String, String> tempMapData = new HashMap<>();
                    Gson gson = new Gson();
                    String jsonString = null;
                    List<List<MultipleLines.GetKeyValue>> getKeyValueList = new ArrayList<>();
                    List<MultipleLines.GetKeyValue> list = new ArrayList<>();
                    MultipleLines.GetKeyValue getKeyValue = new MultipleLines.GetKeyValue();
                    getKeyValue.setKey(UrlConstant.PRODUCT_ID);
                    getKeyValue.setValue("");
                    list.add(getKeyValue);

                    getKeyValueList.add(list);
                    MultipleLines multipleLines = new MultipleLines();
                    multipleLines.setInvoice_line_ids(getKeyValueList);
                    jsonString = gson.toJson(multipleLines);

                    tempMapData.put(UrlConstant.INVOICE_LINE_IDS, jsonString);
                    bundle.putSerializable("JSON_DATA", tempMapData);
                }
                EntityCreationFragment fragment = EntityCreationFragment.newInstance();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(
                        R.id.frag_container, fragment).addToBackStack("").commit();
                break;
            case R.id.search:
                Bundle bundle1 = new Bundle();
                bundle1.putString("KEY_NAME", keyName);
                SearchingFragment fragment1 = SearchingFragment.newInstance();
                fragment1.setArguments(bundle1);
                getFragmentManager().beginTransaction().replace(R.id.frag_container,
                        fragment1).addToBackStack("").commit();
                break;
            case R.id.priceGraph:
                Bundle bundle2 = new Bundle();
                bundle2.putString("KEY_NAME", keyName);
                GraphFragment gFragment = GraphFragment.newInstance();
                gFragment.setArguments(bundle2);
                getFragmentManager().beginTransaction().replace(
                        R.id.frag_container, gFragment).addToBackStack("").commit();
                break;
        }
    }

    private Call<GetEntityList> getListAPI() {
        return AppUtils.getEntityList(
                keyName, null, limit, limitSize, null, null);
    }
}

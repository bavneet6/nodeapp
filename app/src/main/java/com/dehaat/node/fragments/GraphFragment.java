package com.dehaat.node.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.dehaat.node.R;
import com.dehaat.node.database.DatabaseHandler;
import com.dehaat.node.rest.ApiCallBack;
import com.dehaat.node.rest.AppRestClient;
import com.dehaat.node.rest.response.CommodityPrices;
import com.dehaat.node.rest.response.GetIdName;
import com.dehaat.node.rest.response.GetViewFields;
import com.dehaat.node.utilities.AppUtils;
import com.dehaat.node.utilities.UrlConstant;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.EntryXComparator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

public class GraphFragment extends Fragment implements View.OnClickListener {
    private final int maxGraphs = 3;
    private final String[] monthNames = new String[]{
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };
    private final String[] graphColors = new String[]{
            "#ffbd39", "#00bd56", "#6b48ff"
    };
    private final HashMap<String, String> fragHeadings = new HashMap<String, String>() {{
        put(UrlConstant.MANDI_PRICE, "Open Market Price Graph");
        put(UrlConstant.NODE_PRICE, "Node Price Graph");
    }};

    private String keyName;
    private TextView fragHeading;
    private LineChart chart;
    private ImageView backButton, mandiTagClose1, mandiTagClose2, mandiTagClose3;
    private Button addToGraph;
    private Spinner selectMandi, selectCommodity, selectMonth, selectPrice;
    private DatabaseHandler dbHandler;
    private HashMap<String, Long> mandiIdMap = new HashMap<>();
    private HashMap<String, Long> commodityIdMap = new HashMap<>();

    private RelativeLayout[] mandiTags = new RelativeLayout[maxGraphs];
    private TextView[] mandiTagNames = new TextView[maxGraphs];
    private ImageView[] mandiCloseTags = new ImageView[maxGraphs];

    private ArrayList<Entry>[] priceValues = new ArrayList[maxGraphs];
    private LineDataSet[] lineDataSets = new LineDataSet[maxGraphs];
    private ArrayList<ILineDataSet> dataSetArgs = new ArrayList<>();


    public GraphFragment() {
    }

    public static GraphFragment newInstance() {
        return new GraphFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View gFragment = inflater.inflate(R.layout.fragment_graph, container, false);

        fragHeading = gFragment.findViewById(R.id.fragHeading);
        mandiTagClose1 = gFragment.findViewById(R.id.mandiTagClose1);
        mandiTagClose2 = gFragment.findViewById(R.id.mandiTagClose2);
        mandiTagClose3 = gFragment.findViewById(R.id.mandiTagClose3);

        chart = gFragment.findViewById(R.id.chart);
        selectMandi = gFragment.findViewById(R.id.selectMandi);
        selectCommodity = gFragment.findViewById(R.id.selectCommodity);
        selectMonth = gFragment.findViewById(R.id.selectMonth);
        selectPrice = gFragment.findViewById(R.id.selectPrice);
        backButton = gFragment.findViewById(R.id.back);
        addToGraph = gFragment.findViewById(R.id.addToGraph);

        mandiTags[0] = gFragment.findViewById(R.id.mandiTag1);
        mandiTags[1] = gFragment.findViewById(R.id.mandiTag2);
        mandiTags[2] = gFragment.findViewById(R.id.mandiTag3);

        mandiTagNames[0] = gFragment.findViewById(R.id.mandiTagName1);
        mandiTagNames[1] = gFragment.findViewById(R.id.mandiTagName2);
        mandiTagNames[2] = gFragment.findViewById(R.id.mandiTagName3);

        mandiCloseTags[0] = gFragment.findViewById(R.id.mandiTagClose1);
        mandiCloseTags[1] = gFragment.findViewById(R.id.mandiTagClose2);
        mandiCloseTags[2] = gFragment.findViewById(R.id.mandiTagClose3);

        lineDataSets[0] = null;
        lineDataSets[1] = null;
        lineDataSets[2] = null;

        Bundle args = getArguments();
        if (args.get("KEY_NAME") != null) {
            keyName = args.get("KEY_NAME").toString();
            if (keyName.equals(UrlConstant.MANDI_PRICE)) {
                fragHeading.setText(fragHeadings.get(keyName));
            } else
                fragHeading.setText(fragHeadings.get(keyName));
        }
        dbHandler = new DatabaseHandler(getActivity());


        ArrayList<String> priceList = new ArrayList<>();
        ArrayList<String> mandiList = new ArrayList<>();

        ArrayList<GetViewFields> fields;
        if (keyName.equals(UrlConstant.MANDI_PRICE)) {
            for (Map.Entry<String, String> entry : UrlConstant.pricesMapping.entrySet()) {
                priceList.add(entry.getKey());
            }
            fields = dbHandler.getSchemaSingleView(
                    UrlConstant.MANDI_PRICE).getSchema().getFields();

        } else {
            for (Map.Entry<String, String> entry : UrlConstant.nodePricesMapping.entrySet()) {
                priceList.add(entry.getKey());
            }
            fields = dbHandler.getSchemaSingleView(
                    UrlConstant.NODE_PRICE).getSchema().getFields();
        }
        ArrayAdapter<String> priceAdapter = new ArrayAdapter<>(
                getActivity(), R.layout.template_mandi_spinner_text, priceList);
        priceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectPrice.setAdapter(priceAdapter);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                getActivity(), R.layout.template_mandi_spinner_text, monthNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectMonth.setAdapter(adapter);

        AppUtils.showProgressDialog(getActivity());
        ArrayList<String> commodityList = new ArrayList<>();

        for (GetViewFields field : fields) {
            if ((keyName.equals(UrlConstant.MANDI_PRICE) && field.getKey().equals(
                    UrlConstant.MANDI_ID)) || (keyName.equals(
                    UrlConstant.NODE_PRICE) && field.getKey().equals(UrlConstant.TEAM_ID))) {
                ArrayList<GetIdName> mandis = field.getSelection_values();
                for (GetIdName mandi : mandis) {
                    mandiList.add(mandi.getName());
                    mandiIdMap.put(mandi.getName(), mandi.getId());
                }
                Collections.sort(mandiList, String::compareTo);
            } else if (field.getType().equals(UrlConstant.ONE2MANY)) {
                for (int i = 0; i < field.getSub_fields().size(); i++) {
                    if (field.getSub_fields().get(i).getKey().equals(UrlConstant.PRODUCT_ID)) {
                        ArrayList<GetIdName> commodities = field.getSub_fields().get(i).getSelection_values();
                        for (GetIdName commodity : commodities) {
                            commodityList.add(commodity.getName());
                            commodityIdMap.put(commodity.getName(), commodity.getId());
                        }
                    }
                }

                Collections.sort(commodityList, String::compareTo);
            }
        }

        ArrayAdapter<String> mandiAdapter = new ArrayAdapter<>(
                getActivity(), R.layout.template_mandi_spinner_text, mandiList);
        mandiAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectMandi.setAdapter(mandiAdapter);
        ArrayAdapter<String> commodityAdapter = new ArrayAdapter<>(
                getActivity(), R.layout.template_mandi_spinner_text, commodityList);
        commodityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectCommodity.setAdapter(commodityAdapter);

        addToGraph.setOnClickListener(this);
        backButton.setOnClickListener(this);
        mandiTagClose1.setOnClickListener(this);
        mandiTagClose2.setOnClickListener(this);
        mandiTagClose3.setOnClickListener(this);

        chart.setPinchZoom(false);
        chart.getDescription().setEnabled(false);
        chart.getLegend().setEnabled(false);
        chart.setDrawGridBackground(false);
        chart.setNoDataText("Add mandi to render chart");
        chart.setNoDataTextColor(Color.LTGRAY);
        chart.setDragEnabled(false);
        chart.setScaleEnabled(false);
        chart.setDragXEnabled(true);
        chart.setScaleXEnabled(true);

        XAxis xAxis = chart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setValueFormatter(new XAxisValueFormatter());

        YAxis yAxisLeft = chart.getAxisLeft();
        yAxisLeft.setGranularity(0.5f);
        yAxisLeft.setDrawGridLines(false);
        yAxisLeft.setValueFormatter(new YAxisValueFormatter());

        YAxis yAxisRight = chart.getAxisRight();
        yAxisRight.setEnabled(false);

        int monthIndex = selectMonth.getSelectedItemPosition() + 1;
        final int year = 2019;
        if (selectMandi.getSelectedItem() != null && selectCommodity.getSelectedItem() != null) {
            final int mandiId = mandiIdMap.get(selectMandi.getSelectedItem()).intValue();
            final int productId = commodityIdMap.get(selectCommodity.getSelectedItem()).intValue();
            String priceSelection;
            if (keyName.equals(UrlConstant.MANDI_PRICE))
                priceSelection = UrlConstant.pricesMapping.get(selectPrice.getSelectedItem());
            else
                priceSelection = UrlConstant.nodePricesMapping.get(selectPrice.getSelectedItem());
            plotGraph(mandiId, productId, monthIndex, year, priceSelection);
        }

        AppUtils.hideProgressDialog();
        return gFragment;
    }

    private void plotGraph(int mandiId, int productId, int month, int year, String price) {
        fetchPriceData(mandiId, productId, month, year, price);
    }

    private ArrayList<CommodityPrices.GetData> fetchPriceData(int mandiId, int productId, int month,
                                                              int year, String price) {
        Call<CommodityPrices> call;
        if (keyName.equals(UrlConstant.MANDI_PRICE))
            call = AppRestClient.getInstance().getPriceData(
                    mandiId, productId, month, year, price);
        else
            call = AppRestClient.getInstance().getNodePriceData(
                    mandiId, productId, month, year, price);
        call.enqueue(new ApiCallBack<CommodityPrices>() {
            @Override
            public void onResponse(Response<CommodityPrices> response) {
                if (response.body() == null || response.body().getData() == null) return;
                ArrayList<CommodityPrices.GetData> priceData = response.body().getData();
                addDataToChart(priceData);
            }

            @Override
            public void onResponse401(Response<CommodityPrices> response) {
            }
        });
        return null;
    }

    private void addDataToChart(ArrayList<CommodityPrices.GetData> priceData) {
        int index = 0;
        if (chart.getData() != null)
            index = chart.getData().getDataSetCount();
        if (priceData.size() == 0) {
            if (chart.getData() == null) {
                chart.setNoDataText("Data not available for selected values");
                chart.invalidate();
            } else {
                AppUtils.showToast("No data for selected values");
            }
            return;
        } else {
            mandiTags[index].setVisibility(View.VISIBLE);
            mandiTagNames[index].setText(selectMandi.getSelectedItem().toString());
            mandiTagNames[index].setTextColor(Color.parseColor(graphColors[index]));
        }

        priceValues[index] = new ArrayList<>();
        for (int i = 0; i < priceData.size(); i++) {
            CommodityPrices.GetData data = priceData.get(i);
            priceValues[index].add(new Entry(data.getApplicableDate(), data.getPrice()));
        }
        Collections.sort(priceValues[index], new EntryXComparator());
        lineDataSets[index] = new LineDataSet(priceValues[index], "Price on Date");
        lineDataSets[index].setDrawIcons(false);
        lineDataSets[index].setDrawIcons(false);
        lineDataSets[index].setColor(Color.parseColor(graphColors[index]));
        lineDataSets[index].setCircleColor(Color.parseColor(graphColors[index]));
        lineDataSets[index].setLineWidth(1f);
        lineDataSets[index].setDrawFilled(false);

        dataSetArgs.add(lineDataSets[index]);

        LineData data = new LineData(dataSetArgs);
        chart.setData(data);
        chart.getDescription().setEnabled(false);
        chart.invalidate();
        if (chart.getData().getDataSetCount() == maxGraphs) {
            addToGraph.setEnabled(false);
        }
    }

    private void removeDataFromChart(int dataIndex) {
        if (chart.getData() != null) {
            for (int i = dataIndex; i < chart.getData().getDataSetCount() - 1; i++) {
                mandiTagNames[i].setText(mandiTagNames[i + 1].getText());
                lineDataSets[i] = lineDataSets[i + 1];
                lineDataSets[i].setColor(Color.parseColor(graphColors[i]));
                lineDataSets[i].setCircleColor(Color.parseColor(graphColors[i]));
                lineDataSets[i].setFillColor(Color.parseColor(graphColors[i]));
                lineDataSets[i].setCircleHoleColor(Color.parseColor(graphColors[i]));
                dataSetArgs.set(i, dataSetArgs.get(i + 1));
            }
            int delIndex = chart.getData().getDataSetCount() - 1;
            mandiTags[delIndex].setVisibility(View.INVISIBLE);
            lineDataSets[dataIndex] = null;
            dataSetArgs.remove(delIndex);

            LineData data = new LineData(dataSetArgs);
            chart.setData(data);
            chart.invalidate();
            addToGraph.setEnabled(true);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addToGraph:
                if (selectMandi.getSelectedItem() != null) {
                    int mandiId = mandiIdMap.get(selectMandi.getSelectedItem()).intValue();
                    int productId = commodityIdMap.get(selectCommodity.getSelectedItem()).intValue();
                    int month = selectMonth.getSelectedItemPosition() + 1;
                    int year = 2019;
                    String selectedPrice;
                    if (keyName.equals(UrlConstant.MANDI_PRICE))
                        selectedPrice = UrlConstant.pricesMapping.get(selectPrice.getSelectedItem());
                    else
                        selectedPrice = UrlConstant.nodePricesMapping.get(selectPrice.getSelectedItem());
                    plotGraph(mandiId, productId, month, year, selectedPrice);
                } else {
                    AppUtils.showToast("Market not selected to add to graph");
                }
                break;
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.mandiTagClose1:
                removeDataFromChart(0);
                break;
            case R.id.mandiTagClose2:
                removeDataFromChart(1);
                break;
            case R.id.mandiTagClose3:
                removeDataFromChart(2);
                break;
        }
    }

    private class YAxisValueFormatter extends ValueFormatter {
        private DecimalFormat dFormat;

        public YAxisValueFormatter() {
            dFormat = new DecimalFormat("###,###,##0.0");
        }

        @Override
        public String getFormattedValue(float value) {
            return "₹ " + dFormat.format(value);
        }
    }

    private class XAxisValueFormatter extends ValueFormatter {
        @Override
        public String getFormattedValue(float value) {
            return (int) value + " " + selectMonth.getSelectedItem();
        }
    }
}

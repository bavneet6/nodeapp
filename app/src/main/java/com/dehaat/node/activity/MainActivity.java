package com.dehaat.node.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.dehaat.node.R;
import com.dehaat.node.database.DatabaseHandler;
import com.dehaat.node.fragments.EntityCreationFragment;
import com.dehaat.node.fragments.HomeFragment;
import com.dehaat.node.rest.ApiCallBack;
import com.dehaat.node.rest.AppRestClient;
import com.dehaat.node.rest.response.GetEntityList;
import com.dehaat.node.utilities.AppPreference;
import com.dehaat.node.utilities.MainService;
import com.dehaat.node.utilities.UrlConstant;

import org.json.JSONException;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    FrameLayout frameLayout;
    private DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        frameLayout = findViewById(R.id.frag_container);
        FragmentManager fragManager = getSupportFragmentManager();
        Fragment fragment = fragManager.findFragmentByTag(HomeFragment.TAG);
        if (fragment == null)
            fragment = HomeFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frag_container, fragment,
                        HomeFragment.TAG).commitAllowingStateLoss();
        AWSMobileClient.getInstance().initialize(this).execute();
        onNewIntent(getIntent());

    }

    /*
   method is for push notification , to navigate to a particular page
     */
    private void checkNotification(Intent intent) {
        Bundle getData = intent.getExtras();
        if (getData == null)
            return;
        if (getData.getString("screen") == null)
            return;
        if (getData.getString("screen").equals(UrlConstant.PAYMENT_REQUEST))
            getDataAndStore(getData.getString("ID"));
    }

    private void getDataAndStore(String id) {
        AppRestClient client = AppRestClient.getInstance();
        Call<GetEntityList> call = null;
        call = client.getPaymentRequestList(id, 0, 0, null,
                null);
        call.enqueue(new ApiCallBack<GetEntityList>() {
            @Override
            public void onResponse(Response<GetEntityList> response) {
                if (response.body() == null)
                    return;
                if ((response.body().getCreateBody() != null) &&
                        (response.body().getCreateBody().size() != 0))
                    storeDataInDB(id, response.body().getCreateBody());

            }

            @Override
            public void onResponse401(Response<GetEntityList> response) throws JSONException {
            }
        });
    }

    private void storeDataInDB(String id, ArrayList<GetEntityList.CreateBody> createBody) {
        databaseHandler = new DatabaseHandler(this);
        Long rowId = databaseHandler.insertOrUpdateEntityData(UrlConstant.PAYMENT_REQUEST,
                createBody.get(0), Long.parseLong(id));

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment currentFrag = this.getSupportFragmentManager().findFragmentById(R.id.frag_container);
        EntityCreationFragment fragment = EntityCreationFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString("KEY_NAME", UrlConstant.PAYMENT_REQUEST);
        bundle.putLong("ROW_ID", rowId);
        bundle.putBoolean("NOTES", false);
        fragment.setArguments(bundle);
        if (currentFrag instanceof EntityCreationFragment)
            transaction.replace(R.id.frag_container, fragment).commit();
        else
            transaction.replace(R.id.frag_container, fragment).addToBackStack("").commit();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        checkNotification(intent);
    }
    @Override
    protected void onPause() {
        super.onPause();
        Intent intent = new Intent(this, MainService.class);
        startService(intent);
    }


    @Override
    public void onBackPressed() {
        Fragment currentFrag = this.getSupportFragmentManager().findFragmentById(R.id.frag_container);
        if (currentFrag instanceof EntityCreationFragment) {
            if (AppPreference.getInstance().getDATA_CHANGE_FLAG())
                EntityCreationFragment.newInstance().checkData(MainActivity.this, null);
            else
                super.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }
}

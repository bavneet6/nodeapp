package com.dehaat.node.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.dehaat.node.R;
import com.dehaat.node.fragments.LoginFragment;
import com.dehaat.node.utilities.AppPreference;

public class LoginActivity extends AppCompatActivity {
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        frameLayout = findViewById(R.id.frag_cont);

        if (AppPreference.getInstance().getAPP_LOGIN())
            launchMainAct();
        else
            launchLogin();
    }

    private void launchMainAct() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void launchLogin() {
        FragmentManager fragManager = getSupportFragmentManager();
        Fragment fragment = fragManager.findFragmentByTag(LoginFragment.TAG);
        if (fragment == null)
            fragment = LoginFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frag_cont, fragment,
                        LoginFragment.TAG).commitAllowingStateLoss();
    }
}

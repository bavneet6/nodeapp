package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Expenses {
    @SerializedName("expenses")
    public List<Expense> expenses;

    public List<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(List<Expense> expenses) {
        this.expenses = expenses;
    }

    public static class Expense {
        @SerializedName("amount")
        public Float amount;
        @SerializedName("category_id")
        public Long category_id;
        @SerializedName("id")
        public Long id;
        @SerializedName("deleted")
        public Boolean deleted;

        public Boolean isDeleted() {
            return deleted;
        }

        public void setDeleted(boolean deleted) {
            this.deleted = deleted;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Float getAmount() {
            return amount;
        }

        public void setAmount(Float amount) {
            this.amount = amount;
        }

        public Long getCategory_id() {
            return category_id;
        }

        public void setCategory_id(Long category_id) {
            this.category_id = category_id;
        }
    }
}

package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 3/16/2018.
 */

public class VersionName {
    @SerializedName("version")
    private String version;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}

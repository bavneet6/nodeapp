package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserDetails {
    @SerializedName("data")
    public ArrayList<JsonDataStore> data;

    public UserDetails(ArrayList<JsonDataStore> data) {
        this.data = data;
    }

    public ArrayList<JsonDataStore> getData() {
        return data;
    }
}

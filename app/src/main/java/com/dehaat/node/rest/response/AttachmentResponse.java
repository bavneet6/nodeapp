package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AttachmentResponse {

    @SerializedName("data")
    private List<String> data;

    public List<String> getData() {
        return data;
    }
}

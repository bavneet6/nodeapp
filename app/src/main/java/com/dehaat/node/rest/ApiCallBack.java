package com.dehaat.node.rest;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;

import com.dehaat.node.Node;
import com.dehaat.node.utilities.AppUtils;
import com.dehaat.node.utilities.UrlConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class ApiCallBack<T> implements Callback<T> {
    Context context = Node.getInstance();

    @Override
    public void onFailure(Call<T> call, Throwable t) {// when no response is recieved
        AppUtils.hideProgressDialog();
        //TODO: Error
        Log.e("Error", " " + t.getCause());
        t.printStackTrace();
        if (context == null)
            return;
        if (t instanceof ConnectException) {
            if (!haveNetworkConnection())
                AppUtils.showToast(UrlConstant.NO_INTERNET);
            else
                AppUtils.showToast(UrlConstant.SERVER_NOT_RES);
        } else
            AppUtils.showToast(UrlConstant.TECH_PROB);
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            try {
                onResponse(response);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (response.code() == 401) {
            try {
                onResponse401(response);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            String error = null;
            try {
                error = response.errorBody().string();

                if (!TextUtils.isEmpty(error)) {
                    JSONObject obj = new JSONObject(error);
                    Log.e("Error: ", "" + error);
                    if (response.code() == 500) {
                        AppUtils.showToast(UrlConstant.SERVER_ERROR);
                    } else {
                        AppUtils.showToast(UrlConstant.SERVER_NOT_RES);
                    }
                }
            } catch (Exception e) {
                Log.e("Error: ", "" + response.raw().message());
                AppUtils.showToast(UrlConstant.SERVER_NOT_RES);
            }
        }
        AppUtils.hideProgressDialog();

    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public abstract void onResponse(Response<T> response) throws JSONException;

    public abstract void onResponse401(Response<T> response) throws JSONException;
}

package com.dehaat.node.rest;

import android.text.TextUtils;
import android.util.Log;

import com.dehaat.node.Node;
import com.dehaat.node.rest.body.Attachments;
import com.dehaat.node.rest.body.EntityCreateDataBody;
import com.dehaat.node.rest.body.Linking;
import com.dehaat.node.rest.body.LoginBody;
import com.dehaat.node.rest.body.NewRecord;
import com.dehaat.node.rest.response.AttachmentResponse;
import com.dehaat.node.rest.response.CommodityPrices;
import com.dehaat.node.rest.response.EntityView;
import com.dehaat.node.rest.response.FcmToken;
import com.dehaat.node.rest.response.GetEntityList;
import com.dehaat.node.rest.response.GetIdNames;
import com.dehaat.node.rest.response.LoginResponse;
import com.dehaat.node.rest.response.MandiCreate;
import com.dehaat.node.rest.response.ResponseCreateEntity;
import com.dehaat.node.rest.response.UserInfo;
import com.dehaat.node.rest.response.VendorBill;
import com.dehaat.node.rest.response.VersionName;
import com.dehaat.node.utilities.AppPreference;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppRestClient {

    public static String apiBaseUrl = AppPreference.getInstance().getIP();
    static int cacheSize = 10 * 1024 * 1024; // 10 MB
    static Cache cache = new Cache(Node.getInstance().getCacheDir(), cacheSize);
    private static AppRestClient mInstance;
    private static AppRestClientService appRestClientService;

    private AppRestClient() {
        setRestClient();
    }

    public static AppRestClient getInstance() {
        if (mInstance == null) {
            synchronized (AppRestClient.class) {
                if (mInstance == null)
                    mInstance = new AppRestClient();
            }
        }
        return mInstance;
    }

    private static void setRestClient() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        final HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .cache(cache)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .addInterceptor(httpLoggingInterceptor) //TODO: debug
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        String authToken = AppPreference.getInstance().getAuthToken();
                        if (TextUtils.isEmpty(authToken))
                            authToken = "";
                        Request request = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .header("Authorization", "Bearer " + authToken)
                                .method(original.method(), original.body())
                                .build();
                        if (Node.getInstance().isConnectedToNetwork()) {
                            request = request.newBuilder().header("Cache-Control", "public, max-age=" + 60).build();
                            okhttp3.Response response = chain.proceed(request);
                            Log.e("response", new Gson().toJson(response));
                            return response;
                        } else {
                            request = request.newBuilder()
                                    .header("Cache-Control", "public, , max-stale=" + 60 * 60 * 24 * 7).build();
                            okhttp3.Response response = chain.proceed(request);
                            Log.e("response", new Gson().toJson(response));

                            return response;
                        }
                    }
                })
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppPreference.getInstance().getIP())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();

        appRestClientService = retrofit.create(AppRestClientService.class);

    }

    public static void changeApiBaseUrl(String newApiBaseUrl) {
        apiBaseUrl = newApiBaseUrl;
        setRestClient();
    }

    // schema
    public Call<EntityView> getEntitySchema(String key_name) {
        return appRestClientService.getEntitySchema(key_name);
    }

    //List

    public Call<GetEntityList> getSalesOrderList(String ids, int offset, int limit, String searchParam, String searchValue) {
        return appRestClientService.getSalesOrderList(ids, offset, limit, searchParam, searchValue);
    }

    public Call<GetEntityList> getSalesDispatchList(String ids, int offset, int limit, String searchParam, String searchValue) {
        return appRestClientService.getSalesDispatchList(ids, offset, limit, searchParam, searchValue);
    }

    public Call<GetEntityList> getSalesPaymentList(String ids, int offset, int limit, String searchParam, String searchValue) {
        return appRestClientService.getSalesPaymentList(ids, offset, limit, searchParam, searchValue);
    }

    public Call<Void> postSalesVoucherBillLink(Linking linking) {
        return appRestClientService.postSalesVoucherBillLink(linking);
    }

    public Call<Void> postSalesVoucherBillUnLink(Linking linking) {
        return appRestClientService.postSalesVoucherBillUnLink(linking);
    }

    public Call<Void> postPurchaseVoucherBillUnLink(Linking linking) {
        return appRestClientService.postPurchaseVoucherBillUnLink(linking);
    }

    public Call<Void> postBillUnLink(Linking linking) {
        return appRestClientService.postBillUnLink(linking);
    }

    public Call<Void> postPurchaseVoucherBillLink(Linking linking) {
        return appRestClientService.postPurchaseVoucherBillLink(linking);
    }

    public Call<Void> postBillLink(Linking linking) {
        return appRestClientService.postBillLink(linking);
    }

    public Call<GetEntityList> getPurchaseOrderList(String ids, int offset, int limit, String searchParam, String searchValue) {
        return appRestClientService.getPurchaseOrderList(ids, offset, limit, searchParam, searchValue);
    }

    public Call<GetEntityList> getPurchaseRecieptList(String ids, int offset, int limit, String searchParam, String searchValue) {
        return appRestClientService.getPurchaseRecieptList(ids, offset, limit, searchParam, searchValue);
    }


    public Call<GetEntityList> getPurchasePaymentList(String ids, int offset, int limit, String searchParam, String searchValue) {
        return appRestClientService.getPurchasePaymentList(ids, offset, limit, searchParam, searchValue);
    }

    public Call<GetEntityList> getManufacturingOrderList(String ids) {
        return appRestClientService.getManufacturingOrderList(ids);
    }

    public Call<GetEntityList> getVendorBillList(
            String ids, int offset, int limit, String searchParam, String searchValue) {
        return appRestClientService.getVendorBillList(ids, offset, limit, searchParam, searchValue);
    }

    public Call<GetEntityList> getExpenseBillList(
            String ids, int offset, int limit, String searchParam, String searchValue) {
        return appRestClientService.getExpenseBillList(ids, offset, limit, searchParam,
                searchValue);
    }

    public Call<GetEntityList> getDispatchRecieptList(
            String ids, int offset, int limit, String searchParam, String searchValue) {
        return appRestClientService.getDispatchRecieptList(ids, offset, limit, searchParam, searchValue);
    }

    public Call<UserInfo> getInfo() {
        return appRestClientService.getInfo();
    }

    public Call<GetEntityList> getMandiPriceList(
            String ids, int offset, int limit, String searchParam, String searchValue) {
        return appRestClientService.getMandiPriceList(ids, offset, limit, searchParam, searchValue);
    }

    public Call<GetEntityList> getStorageOrders(
            String ids, int offset, int limit, String searchParam, String searchValue) {
        return appRestClientService.getStorageOrders(ids, offset, limit, searchParam, searchValue);
    }

    public Call<GetEntityList> getStorageInvoiceList(
            String ids, int offset, int limit, String searchParam, String searchValue) {
        return appRestClientService.getStorageInvoiceList(ids, offset, limit, searchParam, searchValue);
    }

    public Call<GetEntityList> getStorageReceiptList(
            String ids, int offset, int limit, String searchParam, String searchValue) {
        return appRestClientService.getStorageReceiptList(ids, offset, limit, searchParam,
                searchValue);
    }

    public Call<GetEntityList> getStorageDispatchList(
            String ids, int offset, int limit, String searchParam, String searchValue) {
        return appRestClientService.getStorageDispatchList(ids, offset, limit, searchParam,
                searchValue);
    }

    public Call<GetEntityList> getPaymentRequestList(
            String ids, int offset, int limit, String searchParam, String searchValue) {
        return appRestClientService.getPaymentRequestList(ids, offset, limit, searchParam,
                searchValue);
    }

    public Call<GetEntityList> getNodePriceList(
            String ids, int offset, int limit, String searchParam, String searchValue) {
        return appRestClientService.getNodePriceList(ids, offset, limit, searchParam, searchValue);
    }

    public Call<GetIdNames> getStateList() {
        return appRestClientService.getStateList();
    }

    public Call<GetIdNames> getDistrictList(int state_id) {
        return appRestClientService.getDistrictList(state_id);
    }

    public Call<GetIdNames> getBlockList(int district_id) {
        return appRestClientService.getBlockList(district_id);
    }

    public Call<CommodityPrices> getPriceData(
            int mandi_id, int product_id, int month_index, int year, String price) {
        return appRestClientService.getPriceData(mandi_id, product_id, month_index, year, price);
    }

    public Call<CommodityPrices> getNodePriceData(
            int node_id, int product_id, int month_index, int year, String price) {
        return appRestClientService.getNodePriceData(node_id, product_id, month_index, year, price);
    }

    public Call<GetEntityList> getContacts(String ids, int offset, int limit, String searchParam,
                                           String searchValue) {
        return appRestClientService.getContacts(ids, offset, limit, searchParam, searchValue);
    }

    public Call<GetEntityList> getCommodityLoanList(String ids, int offset, int limit,
                                                    String searchParam, String searchValue) {
        return appRestClientService.getCommodityLoanList(ids, offset, limit, searchParam,
                searchValue);
    }

    public Call<GetEntityList> getCommodityLoanInvoiceList(String ids, int offset, int limit,
                                                           String searchParam, String searchValue) {
        return appRestClientService.getCommodityLoanInvoiceList(ids, offset, limit, searchParam,
                searchValue);
    }

    //creation

    public Call<ResponseCreateEntity> createSalesOrder(boolean dataChange, EntityCreateDataBody
            entityCreateDataBody) {
        return appRestClientService.createSalesOrder(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createSalesDispatch(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createSalesDispatch(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createSalesPayment(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createSalesPayment(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createPurchaseOrder(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createPurchaseOrder(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createPurchaseReceipt(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createPurchaseReceipt(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createPurchasePayment(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createPurchasePayment(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createManufacturingOrder(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createManufacturingOrder(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createVendorBill(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createVendorBill(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createExpenseBill(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createExpenseBill(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createMandi(MandiCreate mandiCreate) {
        return appRestClientService.createMandi(mandiCreate);
    }

    public Call<ResponseCreateEntity> createMandiPrice(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createMandiPrice(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createPaymentRequest(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createPaymentRequest(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createNodePrice(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createNodePrice(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createStorageOrder(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createStorageOrder(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createPartnerContact(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createPartnerContact(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createCommodityLoan(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createCommodityLoan(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createCommodityLoanInvoice(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createCommodityLoanInvoice(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createStorageInvoice(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createStorageInvoice(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createStorageReceipt(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createStorageReceipt(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> createStorageDispatch(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.createStorageDispatch(dataChange, entityCreateDataBody);
    }


    // updation
    public Call<ResponseCreateEntity> updateSalesOrder(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updateSalesOrder(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updateSalesDispatch(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updateSalesDispatch(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updateSalesPayment(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updateSalesPayment(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updatePurchaseOrder(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updatePurchaseOrder(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updatePurchaseReceipt(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updatePurchaseReceipt(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updatePurchasePayment(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updatePurchasePayment(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updateManufacturingOrder(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updateManufacturingOrder(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updateVendorBill(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updateVendorBill(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updateExpenseBill(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updateExpenseBill(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updateMandiPrice(boolean dataChange,
                                                       EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updateMandiPrice(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updateNodePrice(boolean dataChange,
                                                      EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updateNodePrice(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updateStorageOrder(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updateStorageOrder(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updateCommodityLoan(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updateCommodityLoan(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updateCommodityLoanInvoice(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updateCommodityLoanInvoice(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updatePartnerContact(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updatePartnerContact(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updateStorageInvoice(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updateStorageInvoice(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updatePaymentRequest(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updatePaymentRequest(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updateStorageReceipt(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updateStorageReceipt(dataChange, entityCreateDataBody);
    }

    public Call<ResponseCreateEntity> updateStorageDispatch(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody) {
        return appRestClientService.updateStorageDispatch(dataChange, entityCreateDataBody);
    }


    // login api
    public Call<LoginResponse> sendLogin(LoginBody loginBody) {
        return appRestClientService.sendLogin(loginBody);
    }

    // action api

    public Call<ResponseCreateEntity> actionSalesOrder(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody, String actionName) {
        return appRestClientService.actionSalesOrder(dataChange, entityCreateDataBody, actionName);
    }

    public Call<ResponseCreateEntity> actionSalesDispatch(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody, String actionName) {
        return appRestClientService.actionSalesDispatch(dataChange, entityCreateDataBody,
                actionName);
    }

    public Call<ResponseCreateEntity> actionSalesPayment(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody, String actionName) {
        return appRestClientService.actionSalesPayment(dataChange, entityCreateDataBody,
                actionName);
    }

    public Call<ResponseCreateEntity> actionPurchaseOrder(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody, String actionName) {
        return appRestClientService.actionPurchaseOrder(dataChange, entityCreateDataBody,
                actionName);
    }

    public Call<ResponseCreateEntity> actionPurchaseReceipt(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody, String actionName) {
        return appRestClientService.actionPurchaseReceipt(dataChange, entityCreateDataBody,
                actionName);
    }

    public Call<ResponseCreateEntity> actionPurchasePayment(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody, String actionName) {
        return appRestClientService.actionPurchasePayment(dataChange, entityCreateDataBody,
                actionName);
    }

    public Call<ResponseCreateEntity> actionManufacturingOrder(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody, String actionName) {
        return appRestClientService.actionManufacturingOrder(dataChange, entityCreateDataBody,
                actionName);
    }

    public Call<ResponseCreateEntity> actionVendorBill(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody, String actionName) {
        return appRestClientService.actionVendorBill(dataChange, entityCreateDataBody, actionName);
    }

    public Call<ResponseCreateEntity> actionExpenseBill(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody, String actionName) {
        return appRestClientService.actionExpenseBill(dataChange, entityCreateDataBody, actionName);
    }

    public Call<ResponseCreateEntity> actionCommodityLoan(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody, String actionName) {
        return appRestClientService.actionCommodityLoan(dataChange, entityCreateDataBody, actionName);
    }

    public Call<ResponseCreateEntity> actionStorageOrder(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody, String actionName) {
        return appRestClientService.actionStorageOrder(dataChange, entityCreateDataBody, actionName);
    }

    public Call<ResponseCreateEntity> actionPaymentRequest(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody, String actionName) {
        return appRestClientService.actionPaymentRequest(dataChange, entityCreateDataBody, actionName);
    }

    public Call<ResponseCreateEntity> dialogActionPurchaseReceipt(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody, String actionName,
            String dialogName) {
        return appRestClientService.dialogActionPurchaseReceipt(dataChange, entityCreateDataBody,
                actionName, dialogName);
    }

    public Call<ResponseCreateEntity> dialogActionVendorBill(
            VendorBill entityCreateDataBody, String actionName, String dialogName) {
        return appRestClientService.dialogActionVendorBill(entityCreateDataBody, actionName,
                dialogName);
    }

    public Call<ResponseCreateEntity> dialogActionDispatchVoucher(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody, String actionName,
            String dialogName) {
        return appRestClientService.dialogActionDispatchVoucher(dataChange, entityCreateDataBody,
                actionName, dialogName);
    }
    public Call<Void> fcmToken(FcmToken fcmToken) {
        return appRestClientService.fcmToken(fcmToken);
    }


    public Call<ResponseCreateEntity> dialogActionStorageReceipt(
            boolean dataChange, EntityCreateDataBody entityCreateDataBody, String actionName,
            String dialogName) {
        return appRestClientService.dialogActionDispatchVoucher(dataChange, entityCreateDataBody,
                actionName, dialogName);
    }

    public Call<VersionName> getVersionName() {
        return appRestClientService.getVersionName();
    }

    public Call<ResponseCreateEntity> postNewRecord(NewRecord newRecord) {
        return appRestClientService.postNewRecord(newRecord);
    }

    public Call<AttachmentResponse> postAttacthments(List<File> imageList,
                                                     Attachments attachments) {
        MultipartBody.Part part = null;
        List<MultipartBody.Part> parts = new ArrayList();
        for (int i = 0; i < imageList.size(); i++) {
            part = MultipartBody.Part.createFormData(
                    "image" + i, "image" + i + ".jpeg",
                    RequestBody.create(MediaType.parse("image/*"), imageList.get(i)));
            parts.add(part);
        }
        Gson gson = new Gson();
        HashMap<String, RequestBody> mList = new HashMap<>();
        String entity_type = "";
        entity_type = gson.toJson(attachments);
        RequestBody entity = RequestBody.create(MediaType.parse
                ("application/json"), entity_type);
        mList.put("data", entity);
        return appRestClientService.postAttacthments(parts, mList);
    }
}
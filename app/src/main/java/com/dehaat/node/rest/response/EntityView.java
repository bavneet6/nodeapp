package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class EntityView {

    @SerializedName("data")
    private GetView data;

    public GetView getData() {
        return data;
    }


    public static class GetView implements Serializable {
        @SerializedName("schema")
        public GetSchema schema;
        @SerializedName("version")
        public String version;
        @SerializedName("key")
        public String key;
        @SerializedName("display_name")
        public String display_name;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getDisplay_name() {
            return display_name;
        }

        public void setDisplay_name(String display_name) {
            this.display_name = display_name;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public GetSchema getSchema() {
            return schema;
        }

        public void setSchema(GetSchema schema) {
            this.schema = schema;
        }

        public class GetSchema {
            @SerializedName("fields")
            public ArrayList<GetViewFields> fields;
            @SerializedName("actions")
            public ArrayList<GetActions> actions;
            @SerializedName("statuses")
            public ArrayList<GetKeyValue> statuses;

            public ArrayList<GetKeyValue> getStatuses() {
                return statuses;
            }

            public ArrayList<GetViewFields> getFields() {
                return fields;
            }

            public ArrayList<GetActions> getActions() {
                return actions;
            }

            public class GetActions {
                @SerializedName("display_name")
                public String display_name;
                @SerializedName("key")
                public String key;
                @SerializedName("attributes")
                public GetViewFields.Attributes attributes;

                public GetViewFields.Attributes getAttributes() {
                    return attributes;
                }

                public String getKey() {
                    return key;
                }

                public String getDisplay_name() {
                    return display_name;
                }
            }

            public class GetKeyValue {
                @SerializedName("key")
                public String key;
                @SerializedName("value")
                public String value;

                public String getValue() {
                    return value;
                }

                public String getKey() {
                    return key;
                }
            }
        }

    }
}
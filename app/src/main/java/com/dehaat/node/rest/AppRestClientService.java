package com.dehaat.node.rest;

import com.dehaat.node.rest.body.EntityCreateDataBody;
import com.dehaat.node.rest.body.Linking;
import com.dehaat.node.rest.body.LoginBody;
import com.dehaat.node.rest.body.NewRecord;
import com.dehaat.node.rest.response.AttachmentResponse;
import com.dehaat.node.rest.response.CommodityPrices;
import com.dehaat.node.rest.response.EntityView;
import com.dehaat.node.rest.response.FcmToken;
import com.dehaat.node.rest.response.GetEntityList;
import com.dehaat.node.rest.response.GetIdNames;
import com.dehaat.node.rest.response.LoginResponse;
import com.dehaat.node.rest.response.MandiCreate;
import com.dehaat.node.rest.response.ResponseCreateEntity;
import com.dehaat.node.rest.response.UserInfo;
import com.dehaat.node.rest.response.VendorBill;
import com.dehaat.node.rest.response.VersionName;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface AppRestClientService {

    //1
    @GET("/node/output/schema?")
    Call<EntityView> getEntitySchema(@Query("Entity") String key_name);

    //2
    @GET("/node/output/sales/order/list")
    Call<GetEntityList> getSalesOrderList(@Query("ids") String list, @Query("offset") int offset,
                                          @Query("limit") int limit,
                                          @Query("search_parameter") String searchParamter,
                                          @Query("search_value") String searchValue);


    //3
    @GET("/node/output/sales/dispatch/list")
    Call<GetEntityList> getSalesDispatchList(@Query("ids") String list, @Query("offset") int offset,
                                             @Query("limit") int limit,
                                             @Query("search_parameter") String searchParamter,
                                             @Query("search_value") String searchValue);


    //4
    @GET("/node/output/sales/payment/list")
    Call<GetEntityList> getSalesPaymentList(@Query("ids") String list, @Query("offset") int offset,
                                            @Query("limit") int limit,
                                            @Query("search_parameter") String searchParamter,
                                            @Query("search_value") String searchValue);


    //5
    @GET("/node/output/purchase/order/list")
    Call<GetEntityList> getPurchaseOrderList(@Query("ids") String list, @Query("offset") int offset,
                                             @Query("limit") int limit,
                                             @Query("search_parameter") String searchParamter,
                                             @Query("search_value") String searchValue);

    //6
    @GET("/node/output/purchase/receipt/list")
    Call<GetEntityList> getPurchaseRecieptList(@Query("ids") String list,
                                               @Query("offset") int offset,
                                               @Query("limit") int limit,
                                               @Query("search_parameter") String searchParamter,
                                               @Query("search_value") String searchValue);

    //7
    @GET("/node/output/purchase/payment/list")
    Call<GetEntityList> getPurchasePaymentList(@Query("ids") String list,
                                               @Query("offset") int offset,
                                               @Query("limit") int limit,
                                               @Query("search_parameter") String searchParamter,
                                               @Query("search_value") String searchValue);

    //8
    @POST("/node/output/sales/order/create")
    Call<ResponseCreateEntity> createSalesOrder(@Query("to_confirm") boolean data_modified,
                                                @Body() EntityCreateDataBody entityCreateDataBody);

    //9
    @POST("/node/output/sales/dispatch/create")
    Call<ResponseCreateEntity> createSalesDispatch(@Query("to_confirm") boolean data_modified,
                                                   @Body EntityCreateDataBody entityCreateDataBody);

    //10
    @POST("/node/output/sales/payment/create")
    Call<ResponseCreateEntity> createSalesPayment(@Query("to_confirm") boolean data_modified,
                                                  @Body EntityCreateDataBody entityCreateDataBody);

    //11
    @POST("/node/output/purchase/order/create")
    Call<ResponseCreateEntity> createPurchaseOrder(@Query("to_confirm") boolean data_modified,
                                                   @Body EntityCreateDataBody entityCreateDataBody);

    //12
    @POST("/node/output/purchase/receipt/create")
    Call<ResponseCreateEntity> createPurchaseReceipt(
            @Query("to_confirm") boolean data_modified,
            @Body EntityCreateDataBody entityCreateDataBody);

    //13
    @POST("/node/output/purchase/payment/create")
    Call<ResponseCreateEntity> createPurchasePayment(
            @Query("to_confirm") boolean data_modified,
            @Body EntityCreateDataBody entityCreateDataBody);

    //14
    @POST("/node/output/sales/order/update")
    Call<ResponseCreateEntity> updateSalesOrder(@Query("to_confirm") boolean data_modified,
                                                @Body EntityCreateDataBody entityCreateDataBody);

    //15
    @POST("/node/output/sales/dispatch/update")
    Call<ResponseCreateEntity> updateSalesDispatch(@Query("to_confirm") boolean data_modified,
                                                   @Body EntityCreateDataBody entityCreateDataBody);

    //16
    @POST("/node/output/sales/payment/update")
    Call<ResponseCreateEntity> updateSalesPayment(@Query("to_confirm") boolean data_modified,
                                                  @Body EntityCreateDataBody entityCreateDataBody);

    //17
    @POST("/node/output/purchase/order/update")
    Call<ResponseCreateEntity> updatePurchaseOrder(@Query("to_confirm") boolean data_modified,
                                                   @Body EntityCreateDataBody entityCreateDataBody);

    //18
    @POST("/node/output/purchase/receipt/update")
    Call<ResponseCreateEntity> updatePurchaseReceipt(
            @Query("to_confirm") boolean data_modified,
            @Body EntityCreateDataBody entityCreateDataBody);

    //19
    @POST("/node/output/purchase/payment/update")
    Call<ResponseCreateEntity> updatePurchasePayment(
            @Query("to_confirm") boolean data_modified,
            @Body EntityCreateDataBody entityCreateDataBody);

    //20
    @POST("/node/user/login")
    Call<LoginResponse> sendLogin(@Body LoginBody loginBody);

    //21
    @POST("/node/output/sales/order/action?")
    Call<ResponseCreateEntity> actionSalesOrder(
            @Query("data_modified") boolean data_modified,
            @Body EntityCreateDataBody entityCreateDataBody, @Query("action") String key_name);

    //22
    @POST("/node/output/sales/dispatch/action?")
    Call<ResponseCreateEntity> actionSalesDispatch(@Query("data_modified") boolean data_modified,
                                                   @Body EntityCreateDataBody entityCreateDataBody,
                                                   @Query("action") String key_name);

    //23
    @POST("/node/output/sales/payment/action?")
    Call<ResponseCreateEntity> actionSalesPayment(@Query("data_modified") boolean data_modified,
                                                  @Body EntityCreateDataBody entityCreateDataBody,
                                                  @Query("action") String key_name);

    //24
    @POST("/node/output/purchase/order/action?")
    Call<ResponseCreateEntity> actionPurchaseOrder(@Query("data_modified") boolean data_modified,
                                                   @Body EntityCreateDataBody entityCreateDataBody,
                                                   @Query("action") String key_name);

    //25
    @POST("/node/output/purchase/receipt/action?")
    Call<ResponseCreateEntity> actionPurchaseReceipt(@Query("data_modified") boolean data_modified,
                                                     @Body EntityCreateDataBody entityCreateDataBody,
                                                     @Query("action") String key_name);

    //26
    @POST("/node/output/purchase/payment/action?")
    Call<ResponseCreateEntity> actionPurchasePayment(@Query("data_modified") boolean data_modified,
                                                     @Body EntityCreateDataBody entityCreateDataBody,
                                                     @Query("action") String key_name);

    @POST("/node/output/commodity_loan/action?")
    Call<ResponseCreateEntity> actionCommodityLoan(@Query("data_modified") boolean data_modified,
                                                   @Body EntityCreateDataBody entityCreateDataBody,
                                                   @Query("action") String key_name);

    @POST("/node/output/storage/order/action?")
    Call<ResponseCreateEntity> actionStorageOrder(@Query("data_modified") boolean data_modified,
                                                  @Body() EntityCreateDataBody entityCreateDataBody,
                                                  @Query("action") String actionName);

    @POST("/node/output/payment/request/action?")
    Call<ResponseCreateEntity> actionPaymentRequest(@Query("data_modified") boolean data_modified,
                                                    @Body() EntityCreateDataBody entityCreateDataBody,
                                                    @Query("action") String actionName);

    //27
    @GET("/node/version")
    Call<VersionName> getVersionName();

    //28
    @POST("/node/partner/create")
    Call<ResponseCreateEntity> postNewRecord(@Body NewRecord newRecord);

    //29
    @GET("/node/output/manufacturing/order/list")
    Call<GetEntityList> getManufacturingOrderList(@Query("ids") String list);


    //30
    @POST("/node/output/manufacturing/order/update")
    Call<ResponseCreateEntity> updateManufacturingOrder(
            @Query("to_confirm") boolean data_modified,
            @Body EntityCreateDataBody entityCreateDataBody);

    //31
    @POST("/node/output/manufacturing/order/create")
    Call<ResponseCreateEntity> createManufacturingOrder(
            @Query("to_confirm") boolean data_modified,
            @Body EntityCreateDataBody entityCreateDataBody);

    //32
    @POST("/node/output/manufacturing/order/action?")
    Call<ResponseCreateEntity> actionManufacturingOrder(
            @Query("data_modified") boolean data_modified,
            @Body EntityCreateDataBody entityCreateDataBody, @Query("action") String key_name);

    //33
    @POST("/node/output/vendor/bill/action?")
    Call<ResponseCreateEntity> actionVendorBill(
            @Query("data_modified") boolean data_modified,
            @Body EntityCreateDataBody entityCreateDataBody, @Query("action") String key_name);

    //34
    @POST("/node/output/expense/bill/action?")
    Call<ResponseCreateEntity> actionExpenseBill(
            @Query("data_modified") boolean data_modified,
            @Body EntityCreateDataBody entityCreateDataBody, @Query("action") String key_name);


    //36
    @GET("/node/output/vendor/bill/list")
    Call<GetEntityList> getVendorBillList(@Query("ids") String list, @Query("offset") int offset,
                                          @Query("limit") int limit,
                                          @Query("search_parameter") String searchParamter,
                                          @Query("search_value") String searchValue);

    //37
    @POST("/node/output/vendor/bill/create")
    Call<ResponseCreateEntity> createVendorBill(@Query("to_confirm") boolean data_modified,
                                                @Body EntityCreateDataBody entityCreateDataBody);
    //38

    @POST("/node/output/expense/bill/create")
    Call<ResponseCreateEntity> createExpenseBill(@Query("to_confirm") boolean data_modified,
                                                 @Body EntityCreateDataBody entityCreateDataBody);


    //40
    @POST("/node/output/vendor/bill/update")
    Call<ResponseCreateEntity> updateVendorBill(@Query("to_confirm") boolean data_modified,
                                                @Body EntityCreateDataBody entityCreateDataBody);
    //41

    @POST("/node/output/expense/bill/update")
    Call<ResponseCreateEntity> updateExpenseBill(@Query("to_confirm") boolean data_modified,
                                                 @Body EntityCreateDataBody entityCreateDataBody);

    //43
    @GET("/node/output/expense/bill/dispatch_receipt/list")
    Call<GetEntityList> getDispatchRecieptList(@Query("ids") String list, @Query("offset") int offset,
                                               @Query("limit") int limit,
                                               @Query("search_parameter") String searchParamter,
                                               @Query("search_value") String searchValue);

    //44
    @POST("/node/output/sales/dispatch/expense_bill/link")
    Call<Void> postSalesVoucherBillLink(@Body Linking linking);

    //45
    @POST("/node/output/sales/dispatch/expense_bill/unlink")
    Call<Void> postSalesVoucherBillUnLink(@Body Linking linking);

    //46
    @POST("/node/output/purchase/receipt/expense_bill/link")
    Call<Void> postPurchaseVoucherBillLink(@Body Linking linking);
    //47

    @POST("/node/output/purchase/receipt/expense_bill/unlink")
    Call<Void> postPurchaseVoucherBillUnLink(@Body Linking linking);

    //48
    @POST("/node/output/expense/bill/link")
    Call<Void> postBillLink(@Body Linking linking);

    //49
    @POST("/node/output/expense/bill/unlink")
    Call<Void> postBillUnLink(@Body Linking linking);

    //50
    @Multipart
    @POST("/node/entity/add/attachment")
    Call<AttachmentResponse> postAttacthments(@Part List<MultipartBody.Part> files,
                                              @PartMap HashMap<String, RequestBody> attachments);

    //51
    @GET("/node/user/info")
    Call<UserInfo> getInfo();

    //52
    @GET("/node/output/expense/bill/list")
    Call<GetEntityList> getExpenseBillList(@Query("ids") String list, @Query("offset") int offset,
                                           @Query("limit") int limit,
                                           @Query("search_parameter") String searchParamter,
                                           @Query("search_value") String searchValue);

    //54
    @POST("/node/output/purchase/receipt/dialog?")
    Call<ResponseCreateEntity> dialogActionPurchaseReceipt(
            @Query("data_modified") boolean data_modified,
            @Body EntityCreateDataBody entityCreateDataBody,
            @Query("action") String action, @Query("dialog_id") String dialog);

    //55
    @POST("/node/output/sales/dispatch/dialog?")
    Call<ResponseCreateEntity> dialogActionDispatchVoucher(
            @Query("data_modified") boolean data_modified,
            @Body EntityCreateDataBody entityCreateDataBody,
            @Query("action") String action,
            @Query("dialog_id") String dialog);

    //56
    @POST("/node/output/vendor/bill/dialog?")
    Call<ResponseCreateEntity> dialogActionVendorBill(
            @Body VendorBill entityCreateDataBody, @Query("action") String action,
            @Query("dialog_id") String dialog);

    //57
    @POST("/node/output/mandi/price/create")
    Call<ResponseCreateEntity> createMandiPrice(@Query("to_confirm") boolean data_modified,
                                                @Body() EntityCreateDataBody entityCreateDataBody);  //57

    @POST("/node/output/payment/request/create")
    Call<ResponseCreateEntity> createPaymentRequest(@Query("to_confirm") boolean data_modified,
                                                    @Body() EntityCreateDataBody entityCreateDataBody);

    //58
    @POST("/node/output/mandi/price/update")
    Call<ResponseCreateEntity> updateMandiPrice(@Query("to_confirm") boolean data_modified,
                                                @Body() EntityCreateDataBody entityCreateDataBody);

    //59
    @GET("/node/output/mandi/price/list")
    Call<GetEntityList> getMandiPriceList(
            @Query("ids") String list, @Query("offset") int offset, @Query("limit") int limit,
            @Query("search_parameter") String searchParamter,
            @Query("search_value") String searchValue);

    @POST("/node/price/create")
    Call<ResponseCreateEntity> createNodePrice(@Query("to_confirm") boolean data_modified,
                                               @Body() EntityCreateDataBody entityCreateDataBody);

    @POST("/node/price/update")
    Call<ResponseCreateEntity> updateNodePrice(@Query("to_confirm") boolean data_modified,
                                               @Body() EntityCreateDataBody entityCreateDataBody);

    @POST("/node/output/storage/receipt/update")
    Call<ResponseCreateEntity> updateStorageReceipt(
            @Query("to_confirm") boolean dataChange,
            @Body() EntityCreateDataBody entityCreateDataBody);

    @POST("/node/output/storage/dispatch/update")
    Call<ResponseCreateEntity> updateStorageDispatch(
            @Query("to_confirm") boolean dataChange,
            @Body() EntityCreateDataBody entityCreateDataBody);


    @POST("/node/output/storage/order/create")
    Call<ResponseCreateEntity> createStorageOrder(
            @Query("to_confirm") boolean data_modified,
            @Body() EntityCreateDataBody entityCreateDataBody);

    @POST("/node/output/commodity_loan/create")
    Call<ResponseCreateEntity> createCommodityLoan(
            @Query("to_confirm") boolean data_modified,
            @Body() EntityCreateDataBody entityCreateDataBody);

    @POST("/node/output/commodity_loan/invoice/create")
    Call<ResponseCreateEntity> createCommodityLoanInvoice(
            @Query("to_confirm") boolean data_modified,
            @Body() EntityCreateDataBody entityCreateDataBody);

    @POST("/node/output/storage/invoice/create")
    Call<ResponseCreateEntity> createStorageInvoice(
            @Query("to_confirm") boolean data_modified,
            @Body() EntityCreateDataBody entityCreateDataBody);

    @POST("/node/output/storage/receipt/create")
    Call<ResponseCreateEntity> createStorageReceipt(
            @Query("to_confirm") boolean data_modified,
            @Body() EntityCreateDataBody entityCreateDataBody);

    @POST("/node/output/storage/dispatch/create")
    Call<ResponseCreateEntity> createStorageDispatch(
            @Query("to_confirm") boolean data_modified,
            @Body() EntityCreateDataBody entityCreateDataBody);

    @POST("/node/output/commodity_loan/update")
    Call<ResponseCreateEntity> updateCommodityLoan(
            @Query("to_confirm") boolean data_modified,
            @Body() EntityCreateDataBody entityCreateDataBody);

    @POST("/node/output/commodity_loan/invoice/update")
    Call<ResponseCreateEntity> updateCommodityLoanInvoice(
            @Query("to_confirm") boolean data_modified,
            @Body() EntityCreateDataBody entityCreateDataBody);

    @POST("/node/output/storage/order/update")
    Call<ResponseCreateEntity> updateStorageOrder(
            @Query("to_confirm") boolean data_modified,
            @Body() EntityCreateDataBody entityCreateDataBody);

    @POST("/node/output/storage/invoice/update")
    Call<ResponseCreateEntity> updateStorageInvoice(
            @Query("to_confirm") boolean dataChange,
            @Body() EntityCreateDataBody entityCreateDataBody);

    @POST("/node/output/payment/request/update")
    Call<ResponseCreateEntity> updatePaymentRequest(
            @Query("to_confirm") boolean dataChange,
            @Body() EntityCreateDataBody entityCreateDataBody);

    @POST("/node/contact/create")
    Call<ResponseCreateEntity> createPartnerContact(
            @Query("to_confirm") boolean data_modified,
            @Body() EntityCreateDataBody entityCreateDataBody);

    @POST("/node/contact/update")
    Call<ResponseCreateEntity> updatePartnerContact(
            @Query("to_create") boolean data_modified,
            @Body() EntityCreateDataBody entityCreateDataBody);

    @POST("/node/output/commodity_loan/create")
    Call<ResponseCreateEntity> createLoan(
            @Query("to_confirm") boolean data_modified,
            @Body() EntityCreateDataBody entityCreateDataBody);

    @POST("/ndoe/output/commodity_loan/update")
    Call<ResponseCreateEntity> updateLoan(
            @Query("to_confirm") boolean data_modified,
            @Body() EntityCreateDataBody entityCreateDataBody);

    //59-1
    @GET("/node/price/list")
    Call<GetEntityList> getNodePriceList(
            @Query("ids") String list, @Query("offset") int offset, @Query("limit") int limit,
            @Query("search_parameter") String searchParameter,
            @Query("search_value") String searchValue);

    @GET("/node/output/commodity_loan/list")
    Call<GetEntityList> getCommodityLoanList(
            @Query("ids") String ids, @Query("offset") int offset, @Query("limit") int limit,
            @Query("search_parameter") String searchParameter,
            @Query("search_value") String searchValue);

    @GET("/node/output/commodity_loan/invoice/list")
    Call<GetEntityList> getCommodityLoanInvoiceList(
            @Query("ids") String ids, @Query("offset") int offset, @Query("limit") int limit,
            @Query("search_parameter") String searchParameter,
            @Query("search_value") String searchValue);

    //60
    @POST("/node/output/mandi/create")
    Call<ResponseCreateEntity> createMandi(@Body MandiCreate mandiCreate);

    //61
    @GET("/node/state")
    Call<GetIdNames> getStateList();

    //62
    @GET("/node/district/{state_id}")
    Call<GetIdNames> getDistrictList(@Path("state_id") long id);

    //63
    @GET("/node/block/{district_id}")
    Call<GetIdNames> getBlockList(@Path("district_id") int id);

    //64
    @GET("/node/output/mandi/price/chart")
    Call<CommodityPrices> getPriceData(
            @Query("mandi_id") int mandi_id, @Query("product_id") int product_id,
            @Query("month") int month_index, @Query("year") int year, @Query("price") String price);

    //65
    @GET("/node/output/storage/order/list")
    Call<GetEntityList> getStorageOrders(
            @Query("ids") String ids, @Query("offset") int offset, @Query("limit") int limit,
            @Query("search_parameter") String searchParameter,
            @Query("search_value") String searchValue);

    @GET("/node/output/storage/invoice/list")
    Call<GetEntityList> getStorageInvoiceList(
            @Query("ids") String ids, @Query("offset") int offset, @Query("limit") int limit,
            @Query("search_parameter") String searchParameter,
            @Query("search_value") String searchValue);

    @GET("/node/output/storage/receipt/list")
    Call<GetEntityList> getStorageReceiptList(
            @Query("ids") String ids, @Query("offset") int offset, @Query("limit") int limit,
            @Query("search_parameter") String searchParameter,
            @Query("search_value") String searchValue);

    @GET("/node/output/storage/dispatch/list")
    Call<GetEntityList> getStorageDispatchList(
            @Query("ids") String ids, @Query("offset") int offset, @Query("limit") int limit,
            @Query("search_parameter") String searchParameter,
            @Query("search_value") String searchValue);

    @GET("/node/output/payment/request/list")
    Call<GetEntityList> getPaymentRequestList(
            @Query("ids") String ids, @Query("offset") int offset, @Query("limit") int limit,
            @Query("search_parameter") String searchParameter,
            @Query("search_value") String searchValue);

    //66
    @GET("/node/price/chart")
    Call<CommodityPrices> getNodePriceData(
            @Query("team_id") int team_id, @Query("product_id") int product_id,
            @Query("month") int month_index, @Query("year") int year, @Query("price") String price);

    @GET("/node/contact/list")
    Call<GetEntityList> getContacts(
            @Query("ids") String ids,
            @Query("offset") int offset, @Query("limit") int limit,
            @Query("search_parameter") String searchParam,
            @Query("search_value") String searchValue);

    @POST("/node/user/update/FCM_token")
    Call<Void> fcmToken(@Body FcmToken fcmToken);
}



package com.dehaat.node.rest.body;

import com.google.gson.annotations.SerializedName;

public class PhoneNumber {
    @SerializedName("data")
    public Number data;

    public PhoneNumber(Number number) {
        this.data = number;
    }

    public static class Number {
        @SerializedName("phone_number")
        public String phone_number;

        public Number(String phone_number) {
            this.phone_number = phone_number;
        }
    }
}

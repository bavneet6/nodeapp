package com.dehaat.node.rest.response;

public class LotNumber {
    public String temp_lot_number, perm_lot_number;

    public Long product_id, lot_number_id;


    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public Long getLot_number_id() {
        return lot_number_id;
    }

    public void setLot_number_id(Long lot_number_id) {
        this.lot_number_id = lot_number_id;
    }

    public String getPerm_lot_number() {
        return perm_lot_number;
    }

    public void setPerm_lot_number(String perm_lot_number) {
        this.perm_lot_number = perm_lot_number;
    }

    public String getTemp_lot_number() {
        return temp_lot_number;
    }

    public void setTemp_lot_number(String temp_lot_number) {
        this.temp_lot_number = temp_lot_number;
    }

}

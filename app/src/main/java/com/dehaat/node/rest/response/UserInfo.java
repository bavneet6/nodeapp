package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserInfo {
    @SerializedName("data")
    public Info data;

    public Info getData() {
        return data;
    }

    public class Info {
        @SerializedName("warehouse_ids")
        public List<Long> warehouse_id;
        @SerializedName("team_id")
        public Long team_id;

        public Long getTeam_id() {
            return team_id;
        }

        public List<Long> getWarehouse_id() {
            return warehouse_id;
        }
    }
}

package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("auth_token")
    private String auth_token;
    @SerializedName("user_id")
    private Long user_id;

    public Long getUser_id() {
        return user_id;
    }

    public String getAuth_token() {
        return auth_token;
    }

}

package com.dehaat.node.rest.response;

import com.dehaat.node.utilities.AppUtils;
import com.dehaat.node.utilities.UrlConstant;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

public class GetContactList {
    @SerializedName("data")
    private ArrayList<Contact> data;

    public ArrayList<Contact> getData(){
        return this.data;
    }

    public class Contact{
        @SerializedName("entity_fields")
        private ArrayList<JsonDataStore> entityFields;

        @SerializedName("entity_id")
        private long entityId;

        private HashMap<String, String> fieldHash = new HashMap<>();
        private int index;

        public ArrayList<JsonDataStore> getEntityFields() {
            return entityFields;
        }

        public long getEntityId() {
            return entityId;
        }

        public String getName(){
            return getField(UrlConstant.NAME);
        }

        public String getPrimaryMobile(){
            return getField(UrlConstant.PRIMARY_NUMBER);
        }

        public String getStreetAddress(){
            return getField(UrlConstant.STREET);
        }

        public String getField(String fieldName){
            if(fieldHash == null){
                fieldHash = new HashMap<>();
                index = 0;
            }

            if(fieldHash.get(fieldName) != null){
                return fieldHash.get(fieldName);
            }
            else{
                for(int i=index; i<entityFields.size(); i++){
                    JsonDataStore jds = entityFields.get(i);
                    fieldHash.put(jds.key, jds.value);
                    if(jds.key.equals(fieldName)){
                        index = i+1;
                        if(AppUtils.isNullCase(jds.value)){
                            return "N/A";
                        }
                        else{
                            return jds.value;
                        }
                    }
                }
                return null;
            }
        }
    }
}

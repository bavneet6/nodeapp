package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetViewFields implements Serializable {

    @SerializedName("key")
    public String key;
    @SerializedName("attributes")
    public Attributes attributes;
    @SerializedName("display_name")
    public String display_name;
    @SerializedName("default")
    public String defaultText;
    @SerializedName("type")
    public String type;
    @SerializedName("input_validation_error")
    public String input_validation_error;
    @SerializedName("input_validation")
    public String input_validations;
    @SerializedName("selection_values")
    public ArrayList<GetIdName> selection_values;
    @SerializedName("sub_fields")
    public ArrayList<GetViewFields> sub_fields;
    @SerializedName("mapping_values")
    public ArrayList<MappingValues> mapping_values;
    @SerializedName("lot_numbers")
    public ArrayList<LotNumbers> lot_numbers;

    public ArrayList<LotNumbers> getLot_numbers() {
        return lot_numbers;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDefaultText() {
        return defaultText;
    }

    public ArrayList<GetViewFields> getSub_fields() {
        return sub_fields;
    }

    public ArrayList<MappingValues> getMapping_values() {
        return mapping_values;
    }

    public ArrayList<GetIdName> getSelection_values() {
        return selection_values;
    }

    public String getInput_validation_error() {
        return input_validation_error;
    }

    public String getInput_validations() {
        return input_validations;
    }

    public String getKey() {
        return key;
    }

    public String getDisplay_Name() {
        return display_name;
    }

    public class MappingValues {
        @SerializedName("id")
        public Long id;
        @SerializedName("uom_id")
        public List<String> uom_id;
        @SerializedName("raw_product_id")
        public Long raw_product_id;
        @SerializedName("finished_products")
        public ArrayList<FinishProd> finished_products;

        public Long getRaw_product_id() {
            return raw_product_id;
        }

        public ArrayList<FinishProd> getFinished_products() {
            return finished_products;
        }

        public List<String> getUom_id() {
            return uom_id;
        }

        public Long getId() {
            return id;
        }

        public class FinishProd {
            @SerializedName("product_id")
            public Long product_id;
            @SerializedName("product_uom_id")
            public Long product_uom_id;

            public Long getProduct_id() {
                return product_id;
            }

            public Long getProduct_uom_id() {
                return product_uom_id;
            }
        }
    }

    public class Attributes {
        @SerializedName("readonly")
        public String readonly;
        @SerializedName("invisible")
        public String invisible;
        @SerializedName("required")
        public String required;
        @SerializedName("create")
        public String create;

        public String isCreate() {
            return create;
        }

        public String getReadonly() {
            return readonly;
        }

        public String getInvisible() {
            return invisible;
        }

        public String getRequired() {
            return required;
        }
    }

    public class LotNumbers {
        @SerializedName("name")
        public String name;
        @SerializedName("product_id")
        public Long product_id;
        @SerializedName("id")
        public Long lot_id;
        @SerializedName("ref")
        public String ref;

        public Long getLot_id() {
            return lot_id;
        }

        public Long getProduct_id() {
            return product_id;
        }

        public String getName() {
            return name;
        }

        public String getRef() {
            return ref;
        }
    }
}

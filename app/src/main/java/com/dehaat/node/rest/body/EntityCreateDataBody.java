package com.dehaat.node.rest.body;

import com.dehaat.node.rest.response.JsonDataStore;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class EntityCreateDataBody {
    @SerializedName("data")
    public CreateBody createBody;
    @SerializedName("metadata")
    public MetaData metadata;

    public EntityCreateDataBody(CreateBody createBody, MetaData metadata) {
        this.createBody = createBody;
        this.metadata = metadata;
    }

    public static class MetaData {
        @SerializedName("client_id")
        public Long client_id;

        @SerializedName("entity_id")
        public Long entity_id;

        @SerializedName("action_name")
        public String action_name;

        @SerializedName("schema_version")
        public String version;

        public void setClient_id(Long client_id) {
            this.client_id = client_id;
        }

        public Long getEntity_id() {
            return entity_id;
        }

        public void setEntity_id(Long entity_id) {
            this.entity_id = entity_id;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }
    }

    public static class CreateBody {
        @SerializedName("entity_fields")
        public ArrayList<JsonDataStore> fields;

        public void setFields(ArrayList<JsonDataStore> fields) {
            this.fields = fields;
        }
    }
}

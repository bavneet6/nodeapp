package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 2/2/2018.
 */

public class Token {
    @SerializedName("FCM_token")
    private String fcm_token;


    public String getFcm_token() {
        return fcm_token;
    }

    public void setFcm_token(String fcm_token) {
        this.fcm_token = fcm_token;
    }
}

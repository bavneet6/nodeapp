package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

public class ResponseCreateEntity {

    @SerializedName("metadata")
    private GetResponse metadata;
    @SerializedName("dialog_id")
    private String dialog_id;
    @SerializedName("error_message")
    private String error_message;
    @SerializedName("error_type")
    private String error_type;
    @SerializedName("message")
    private String message;
    @SerializedName("dialog_occured")
    private boolean dialog_occured;


    public String getError_message() {
        return error_message;
    }

    public String getError_type() {
        return error_type;
    }

    public String getMessage() {
        return message;
    }

    public boolean isDialog_occured() {
        return dialog_occured;
    }

    public String getDialog_id() {
        return dialog_id;
    }

    public GetResponse getMetadata() {
        return metadata;
    }

    public class GetResponse {

        @SerializedName("entity_id")
        private Long entity_id;

        public Long getEntity_id() {
            return entity_id;
        }

    }
}

package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

public class MandiCreate {

    @SerializedName("data")
    public Data data;

    public MandiCreate(Data data) {
        this.data = data;
    }

    public static class Data {
        @SerializedName("state_id")
        private long state_id;

        @SerializedName("district_id")
        private long district_id;

        @SerializedName("block_id")
        private long block_id;

        @SerializedName("mandi_name")
        private String mandi_name;

        @SerializedName("address_line")
        private String address_line;

        @SerializedName("pincode")
        private String pincode;

        public Data(
                String mandiName, Long stateId, Long districtId, Long blockId, String addressLine,
                String pinCode) {
            this.mandi_name = mandiName;
            this.state_id = stateId;
            this.district_id = districtId;
            this.block_id = blockId;
            this.address_line = addressLine;
            this.pincode = pinCode;
        }
    }
}

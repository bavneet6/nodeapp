package com.dehaat.node.rest.response;

public class EntityDataJson {
    public String entity_type, json_payload, sync_status, validation_status, entity_status,
            payload_version, write_date, lock_data, attachments, dialog_id;

    public Long entity_id, row_id;
    public int action_required;

    public Long getRow_id() {
        return row_id;
    }

    public void setRow_id(Long row_id) {
        this.row_id = row_id;
    }

    public String getPayload_version() {
        return payload_version;
    }

    public void setPayload_version(String payload_version) {
        this.payload_version = payload_version;
    }

    public int getAction_required() {
        return action_required;
    }

    public void setAction_required(int action_required) {
        this.action_required = action_required;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public String getDialog_id() {
        return dialog_id;
    }

    public void setDialog_id(String dialog_id) {
        this.dialog_id = dialog_id;
    }

    public Long getEntity_id() {
        return entity_id;
    }

    public void setEntity_id(Long entity_id) {
        this.entity_id = entity_id;
    }

    public String getEntity_status() {
        return entity_status;
    }

    public void setEntity_status(String entity_status) {
        this.entity_status = entity_status;
    }

    public String getEntity_type() {
        return entity_type;
    }

    public void setEntity_type(String entity_type) {
        this.entity_type = entity_type;
    }

    public String getJson_payload() {
        return json_payload;
    }

    public void setJson_payload(String json_payload) {
        this.json_payload = json_payload;
    }

    public String getSync_status() {
        return sync_status;
    }

    public void setSync_status(String sync_status) {
        this.sync_status = sync_status;
    }

    public String getValidation_status() {
        return validation_status;
    }

    public void setValidation_status(String validation_status) {
        this.validation_status = validation_status;
    }

    public String getWrite_date() {
        return write_date;
    }

    public void setWrite_date(String write_date) {
        this.write_date = write_date;
    }

    public String getLock_data() {
        return lock_data;
    }

    public void setLock_data(String lock_data) {
        this.lock_data = lock_data;
    }
}

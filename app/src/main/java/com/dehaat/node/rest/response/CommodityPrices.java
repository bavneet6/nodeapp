package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class CommodityPrices {

    @SerializedName("data")
    private ArrayList<CommodityPrices.GetData> data;

    public ArrayList<GetData> getData() {
        return data;
    }

    public class GetData {
        @SerializedName("applicable_date")
        private int applicableDate;

        @SerializedName("price")
        private float price;

        public float getPrice() {
            return price;
        }

        public int getApplicableDate() {
            return applicableDate;
        }
    }
}


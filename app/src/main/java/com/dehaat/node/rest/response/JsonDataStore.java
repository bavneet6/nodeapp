package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

public class JsonDataStore {

    @SerializedName("key")
    public String key;
    @SerializedName("value")
    public String value;

    public JsonDataStore() {

    }

    public JsonDataStore(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

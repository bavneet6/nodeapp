package com.dehaat.node.rest.body;

import com.google.gson.annotations.SerializedName;

public class NewRecord {
    @SerializedName("data")
    public Data data;

    public NewRecord(Data data) {
        this.data = data;
    }

    public static class Data {
        @SerializedName("mobile")
        private String mobile;
        @SerializedName("name")
        private String name;

        @SerializedName("tag")
        private String tag;

        public void setTag(String tag) {
            this.tag = tag;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }
    }
}

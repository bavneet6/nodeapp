package com.dehaat.node.rest.body;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Linking {
    @SerializedName("data")
    public Data data;

    public Linking(Data data) {
        this.data = data;
    }

    public static class Data {
        @SerializedName("link_from_id")
        public Long link_from_id;
        @SerializedName("link_to_id")
        public Long link_to_id;
        @SerializedName("link_to_list_ids")
        public List<Long> link_to_list_ids;

        public Data(Long link_from_id, Long link_to_id) {
            this.link_from_id = link_from_id;
            this.link_to_id = link_to_id;
        }

        public Data(Long link_from_id, List<Long> link_to_list_ids) {
            this.link_from_id = link_from_id;
            this.link_to_list_ids = link_to_list_ids;
        }
    }
}

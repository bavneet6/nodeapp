package com.dehaat.node.rest.response;

import com.dehaat.node.utilities.AppUtils;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

public class GetEntityList {

    @SerializedName("data")
    public ArrayList<CreateBody> createBody;

    public ArrayList<CreateBody> getCreateBody() {
        return createBody;
    }

    public static class CreateBody {
        @SerializedName("entity_fields")
        public ArrayList<JsonDataStore> fields;
        @SerializedName("attachments")
        public ArrayList<String> attachments;
        @SerializedName("entity_id")
        public Integer entity_id;
        @SerializedName("entity_status")
        public String entity_status;
        @SerializedName("entity_type")
        public String entity_type;
        @SerializedName("version")
        public String version;
        @SerializedName("write_date")
        public String write_date;


        private HashMap<String, String> fieldHash = new HashMap<>();
        private int index;


        public ArrayList<String> getAttachments() {
            return attachments;
        }

        public String getEntity_type() {
            return entity_type;
        }

        public String getWrite_date() {
            return write_date;
        }

        public String getEntity_status() {
            return entity_status;
        }

        public Integer getEntity_id() {
            return entity_id;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public ArrayList<JsonDataStore> getFields() {
            return fields;
        }

        public String getField(String fieldName){
            if(fieldHash == null){
                fieldHash = new HashMap<>();
                index = 0;
            }

            String valReturn = "N/A";
            if(!AppUtils.isNullCase(fieldHash.get(fieldName))) {
                valReturn = fieldHash.get(fieldName);
            } else {
                for(int i=index; i<fields.size(); i++) {
                    JsonDataStore jds = fields.get(i);
                    if(!AppUtils.isNullCase(jds.getValue())) {
                        fieldHash.put(jds.getKey(), jds.getValue());
                        if(jds.getKey().equals(fieldName)){
                            valReturn = jds.getValue();
                            index = i+1;
                            break;
                        }
                    } else {
                        fieldHash.put(jds.getKey(), valReturn);
                    }
                }
            }
            return valReturn;
        }
    }
}

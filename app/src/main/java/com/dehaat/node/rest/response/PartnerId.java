package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

public class PartnerId {
    @SerializedName("partner_id")
    public Long partner_id;

    public Long getPartner_id() {
        return partner_id;
    }
}

package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetIdName {
    @SerializedName("id")
    public long id;
    @SerializedName("name")
    public String name;
    @SerializedName("display_name")
    public String display_name;
    @SerializedName("mobile")
    public String mobile;
    @SerializedName("ifsc_code")
    public String ifsc_code;
    @SerializedName("vendor_account_no")
    public String account_no;
    @SerializedName("point_of_contact")
    public String point_of_contact;
    @SerializedName("tracking")// used for lot
    public String tracking;
    @SerializedName("sub_categories")
    public ArrayList<GetIdName> sub_categories;

    public String getAccount_no() {
        return account_no;
    }

    public String getIfsc_code() {
        return ifsc_code;
    }

    public String getPoint_of_contact() {
        return point_of_contact;
    }

    public String getTracking() {
        return tracking;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getMobile() {
        return mobile;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ArrayList<GetIdName> getSub_categories() {
        return sub_categories;
    }

}

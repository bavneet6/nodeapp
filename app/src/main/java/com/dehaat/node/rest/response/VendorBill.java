package com.dehaat.node.rest.response;

import com.dehaat.node.rest.body.EntityCreateDataBody;
import com.google.gson.annotations.SerializedName;

public class VendorBill {
    @SerializedName("data")
    public Data data;
    @SerializedName("metadata")
    private EntityCreateDataBody.MetaData metaData;

    public VendorBill(Data data, EntityCreateDataBody.MetaData metaData) {

        this.data = data;
        this.metaData = metaData;
    }

    public static class Data {
        @SerializedName("reason")
        public String reason;


        public void setReason(String reason) {
            this.reason = reason;
        }
    }
}

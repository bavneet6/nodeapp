package com.dehaat.node.rest.body;

import com.google.gson.annotations.SerializedName;

public class Attachments {

    @SerializedName("entity_id")
    public Long entity_id;
    @SerializedName("entity_type")
    public String entity_type;

    public Attachments(Long entity_id, String entity_type) {
        this.entity_id = entity_id;
        this.entity_type = entity_type;
    }

    public Long getEntity_id() {
        return entity_id;
    }
}

package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 2/2/2018.
 */

public class FcmToken {
    @SerializedName("data")
    private Token data;

    public FcmToken(Token token) {
        this.data = token;

    }

    public Token getData() {
        return data;
    }

    public void setData(Token data) {
        this.data = data;
    }
}

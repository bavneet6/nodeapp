package com.dehaat.node.rest.body;

import com.google.gson.annotations.SerializedName;

public class LoginBody {
    @SerializedName("data")
    private DataBody dataBody;

    public LoginBody(DataBody dataBody) {

        this.dataBody = dataBody;
    }

    public static class DataBody {
        @SerializedName("username")
        private String username;
        @SerializedName("password")
        private String password;

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }
}

package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FinsihedProduct {
    @SerializedName("finished_moves")
    private List<FinishedProd> finished_moves;

    public List<FinishedProd> getFinished_moves() {
        return finished_moves;
    }

    public void setFinished_moves(List<FinishedProd> finished_moves) {
        this.finished_moves = finished_moves;
    }

    public static class FinishedProd {


        @SerializedName("product_id")
        private Long product_id;
        @SerializedName("product_qty")
        private Float product_qty;
        @SerializedName("product_uom")
        private Long product_uom;
        @SerializedName("lot_number")
        private String lot_number;

        public Long getProduct_id() {
            return product_id;
        }

        public void setProduct_id(Long product_id) {
            this.product_id = product_id;
        }

        public Float getProduct_qty() {
            return product_qty;
        }

        public void setProduct_qty(Float product_qty) {
            this.product_qty = product_qty;
        }

        public Long getProduct_uom() {
            return product_uom;
        }

        public void setProduct_uom(Long product_uom) {
            this.product_uom = product_uom;
        }

        public String getLot() {
            return lot_number;
        }

        public void setLot_number(String lot_number) {
            this.lot_number = lot_number;
        }
    }
}

package com.dehaat.node.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MultipleLines {
    @SerializedName("invoice_line_ids")
    private List<List<GetKeyValue>> invoice_line_ids;
    @SerializedName("product_price_lines")
    private List<List<GetKeyValue>> product_price;
    @SerializedName("request_line_ids")
    private List<List<GetKeyValue>> request_line_ids;

    public List<List<GetKeyValue>> getRequest_line_ids() {
        return request_line_ids;
    }

    public List<List<GetKeyValue>> getProduct_price() {
        return product_price;
    }

    public void setProduct_price(List<List<GetKeyValue>> product_price) {
        this.product_price = product_price;
    }

    public void setRequest_line_ids(List<List<GetKeyValue>> request_line_ids) {
        this.request_line_ids = request_line_ids;
    }

    public List<List<GetKeyValue>> getInvoice_line_ids() {
        return invoice_line_ids;
    }

    public void setInvoice_line_ids(List<List<GetKeyValue>> invoice_line_ids) {
        this.invoice_line_ids = invoice_line_ids;
    }

    public static class GetKeyValue {
        @SerializedName("key")
        private String key;

        @SerializedName("value")
        private String value;

        public GetKeyValue(){

        }

        public GetKeyValue(String key, String value){
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public void setKeyAndValue(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }
}

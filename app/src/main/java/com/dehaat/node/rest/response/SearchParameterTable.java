package com.dehaat.node.rest.response;

public class SearchParameterTable {
    private String entity_type, search_parameter, search_value;
    private Long row_id;

    public Long getRow_id() {
        return row_id;
    }

    public void setRow_id(Long row_id) {
        this.row_id = row_id;
    }

    public String getEntity_type() {
        return entity_type;
    }

    public void setEntity_type(String entity_type) {
        this.entity_type = entity_type;
    }

    public String getSearch_parameter() {
        return search_parameter;
    }

    public void setSearch_parameter(String search_parameter) {
        this.search_parameter = search_parameter;
    }

    public String getSearch_value() {
        return search_value;
    }

    public void setSearch_value(String search_value) {
        this.search_value = search_value;
    }
}
